#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnEmpresa -- formulario de ingreso, modificación, eliminación de Empresas del sistema.

# (c) Fernando San Martín Woerner 2003, 2004
# snmartin@galilea.cl

#This file is part of pyGestor.
#
# pyGestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# (C)    José Luis Álvarez Morales     2006, 2007.
#            josealvarezm@gmail.com
#revisado y corregido  Ricardo Méndez Lorca
# rmendez@galilea.cl

from GladeConnect import GladeConnect
import completion
from completion import GenericCompletion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
from comunes import iif,es_rut,CRut
from strSQL import strSelectEmpresa,strSelectEmpresaNivel


(COD_EMPRESA,
 DESCRIPCION_EMPRESA,
 TIPO_NUMERACION,
 DESCRIPCION_NUMERACION,
 COD_MES_CIERRE,
 DESCRIPCION_MES,
 NIVEL_CUENTA,
 COD_MONEDA,
 DESCRIPCION_MONEDA,
 RUT_EMPRESA,
 DIRECCION,
 PERIODOS_ABIERTOS,
 CUENTA_RETENCION,
 DESCRIPCION_RETENCION,
 CUENTA_LIQUIDO,
 DESCRIPCION_LIQUIDO,
 TELEFONO) = range(17)

( DIGITOS,
NIVEL) = range(2)
 
class wnEmpresa(GladeConnect):
    def __init__(self, conexion=None, padre=None, empresa=1, root="wnEmpresa"):
        GladeConnect.__init__(self, "glade/wnEmpresa.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnEmpresa.maximize()
            self.frm_padre = self.wnEmpresa
            self.padre
        else:
            self.frm_padre = padre.frm_padre            
        self.cod_empresa=None
        if empresa!=None:
            self.cod_empresa = int(empresa)
        self.modelo=None
        self.crea_columnas()
        self.carga_datos()

    def crea_columnas(self):
        self.modelo=gtk.ListStore(*(17*[str]))
        columnas = []
        columnas.append([RUT_EMPRESA,"R.U.T","str"])
        columnas.append([DIRECCION,"Dirección Empresa","str"])        
        columnas.append([DESCRIPCION_EMPRESA,"Descripción Empresa","str"])      
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeEmpresa)        
        
    def carga_datos(self):
        self.cursor.execute( strSelectEmpresa%'')
        r=self.cursor.fetchall()
        self.modelo.clear()
        for i in r:
            self.modelo.append(i)
        
    def on_btnAnadir_clicked(self, btn=None):        
       
        band=False
        dlg = dlgEmpresa(self.cnx, self.frm_padre, self.cod_empresa, False)
        dlg.entLiquido.set_editable(False)
        dlg.entRetencion.set_editable(False)
        sql="select False,cod_tipo_cuenta,descripcion_tipo_cuenta from ctb.tipo_cuenta"
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)>0:
            for i in r: 
                iter=dlg.modelo_plan_basico.append()
                dlg.modelo_plan_basico.set(iter,0,True,1,i[1],2,i[2])
        while band<>True:
            response=dlg.dlgEmpresa.run()
            if response==gtk.RESPONSE_OK:
                if dlg.response==True:
                    cod_mes_cierre=''
                    descripcion_cierre=''
                    nivel_cuenta=''
                    descripcion_cuenta=''
                    cod_moneda=''
                    descripcion_moneda=''
                    tipo_numeracion=''
                    descripcion_tipo_numeracion=''
                    cod_retencion=''
                    descripcion_retencion=''
                    cod_liquido=''
                    descripcion_liquido=''
                    telefono=''
                    if dlg.codigo_mes not in('',None):
                        cod_mes_cierre=dlg.codigo_mes
                        descripcion_cierre=dlg.entMes.get_text().upper()
                    if dlg.entTelefono.get_text() not in('',None):
                        telefono=dlg.entTelefono.get_text()
                    if dlg.nivel_cuenta not in('',None):
                        nivel_cuenta=dlg.nivel_cuenta
                        
                    if dlg.codigo_moneda not in('',None):
                        cod_moneda=dlg.codigo_moneda
                        descripcion_moneda=dlg.entMoneda.get_text().upper()
                    if dlg.tipo_numeracion not in ('',None):
                        tipo_numeracion=dlg.tipo_numeracion
                        descripcion_tipo_numeracion=dlg.entTipoNum.get_text().upper()
                    if dlg.codigo_retencion not in('',None):
                        cod_retencion=dlg.codigo_retencion
                        descripcion_retencion=dlg.entRetencion.get_text().upper()
                    if dlg.codigo_liquido not in('',None):
                        cod_liquido=dlg.codigo_liquido
                        descripcion_liquido=dlg.entLiquido.get_text().upper()
                    it=self.modelo.append()
                    self.modelo.set(it,COD_EMPRESA ,dlg.cod_empresa,
                                        DESCRIPCION_EMPRESA,dlg.entDescripcion.get_text().upper(),
                                        DIRECCION,dlg.entDireccion.get_text().upper(),                                
                                        COD_MES_CIERRE,cod_mes_cierre,                                
                                        NIVEL_CUENTA,dlg.nivel_cuenta,
                                        COD_MONEDA,cod_moneda,                                
                                        RUT_EMPRESA,dlg.entRut.get_text().upper(),                               
                                        TIPO_NUMERACION,tipo_numeracion,
                                        DESCRIPCION_NUMERACION, descripcion_tipo_numeracion,
                                        DESCRIPCION_MES,descripcion_cierre,                                                          
                                        DESCRIPCION_MONEDA,   descripcion_moneda,                             
                                        PERIODOS_ABIERTOS,dlg.spnPeriodos.get_value(),
                                        CUENTA_RETENCION, cod_retencion,
                                        DESCRIPCION_RETENCION,descripcion_retencion,
                                        CUENTA_LIQUIDO,cod_liquido,
                                        DESCRIPCION_LIQUIDO, descripcion_liquido,
                                        TELEFONO,telefono)                                              
                    band=True
            else:
                band=True
    def on_btnQuitar_clicked(self, btn=None):
        model, it = self.treeEmpresa.get_selection().get_selected()
        if model is None or it is None:
            return
        codigo = model.get_value(it, COD_EMPRESA)
        descripcion = model.get_value(it, DESCRIPCION_EMPRESA)
        
        if dialogos.yesno("¿Desea eliminar Empresa <b>%s</b>?\nEsta acción no se puede deshacer!"%descripcion, self.frm_padre) == gtk.RESPONSE_YES:
            self.cursor.execute('begin')
            llaves = {'cod_empresa':codigo}
            sql="""delete from ctb.nivel_cuenta where cod_empresa=%s"""%llaves['cod_empresa']
            self.cursor.execute(sql)
            sql = ifd.deleteFromDict('ctb.empresa', llaves)
            
            try:
                self.cursor.execute(sql, llaves)                
                self.cursor.execute('commit')
                model.remove(it)
            except:
                print sys.exc_info()[1]
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>\nLa Empresa <b>%s</b>, se encuentra relacionado."%descripcion)
                self.cursor.execute('rollback')
    
    def on_btnPropiedades_clicked(self, btn=None):
        model, it = self.treeEmpresa.get_selection().get_selected()
        if model is None or it is None:
            return
        self.rut_ficha = -1      
        
        dlg = dlgEmpresa(self.cnx, self.frm_padre, model.get_value(it,0),True)
        sql="select False,cod_tipo_cuenta,descripcion_tipo_cuenta from ctb.tipo_cuenta"
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)>0:
            for i in r: 
                dlg.modelo_plan_basico.append(i)
        sql="""select case when num_cuenta is null then False else True end,
                tipo_cuenta,
                descripcion_cuenta
                from ctb.empresa left join ctb.cuenta_contable using(cod_empresa)
                where cod_empresa=%s and ctb.cuenta_contable.nivel_cuenta=1"""%(int(model.get_value(it,0)))
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)>0:
            for i in r:
                if i[0]==True:
                    for j in dlg.modelo_plan_basico:
                        if i[1]==j[1]:
                            dlg.modelo_plan_basico.set(j.iter,0,True)
                
        dlg.treePlanBasico.set_sensitive(False)
        dlg.entCodigo.set_text(model.get_value(it, COD_EMPRESA))
        dlg.carga_datos()
        
        dlg.cod_empresa = int(dlg.entCodigo.get_text())
        dlg.autocompletar_cuenta_contable()
        if model.get_value(it, RUT_EMPRESA) not in('',None):
            dlg.entRut.set_text(model.get_value(it, RUT_EMPRESA))
        dlg.entRut.set_editable(False)
        dlg.entDescripcion.set_text(model.get_value(it, DESCRIPCION_EMPRESA))
        if model.get_value(it, RUT_EMPRESA) not in('',None):
            dlg.entDireccion.set_text(model.get_value(it, DIRECCION))
        if model.get_value(it, TELEFONO) not in ('',None):            
            dlg.entTelefono.set_text(model.get_value(it, TELEFONO))
        if model.get_value(it, DESCRIPCION_NUMERACION) not in('',None):            
            dlg.tipo_numeracion = dlg.pecTipoNumeracion.select(model.get_value(it, DESCRIPCION_NUMERACION),1)
        if model.get_value(it, DESCRIPCION_MES) not in('',None):            
            dlg.codigo_mes = dlg.pecMes.select(model.get_value(it, DESCRIPCION_MES),1)
        if model.get_value(it, DESCRIPCION_MONEDA) not in('',None):
            dlg.codigo_moneda = dlg.pecMoneda.select(model.get_value(it, DESCRIPCION_MONEDA),1)
        dlg.spnPeriodos.set_value(int(model.get_value(it, PERIODOS_ABIERTOS).replace(".0","")))
        if model.get_value(it, DESCRIPCION_RETENCION) not in('',None):
            dlg.codigo_retencion = dlg.pecRetencion.select(model.get_value(it, DESCRIPCION_RETENCION),1)
        if model.get_value(it, DESCRIPCION_LIQUIDO) not in('',None):
            dlg.codigo_liquido = dlg.pecLiquido.select(model.get_value(it, DESCRIPCION_LIQUIDO),1)
        if model.get_value(it, NIVEL_CUENTA) not in('',None):
            dlg.nivel_cuenta=int(model.get_value(it, NIVEL_CUENTA))                        
        
        dlg.editando = True
        band=False
        while band==False:
            response = dlg.dlgEmpresa.run()
            if response == gtk.RESPONSE_OK:
                if dlg.response==True:
                    cod_mes_cierre=''
                    descripcion_cierre=''
                    nivel_cuenta=''
                    descripcion_cuenta=''
                    cod_moneda=''
                    descripcion_moneda=''
                    tipo_numeracion=''
                    descripcion_tipo_numeracion=''
                    cod_retencion=''
                    descripcion_retencion=''
                    cod_liquido=''
                    descripcion_liquido=''
                    telefono=''
                    if dlg.codigo_mes not in('',None):
                        cod_mes_cierre=dlg.codigo_mes
                        descripcion_cierre=dlg.entMes.get_text().upper()
                    if dlg.entTelefono.get_text() not in('',None):
                        telefono=dlg.entTelefono.get_text()
                    if dlg.nivel_cuenta not in('',None):
                        nivel_cuenta=dlg.nivel_cuenta
                        
                    if dlg.codigo_moneda not in('',None):
                        cod_moneda=dlg.codigo_moneda
                        descripcion_moneda=dlg.entMoneda.get_text().upper()
                    if dlg.tipo_numeracion not in ('',None):
                        tipo_numeracion=dlg.tipo_numeracion
                        descripcion_tipo_numeracion=dlg.entTipoNum.get_text().upper()
                    if dlg.codigo_retencion not in('',None):
                        cod_retencion=dlg.codigo_retencion
                        descripcion_retencion=dlg.entRetencion.get_text().upper()
                    if dlg.codigo_liquido not in('',None):
                        cod_liquido=dlg.codigo_liquido
                        descripcion_liquido=dlg.entLiquido.get_text().upper()
                    
                    self.modelo.set(it,COD_EMPRESA ,dlg.cod_empresa,
                                        DESCRIPCION_EMPRESA,dlg.entDescripcion.get_text().upper(),
                                        DIRECCION,dlg.entDireccion.get_text().upper(),                                
                                        COD_MES_CIERRE,cod_mes_cierre,                                
                                        NIVEL_CUENTA,dlg.nivel_cuenta,
                                        COD_MONEDA,cod_moneda,                                
                                        RUT_EMPRESA,dlg.entRut.get_text().upper(),                               
                                        TIPO_NUMERACION,tipo_numeracion,
                                        DESCRIPCION_NUMERACION, descripcion_tipo_numeracion,
                                        DESCRIPCION_MES,descripcion_cierre,                                                          
                                        DESCRIPCION_MONEDA,   descripcion_moneda,                             
                                        PERIODOS_ABIERTOS,dlg.spnPeriodos.get_value(),
                                        CUENTA_RETENCION, cod_retencion,
                                        DESCRIPCION_RETENCION,descripcion_retencion,
                                        CUENTA_LIQUIDO,cod_liquido,
                                        DESCRIPCION_LIQUIDO, descripcion_liquido,
                                        TELEFONO,telefono)
                    band=True
            else:
                band=True

    
        
    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Empresa")
            
    #def on_treeEmpresa_row_activated(self, tree=None, path=None, column=None, data=None):
    def on_treeEmpresa_row_activated(self, widget=None, path=None, column=None, data=None):
        self.on_btnPropiedades_clicked(None)

class dlgEmpresa(GladeConnect):

    def __init__(self, conexion=None, padre=None, empresa=1, editando=False):
        GladeConnect.__init__(self, "glade/wnEmpresa.glade", "dlgEmpresa")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.editando = editando
        self.entRut.grab_focus()
        self.dlgEmpresa.show_all()
        self.nivel_cuenta=None
        self.tipo_numeracion = None
        self.codigo_mes = None
        self.codigo_moneda = None
        self.codigo_retencion = None
        self.codigo_liquido = None
        self.cod_empresa = empresa
        self.formula_validacion = None
        
        self.pecTipoNumeracion = completion.CompletionTipoNumeracion(self.entTipoNum,
                self.sel_tipo_numeracion,
                self.cnx)
        
        self.pecFormulaValidacion = completion.CompletionFormulaValidacion(self.entFormulaValidacion,
                self.sel_formula_validacion,
                self.cnx)
        self.pecMes = completion.CompletionMes(self.entMes,
                self.sel_mes,
                self.cnx)
        
        self.pecMoneda = completion.CompletionMoneda(self.entMoneda,
                self.sel_moneda,
                self.cnx)        
        self.response=False
        self.modelo_plan_basico=gtk.ListStore(bool,str,str)
        self.crea_columnas_modelo_plan()
        self.carga_niveles()    
    def crea_columnas_modelo_plan(self):
        columnas=[]
        columnas.append([0, "ok", "bool"])            
        columnas.append([2, "Descripcion Cuenta", "str"])
        
        self.modelo = gtk.ListStore(*(2*[str]))
        SimpleTree.GenColsByModel(self.modelo_plan_basico, columnas, self.treePlanBasico)
 
    
    
    def autocompletar_cuenta_contable(self):
        sql="""SELECT
                        descripcion_cuenta,
                        num_cuenta
                FROM
                        ctb.cuenta_contable
                WHERE
                    cod_empresa = %s 
                ORDER BY
                        descripcion_cuenta"""%self.cod_empresa
        self.pecRetencion=GenericCompletion(self.entRetencion,self.sel_retencion,self.cnx,sql)
        self.pecLiquido=GenericCompletion(self.entLiquido,self.sel_liquido,self.cnx,sql)
    def sel_tipo_numeracion(self, completion, model, iter):
        self.tipo_numeracion = model.get_value(iter, 1)
    def sel_mes(self, completion, model, iter):
        self.codigo_mes = model.get_value(iter, 1)
    def sel_moneda(self, completion, model, iter):
        self.codigo_moneda = model.get_value(iter, 1)
    def sel_retencion(self, completion, model, iter):
        self.codigo_retencion = model.get_value(iter, 1)
    def sel_liquido(self, completion, model, iter):
        self.codigo_liquido = model.get_value(iter, 1)
    def carga_niveles(self):
        columnas = []
        columnas.append([DIGITOS, "Dígitos", "str"])            
        columnas.append([NIVEL, "Nivel", "str"])
        
        self.modelo = gtk.ListStore(*(2*[str]))
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeFormato)
    
    def carga_datos(self):
        
        sql=strSelectEmpresaNivel%(int(self.entCodigo.get_text()),'')
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        for i in r:
            self.modelo.append(i)            
        
        
    
    def on_treeFormato_row_activated(self, widget=None, path=None, column=None, data=None):
        model,iter = self.treeFormato.get_selection().get_selected()
        #Nuestro iterador
        if model is None or iter is None:
            return
        row = model.get_path(iter)
        #Los dígitos del nivel
        dig = model.get_value(iter,0)
        #La fila editada
        self.fila_editada = column
        #Le damos al spinner el valor de los dígitos
        self.spnDigitos.set_value(float(dig))
##        model.remove(iter)
        
    def on_btnNuevoNivel_clicked(self, btn=None):            
        store = self.treeFormato.get_model()
        iter = store.append()
        if self.nivel_cuenta == None:
            self.nivel_cuenta = 0
        self.nivel_cuenta = int(self.nivel_cuenta) + 1
        store.set(iter, 0, self.spnDigitos.get_value_as_int(), 1,self.nivel_cuenta)
        return 0

    def on_btnEliminaNivel_clicked(self, btn=None):
        model,iter = self.treeFormato.get_selection().get_selected()
        #Nuestro iterador
        if model is None or iter is None:
            return
        row = model.get_path(iter)
        #Los dígitos del nivel
        model.remove(iter)
        self.nivel_cuenta=int(self.nivel_cuenta)-1

    def on_btnCancelar_clicked(self, btn=None):
        self.on_btnEditar_clicked()
        self.hboxNivel.set_sensitive(0)
        self.lstEmpresa.set_sensitive(1)
        return 0

    def on_btnAceptar_clicked(self, btn=None):
##        self.hboxNivel.set_sensitive(0)
#        if self.entRut == "  .   .   - ":
#            dialogos.error("El campo <b>R.U.T.</b> no puede estar vacío.")
#            return
        if not es_rut(self.entRut.get_text().upper()):
            dialogos.error("El <b>R.U.T.</b> no es valido.\n Ingrese nuevamente el R.U.T")
            return
        if self.entDescripcion.get_text() == "":
            dialogos.error("El campo <b>Descripción</b> no debe estar vacio")
            return
        
        if self.entDireccion.get_text() == "":
            dialogos.error("El campo <b>Dirección</b> no debe estar vacio")
            return
        if self.formula_validacion == None:
            dialogos.error("El campo <b>Formula de Validacion</b> no debe estar vacio")
            return
        
        if self.spnPeriodos.get_value() == '0':
            dialogos.error("El campo <b>Máximo de Periodos Abiertos</b> debe ser mayor al valor cero")
            return
        sql="""select rut_empresa from ctb.empresa where rut_empresa='%s'"""%(self.entRut.get_text().upper())
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)>0 and self.editando==False:
            dialogos.error("Existe otra empresa con rut <b>%s</b>"%(self.entRut.get_text().upper()))
            return
        model=self.treeFormato.get_model()
        if len(model)==0:
            dialogos.error("Debe ingresar el formato de la numeracion de las cuentas")                
            return
        model1=self.treePlanBasico.get_model()
        sel=0
        for i in model1:
            if i[0]==True:
                sel+=1
        if sel<4:
            dialogos.error("Debe seleccionar el plan de cuentas basico")                
#            return
        campos = {}
        llaves = {}                        
        try:
            self.cursor.execute('begin')
            nivel_cuenta='Null'
            nivel_cuenta=len(model)                      
            if not self.editando:        
                try:
                    sql="""insert into ctb.empresa              (tipo_numeracion,cod_mes_cierre,nivel_cuenta,cod_moneda,rut_empresa,descripcion_empresa,direccion_empresa,telefono_empresa,periodos_abiertos,formula_rut) values(%s,%s,%s,%s,'%s','%s','%s',%s,%s,'%s') """%(iif(self.tipo_numeracion==None,'Null',"'"+str(self.tipo_numeracion)+"'"),iif(self.codigo_mes==None,'Null',self.codigo_mes),nivel_cuenta,iif(self.codigo_moneda==None,'Null',self.codigo_moneda),self.entRut.get_text().upper(),self.entDescripcion.get_text().upper(),self.entDireccion.get_text().upper(),iif(self.entTelefono.get_text() in ('',None),'Null',"'"+str(self.entTelefono.get_text())+"'"),self.spnPeriodos.get_value_as_int(), self.formula_validacion)
                    self.cursor.execute(sql)
                except:
                    sql="""insert into ctb.empresa              (tipo_numeracion,cod_mes_cierre,nivel_cuenta,cod_moneda,rut_empresa,descripcion_empresa,direccion_empresa,telefono_empresa,periodos_abiertos) values(%s,%s,%s,%s,'%s','%s','%s',%s,%s) """%(iif(self.tipo_numeracion==None,'Null',"'"+str(self.tipo_numeracion)+"'"),iif(self.codigo_mes==None,'Null',self.codigo_mes),nivel_cuenta,iif(self.codigo_moneda==None,'Null',self.codigo_moneda),self.entRut.get_text().upper(),self.entDescripcion.get_text().upper(),self.entDireccion.get_text().upper(),iif(self.entTelefono.get_text() in ('',None),'Null',"'"+str(self.entTelefono.get_text())+"'"),self.spnPeriodos.get_value_as_int())
                    self.cursor.execute(sql)
                sql="""select cod_empresa from ctb.empresa where rut_empresa='%s'"""%self.entRut.get_text().upper()
                self.cursor.execute(sql)
                r=self.cursor.fetchall()
                self.cod_empresa=r[0][0]
            else:
                cuenta_retencion='Null'
                cuenta_liquido='Null'                
                if iif(self.codigo_retencion==None,'Null',self.codigo_retencion)<>'Null':                    
                    cuenta_retencion= "'"+str(self.codigo_retencion)+"'"
                if iif(self.codigo_liquido==None,'Null',self.codigo_liquido)<>'Null':
                    cuenta_liquido = "'"+str(self.codigo_liquido)+"'"
                sql="""update ctb.empresa set tipo_numeracion=%s, cod_mes_cierre=%s ,nivel_cuenta=%s, cod_moneda=%s,descripcion_empresa='%s', direccion_empresa='%s', telefono_empresa=%s,periodos_abiertos=%s, cuenta_retencion=%s,cuenta_liquido=%s where cod_empresa=%s"""%(iif(self.tipo_numeracion==None,'Null',"'"+str(self.tipo_numeracion)+"'"),iif(self.codigo_mes==None,'Null',self.codigo_mes),nivel_cuenta,iif(self.codigo_moneda==None,'Null',self.codigo_moneda),self.entDescripcion.get_text().upper(),self.entDireccion.get_text().upper(),iif(self.entTelefono.get_text() in ('',None),'Null',"'"+str(self.entTelefono.get_text())+"'"),self.spnPeriodos.get_value_as_int(),cuenta_retencion,cuenta_liquido,self.cod_empresa)                
                self.cursor.execute(sql)
                sql="""delete from ctb.nivel_cuenta where cod_empresa='%s'"""%self.cod_empresa
                self.cursor.execute(sql) 
            self.dlgEmpresa.hide()
##            model=self.treeFormato.get_model()            
            digitos_nivel={}
            for i in model:     
                campos={}
                campos['cod_nivel']=i[1]                
                campos['cod_empresa']=self.cod_empresa
                campos['digitos']=i[0]
                sql=ifd.insertFromDict("ctb.nivel_cuenta",campos)
                self.cursor.execute(sql,campos)               
                digitos_nivel[i[1]]=int(i[0])
            if not self.editando:
                for i in self.modelo_plan_basico:
                    cuenta_bases={}
                    if i[0]==True:
                        if i[1]=='A':
                            cuenta_bases['num_cuenta']=self.cuenta_base('1',digitos_nivel)                        
                        elif i[1]=='P':
                            cuenta_bases['num_cuenta']=self.cuenta_base('2',digitos_nivel)                                                
                        elif i[1]=='I':
                            cuenta_bases['num_cuenta']=self.cuenta_base('3',digitos_nivel)                        
                        elif i[1]=='E':
                            cuenta_bases['num_cuenta']=self.cuenta_base('4',digitos_nivel)                        
                        elif i[1]=='R':
                            cuenta_bases['num_cuenta']=self.cuenta_base('5',digitos_nivel)                                    
                        cuenta_bases['descripcion_cuenta']=i[2]
                        cuenta_bases['tipo_cuenta']=i[1]
                        cuenta_bases['cod_empresa']=int(self.cod_empresa)
                        cuenta_bases['nivel_cuenta']=1
                        cuenta_bases['tipo_analisis']='N'
                        cuenta_bases['aplica_resultado']='N'
                        sql=ifd.insertFromDict("ctb.cuenta_contable",cuenta_bases)
                        self.cursor.execute(sql,cuenta_bases)               
            self.cursor.execute('commit')
            self.response=True
        except:       
            dialogos.error("Error al realizar la operacion en la base de datos")
            self.cursor.execute('rollback')              
    def cuenta_base(self,num_cuenta_base,digitos_nivel):
        for i in range(2,len(digitos_nivel)+1):
                num_cuenta_base+='.'
                n=digitos_nivel[str(i)]
                for j in range(0,n):
                    num_cuenta_base+=str(0)
        return num_cuenta_base
    def sel_tipo_numeracion(self, completion, model, it):
        self.tipo_numeracion = model.get_value(it, 1)
    
    def sel_mes(self, completion, model, it):
        self.codigo_mes = model.get_value(it, 1)
    
    def sel_moneda(self, completion, model, it):
        self.codigo_moneda = model.get_value(it, 1)
    
    def sel_retencion(self, completion, model, it):
        self.codigo_retencion = model.get_value(it,1)
    def sel_formula_validacion(self, completion, model, it):
        self.formula_validacion = model.get_value(it,1)
        
    def sel_liquido(self, completion, model, it):
        self.codigo_liquido = model.get_value(it,1)
        
    
            
    def on_btnCancelar_clicked(self, btn=None):
        self.dlgEmpresa.hide()

if __name__ == "__main__":
    cnx = connect("host=localhost dbname=scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    w = wnEmpresa(cnx)    
    
    gtk.main()
