alter TABLE ctb.empresa add formula_rut varchar(10)  default 'rut';
alter TABLE ctb.moneda add decimales int  default 0;

DROP VIEW ctb.vw_libro_diario;

CREATE OR REPLACE VIEW ctb.vw_libro_diario AS 
 SELECT c.fecha, c.folio_comprobante AS folio, c.cod_tipo_comprobante AS tipo, 
        CASE
            WHEN c.estado = 'V'::bpchar THEN 'VIGENTE'::text
            WHEN c.estado = 'N'::bpchar THEN 'NULO'::text
            WHEN c.estado = 'P'::bpchar THEN 'PARCIAL'::text
            ELSE NULL::text
        END AS estado, c.glosa AS glosa_comprobante, c.debe AS total_debe, c.haber AS total_haber, c.cod_empresa, c.periodo, c.mes, d.num_linea, d.num_cuenta, t.descripcion_cuenta, d.glosa AS glosa_detalle, d.monto_debe::numeric(14,2) AS monto_debe, d.monto_haber::numeric(14,2) AS monto_haber, d.folio_documento, d.fecha_doc, d.cod_doc_contable, d.rut, d.cod_comprobante
   FROM ctb.comprobante c
   JOIN ctb.detalle_comprobante d ON d.cod_comprobante = c.cod_comprobante
   JOIN ctb.empresa e ON e.cod_empresa = c.cod_empresa
   JOIN ctb.cuenta_contable t ON d.num_cuenta::text = t.num_cuenta::text AND e.cod_empresa = t.cod_empresa
  ORDER BY c.fecha, c.folio_comprobante, d.cod_comprobante, d.num_linea;

ALTER TABLE ctb.vw_libro_diario OWNER TO vbenitez;
GRANT ALL ON TABLE ctb.vw_libro_diario TO vbenitez;
GRANT ALL ON TABLE ctb.vw_libro_diario TO public;

DROP VIEW ctb.vw_libro_mayor;

CREATE OR REPLACE VIEW ctb.vw_libro_mayor AS 
 SELECT d.num_cuenta, t.descripcion_cuenta, c.fecha, c.cod_tipo_comprobante AS tipo, c.folio_comprobante AS folio, c.glosa, d.monto_debe::numeric(14,2) AS monto_debe, d.monto_haber::numeric(14,2) AS monto_haber, c.cod_empresa, c.periodo, c.mes, d.folio_documento, d.fecha_doc, d.cod_doc_contable, d.rut, d.cod_comprobante, c.estado, d.num_linea
   FROM ctb.comprobante c
   JOIN ctb.detalle_comprobante d ON d.cod_comprobante = c.cod_comprobante
   JOIN ctb.empresa e ON e.cod_empresa = c.cod_empresa
   JOIN ctb.cuenta_contable t ON d.num_cuenta::text = t.num_cuenta::text AND t.cod_empresa = e.cod_empresa
  WHERE c.estado = 'V'::bpchar
  ORDER BY d.num_cuenta, t.descripcion_cuenta, c.fecha, c.cod_tipo_comprobante, c.folio_comprobante, d.glosa, int8(d.monto_debe), int8(d.monto_haber), c.cod_empresa, c.periodo, c.mes, d.folio_documento, d.fecha_doc, d.cod_doc_contable, d.rut, d.cod_comprobante, c.estado;

ALTER TABLE ctb.vw_libro_mayor OWNER TO vbenitez;
GRANT ALL ON TABLE ctb.vw_libro_mayor TO vbenitez;
GRANT ALL ON TABLE ctb.vw_libro_mayor TO public;


DROP VIEW ctb.vw_mayor;

CREATE OR REPLACE VIEW ctb.vw_mayor AS 
 SELECT cc.num_cuenta, cc.descripcion_cuenta, c.fecha, c.cod_tipo_comprobante, c.folio_comprobante, dc.glosa, dc.monto_debe::numeric(14,2) AS monto_debe, dc.monto_haber::numeric(14,2) AS monto_haber, c.cod_empresa, c.estado
   FROM ctb.detalle_comprobante dc
   JOIN ctb.comprobante c ON c.cod_comprobante = dc.cod_comprobante
   JOIN ctb.cuenta_contable cc ON dc.num_cuenta::text = cc.num_cuenta::text AND cc.cod_empresa = c.cod_empresa
   JOIN ctb.empresa e ON e.cod_empresa = c.cod_empresa
  ORDER BY cc.num_cuenta, c.fecha;

ALTER TABLE ctb.vw_mayor OWNER TO vbenitez;
GRANT ALL ON TABLE ctb.vw_mayor TO vbenitez;
GRANT ALL ON TABLE ctb.vw_mayor TO public;

DROP VIEW ctb.vw_consulta_mayor;

CREATE OR REPLACE VIEW ctb.vw_consulta_mayor AS 
 SELECT d.num_cuenta, t.descripcion_cuenta, c.fecha, c.cod_tipo_comprobante AS tipo, c.folio_comprobante AS folio, c.origen, c.glosa, d.monto_debe::numeric(14,2) AS monto_debe, d.monto_haber::numeric(14,2) AS monto_haber, c.cod_empresa, c.periodo, c.mes, d.folio_documento, d.fecha_doc, d.cod_doc_contable, d.rut, d.cod_comprobante, c.estado, d.num_linea
   FROM ctb.comprobante c
   JOIN ctb.detalle_comprobante d ON d.cod_comprobante = c.cod_comprobante
   JOIN ctb.empresa e ON e.cod_empresa = c.cod_empresa
   JOIN ctb.cuenta_contable t ON d.num_cuenta::text = t.num_cuenta::text AND t.cod_empresa = e.cod_empresa
  WHERE c.estado = 'V'::bpchar
  ORDER BY d.num_cuenta, t.descripcion_cuenta, c.fecha, c.cod_tipo_comprobante, c.folio_comprobante, d.glosa, int8(d.monto_debe), int8(d.monto_haber), c.cod_empresa, c.periodo, c.mes, d.folio_documento, d.fecha_doc, d.cod_doc_contable, d.rut, d.cod_comprobante, c.estado;

ALTER TABLE ctb.vw_consulta_mayor OWNER TO vbenitez;
GRANT ALL ON TABLE ctb.vw_consulta_mayor TO vbenitez;
GRANT ALL ON TABLE ctb.vw_consulta_mayor TO public;

