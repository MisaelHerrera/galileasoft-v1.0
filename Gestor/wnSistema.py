#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnSistema -- formulario de ingreso, eliminación y edición, de los distintos Sistemas.

# (C)    José Luis Álvarez Morales 2006
#           josealvarezm@gmail.com

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
from strSQL import strSelectSistema

(CODIGO,
 DESCRIPCION) = range(2)

class wnSistema(GladeConnect):
    def __init__(self, conexion=None, padre=None, root="wnSistema"):
        GladeConnect.__init__(self, "glade/wnSistema.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnSistema.maximize()
            self.frm_padre = self.wnSistema
            self.padre
        else:
            self.frm_padre = padre.frm_padre
            
        self.crea_columnas()
        #self.carga_datos()
    
    def crea_columnas(self):
        columnas = []
        columnas.append([CODIGO, "Código", "str"])
        columnas.append([DESCRIPCION, "Nombre Sistema", "str"])
        self.modelo=gtk.ListStore(str, str)        
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeSistema)
        self.carga_datos()
    def carga_datos(self):
        self.cursor.execute(strSelectSistema)
        r=self.cursor.fetchall()
        for i in r:
            self.modelo.append(i)
        #self.modelo = ifd.ListStoreFromSQL(self.cnx, strSelectSistema)
        #self.treeSistema.set_model(self.modelo)
            
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgSistema(self.cnx, self.frm_padre, False)
        dlg.editando = False
        band = False
        while not band:
            response = dlg.dlgSistema.run()
            if response == gtk.RESPONSE_OK:
                if dlg.commit == True:                    
                    self.modelo.append(dlg.response)
                    band = True
            else:
                band = True
        
        dlg.dlgSistema.destroy()
                       
    def on_btnQuitar_clicked(self, btn=None):
        model, it = self.treeSistema.get_selection().get_selected()
        if model is None or it is None:
            return
        codigo = model.get_value(it, CODIGO)
        descripcion = model.get_value(it, DESCRIPCION)
        
        if dialogos.yesno("¿Desea eliminar el Sistema <b>%s</b>?\nEsta acción no se puede deshacer!!!"%descripcion, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'cod_sistema':codigo}
            sql = ifd.deleteFromDict('ctb.sistema', llaves)
            
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()[1]
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>\nEl Sistema <b>%s</b>, se encuentra relacionado.")
                
    def on_btnPropiedades_clicked(self, btn=None):
        model, it = self.treeSistema.get_selection().get_selected()
        if model is None or it is None:
            return
        
        dlg = dlgSistema(self.cnx, self.frm_padre, False)
        dlg.entCodigo.set_text(model.get_value(it, CODIGO))
        dlg.entDescripcion.set_text(model.get_value(it, DESCRIPCION))
        
        dlg.entCodigo.set_sensitive(False)
        
        dlg.editando = True
        band = False
        while not band:
            response = dlg.dlgSistema.run()
            if response == gtk.RESPONSE_OK:
                if dlg.commit==True:
                    del model[it]
                    self.modelo.append(dlg.response)
                    band=True
            else:
                band=True
        dlg.dlgSistema.destroy()
            
    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Sistema")
            
    def on_treeSistema_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()
        
class dlgSistema(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=None):
        GladeConnect.__init__(self, "glade/wnSistema.glade", "dlgSistema")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.editando = editando
        self.entCodigo.grab_focus()
        self.response = None
        self.dlgSistema.show_all()
        self.commit=False
    def on_btnAceptar_clicked(self, btn=None):
        
        if self.entCodigo.get_text() == "" or len(self.entCodigo.get_text()) > 3:
            dialogos.error("El campo <b>Código Sistema</b> no puede estar vacío o exceder el máximo de tres caracteres.")
            return
        
        if self.entDescripcion.get_text() == "" or len(self.entDescripcion.get_text().replace(" ",""))==0:
            dialogos.error("El campo <b>Nombre Sistema</b> no puede estar vacío.")
            return
        
        campos = {}
        llaves = {}
        
        campos ['nombre_sistema']=self.entDescripcion.get_text().upper()  
        
        if not self.editando:
            campos['cod_sistema']  = self.entCodigo.get_text().upper()
            sql = ifd.insertFromDict("ctb.sistema", campos)
        else:
            llaves['cod_sistema'] = self.entCodigo.get_text().upper()
            sql, campos = ifd.updateFromDict("ctb.sistema", campos, llaves)
        
        try:
            self.cursor.execute(sql,campos)
            self.dlgSistema.hide()
            
            self.response = [self.entCodigo.get_text().upper(),
                                    self.entDescripcion.get_text().upper()]
            self.commit=True
        except:
            print sys.exc_info()[1]
            cod_sistema = self.entCodigo
            dialogos.error("En la Base de Datos ya existe el Sistema <b>%s</b>"%campos['cod_sistema'])
            self.entCodigo.grab_focus()
                
    def on_btnCancelar_clicked(self, btn=None):
        self.dlgSistema.hide()
        
if __name__ == '__main__':
    cnx = connect("host=localhost dbname=scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    d = wnSistema(cnx)
    
    gtk.main()
