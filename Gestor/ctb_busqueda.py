#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ctb_busqueda.py - M�dulo que define el di�logo de b�squeda utilizado en el sistema, en reemplazo de los gtk.combo
#        los cuales no tienen autocompletar
# (C)   Fernando San Mart�n W. 2003
#       snmartin@galilea.cl  

#This file is part of pyGestor.
#
# pyGestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sys 
from string import zfill
import gobject
import gtk
from time import *
from ctb_rutinas import  *
from calendar import *
import os

if os.name == "nt":
    sys.path.append("C:\\Python23\\Lib\\site-packages\\pyPgSQL")

if os.name == "posix":
    sys.path.append("/usr/lib/python2.3/site-packages/pyPgSQL")

def ctb_busqueda(window, cnx, sql, col_filtro, col_retorno, titulos, texto = None):
    """dialogo de busqueda para b�squedas, recibe 
    cnx = conecci�n a la base de datos
    sql = consulta a la base de datos
    col_filtro = columna para filtrar
    col_retorno = columna a retornar
    
    """ 
    if not cnx:
        raise ValueError, "No hay conecci�n"
        return 0
        
    if not sql:
        raise ValueError, "No ha consulta"
        return 0 
        
    if col_filtro is None:
        raise ValueError, "No hay columna a filtrar"
        return 0
        
    if col_retorno is None:
        raise ValueError, "No hay columna a retornar"
        return 0
        
    if not titulos:
        raise ValueError, "No hay titulos para las columnas"


    t = unicode('B�squeda', 'latin-1')
    dialog = gtk.Dialog(t.encode('utf-8'), window, 0, (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_OK, gtk.RESPONSE_OK,))
    
    dialog.set_default_size(700,400)
    
    hbox = gtk.HBox(False, 8)
    hbox.set_border_width(8)
    dialog.vbox.pack_start(hbox, 1, 1, 0)
    
    vbox1 = gtk.VBox(False)
    vbox1.set_border_width(8)
    hbox.pack_start(vbox1, False, False, 1)
    stock = gtk.image_new_from_stock(gtk.STOCK_FIND, gtk.ICON_SIZE_DIALOG)
    vbox1.pack_start(stock, False, False, 0)
    vbox1.pack_start(
    gtk.Label(
    unicode("\n\nD�gite el texto a buscar \nen la entrada de filtro\n"+
            "y presione la tecla INTRO. \n\nLuego haga doble-click \n"+
            "sobre el �tem deseado \no seleccionelo y\n"+
            "presione el bot�n ACEPTAR",
            'latin-1').encode('utf-8')), False, False, False)
    vbox1 = gtk.VBox(False)
    vbox1.set_border_width(8)
    hbox.pack_start(vbox1, True, True, 1)
    
    lbl = gtk.Label('Filtro:')
    vbox1.pack_start(lbl, False, 0)
    
    
    def on_btnFechaD_click(self,btn=None):
        fecha =txtFechaD.get_text()
        txtFechaD.set_text(ctb_calendar(self,None,fecha))
        
    def on_btnFechaH_click(self,btn=None):
        fecha =txtFechaH.get_text()
        txtFechaH.set_text(ctb_calendar(self,None,fecha))
        
    def on_entry_key_press_cb(entry, event):
        if event.keyval == gtk.keysyms.Return:
            buscar()
    def on_entry_changed_cb(entry):
        entry.set_text(entry.get_text().upper())

    def cargar():
        global r
        cursor = cnx.cursor()
        cursor.execute(sql)
        r = cursor.fetchall()
                    
    def buscar():
        store = gtk.ListStore(gobject.TYPE_STRING,
                  gobject.TYPE_STRING)
                  
        filtro = txt.get_text().upper()
        
        
        
        for i in range(len(r)):

            if len(filtro)>0:
                    
                if r[i][col_filtro].upper().find(filtro) > 0:
                        
                    iter = store.append()
                    n = 0

                    for j in r[i]:
                        desc = unicode(str(j),'latin-1')
                        store.set(iter, n, desc.encode('utf-8'))
                        n = n + 1
                
                else:
                        
                    if r[i][1].upper()[:len(filtro)] == filtro:
                            
                        iter = store.append()
                        n = 0
                        
                        for j in r[i]:  
                                
                            desc = unicode(str(j),'latin-1')
                            store.set(iter, n, desc.encode('utf-8'))
                            n = n + 1
    
        tree.set_model(store)
        
        
        
    def on_tree_row_activated(tree, row, column):
            
        model, iter = tree.get_selection().get_selected()
        if not iter:
            return 0
    
        dialog.response(gtk.RESPONSE_OK)
        
        
        
    txt = gtk.Entry()
    txt.add_events(gtk.gdk.KEY_PRESS_MASK)
    txt.connect('key-press-event', on_entry_key_press_cb)
    txt.connect('changed', on_entry_changed_cb)
    vbox1.pack_start(txt, 0, 0)
    s= gtk.ScrolledWindow()
    
    tree = gtk.TreeView()
    
    n = 0
    
    for i in titulos:
        lbl = unicode(i, 'latin-1')
        column = gtk.TreeViewColumn(lbl.encode('utf-8'), gtk.CellRendererText(), text=n)
        n = n + 1
        tree.append_column(column)

    cargar()
    
    if texto:
        txt.set_text(texto)
        buscar()

    tree.connect("row-activated", on_tree_row_activated)
    selection = tree.get_selection()
    selection.set_mode('single')
    
    vbox1.pack_start(s, 1, 1)
    s.add(tree)
    
    dialog.show_all()

    if texto:
            
        tree.grab_focus()
        
    response = dialog.run()
    retorno = []
    
    if response == gtk.RESPONSE_OK:
            
        store = tree.get_model()
        
        if store== None:
            dialog.destroy()
            return 0
            
        a = tree.get_selection() 
        
        if tree.get_selection() == None :
            return 0
            
        model, iter = tree.get_selection().get_selected()
        retorno = []
        
        if iter:
        
            for i in col_retorno:
                retorno.append(model.get_value(iter, i))
                
    else:
            
        for i in col_retorno:
            retorno.append("")
            

    dialog.destroy()
        
    return retorno

#BUSQUEDA DE COMPROBANTES

def ctb_busqueda_comprobantes(self,window, cnx, sql, col_retorno, titulos, texto = None):
    """dialogo de busqueda para b�squedas, recibe 
    cnx = conecci�n a la base de datos
    sql = consulta a la base de datos
    col_retorno = columna a retornar
    titulos = Titulos de las columnas del tree
    
    """ 
    if not cnx:
        raise ValueError, "No hay conecci�n"
        return 0
        
    if not sql:
        raise ValueError, "No ha consulta"
        return 0
        
    if col_retorno is None:
        raise ValueError, "No hay columna a retornar"
        return 0
        
    if not titulos:
        raise ValueError, "No hay titulos para las columnas"
        
    def on_btnFechaD_click(self,btn=None):
        fecha =txtFechaD.get_text()
        txtFechaD.set_text(ctb_calendar(self,None,fecha))
        
    def on_btnFechaH_click(self,btn=None):
        fecha =txtFechaH.get_text()
        txtFechaH.set_text(ctb_calendar(self,None,fecha))

    t = unicode('B�squeda de Comprobantes', 'latin-1')
    
    dialog = gtk.Dialog(t.encode('utf-8'), window, 0, (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_OK, gtk.RESPONSE_OK))

    dialog.set_default_size(700,400)
    
    hboxFechaD = gtk.HBox(False, 8)
    txtFechaD = gtk.Entry()
    txtFechaD.set_text(str(ceros("1",2)+"/"+ ceros(self.mes,2) +"/"+ ceros(self.ano,4)))
    hboxFechaD.pack_start(txtFechaD, False, False, 0)
    btnFechaD = gtk.Button()
    btnFechaD.connect("clicked", on_btnFechaD_click)
    hboxFechaD.pack_start(btnFechaD, False, False, 0)
    #hbox.pack_start(hboxFechaD, False, False, 0)
    hboxFechaH = gtk.HBox(False, 8)
    txtFechaH = gtk.Entry()
    txtFechaH.set_text(str(ceros(monthrange(int(self.ano), int(self.mes))[1],2)+"/"+ ceros(self.mes,2) +"/"+ ceros(self.ano,4)))
    hboxFechaH.pack_start(txtFechaH, False, False, 0)
    btnFechaH = gtk.Button()
    btnFechaH.connect("clicked", on_btnFechaH_click)
    hboxFechaH.pack_start(btnFechaH, False, False, 0)
    

    hboxFechas = gtk.HBox(False, 0)
    
    hboxFechas.set_border_width(0)
    
    chkFechas = gtk.CheckButton('FECHAS')
    chkFechas.show()
    hboxFechas.pack_start(chkFechas, True, True, 1)
    hboxFechas.pack_start(hboxFechaD, True, True, 1)
    hboxFechas.pack_start(hboxFechaH, True, True, 1)
    dialog.vbox.pack_start(hboxFechas, False, 1, 0)
    hboxFechas.show()
    hboxFolios = gtk.HBox(False, 0)
    hboxFolios.set_border_width(0)
    hboxFolios.show()
    txtFolioDesde = gtk.Entry()
    txtFolioDesde.show()
    txtFolioHasta = gtk.Entry()
    txtFolioHasta.show()
    txtFolioDesde.set_text("0")
    
    txtFolioHasta.set_text("0") 
    chkFolios = gtk.CheckButton('FOLIOS :')
    chkFolios.show()
    hboxFolios.pack_start(chkFolios, True, True, 1)
    hboxFolios.pack_start(txtFolioDesde, True, True, 1)
    hboxFolios.pack_start(txtFolioHasta, True, True, 1)
    dialog.vbox.pack_start(hboxFolios, False, 1, 0)
    
    
    def on_btn_buscar_clicked(btn=None):
    
        buscar()

    hboxbtnBuscar = gtk.HBox(False, 8)
    hboxbtnBuscar.show()
    hboxbtnBuscar.set_border_width(8)
    btnBuscar = gtk.Button('BUSCAR',gtk.STOCK_FIND)
    btnBuscar.connect('clicked', on_btn_buscar_clicked) 
    btnBuscar.show()
    hboxbtnBuscar.pack_start(btnBuscar, True, True, 1)
    dialog.vbox.pack_start(hboxbtnBuscar, False, 1, 0)
    
    hboxGrilla = gtk.HBox(False, 8)
    hboxGrilla.show()
    hboxGrilla.set_border_width(8)
    dialog.vbox.pack_start(hboxGrilla, 1, 1, 0)
                        
    def buscar():
    
        where =""

        if chkFechas.get_active():#txtFechaDesde.get_text() != "" and txtFechaHasta.get_text() != "":

            where = " fecha between '" + ctb_formato_fecha_db(txtFechaD.get_text()) + "' and '"+ ctb_formato_fecha_db(txtFechaH.get_text()) + "'"

        if chkFolios.get_active():#txtFolioDesde.get_text() != "" and txtFolioHasta.get_text() != "":

            where = where + iif(where!=""," and ","") + " folio_comprobante between " + str(int("0" + txtFolioDesde.get_text())) + " and " +  str(int("0" + txtFolioHasta.get_text()))

        where = sql + iif(where!=""," and ","") + where + " order by cod_tipo_comprobante ,folio_comprobante"
        cursor = cnx.cursor()
        cursor.execute(where)
        r = cursor.fetchall()

        

        store = gtk.ListStore(gobject.TYPE_STRING,
                          gobject.TYPE_STRING,gobject.TYPE_STRING,gobject.TYPE_STRING,gobject.TYPE_STRING)

        

        for i in range(len(r)):
            iter = store.append()
            n = 0
            for j in r[i]:
                if n== 1:
                    desc = ctb_formato_fecha_local(str(j))
                else:
                    desc = unicode(str(j),'latin-1')
                store.set(iter, n, desc.encode('utf-8'))
                n = n + 1
                
        tree.set_model(store)
        
        
    def on_tree_row_activated(tree, row, column):
        model, iter = tree.get_selection().get_selected()
        if not iter:
            return

        dialog.response(gtk.RESPONSE_OK)


    s = gtk.ScrolledWindow()
    
    tree = gtk.TreeView()
    
    n = 0
    
    for i in titulos:

        lbl = unicode(i, 'latin-1')
        column = gtk.TreeViewColumn(lbl.encode('utf-8'), gtk.CellRendererText(), text=n)
        n = n + 1
        tree.append_column(column)



    if texto:

        txt.set_text(texto)
        buscar()



    tree.connect("row-activated", on_tree_row_activated)
    tree.show()
    selection = tree.get_selection()
    selection.set_mode('single')
    
    hboxGrilla.pack_start(s, 1, 1)
    s.add(tree)
    s.show()
    hboxGrilla.show()
    dialog.show_all()
    
    
    if texto:
        tree.grab_focus()
        
        
    response = dialog.run()
    retorno = []
    
    
    if response == gtk.RESPONSE_OK:
        try:
            store = tree.get_model()
            if store== None:

                dialog.destroy()
                return 0
                
            a = tree.get_selection() 
            if tree.get_selection() == None :
                    
                return 0
                
            model, iter = a.get_selected()
            retorno = []
            if iter:
                for i in col_retorno:
                    retorno.append(model.get_value(iter, i))
            
            else:
                retorno.append("0")
                
        except:
            retorno.append("0")
    else:
        for i in col_retorno:
            retorno.append("")

    dialog.destroy()
    return retorno
