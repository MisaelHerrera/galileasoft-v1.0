#!/usr/bin/env python
# -*- coding: utf-8 -*-
import comunes

import SimpleTree
import gtk
import sys
import completion
from spg import connect
import debugwindow


from reportlab.pdfbase.ttfonts import *
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import A4, landscape

FECHA = 0
GLOSA = 1
DOCUMENTO = 2
DEBE = 3
HABER = 4
CONCILIADO = 5
COMPROBANTE = 6
NUM_LINEA = 7

class wnConciliacion(comunes.GladeConnect):
    """Conciliacion bancaria"""
    def __init__(self, cnx=None, padre=None, empresa=1,root="wnConciliacion"):
        """Constructor de la clase, inicializa la clase base GladeConnect
        con el parametro del fichero glade a utilizar para generar la
        interfaz del usuario.
        """        
        comunes.GladeConnect.__init__(self, "glade/wn_conciliacion.glade",root)
        self.cod_empresa = empresa
        self.cnx = cnx
##        self.wnConciliacion.maximize()
        self.cursor = self.cnx.cursor()
        self.cod_cuenta = None
        self.pecCuenta = completion.CompletionCuentaContable(self.entCuenta,
                    self.sel_cuenta,
                    self.cnx,
                    self.cod_empresa)

        self.crea_modelo()
        self.crea_columnas()
##        self.entFechaDesde.set_text(comunes.ND().firstDayOfMonth().formatESCentury())
##        day = str(comunes.ND().lastDayOfMonth())
##        month = str(comunes.ND().month()).zfill(2)
##        year = str(comunes.ND().year())
##        self.entFechaHasta.set_text('%s/%s/%s' % (day, month, year))
        self.entCuenta.grab_focus()
        if padre is None:
            self.wnConciliacion.show_all()
        else:
            self.vboxConciliacion.show_all()
        self.ventana=padre
    def fixed_toggled(self, render, path):
        self.modelo[path][CONCILIADO] = not self.modelo[path][CONCILIADO]
        if self.modelo[path][CONCILIADO]:
            con = 'S'
        else:
            con = 'N'
        self.cursor.execute("""UPDATE ctb.detalle_comprobante 
                                SET conciliado = '%s'
                                WHERE cod_empresa = %s
                                AND cod_comprobante = %s
                                AND num_linea = %s
                                """ % (con, 
                                        self.cod_empresa, 
                                        self.modelo[path][COMPROBANTE],
                                        self.modelo[path][NUM_LINEA]))
        return

    def crea_columnas(self):
        """Crea las columnas del gtk.TreeView treeFicha"""
        columnas = []
        columnas.append([FECHA,"Fecha","dte"])
        columnas.append([GLOSA,"Glosa","str"])
        columnas.append([DOCUMENTO,"Documento","int"])
        columnas.append([DEBE,"Debe","int"])
        columnas.append([HABER,"Haber","int"])
        
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeConciliacion)
        renderer = gtk.CellRendererToggle()
        renderer.connect('toggled', self.fixed_toggled)
        column = gtk.TreeViewColumn('Conciliado', renderer, active=CONCILIADO)
        column.set_clickable(True)
        self.treeConciliacion.append_column(column)

    def crea_modelo(self):
        """Crea un modelo basado en gtk.ListStore, en el cual carga
        los datos de la tabla cliente de la base de datos local.
        """
        self.modelo = gtk.ListStore(str, str, str, str, str, bool, str, str,str)
    
    def buscar(self):
        if self.cod_cuenta is None:
            self.stbConciliacion.push(0, "Debe indicar una cuenta contable de banco.")
            return
        cuenta = self.cod_cuenta
        desde = comunes.CDateDB(self.entFechaDesde.get_text())
        hasta = comunes.CDateDB(self.entFechaHasta.get_text())
        self.crea_modelo()
        try:
            sql="""SELECT 
                                        c.fecha,
                                        c.glosa,
                                        d.folio_documento,
                                        d.monto_debe,
                                        d.monto_haber,
                                        d.conciliado,
                                        d.cod_comprobante,
                                        d.num_linea,
                                        c.cod_tipo_comprobante
                                   FROM 
                                       ctb.comprobante c
                                    JOIN 
                                       ctb.detalle_comprobante d
                                   ON c.cod_comprobante = d.cod_comprobante
                                   AND c.cod_empresa = d.cod_empresa
                                   WHERE c.fecha BETWEEN '%s' and '%s'
                                   AND d.num_cuenta = '%s'
                                   and d.cod_empresa = %s
                                   """ % (desde, hasta, cuenta, self.cod_empresa)
            self.cursor.execute(sql)

            records = self.cursor.fetchall()
            for i in records:
                record = [n for n in i]
##                if record[8]!='A':
                if record[CONCILIADO] == 'S':
                    record[CONCILIADO] = True
                else:
                    record[CONCILIADO] = False
                    
                if record[DOCUMENTO] is None:
                    record[DOCUMENTO] = "Sin Documento"
                self.modelo.append(record)
##                else:
##                    sql="""select cod_comprobante from ctb.detalle_comprobante d join ctb.comprobante c using(cod_comprobante) where c.cod_empresa=%s and num_cuenta='%s' and (monto_debe=%s or monto_haber=%s) and c.cod_tipo_comprobante!='A'"""%(self.cod_empresa,self.cod_cuenta,comunes.iif(float(record[HABER])==0L,float(record[DEBE]),float(record[HABER])),comunes.iif(float(record[HABER])==0L,float(record[DEBE]),float(record[HABER])))
##                    self.cursor.execute(sql)
##                    r=self.cursor.fetchall()
####                    if record[CONCILIADO] == 'S':
####                        record[CONCILIADO] = True
####                    else:
####                        record[CONCILIADO] = False
##                    if len(r)>0:
##                        self.modelo.append(record)
            self.treeConciliacion.set_model(self.modelo)
        except:
            comunes.dlgError()
            self.stbConciliacion.push(0, "Error al obtener los datos.")

    def sel_cuenta(self, completion, model, iter):
        """Recupera el número de cuenta contable"""
        self.cod_cuenta = model.get_value(iter, 1)

    def on_btnBuscar_clicked(self, btn=None, data=None):
        self.buscar()

    def on_btnCerrar_clicked(self, btn=None, data=None):
        """Callback para el evento clicked del boton Cerrar,
        termina la aplicación."""
        if self.ventana==self.vboxConciliacion:
            self.vboxConciliacion.destroy()
        else:            
            self.ventana.remove_tab("Conciliacion Bancaria")

    def on_btnFecha_clicked(self, btn = None, widget=None):
        if btn.name == "btnDesde":
            widget = self.entFechaDesde
        elif btn.name == "btnHasta":
            widget = self.entFechaHasta
        else:
            return
        fecha = comunes.dlgCalendario(None, widget.get_text())
        widget.set_text(fecha.date)        

    def on_btnImprimir_clicked(self, btn=None):
        if not self.cod_empresa:
            self.stbConciliacion.push(0, "No se ha seleccionado una empresa")
            return 0
        if len(self.modelo)==0:
            return 0
        try:
            self.cursor.execute("""SELECT 
                                    descripcion_empresa, 
                                    rut_empresa, 
                                    direccion_empresa 
                                  FROM ctb.empresa 
                                  WHERE cod_empresa = %s """ % self.cod_empresa)

            r = self.cursor.fetchall()
        except:
            comunes.dlgError()
            return 0

        self.desc_empresa = r[0][0]
        self.rut_empresa = r[0][1]
        self.direccion_empresa = r[0][2]
        lineas = 83
        linea_inicial = 7
        
        def cabecera_conciliacion(self, pc, page_num):
            pc.setFont('Times-Bold', 12)
            w = pc.stringWidth("Conciliacion Bancaria",'Times-Bold', 12)
            pc.drawString(275 - w/2,800, "Conciliacion Bancaria")
            pc.drawString(30,782, self.desc_empresa)
            pc.drawString(30,770, self.rut_empresa)
            pc.drawString(30,758, self.direccion_empresa)
            pc.drawString(430,758,"Pagina: %s"%(str(int(page_num)).zfill(10)))
            pc.drawString(430,782,"Desde : " +  self.entFechaDesde.get_text())
            pc.drawString(430,770,"Hasta : " + self.entFechaHasta.get_text())
            pc.setFont('Courier-Bold', 7)
            pc.line(30, 756- (linea_inicial - 7)*9, 530, 756- (linea_inicial - 7)*9)
            l = "Fecha".ljust(12) + " " +\
                "Glosa".center(40) + " " + \
                "Documento".rjust(12) + " " + \
                "Debe".rjust(20) + " " +\
                "Haber".rjust(20) + " " +\
                "Conciliado".rjust(5)
            pc.drawString(30,749- (linea_inicial - 7)*9,l)
            pc.line(30, 748- (linea_inicial - 7)*9, 530, 748- (linea_inicial - 7)*9)
            pc.setFont('Courier', 7)

        def put_linea(pc,linea,strLinea,pagina):
            if linea > 83:
                pc.drawString(30,748 -((linea - 4)*9) ,"Fecha Impresion: " + comunes.ND().formatESCentury())
                c.showPage()
                pagina = pagina + 1
                cabecera_conciliacion(self,pc,pagina)
                linea = linea_inicial
            pc.drawString(30,748 -((linea -6)*9) ,strLinea)
            return linea +1,pagina

        pagina = 0                  # contador de páginas
        cta = self.cod_cuenta       #cuenta inicial de la página
        suma_debe = 0L             # total general de debe
        suma_haber = 0L            # y haber
        debe_final=0L
        haber_final=0L
        saldo_final=0L
        saldo_inicial=0L

        c = Canvas('conciliacion.pdf')
        c.setPageCompression(0)
        pagina = 1
        cabecera_conciliacion(self,c,pagina)
        linea = linea_inicial       # linea actual en la página

        sql = """SELECT 
                    sum(monto_debe)::numeric, 
                    sum(monto_haber)::numeric 
                 FROM ctb.vw_mayor c 
                 WHERE  
                     date_part('year', c.fecha) = date_part('year','%s'::date)
                     AND (c.fecha <= '%s' OR c.cod_tipo_comprobante = 'A')
                     AND c.cod_empresa = %s
                     AND c.num_cuenta = '%s' 
                     AND estado = 'V'
                     """ % (comunes.CDateDB(self.entFechaDesde.get_text()),
                             comunes.CDateDB(self.entFechaDesde.get_text()),
                             self.cod_empresa,
                             self.cod_cuenta)
        try:
            self.cursor.execute(sql)
            saldo = self.cursor.fetchall()
        except:
            comunes.dlgError()

        if len(saldo) > 0:
            debe_inicial = long(comunes.iif(saldo[0][0] == None,0,saldo[0][0]))
            haber_inicial = long(comunes.iif(saldo[0][1]==None,0,saldo[0][1]))
            saldo_inicial = debe_inicial - haber_inicial
            print debe_inicial, haber_inicial, saldo_inicial, saldo_final

        l1 = " " + \
            "Saldo"
            #"Debe".center(20) + \
            #"Haber".center(20) + \
        total_debe=0L
        total_haber=0L
        for i in self.modelo:
            total_debe+=long(float(i[DEBE]))
            if i[8]!='A':
                total_haber+=long(float(i[HABER]))
##        si=saldo_inicial+total_debe-total_haber
        si=total_debe-total_haber
        lc = "[%s] %s Saldo Igual Libro Mayor : " % (self.cod_cuenta, self.entCuenta.get_text())
        l = lc + \
        comunes.CMon(long(si))
        #comunes.CMon(long(debe_inicial)).rjust(20) + \
        #comunes.CMon(long(haber_inicial)).rjust(20) + \
        
        s =  "_" * len(l)
        linea,pagina = put_linea(c,linea,s,pagina)
        linea,pagina = put_linea(c,linea,l1,pagina)
        linea,pagina = put_linea(c,linea,l,pagina)
        
        linea,pagina = put_linea(c,linea,s,pagina)

        conciliados = [columna for columna in self.modelo if not columna[CONCILIADO]]
        if len(conciliados)!=0:
            total_debe=0L
            total_haber=0L
        if len(conciliados) >0:
            for i in self.modelo:
                if not i[CONCILIADO]:
                   # con = "No"
                    l = comunes.CDateLocal(i[FECHA]).ljust(12) + " " \
                        + i[GLOSA][:40].ljust(40) + " " \
                        + i[DOCUMENTO].rjust(3) + " " \
                        + comunes.CMon(long(float(i[DEBE]))).rjust(20) + " " \
                        + comunes.CMon(long(float(i[HABER]))).rjust(15) + " " \
                      #  + con.center(5)
                    ##movimiento no conciliados al debe y haber
                    suma_debe += long(float(i[DEBE]))
                    suma_haber+=long(float(i[HABER]))
##                    suma_haber += long(comunes.iif(i[8]!='A',float(i[HABER]),0))
                    linea, pagina = put_linea(c, linea, l, pagina)
                total_debe+=long(float(i[DEBE]))
                if i[8]!='A':
                    total_haber+=long(float(i[HABER]))
            print i[HABER]
            debe_final = long(debe_inicial - suma_debe)
            haber_final = long(haber_inicial + suma_haber)
            saldo_final = long(saldo_inicial + (haber_final - debe_final))
            movimientos_no_conciliados=(suma_debe+saldo_final)+suma_haber
##            movimientos_no_conciliados=long(suma_debe-suma_haber)
            saldo_cartola=long(suma_haber+saldo_inicial-suma_debe)
            print debe_final, haber_final, saldo_final 
        else:
            linea, pagina = put_linea(c, linea, "No hay cheques girados no cobrados.", pagina)
            debe_final = debe_inicial
            haber_final = haber_inicial 
            saldo_final = saldo_inicial 
            movimientos_no_conciliados=long(suma_debe-suma_haber)
            saldo_cartola=long(movimientos_no_conciliados+saldo_inicial)
        l1 = " " + \
            "Saldo"
            #"Debe".center(20) + \
            #"Haber".center(20) + \
            
        lc = "[%s] %s Cargos no Conciliados : " % (self.cod_cuenta, self.entCuenta.get_text())     
        l = lc + \
        comunes.CMon(long(suma_haber))
        #comunes.CMon(long(debe_final)).rjust(20) + \
        #comunes.CMon(long(haber_final)).rjust(20) + \        
      
##        s =  "_" * len(l)
##        linea,pagina = put_linea(c,linea,s,pagina)
        linea,pagina = put_linea(c,linea,l1,pagina)
        linea,pagina = put_linea(c,linea,l,pagina)
        lc = "[%s] %s Abonos no Conciliados : " % (self.cod_cuenta, self.entCuenta.get_text())     
        l = lc + \
        comunes.CMon(long(suma_debe))
        #comunes.CMon(long(debe_final)).rjust(20) + \
        #comunes.CMon(long(haber_final)).rjust(20) + \        
        saldo_igual_cartola=total_debe-total_haber-suma_debe+suma_haber
        s =  "_" * len(l)
        linea,pagina = put_linea(c,linea,s,pagina)
        linea,pagina = put_linea(c,linea,l1,pagina)
        linea,pagina = put_linea(c,linea,l,pagina)
        lc="[%s] %s Saldo igual Cartola :"% (self.cod_cuenta, self.entCuenta.get_text())        
        l2 = lc + \
        comunes.CMon(long(saldo_igual_cartola))
        
        linea,pagina= put_linea(c,linea,l2,pagina)
        linea,pagina = put_linea(c,linea,s,pagina)
        
        c.showPage()
        c.save()
        comunes.Abre_pdf("conciliacion.pdf")

if __name__ == "__main__":
    cnx =connect("dbname=scc")
    cnx.autocommit()
    c = wnConciliacion(cnx)
    c.wnConciliacion.show()
    sys.excepthook = debugwindow.show
    gtk.main()
