#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# ctb_libro_diario -- Libro diario del sistema
# (c)  Fernando San Martín Woerner      2003    fernando@galilea.cl
# (c)  Victor Benitez T.                2003    vbenitez@galilea.cl

#from reportlab.test import unittest
#from reportlab.test.utils import makeSuiteForClasses
# Generated on Thu Nov  2 18:19:49 2006

# (c)    Revisado Ricardo Mendez Lorca             2006     rmendez@galilea.cl

# Warning: Do not delete or modify comments related to context
# They are required to keep usezir's code

# (C)    José Luis Álvarez Morales      2006, 2007.
#            josealvarezm@gmail.com

import xlsxwriter
import xlwt
import os, gtk
import datetime
import fechas
from SimpleGladeApp import SimpleGladeApp
from GladeConnect import GladeConnect
from spg import connect
import SimpleTree
from comunes import *
import sys
import dialogos
import debugwindow
from impresion_fecus import BalanceFecus
from ctb_rutinas import ctb_formato_moneda
from completion import CompletionCuentaContable
from strSQL import strSelectEmpresa
from strSQL import strSelectEmpresaNivel
from codificacion import CISO
from reportlab.pdfbase.ttfonts import *
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import A4, landscape
glade_dir = ""

class wnLibrosElectronicos(GladeConnect):

    def __init__(self, conexion=None, padre=None, empresa=1, root="wnLibrosElectronicos"):
        GladeConnect.__init__(self, "glade/wnLibrosElectronicos.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.cod_empresa = empresa
        self.padre = padre
        self.Nivel=None
        #self.txtCuentaDesde.set_sensitive(False)
        #self.txtCuentaHasta.set_sensitive(False)
        #self.spnFolioDesde.set_sensitive(False)
        #self.spnFolioHasta.set_sensitive(False)
        #self.txtNumCuentaDesde.set_sensitive(False)
        #self.txtNumCuentaHasta.set_sensitive(False)
        if padre is None:
            self.wnLibrosElectronicos.minimize()
            self.frm_padre = self.wnLibrosElectronicos
            self.padre
        else:
            self.frm_padre = padre.frm_padre
        #self.pecInformeFecus = CompletioninformeFecus(self.txtInformeFecus, None, self.cnx,  self.cod_empresa)
        #self.pecCuentaContableDesde = CompletionCuentaContable(self.txtCuentaDesde,
        #        self.sel_cuenta_contable,
        #        self.cnx,
        #        self.cod_empresa)

        #self.pecCuentaContableHasta=CompletionCuentaContable(self.txtCuentaHasta,
        #        self.sel_cuenta_contable1,
        #        self.cnx,
        #        self.cod_empresa)
        self.tipos_comprobantes=[]
        #self.carga_tipo_comprobante()
        if padre is None:
            self.wnLibrosElectronicos.show_all()
        else:
            self.vboxLibrosElectronicos.show_all()

        sql="""SELECT nivel_cuenta from ctb.empresa where cod_empresa = %s"""%self.cod_empresa
        self.cursor.execute(sql)
        print sql
        r_nivel=self.cursor.fetchall()
        modelo = gtk.ListStore(str, str)
        for i in range(1, r_nivel[0][0]+1):
            modelo.append((i, i))
        #self.pecNivel=PixEntryCompletion(self.spnNivel, modelo,0, self.selecciona_nivel)


        #self.hbNivel.hide()
        #self.hbFecus.hide()
        #self.hbEstadoComprobante.hide()
        #(y,m,d) = time.localtime()[0:3]
        self.libro = None
        ##VARIABLES NUEVA
        #################################
        self.periodoDesde = None
        self.entPeriodoDesde = None
        self.periodoHasta = None
        self.entPeriodoHasta = None
        self.anioDesde = None
        self.anioHasta = None
        self.ultimoDia = None
        ################################
        #self.entHasta.set_date(datetime.date(y,m,d))
        #self.entDesde.set_date(datetime.date(y,01,01))
        #self.num_cuenta_desde=None
        #self.num_cuenta1_hasta=None
        self.Empresa()

    def Empresa(self, Where=''):
        Where='Where e.cod_empresa = %s'%(self.cod_empresa)
        sql = strSelectEmpresa %(Where)
        self.cursor.execute(sql)
        r = self.cursor.fetchall()
        if len(r)==0:
            dialogos.error("No hay Empresa seleccionada")
            return
        self.descripcion_empresa=r[0][1]
        self.rut_empresa=r[0][9]
        self.direccion_empresa=r[0][10]

    def sel_cuenta_contable(self, completion, model, iter):
        self.num_cuenta_desde = model.get_value(iter, 1)
        self.txtCuentaDesde.set_text(model.get_value(iter,0))
        self.txtNumCuentaDesde.set_text(self.num_cuenta_desde)

    def selecciona_nivel(self, completion, model, iter):
        self.Nivel = model.get_value(iter, 1)

    def on_btnDescargar_clicked(self, btn=None):
        active = self.cmbLibro.get_active()
        if active == -1:
            dialogos.error("Debe escoger un Libro Contable!!!")
            return
        if self.libro == 'BALANCE' and self.Nivel!=None:
            if float(self.Nivel)==0:
                dialogos.error("El nivel debe ser mayor que 0")
                return
##            if not self.pecCuentaContableDesde.get_selected() or  not self.pecCuentaContableHasta.get_selected():
##                dialogos.error("Debe seleccionar las cuentas,para generar el Balance")
##                return
            self.exportar_balance2()
            return

    def on_btnGenerarTxt_clicked(self, btn=None):
        ventas = 0
        strContenido = []
        active = self.cmbLibro.get_active()
        if active == -1:
            dialogos.error("Debe escoger un Libro Electrónico!!!")
            return
        filtro = "where"
        select = None
        codigo = None
        ##formatos fecha        
        f_desde = str(self.entPeriodoDesde) + '/' + '01' + '/' + str(self.anioDesde)
        f_hasta = str(self.entPeriodoHasta) + '/' + self.ultimoDia + '/' + str(self.anioHasta)
        print self.periodoDesde
        print self.entPeriodoDesde
        print self.periodoHasta
        print self.entPeriodoHasta
        print self.anioDesde
        print self.anioHasta
        periodo = str(self.anioDesde)+str(self.entPeriodoDesde)+'00'
        
        if self.anioDesde is None or self.anioHasta is None or self.entPeriodoDesde is None or self.entPeriodoHasta is None:
            dialogos,error("Faltan datos para el rango de fechas")
            return
        
        if self.anioHasta < self.anioDesde:
            dialogos.error("El año(hasta) debe ser menor al año(desde)")
            return
        elif self.entPeriodoHasta < self.entPeriodoDesde:
            dialogos.error("El mes(hasta) debe ser menor al mes(desde)")
            return
        
        if self.periodoDesde != None and self.anioDesde != None:
             if self.libro == 'LIBRO DIARIO' or self.libro == 'LIBRO MAYOR':
                filtro += " to_char(c.fecha,'YYYY')>='%s' and to_char(c.fecha,'MM') >= '%s'"%(str(self.anioDesde),str(self.entPeriodoDesde))    
             elif self.libro == 'DETALLE DEL PLAN CONTABLE UTILIZADO':
                filtro += " to_char(cuenta_contable.inserttime,'YYYY')>='%s' and to_char(cuenta_contable.inserttime,'MM') >= '%s'"%(str(self.anioDesde),str(self.entPeriodoDesde))
             elif self.libro == 'REGISTRO DE COMPRAS':
                filtro = ''
                filtro += " and c.periodo >= %s and c.mes >= %s"%(str(self.anioDesde),str(self.entPeriodoDesde))
             elif self.libro == 'LIBRO DE RETENCIONES':   
                filtro = ''
                filtro += " and cr.periodo >= %s and cr.mes >= %s"%(str(self.anioDesde),str(self.entPeriodoDesde))
             elif self.libro == 'REGISTRO DE VENTAS':   
                filtro += " trim(periodo) = '%s'"%(periodo)
             else:
                filtro += " substr(periodo,1,4)>='%s' and substr(periodo,5,2)>='%s'"%(str(self.anioDesde),str(self.entPeriodoDesde))
        if self.periodoHasta != None and self.anioHasta != None:
             if self.libro == 'LIBRO DIARIO' or self.libro == 'LIBRO MAYOR':
                filtro += " and to_char(c.fecha,'YYYY')<='%s' and to_char(c.fecha,'MM') <= '%s'"%(str(self.anioHasta),str(self.entPeriodoHasta))    
             elif self.libro == 'DETALLE DEL PLAN CONTABLE UTILIZADO':
                filtro += " and to_char(cuenta_contable.inserttime,'YYYY')<='%s' and to_char(cuenta_contable.inserttime,'MM') <= '%s'"%(str(self.anioHasta),str(self.entPeriodoHasta))    
             elif self.libro == 'REGISTRO DE COMPRAS':
                filtro += " and c.periodo <= %s and c.mes <= %s"%(str(self.anioHasta),str(self.entPeriodoHasta))
             elif self.libro == 'LIBRO DE RETENCIONES':
                filtro += " and cr.periodo <= %s and cr.mes <= %s"%(str(self.anioHasta),str(self.entPeriodoHasta))
             elif self.libro == 'REGISTRO DE VENTAS':   
                filtro += " "
             else:
                filtro += " and substr(periodo,1,4)<='%s' and substr(periodo,5,2)<='%s'"%(str(self.anioHasta),str(self.entPeriodoHasta))

        print filtro
        print self.libro
        

        #### VERIFICANDO QUE TIPO DE LIBRO PARA ASIGNAR UN QUERY ESPECÍFICO Y CÓDIGO
        if self.libro == 'LIBRO DIARIO':
            
            codigo = "00050100001111"
            select = """SELECT concat(to_char(c.fecha, 'YYYY'::text), to_char(c.fecha, 'MM'::text), '00')||'|'||
                        d.id||'|'||
                        CASE
                            WHEN c.cod_tipo_comprobante = 'A'::bpchar THEN concat('A', d.id)::character varying(10)
                            WHEN c.cod_tipo_comprobante = 'C'::bpchar THEN concat('C', d.id)::character varying(10)
                            ELSE concat('M', d.id)::character varying(10)
                        END||'|'||
                        replace(d.num_cuenta::text, '.'::text, ''::text)::numeric ||'|'||
                        ''::character varying||'|'||
                        ''::character varying||'|'||
                        'PEN'::character varying(3)||'|'||
                        ''::character varying||'|'||
                        ''::character varying||'|'||
                        CASE
                            WHEN d.cod_documento IS NOT NULL AND doc.cod_tipo_documento IS NULL AND d.cod_doc_contable IS NOT NULL THEN to_char(tdc.cod_sunat,'00')
                            WHEN d.cod_documento IS NULL AND d.cod_doc_contable IS NOT NULL THEN to_char(tdc.cod_sunat,'00')
                            ELSE to_char(0,'00')
                        END||'|'||
                        CASE
                            WHEN d.cod_doc_contable = 9 THEN '0001'::character varying(20)
                            WHEN d.cod_doc_contable = ANY (ARRAY[1, 3, 4, 6, 7, 8, 23, 25, 34, 35, 36, 48, 56]) THEN '0001'::character varying(20)
                            WHEN d.serie IS NULL OR d.serie::text = ''::text THEN '1'::character varying(20)
                            ELSE d.serie::character varying(20)
                        END||'|'||
                        coalesce(CASE
                            WHEN d.cod_documento IS NOT NULL AND doc.num_documento IS NOT NULL THEN doc.num_documento::character varying(20)
                            WHEN d.cod_documento IS NOT NULL AND doc.num_documento IS NULL AND c.glosa ~~ '%OBLIGACION DE RETENCION%'::text THEN cr.num_comp_retencion::character varying(20)
                            WHEN d.cod_documento IS NOT NULL AND doc.num_documento IS NULL AND c.cod_tipo_comprobante = 'E'::bpchar THEN egr.num_egreso::character varying(20)
                            WHEN d.cod_documento IS NOT NULL AND doc.num_documento IS NULL AND c.cod_tipo_comprobante = 'T'::bpchar AND mf.num_factura IS NOT NULL THEN mf.num_factura::character varying(20)
                            WHEN d.cod_documento IS NOT NULL AND doc.num_documento IS NULL AND c.cod_tipo_comprobante = 'T'::bpchar AND mf.num_factura IS NULL THEN cdc.cod_comprobante::character varying(20)
                            WHEN d.cod_documento IS NOT NULL AND doc.num_documento IS NULL AND c.cod_tipo_comprobante = 'I'::bpchar THEN cdc.cod_comprobante::character varying(20)
                            WHEN d.cod_documento IS NULL AND c.origen::text = 'SCB'::text THEN d.serie::character varying(20)
                            ELSE c.cod_comprobante::character varying(20)
                        END,'')||'|'||
                        ''::character varying||'|'||
                        ''::character varying||'|'||
                        to_char(c.fecha::timestamp with time zone, 'DD/MM/YYYY'::text)||'|'||
                            CASE
                                WHEN d.glosa IS NOT NULL OR d.glosa::text <> ''::text THEN replace(d.glosa::text, chr(10), ''::text)::character varying(200)
                                ELSE '---'::character varying(200)
                            END||'|'||
                        ''::character varying||'|'||
                        round(d.monto_debe::numeric, 2)||'|'||
                        round(d.monto_haber::numeric, 2)||'|'||
                        ''::character varying||'|'||
                        1||'|'
                   FROM ctb.comprobante c
                     JOIN ctb.detalle_comprobante d ON d.cod_comprobante = c.cod_comprobante
                     LEFT JOIN documento doc ON d.cod_documento = doc.cod_documento
                     JOIN ctb.empresa e ON e.cod_empresa = c.cod_empresa
                     JOIN ctb.cuenta_contable t ON d.num_cuenta::text = t.num_cuenta::text AND e.cod_empresa = t.cod_empresa
                     LEFT JOIN ctb.tipo_doc_contable tdc ON d.cod_doc_contable = tdc.cod_doc_contable
                     LEFT JOIN egreso egr ON d.cod_documento = egr.cod_egreso
                     LEFT JOIN cc.multi_factura mf ON d.cod_documento = mf.cod_multi_factura
                     LEFT JOIN cc.detalle_comprobante cdc ON d.cod_documento = cdc.id_detalle
                     LEFT JOIN comp_retencion cr ON d.cod_documento = cr.cod_comp_retencion"""
                        
        if self.libro == 'DETALLE DEL PLAN CONTABLE UTILIZADO':
            
            codigo = "00050300001111"
            select = """SELECT  concat(to_char(cuenta_contable.inserttime, 'YYYY'::text), to_char(cuenta_contable.inserttime, 'MM'::text), to_char(cuenta_contable.inserttime, 'DD'::text))||'|'||
                                replace(cuenta_contable.num_cuenta::text, '.'::text, ''::text)::numeric||'|'||
                                cuenta_contable.descripcion_cuenta||'|'||
                                to_char(1,'00')||'|'||
                                'PLAN CONTABLE GENERAL EMPRESARIAL'::character varying||'|'||
                                ''::character varying||'|'||
                                ''::character varying||'|'||
                                1||'|'
                                FROM ctb.cuenta_contable """
            
        if self.libro == 'LIBRO MAYOR' :
            
            ##SE ESTÁ UTILIZANDO EL MISMO QUERY DEBIDO QUE SON LOS MISMOS CAMPOS, SOLO SE HACE EL CAMBIO AL CÓDIGO
            codigo = "00060100001111"
            select = """SELECT concat(to_char(c.fecha, 'YYYY'::text), to_char(c.fecha, 'MM'::text), '00')||'|'||
                        d.id||'|'||
                        CASE
                            WHEN c.cod_tipo_comprobante = 'A'::bpchar THEN concat('A', d.id)::character varying(10)
                            WHEN c.cod_tipo_comprobante = 'C'::bpchar THEN concat('C', d.id)::character varying(10)
                            ELSE concat('M', d.id)::character varying(10)
                        END||'|'||
                        replace(d.num_cuenta::text, '.'::text, ''::text)::numeric ||'|'||
                        ''::character varying||'|'||
                        ''::character varying||'|'||
                        'PEN'::character varying(3)||'|'||
                        ''::character varying||'|'||
                        ''::character varying||'|'||
                        CASE
                            WHEN d.cod_documento IS NOT NULL AND doc.cod_tipo_documento IS NULL AND d.cod_doc_contable IS NOT NULL THEN to_char(tdc.cod_sunat,'00')
                            WHEN d.cod_documento IS NULL AND d.cod_doc_contable IS NOT NULL THEN to_char(tdc.cod_sunat,'00')
                            ELSE to_char(0,'00')
                        END||'|'||
                        CASE
                            WHEN d.cod_doc_contable = 9 THEN '0001'::character varying(20)
                            WHEN d.cod_doc_contable = ANY (ARRAY[1, 3, 4, 6, 7, 8, 23, 25, 34, 35, 36, 48, 56]) THEN '0001'::character varying(20)
                            WHEN d.serie IS NULL OR d.serie::text = ''::text THEN '1'::character varying(20)
                            ELSE d.serie::character varying(20)
                        END||'|'||
                        coalesce(CASE
                            WHEN d.cod_documento IS NOT NULL AND doc.num_documento IS NOT NULL THEN doc.num_documento::character varying(20)
                            WHEN d.cod_documento IS NOT NULL AND doc.num_documento IS NULL AND c.glosa ~~ '%OBLIGACION DE RETENCION%'::text THEN cr.num_comp_retencion::character varying(20)
                            WHEN d.cod_documento IS NOT NULL AND doc.num_documento IS NULL AND c.cod_tipo_comprobante = 'E'::bpchar THEN egr.num_egreso::character varying(20)
                            WHEN d.cod_documento IS NOT NULL AND doc.num_documento IS NULL AND c.cod_tipo_comprobante = 'T'::bpchar AND mf.num_factura IS NOT NULL THEN mf.num_factura::character varying(20)
                            WHEN d.cod_documento IS NOT NULL AND doc.num_documento IS NULL AND c.cod_tipo_comprobante = 'T'::bpchar AND mf.num_factura IS NULL THEN cdc.cod_comprobante::character varying(20)
                            WHEN d.cod_documento IS NOT NULL AND doc.num_documento IS NULL AND c.cod_tipo_comprobante = 'I'::bpchar THEN cdc.cod_comprobante::character varying(20)
                            WHEN d.cod_documento IS NULL AND c.origen::text = 'SCB'::text THEN d.serie::character varying(20)
                            ELSE c.cod_comprobante::character varying(20)
                        END,'')||'|'||
                        ''::character varying||'|'||
                        ''::character varying||'|'||
                        to_char(c.fecha::timestamp with time zone, 'DD/MM/YYYY'::text)||'|'||
                            CASE
                                WHEN d.glosa IS NOT NULL OR d.glosa::text <> ''::text THEN replace(d.glosa::text, chr(10), ''::text)::character varying(200)
                                ELSE '---'::character varying(200)
                            END||'|'||
                        ''::character varying||'|'||
                        round(d.monto_debe::numeric, 2)||'|'||
                        round(d.monto_haber::numeric, 2)||'|'||
                        ''::character varying||'|'||
                        1||'|'
                   FROM ctb.comprobante c
                     JOIN ctb.detalle_comprobante d ON d.cod_comprobante = c.cod_comprobante
                     LEFT JOIN documento doc ON d.cod_documento = doc.cod_documento
                     JOIN ctb.empresa e ON e.cod_empresa = c.cod_empresa
                     JOIN ctb.cuenta_contable t ON d.num_cuenta::text = t.num_cuenta::text AND e.cod_empresa = t.cod_empresa
                     LEFT JOIN ctb.tipo_doc_contable tdc ON d.cod_doc_contable = tdc.cod_doc_contable
                     LEFT JOIN egreso egr ON d.cod_documento = egr.cod_egreso
                     LEFT JOIN cc.multi_factura mf ON d.cod_documento = mf.cod_multi_factura
                     LEFT JOIN cc.detalle_comprobante cdc ON d.cod_documento = cdc.id_detalle
                     LEFT JOIN comp_retencion cr ON d.cod_documento = cr.cod_comp_retencion"""
        
        if self.libro == 'REGISTRO DE INVENTARIO PERMANENTE VALORIZADO':
            
            codigo = "00130100001111"
            sql = """ select bdg.fn_genera_kardex('%s', '%s')"""%(f_desde,f_hasta)
            self.cursor.execute(sql)
            select = """ select periodo||'|'||cod_documento_bodega||cod_detalle_documento_bodega||'|M'||cod_detalle_documento_bodega||'|'||coalesce(cod_establecimiento,'')||'|9|03|'||cod_producto||'||'||
                                to_char(fecha_emision,'dd/mm/yyyy')||'|'||(case when tipo_operacion='05' then '09' else tipo_documento end)
                                ||'|0001|'||num_documento||'|'||tipo_operacion||'|'||
                                replace(replace(replace(desc_producto,',',''), '"',''),'/','')||'|'||cod_unidad||'|1|'||
                                round(ingreso_unidades,2)||'|'||round(ingreso_costo_unitario,2)||'|'||round(ingreso_costo_total,2)||'|'||
                                round(salida_unidades,2)||'|'||round(salida_costo_unitario,2)||'|'||round(salida_costo_total,2)||'|'||
                                round(saldo_unidades,2)||'|'||round(saldo_costo_unitario,2)||'|'||round(saldo_costo_total,2)||'|1|' tupla
                                from bdg.kardex a """           
        
        if self.libro == 'REGISTRO DE COMPRAS':
            
            codigo = "00080100001111"
            select = """ select 
                          --(select descripcion_documento from ctb.tipo_doc_contable where cod_doc_contable = tdc.cod_sunat),
                          --d.cod_documento,
                           case when mes < 10 then periodo::text || '0' || mes::text || '00' 
                            when mes > 9 then periodo::text || mes::text || '00' 
                           end 
                           ||'|'||
                           --c.cod_comprobante::text,
                           c.cod_comprobante::text||dc.id::text
                           ||'|'||
                           case when c.cod_tipo_comprobante = 'A' then 'A' || dc.id::text
                            when c.cod_tipo_comprobante = 'C' then 'C' || dc.id::text
                            else 'M' || dc.id::text
                           end
                           ||'|'||
                           to_char(d.fecha_emision, 'dd/mm/yyyy')
                           ||'|'||
                           coalesce(case when tdc.cod_sunat = 14 then to_char(d.fecha_emision+15, 'dd/mm/yyyy') end,'')
                           ||'|'||
                           case when tdc.cod_sunat = 10 then '00'
                            when tdc.cod_sunat < 10 then '0' || tdc.cod_sunat::text
                                else tdc.cod_sunat::text end
                           ||'|'||
                           --d.serie, --7
                           case when cod_sunat = 5 then 3::text else lpad(d.serie::text,4,'0') end
                           ||'||'||
                           d.num_documento
                           ||'||'||
                           case when d.rut_proveedor in ('00000000009','89086202000') then 0 else 6 end ||'|'|| --cod_tipo_documento_identidad (RUC)
                           d.rut_proveedor
                           ||'|'||
                           --(select nom_proveedor from proveedor where rut_proveedor = d.rut_proveedor)
                           p.nom_proveedor
                           ||'|||'||
                           case when monto_debe <> 0 then c1
                            when cod_sunat = 7 then (c1-c2-c3)*-1
                                else 0 end
                           ||'|'||
                           case when cod_sunat = 7 then c3*-1
                                when monto_debe <> 0 then c3
                                else 0 end 
                           ||'|||'||
                           case when monto_debe = 0 and cod_sunat <> 7 then c1+c2+c3
                                else c2 end 
                           ||'|||'||
                           case when cod_sunat = 7 then (c1+c2+c4)*-1
                               -- else c1+c2+c3+c4 end as importeTotal,
                                else c1+c2+c3 end
                           ||'|||'||
                                
                           --columna 26, solo para Notas de Credito
                           coalesce(case 
                            when tdc.cod_sunat in (7,8) then (select to_char(fecha_emision, 'dd/mm/yyyy') 
                                         from documento 
                                         where cod_documento = (select documento_aux 
                                                    from detalle_documento 
                                                    where cod_documento = d.cod_documento limit 1))
                           end,'')
                           ||'|'||
                           --columna 27, solo para Notas de Credito
                           coalesce(case 
                            when tdc.cod_sunat in (7,8) then (select case when t.cod_sunat < 10 then '0' || t.cod_sunat::text else t.cod_sunat::text end
                                         from documento doc, ctb.tipo_doc_contable t 
                                         where doc.cod_doc_contable = t.cod_doc_contable
                                         and cod_documento = (select documento_aux 
                                                    from detalle_documento 
                                                    where cod_documento = d.cod_documento limit 1))
                           end,'')
                           ||'|'||
                           --columna 28, solo para Notas de Credito
                           coalesce(case 
                            when tdc.cod_sunat in (7,8) then (select case when t.cod_sunat = 5 then 3::text else lpad(doc.serie::text,4,'0') end
                                         from documento doc, ctb.tipo_doc_contable t 
                                         where doc.cod_doc_contable = t.cod_doc_contable
                                         and cod_documento = (select documento_aux 
                                                    from detalle_documento 
                                                    where cod_documento = d.cod_documento limit 1))
                           end,'')
                           ||'||'||
                           --columna 30, solo para Notas de Credito
                           coalesce(case 
                            when tdc.cod_sunat in (7,8) then (select num_documento 
                                         from documento 
                                         where cod_documento = (select documento_aux 
                                                    from detalle_documento 
                                                    where cod_documento = d.cod_documento limit 1))::text
                           end,'')
                        ||'|||||||||||'||   
                             case 
                            when tdc.cod_sunat in (2,3,10) then 0	
                                when (to_char(d.fecha_emision,'mm')::numeric < mes or to_char(d.fecha_emision,'yyyy')::numeric < periodo ) then 6
                                when monto_debe = 0 then 0 
                            else 1
                           end
                          ||'|'
                    
                    from documento d, ctb.tipo_doc_contable tdc, ctb.comprobante c, ctb.detalle_comprobante dc, proveedor p
                    where
                    d.cod_doc_contable = tdc.cod_doc_contable 
                    and dc.cod_documento = d.cod_documento
                    and dc.cod_comprobante = c.cod_comprobante
                    and p.rut_proveedor = d.rut_proveedor
                    and d.cod_doc_contable in (1,2,3,4,5,6,7,8,10,11,12,13,14,15,16,17,18,19,21,22,23,24,25,26,27,28,29,30,32,34,35,36,37,42,43,44,45,46,48,49,50,51,52,53,54,55,56,87,88,89,96,99)
                    and d.cod_tipo_documento in (4,5,6)
                    and cod_tipo_comprobante = 'T'
                    and origen = 'SCP'
                    and num_cuenta ilike '%4.40%'
                    and p.nom_proveedor not ilike '%RENDICION MOVILIDAD%'
                    and d.rut_proveedor <> '20521119943'
                    and d.rut_proveedor <> '20100030595'"""
        #jqd
        if self.libro == 'REGISTRO DE VENTAS':                     
            codigo = "00140100001111"
            select = """SELECT trim(periodo)||'|'||cod_documento2||'|'||correlativo||'|'||fecha_emision||'|'||fecha_vencimiento||'|'||tipo_documento||'|'||trim(serie_doc)||'|'|| 
                        trim(numero_doc)||'| |'||tipo_documento_identidad||'|'||num_dni||'|'||razon_social||'|'||
                        valor_factura_exportacion||'|'||base_imponible||'|'||desc_base_imponible||'|'||igv||'|'||valor_exonerada||'|'||valor_isc||'|'||inafecto||'| | | |'||valor_otros_cargos||'|'||
                        total||'|'||cod_moneda||'|'||tipo_cambio||'|'||fecha_nota_credito||'|'||tipo_nota_credito||'|'||serie_nota_credito||'|'||numero_comprobante||'|000000000000|'||error_tipo_uno||'|'||indicador_comp_pago_cancelado||'|'||anotacion_o_indicacion||'|'
        FROM
    (SELECT DISTINCT 
            aa.periodo, 
            aa.cod_documento2, 
            aa.correlativo, 
            aa.fecha_emision, 
            aa.fecha_vencimiento, 
            aa.tipo_documento, 
            aa.serie_doc, 
            aa.numero_doc, 
            aa.registro_ticket, 
            aa.tipo_documento_identidad,
            case when length(trim(aa.numero_documento,'0'))>8 then ltrim(aa.numero_documento,'0') else substr(aa.numero_documento,4,8) end num_dni,
            aa.razon_social, 
            aa.valor_factura_exportacion, 
            round(aa.valor_base_imponible::numeric,2) base_imponible,
            aa.desc_base_imponible, 
            round(aa.valor_igv::numeric,2) igv,
            aa.valor_exonerada, 
            aa.valor_isc,
            round(aa.valor_inafecto::numeric,2) inafecto, 
            aa.desc_igv,
            aa.base_imponible_arroz_pilado, 
            aa.impuesto_venta_arroz_pilado, 
            aa.valor_otros_cargos, 
            aa.total, 
            aa.cod_moneda, 
            aa.tipo_cambio,
            (case when aa.tipo_documento = '07' then (case when bb.fecha_documento_afectado is null then '01/01/0001' else to_char(bb.fecha_documento_afectado, 'DD/MM/YYYY') end) else aa.fecha_nota_credito end),
            (case when aa.tipo_documento = '07' then '03' else aa.tipo_nota_credito end),
            case when position ('-' in (case when aa.tipo_documento = '07' then (bb.numero_documento_afectado) else aa.serie_nota_credito end)) > 0 then (case when aa.tipo_documento = '07' then lpad((SUBSTRING(bb.numero_documento_afectado,1, position ('-' in (case when aa.tipo_documento = '07' then (bb.numero_documento_afectado) else aa.serie_nota_credito end)) - 1)),4,'0') else aa.serie_nota_credito end) else (case when aa.tipo_documento = '07' then ('0000') else aa.serie_nota_credito end) end,
            --(case when aa.tipo_documento = '07' then (bb.numero_documento_afectado) else aa.serie_nota_credito end)
            case when position ('-' in (case when aa.tipo_documento = '07' then (bb.numero_documento_afectado) else aa.numero_comprobante end)) > 0 then (case when aa.tipo_documento = '07' then lpad((SUBSTRING(bb.numero_documento_afectado,position ('-' in (case when aa.tipo_documento = '07' then (bb.numero_documento_afectado) else aa.numero_comprobante end)) + 1,length(bb.numero_documento_afectado))),4,'0') else aa.numero_comprobante end) else (case when aa.tipo_documento = '07' then ('0000') else aa.numero_comprobante end) end,
            --(case when aa.tipo_documento = '07' then (bb.numero_documento_afectado) else aa.serie_nota_credito end)
            aa.nom_proyecto, --W
            aa.error_tipo_uno, --nuevo
            aa.indicador_comp_pago_cancelado, --nuevo
            aa.anotacion_o_indicacion          
            from ((SELECT
                    to_char(mfa.cod_libro,'000000')||'00' as periodo,
                    --(case when mfa.fecha_factura is null then '01/01/0001' else to_char(mfa.fecha_factura, 'YYYYMM00') end) as periodo,
                    mfa.cod_multi_factura as codigo,
                    CONCAT('M',mfa.cod_multi_factura)::varchar(10) as correlativo,
                    --(case when mfa.factura_nula = 0 then (case when mfa.fecha_factura is null then '' else to_char(mfa.fecha_factura, 'DD/MM/YYYY') end) else '0' end) as fecha_emision,
                    (case when mfa.fecha_factura is null then '01/01/0001' else to_char(mfa.fecha_factura, 'DD/MM/YYYY') end) as fecha_emision,			
                    (case when mfa.fecha_factura is null then '01/01/0001' else to_char(mfa.fecha_factura, 'DD/MM/YYYY') end) as fecha_vencimiento,                    
                    (case when tipo_documento = 'B' then '03' when tipo_documento = 'F' then '01' else ' ' end) as tipo_documento, 		
                    (case when mfa.serie_factura is null then '' else to_char(mfa.serie_factura::integer,'0000') end ) as serie_doc,
                    (case when mfa.num_factura is null then ' ' else to_char(mfa.num_factura::integer,'0000000') end ) as numero_doc,
                    (case when tipo_documento = 'F' then null::integer else mfa.cod_multi_factura end) as registro_ticket,
                    (case when mfa.factura_nula = 0 then (case when cl.rut_cliente is null then ' ' else (case when substr(cl.rut_cliente,1,3)= '000' then '1' else '6' end) end) else '1' end) as tipo_documento_identidad,
                    (case when mfa.factura_nula = 0 then (case when cl.rut_cliente is null then 'N.N.' else cl.rut_cliente end) else '00000000000' end) as numero_documento,
                    (case when mfa.factura_nula = 0 then (case when cl.nom_cliente is null then 'N.N.' else cl.nom_cliente end) else 'DOCUMENTO ANULADO' end) as razon_social,
                    0 as valor_factura_exportacion,
                    (case when mfa.factura_nula = 0 then (case when mfa.igv > 0 then mfa.monto/2 else (0) end) else 0 end) as valor_base_imponible,
                    0 as desc_base_imponible, 
                    0 as valor_exonerada,
                    (case when mfa.factura_nula = 0 then (case when mfa.igv > 0 then mfa.monto/2 else (mfa.monto + mfa.igv) end) else 0 end) as valor_inafecto,
                    0 as valor_isc,
                    (case when mfa.factura_nula = 0 then (mfa.igv) else 0 end) as valor_igv,
                    null as desc_igv,
                    null as base_imponible_arroz_pilado,
                    null as impuesto_venta_arroz_pilado, 
                    0 as valor_otros_cargos,
                    (case when mfa.factura_nula = 0 then (mfa.monto + mfa.igv) else 0 end) as total,
                    'PEN' as cod_moneda,
                    (select valor from comun.tipo_cambio where fecha = mfa.fecha_factura and tipo = 'Venta') as tipo_cambio,
                    ' ' as fecha_nota_credito,
                    '00' as tipo_nota_credito,
                    '0000' as serie_nota_credito,
                    '0' as numero_comprobante,
                    mfa.cod_libro,
                    mfa.cod_multi_factura as cod_documento2,
                    (select nom_proyecto from cc.proyecto where cod_proyecto in (select cod_proyecto from comun.lote where cod_lote=(select cod_lote from cc.contrato where cod_contrato=con.cod_contrato))) as nom_proyecto,
                    1 as error_tipo_uno,
                    1 as indicador_comp_pago_cancelado,
                    (case when to_char(mfa.fecha_factura,'MM')::integer<>(mfa.cod_libro%100) then 8 else 1 end) as anotacion_o_indicacion	
                    FROM cc.multi_factura mfa
                    left join cc.contrato con on mfa.cod_contrato = con.cod_contrato
                    left JOIN comun.lote AS com ON con.cod_lote = com.cod_lote
                    left JOIN cc.proyecto AS pro ON com.cod_proyecto = pro.cod_proyecto
                    left JOIN cc.sucursal s ON s.cod_sucursal = pro.cod_sucursal AND s.cod_empresa = pro.cod_empresa                    
                    LEFT JOIN cc.cliente cl ON cl.rut_cliente = con.rut_cliente 
                    WHERE mfa.rut_clie is null and mfa.serie_factura is not null and con.rut_cliente is not null and pro.cod_empresa = 1 )--and (mfa.cod_libro = 201602)  ) --acá termina el 2do paréntesis del primer from
                    --PIMER GRAN UNION
                    union
                    (
                        (select
                        to_char(c.cod_libro,'000000')||'00' as periodo,
                        --(case when c.fecha_comprobante is null then '01/01/0001' else to_char(c.fecha_comprobante, 'YYYYMM00') end) as periodo, 
                         c.cod_comprobante as codigo,
                         CONCAT('M',dc.id_detalle)::varchar(10) as correlativo,
                        (case when c.fecha_comprobante is null then '01/01/0001' else to_char(c.fecha_comprobante, 'DD/MM/YYYY') end) as fecha_emision,		
                        (case when c.fecha_comprobante is null then '' else to_char(c.fecha_comprobante, 'DD/MM/YYYY') end) as fecha_vencimiento,                    
                        ('07') as tipo_documento,		                     
                        (case when serie_nc is null then '' else to_char(serie_nc::integer,'0000') end ) as serie_doc,
                        (case when numero_nc is null then ' ' else to_char(numero_nc::integer,'0000000') end ) as numero_doc,
                        null::integer as registro_ticket,                    
                        (case when cl.rut_cliente is null then ' ' else (case when substr(cl.rut_cliente,1,3)= '000' then '1' else '6' end) end) as tipo_documento_identidad,
                        (case when dc.cod_contrato = -300 then cl2.rut_cliente else (case when cl.rut_cliente is null then 'N.N.' else cl.rut_cliente end) end) as numero_documento,
                        (case when dc.cod_contrato = -300 then cl2.nom_cliente else (case when cl.nom_cliente is null then 'N.N.' else cl.nom_cliente end) end) as razon_social,
                        0 as valor_factura_exportacion,
                        (case when c.cod_comprobante = 6143 then 0 else
                        (case when dc.cod_contrato = -300 then (dc.monto/1.18) else
                        (case when con.valor_venta > 127750 then (dc.monto/1.09)/2 else 0 end) end) end) as valor_base_imponible,
                        0 as desc_base_imponible, 
                        0 as valor_exonerada,
                        (case when c.cod_comprobante = 6143 then (case when con.valor_venta > 127750 then (dc.monto) else dc.monto end) else (case when dc.cod_contrato = -300 then 0 else (case when con.valor_venta > 127750 then (dc.monto/1.09)/2 else dc.monto end) end) end) as valor_inafecto,
                        0 as valor_isc,
                        (case when c.cod_comprobante = 6143 then 0 else
                        (case when dc.cod_contrato = -300 then
                        ((case when c.cod_comprobante = 6143 then 0 else
                        (case when dc.cod_contrato = -300 then (dc.monto/1.18) else
                        (case when con.valor_venta > 127750 then (dc.monto/1.09)/2 else 0 end) end) end)*0.18) else (case when con.valor_venta > 127750 then ((case when c.cod_comprobante = 6143 then 0 else
                        (case when dc.cod_contrato = -300 then (dc.monto/1.18) else
                        (case when con.valor_venta > 127750 then (dc.monto/1.09)/2 else 0 end) end) end)*0.18) else 0 end) end) end) as valor_igv,
                        null as desc_igv,
                        null as base_imponible_arroz_pilado,
                        null as impuesto_venta_arroz_pilado, 
                        0 as valor_otros_cargos,
                        (dc.monto) as total,
                        'PEN' as cod_monera,
                        (select valor from comun.tipo_cambio where fecha = c.fecha_comprobante and tipo = 'Venta') as tipo_cambio,
                        ' ' as fecha_nota_credito,
                        '00' as tipo_nota_credito,
                        '0000' as serie_nota_credito,
                        '0' as numero_comprobante,
                        c.cod_libro,
                        dc.id_detalle as cod_documento2,
                        (select nom_proyecto from cc.proyecto where cod_proyecto in (select cod_proyecto from comun.lote where cod_lote=(select cod_lote from cc.contrato where cod_contrato=con.cod_contrato))) as nom_proyecto,
                        1 as error_tipo_uno,
			1 as indicador_comp_pago_cancelado,
			(case when to_char(c.fecha_comprobante,'MM')::integer<>(c.cod_libro%100) then 8 else 1 end) as anotacion_o_indicacion             
                        from cc.detalle_comprobante dc
                        left join cc.comprobante c on dc.cod_comprobante = c.cod_comprobante
                        left join cc.contrato con on dc.cod_contrato = con.cod_contrato
                        LEFT JOIN cc.cliente cl ON cl.rut_cliente = con.rut_cliente
                        left join cc.cliente cl2 on dc.rut_cliente = cl2.rut_cliente
                        where dc.monto < 0 and (case when serie_nc is null then ' ' else (case when length(rtrim(serie_nc,' '))=0 then ' ' else to_char(serie_nc::integer,'000') end) end ) <> '000' --and (c.cod_libro = 201602) 
                        )
                        union
                        (select 
                        '0' as periodo,
                        c.old_cod_comprobante as codigo,
                        CONCAT('M',dc.old_id_detalle)::varchar(10) as correlativo,
                        '0' as fecha_emision,			
                        '0' as fecha_vencimiento,                    
                        ('07') as tipo_documento,		                     
                        (case when old_serie_nc is null then '' else to_char(old_serie_nc::integer,'0000') end ) as serie_doc,
                        (case when old_numero_nc is null then ' ' else to_char(old_numero_nc::integer,'0000000') end ) as numero_doc, 
                         null::integer as registro_ticket,                   
                        (case when cl.rut_cliente is null then ' ' else (case when substr(cl.rut_cliente,1,3)= '000' then '1' else '6' end) end) as tipo_documento_identidad,
                        '00000000000' as numero_documento,
                        'DOCUMENTO ANULADO' as razon_social,
                        0 as valor_factura_exportacion,
                        0,
                        0,
                        0 as valor_exonerada,
                        0,
                        0 as valor_isc,
                        0,
                        null as desc_igv,
                        null as base_imponible_arroz_pilado,
                        null as impuesto_venta_arroz_pilado, 
                        0 as valor_otros_cargos,
                        0,
                        'PEN' as cod_moneda,
                        0 as tipo_cambio,
                        ' ' as fecha_nota_credito,
                        '00' as tipo_nota_credito,
                        '0000' as serie_nota_credito,
                        '0' as numero_comprobante,
                        c.old_cod_libro,
                        dc.old_id_detalle as cod_documento2,
                        (select nom_proyecto from cc.proyecto where cod_proyecto in (select cod_proyecto from comun.lote where cod_lote=(select cod_lote from cc.contrato where cod_contrato=con.cod_contrato))) as nom_proyecto,
                        1 as error_tipo_uno,
                        1 as indicador_comp_pago_cancelado,
                        0 as anotacion_o_indicacion
                    from 	cc.historia_detalle_comprobante dc
                        left join cc.historia_comprobante_ c on dc.old_cod_comprobante = c.old_cod_comprobante
                        left join cc.contrato con on dc.old_cod_contrato = con.cod_contrato
                        LEFT JOIN cc.cliente cl ON cl.rut_cliente = con.rut_cliente 
                    where 	dc.old_monto < 0 
                        and (case when old_serie_nc is null then ' ' else (case when length(rtrim(old_serie_nc,' '))=0 then ' ' else to_char(old_serie_nc::integer,'000') end) end ) <> '000'
                        and dc.accion = 'DELETE'
                        and c.accion = 'DELETE'
                        and (substr((case when old_fecha_documento is null then ' ' else to_char(old_fecha_documento, 'DD/MM/YYYY') end),7,4)||substr((case when old_fecha_documento is null then ' ' else to_char(old_fecha_documento, 'DD/MM/YYYY') end),4,2))::int >= 201307 --and (c.old_cod_libro = 201602) 
                                )
                    )
          --2DO GRAN UNION       
                    union                    
                    (SELECT
                    to_char(mfa.cod_libro,'000000')||'00' as periodo,
                    --(case when mfa.fecha_factura is null then '01/01/0001' else to_char(mfa.fecha_factura, 'YYYYMM00') end) as periodo,
                    mfa.cod_multi_factura as codigo,
                    CONCAT('M',mfa.cod_multi_factura)::varchar(10) as correlativo,
                    --(case when mfa.factura_nula = 0 then (case when mfa.fecha_factura is null then '' else to_char(mfa.fecha_factura, 'DD/MM/YYYY') end) else '0' end) as fecha_emision,
                    (case when mfa.fecha_factura is null then '01/01/0001' else to_char(mfa.fecha_factura, 'DD/MM/YYYY') end) as fecha_emision,			
                    (case when mfa.fecha_factura is null then '01/01/0001' else to_char(mfa.fecha_factura, 'DD/MM/YYYY') end) as fecha_vencimiento,                    
                    (case when tipo_documento = 'B' then '03' when tipo_documento = 'F' then '01' else ' ' end) as tipo_documento,		
                    (case when mfa.serie_factura is null then '' else to_char(mfa.serie_factura::integer,'0000') end ) as serie_doc,
                    (case when mfa.num_factura is null then ' ' else to_char(mfa.num_factura::integer,'0000000') end ) as numero_doc,
                    (case when tipo_documento = 'F' then null::integer else mfa.cod_multi_factura end) as registro_ticket, 
                    (case when mfa.factura_nula = 0 then (case when cl.rut_cliente is null then ' ' else (case when substr(cl.rut_cliente,1,3)= '000' then '1' else '6' end) end) else '1' end) as tipo_documento_identidad,
                    (case when mfa.factura_nula = 0 then (case when cl.rut_cliente is null then 'N.N.' else cl.rut_cliente end) else '00000000000' end) as numero_documento,
                    (case when mfa.factura_nula = 0 then (case when cl.nom_cliente is null then 'N.N.' else cl.nom_cliente end) else 'DOCUMENTO ANULADO' end) as razon_social,
                    0 as valor_factura_exportacion,
                    (case when mfa.factura_nula = 0 then (case when mfa.igv > 0 then mfa.monto else (0) end) else 0 end) as valor_base_imponible,
                    0 as desc_base_imponible, 
                    0 as valor_exonerada,
                    (case when mfa.factura_nula = 0 then (case when mfa.igv > 0 then 0 else (mfa.monto + mfa.igv) end) else 0 end) as valor_inafecto,
                    0 as valor_isc,
                    (case when mfa.factura_nula = 0 then (mfa.igv) else 0 end) as valor_igv,
                    null as desc_igv,
                    null as base_imponible_arroz_pilado,
                    null as impuesto_venta_arroz_pilado, 
                    0 as valor_otros_cargos,
                    (case when mfa.factura_nula = 0 then (mfa.monto + mfa.igv) else 0 end) as total,
                    'PEN' as cod_moneda,
                    (select valor from comun.tipo_cambio where fecha = mfa.fecha_factura and tipo = 'Venta') as tipo_cambio,
                    ' ' as fecha_nota_credito,
                    '00' as tipo_nota_credito,
                    '0000' as serie_nota_credito,
                    '0' as numero_comprobante,
                    mfa.cod_libro,
                    mfa.cod_multi_factura as cod_documento2,
                    case when mfa.cod_contrato=-200 then (select nom_sucursal from cc.sucursal where cod_sucursal=mfa.cod_sucursal) else (select nom_proyecto from cc.proyecto where cod_proyecto in (select cod_proyecto from comun.lote where cod_lote=(select cod_lote from cc.contrato where cod_contrato=mfa.cod_contrato))) end as nom_proyecto,
                    1 as error_tipo_uno,
                    1 as indicador_comp_pago_cancelado,
                    (case when to_char(mfa.fecha_factura,'MM')::integer<>(mfa.cod_libro%100) then 8 else 1 end) as anotacion_o_indicacion
                    FROM cc.multi_factura mfa
                    JOIN cc.cliente cl ON cl.rut_cliente = mfa.rut_clie
                    WHERE mfa.rut_clie is not null and mfa.serie_factura is not null )--and (mfa.cod_libro = 201602)  )
                    
                    ) --acá se cierra el el primer from

                    aa left join
                    (
                        SELECT
                        c.cod_comprobante,
                        c.serie_nc,
                        c.numero_nc, 
                        d.cod_multi_factura,
                        c.numero_documento_afectado,
                        c.fecha_documento_afectado,
                        d.id_detalle as cod_documento2
                        FROM
                        cc.comprobante c                    
                        JOIN cc.detalle_comprobante d
                        ON d.cod_comprobante = c.cod_comprobante
                        JOIN cc.vw_ultimo_id_detalle dcr ON dcr.id_detalle = d.id_detalle
                        --JOIN cc.cuenta u ON u.cod_cuenta = c.cod_cuenta
                        LEFT JOIN cc.multi_factura mf on d.cod_multi_factura = mf.cod_multi_factura 
                        WHERE
                        c.cod_empresa = 1 and d.monto < 0 
                        ORDER BY  c.cod_comprobante
                    ) bb on aa.codigo = bb.cod_comprobante
                                        
                    left join (
                            select distinct bb.cod_comprobante,bb.cod_documento,bb.rut,bb.cod_comprobante::text || (row_number() OVER (PARTITION BY bb.cod_comprobante ORDER BY bb.cod_comprobante))::text as cod_comprobante2
                            from 
                            (select cod_comprobante,cod_documento,rut from 
                            (select cod_comprobante,cod_documento,rut,row_number() OVER (PARTITION BY cod_documento,rut ORDER BY cod_documento,rut) as num                            
                            from ctb.detalle_comprobante 
                            where rut is not null and cod_documento is not null
                            and glosa not ilike '%FC %' and glosa not ilike '%B NETO%' and glosa not ilike '%B IGV%'                            
                            order by 4) aa where aa.num = 1) bb
                            where bb.rut is not null and bb.cod_documento is not null                            
                            group by 1,2,3
                        --select * from ctb.detalle_comprobante where (cod_documento,rut) in 
                        --(select distinct cod_documento,rut
                         --from ctb.detalle_comprobante 
                         --where rut is not null and cod_documento is not null
                         --group by 1,2)
                         
                    ) rr on aa.cod_documento2::bigint = rr.cod_documento and rtrim(ltrim(aa.numero_documento)) = rtrim(ltrim(rr.rut)) 

                    ORDER BY  aa.serie_doc,aa.numero_doc,aa.fecha_emision
                    ) REGISTRO_VENTAS"""
            
        if self.libro == 'LIBRO DE RETENCIONES':
            codigo = '00040100001111'
            select = """ select case when cr.mes < 10 then cr.periodo::text || '0' || cr.mes::text || '00' 
                            when cr.mes > 9 then cr.periodo::text || cr.mes::text || '00' 
                           end 
                           ||'|'||
                           dc.cod_comprobante::text || dc.id::text
                           ||'|'||
                           case when c.cod_tipo_comprobante = 'A' then 'A' || dc.id::text
                            when c.cod_tipo_comprobante = 'C' then 'C' || dc.id::text
                            else 'M' || dc.id::text
                           end
                           ||'|'||
                           to_char(cr.fecha_comp_retencion, 'dd/mm/yyyy')
                           ||'|'||
                           6
                           ||'|'||
                           p.rut_proveedor
                           ||'|'||
                           p.nom_proveedor
                           ||'|'||
                           (select sum(monto_pago)+sum(monto_retenido) from detalle_comp_retencion where cod_comp_retencion = cr.cod_comp_retencion)
                           ||'|'||
                           (select sum(monto_retenido) from detalle_comp_retencion where cod_comp_retencion = cr.cod_comp_retencion)*-1
                           ||'|'||
                           case when to_char(cr.fecha_comp_retencion,'mm')::integer = cr.mes::integer then 1
                            else 666
                           end
                           ||'|'
                    from comp_retencion cr left join proveedor p on (cr.rut_proveedor = p.rut_proveedor)
                    left join ctb.comprobante c on (c.glosa ilike '%OBLIGACION DE RETENCION%'||p.nom_proveedor||'%'||cr.num_comp_retencion)
                    left join ctb.detalle_comprobante dc on (c.cod_comprobante = dc.cod_comprobante)
                    where 
                    cr.anulado = false
                    and dc.glosa ilike '%RETENCION DE OBLIGACIONES%' """
        
        print "aquí"
        sql = "%s %s"%(select,filtro)
        print sql
        self.cursor.execute(sql)
        r = self.cursor.fetchall()
        cont = 1
        max = float(len(r))
        
        print cont
        print max
            
        for i in r:
            strContenido.append(str(i[0])+str('\n'))
            #strContenido.append("\n")
            #print str(i[0])
            #BARRA DE PROGRESO
            self.pbProgreso.set_fraction(cont / float(max))
            self.pbProgreso.set_text(str(round(((cont / float(max))*100),2)).replace('.0','')+"% COMPLETADO.")
            while gtk.events_pending():
                  gtk.mainiteration(gtk.FALSE)
            cont = cont +1
                        
        strFile = "%s%s%s.txt" % ('LE20521119943', str(self.anioHasta)+str(self.entPeriodoHasta),codigo)
        f=open(strFile, 'w')        
        f.writelines(strContenido)
        f.close()
        abreTxt(strFile)
        
    def on_btnTexto_clicked(self, btn=None):
        active = self.cmbLibro.get_active()
        if active == -1:
            dialogos.error("Debe escoger un Libro Electrónico!!!")
            return
        Filtro_comprobante=''
        filtro=''
        filtro="where cod_empresa=%s and fecha between '%s' and '%s'"%(self.cod_empresa,str(self.entDesde.get_date()),str(self.entHasta.get_date()))

        if self.libro=='LIBRO DIARIO' or self.libro=='LIBRO MAYOR':
            if self.chkCuenta.get_active():
                if (self.num_cuenta_desde) in('',None) or self.num_cuenta1_hasta in('',None):
                    dialogos.error("Debe completar la(s) cuenta contable")
                    return

                else:
                    if self.num_cuenta_desde == self.num_cuenta1_hasta:
                        filtro+=" and num_cuenta like '%s'"%(self.num_cuenta_desde)
                    else:
                        filtro+=" and num_cuenta >= '%s' and num_cuenta<='%s'"%(self.num_cuenta_desde,self.num_cuenta1_hasta)
##            else:
##                if not self.chkCuenta.get_active():
##                    dialogos.error("Debe seleccionar la(s) cuenta contable")
##                    return
            Filtro_comprobante    =self.filtro_tipo_comprobante()
            if Filtro_comprobante==' and tipo in ()':
                dialogos.error("Debe seleccionar un <b>Tipo de Comprobante</b>")
                return
            if self.libro == 'LIBRO MAYOR':
                self.estado='V'
                if Filtro_comprobante.find('A') != -1:
                    #Filtro_comprobante=Filtro_comprobante.replace("'A',",'')
                    Filtro_comprobante=Filtro_comprobante
            filtro+=Filtro_comprobante
            if self.libro=='LIBRO DIARIO':
                if self.optVigente.get_active():
                    filtro+=" and estado='VIGENTE'"
                    self.estado = 'V'
                else:
                    filtro+=" and estado='PARCIAL'"
                    self.estado = 'P'
            if self.chkFolio.get_active():
                filtro+=" and folio between %s and %s"  %(str(int(self.spnFolioDesde.get_value())), str(int(self.spnFolioHasta.get_value())))

            if self.libro == 'LIBRO DIARIO':
                cuenta = 'ctb.vw_libro_diario_nuevo'
            elif self.libro == 'LIBRO MAYOR':
                cuenta= 'ctb.vw_libro_mayor_nuevo'
                filtro = filtro.replace("VIGENTE","V").replace("NULO","")
    ##    cuenta
            sql = "select * from %s %s"%(cuenta, filtro)
            self.cursor.execute(sql)
            print(sql)
            r=self.cursor.fetchall()
            if len(r)==0:
                return
            if self.libro=='LIBRO DIARIO':
                self.imprime_libro_diario_2(r)

                sql_consulta="""select
                                    replace(inserttime::text,'-','')::text
                                    ,num_cuenta
                                    ,descripcion_cuenta
                                from
                                    ctb.cuenta_contable
                                where
                                    cod_empresa=%s
                                    and nivel_cuenta=7
                                    and inserttime between '%s' and '%s'"""%(self.cod_empresa,str(self.entDesde.get_date()),str(self.entHasta.get_date()))
                print sql_consulta
                self.cursor.execute(sql_consulta)
                records=self.cursor.fetchall()
                self.imprime_libro_diario_detalle(records)
            else:
                self.exportar_libro_mayor_2(r)

    def imprime_libro_diario_2(self,r):

        #strContenido = ['*']
        strContenido = []
        total = 0

        pagina = 0      # contador de páginas
        comp = -1       #comprobante inicial de la página
        ncomp = 0       # número de comprobante para la impresión de totales
        ucomp = 0
        total_debe = total_haber = 0L
        tdebe = thaber = 0L     # total general

        pagina = 1

        #strContenido.append(("%s|%s|%s|%s|%s|%s|%s|%s|%s" %
        #                             (
        #                               str('DPERIODO'),
        #                               str('DNUMSIOPE'),
        #                               str('DCODCUE'),
        #                               str('DNUMCTACON'),
        #                               str('DFECOPE'),
        #                               str('DGLOSA'),
        #                               str('DDEBE'),
        #                               str('DHABER'),
        #                               str('DESTOPE')
        #                             )
        #                             ).replace('\n',''))
        #strContenido.append("\n")
        #

        max = float(len(r))
        cont = 1

        for i in r:
            if(str(i[14]) <> str(i[15])):
                i = map(str, i)     #todo el registro es mapeado como string

                ##$ PLE ANTERIOR
                #strContenido.append(("%8s|%40s|%2s|%24s|%10s|%100s|%s|%s|%s|" %
                #                         (
                #                            str(CDateLocal(i[0][0:10])[6:10] + CDateLocal(i[0][0:10])[3:5]) + '00',
                #
                #
                #
                #                            str(str(iif(i[1] not in ('', None,' ','None'), str(i[1]), '')) + str(iif(i[2] not in ('', None,' ','None'), str(i[2]), '')) + str(iif(i[16] not in ('', None,' ','None'), str(i[16]), ''))  + str(iif(i[20] not in ('', None,' ','None'), str(i[20]), '')) + str(iif(i[10] not in ('', None,' ','None'), str(i[10]), ''))).encode('ascii', 'replace'),
                #                            "01",
                #                            str(i[11]).encode('ascii', 'replace').replace('.',''),
                #                            str(CDateLocal(i[0][0:10])),
                #                            str(i[13]).encode('ascii', 'replace'),
                #                            str((i[14]).replace(',','')),
                #                            str((i[15]).replace(',','')),
                #                            str('1').encode('ascii', 'replace')
                #                         )
                #                         ).replace('\n',''))
                #strContenido.append("\n")

                strContenido.append(("%8s|%40s|%10s|%s|%24s|%s|%s|%s|%s||||%s|" %
                                         (
                                            str(CDateLocal(i[0][0:10])[6:10] + CDateLocal(i[0][0:10])[3:5]) + '00',
                                            str(i[22]).encode('ascii', 'replace'),
                                            str(str(iif(i[2]=='A',str('A'),str('M')))+str(i[22])).encode('ascii', 'replace'),
                                            str('01').encode('ascii', 'replace'),

                                            str(i[11]).encode('ascii', 'replace').replace('.',''),

                                            str(CDateLocal(i[0][0:10])).zfill(10),
                                            str(i[13]).replace('º','').replace('?','N').encode('utf-8').replace('°',''),
                                            str((i[14]).replace(',','')),
                                            str((i[15]).replace(',','')),
                                            str('1').encode('ascii', 'replace')
                                         )
                                         ).replace('\n',''))
                strContenido.append("\n")

            #BARRA DE PROGRESO
            self.pbProgreso.set_fraction(cont / float(max))
            self.pbProgreso.set_text(str(round(((cont / float(max))*100),2)).replace('.0','')+"% COMPLETADO.")
            while gtk.events_pending():
                gtk.mainiteration(gtk.FALSE)
            cont = cont +1

        AA = str(time.localtime().tm_year)[2:4]
        #strFile = "%s%s.txt" % ('CONSTRUCTORA_GALILEA_SAC', AA)
        anio_mes = str(self.entHasta.get_date())[0:4] + str(self.entHasta.get_date())[5:7]
        strFile = "%s%s00050100001111.txt" % ('LE20521119943', anio_mes)
        f=open(strFile, 'w')
        #strContenido[0] = '\n'
        f.writelines(strContenido)
        f.close()
        abreTxt(strFile)


    def imprime_libro_diario_detalle(self,r):

        print "detalle"

        strContenido = []
        total = 0

        pagina = 0      # contador de páginas
        comp = -1       #comprobante inicial de la página
        ncomp = 0       # número de comprobante para la impresión de totales
        ucomp = 0
        total_debe = total_haber = 0L
        tdebe = thaber = 0L     # total general

        pagina = 1

        max = float(len(r))
        cont = 1

        for i in r:
            #if(str(i[14]) <> str(i[15])):
            i = map(str, i)     #todo el registro es mapeado como string
            strContenido.append(("%8s|%24s|%100s|%s|%60s|%s|" %
                                     (
                                        str(i[0]).encode('ascii', 'replace'),
                                        str(i[1]).encode('ascii', 'replace').replace('.',''),
                                        str(i[2]).encode('ascii', 'replace'),
                                        str('01').encode('ascii', 'replace'),
                                        str('-').encode('ascii', 'replace'),
                                        str('1').encode('ascii', 'replace')
                                     )
                                     ).replace('\n',''))
            strContenido.append("\n")

            #BARRA DE PROGRESO
            self.pbProgreso.set_fraction(cont / float(max))
            self.pbProgreso.set_text(str(round(((cont / float(max))*100),2)).replace('.0','')+"% COMPLETADO.")
            while gtk.events_pending():
                gtk.mainiteration(gtk.FALSE)
            cont = cont +1

        AA = str(time.localtime().tm_year)[2:4]
        #strFile = "%s%s.txt" % ('CONSTRUCTORA_GALILEA_SAC', AA)
        anio_mes = str(self.entHasta.get_date())[0:4] + str(self.entHasta.get_date())[5:7]
        strFile = "%s%s00050300001111.txt" % ('LE20521119943', anio_mes)
        f=open(strFile, 'w')
        #strContenido[0] = '\n'
        f.writelines(strContenido)
        f.close()
        abreTxt(strFile)

    def exportar_libro_mayor_2(self,r):

        data = []
        i = 1

        strContenido = []
        total = 0


        cta = -1        #cuenta inicial de la página
        ncomp =0
        ncta = 0        # número de cuenta para la impresión de totales
        ucta = 0
        total_debe = 0L      # total general de debe
        total_haber = 0L     # y haber
        tdebe=0L
        thaber=0L
        sDebe = sHaber =0L
        cuenta = ""
        cont = 0

        e = 1
        #
        #strContenido.append(("%s|%s|%s|%s|%s|%s|%s|%s|%s" %
        #                             (
        #                               str('MPERIODO'),
        #                               str('MNUMSIOPE'),
        #                               str('MNUMCTACON'),
        #                               str('MFECOPE'),
        #                               str('MGLOSA'),
        #                               str('MDEBE'),
        #                               str('MHABER'),
        #                               str('MESTOPE'),
        #                               str('MCENCOS')
        #                             )
        #                             ).replace('\n',''))
        #strContenido.append("\n")

        max = float(len(r))
        cont = 1

        for i in r:
            if(str(i[6]) <> str(i[7])):

                i = map(str, i)
                glosa="select substring(coalesce(glosa,' '),0,100) from ctb.detalle_comprobante where cod_comprobante=%s and num_linea=%s"%(i[15],i[17])
                self.cursor.execute(glosa)
                glosas=self.cursor.fetchall()
                gl=''
                if cta == i[0]:
                    if glosas[0][0] not in (None,''):
                        gl=glosas[0][0]
                    else:
                        gl=i[5]

                    e = e +1

                    ###PLE ANTERIOR
                    #strContenido.append(("%8s|%40s|%24s|%10s|%100s|%s|%s|%s|" %
                    #                     (
                    #                        str(CDateLocal(i[2][0:10])[6:10] + CDateLocal(i[2][0:10])[3:5]) + '00',
                    #                        str(i[4]+i[3]+ str(iif(i[11] not in ('', None,' ','None'), str(i[11]), '')) + str(iif(i[15] not in ('', None,' ','None'), str(i[15]), ''))+ str(iif(i[17] not in ('', None,' ','None'), str(i[17]), ''))).encode('ascii', 'replace'),
                    #                        str(i[0]).encode('ascii', 'replace').replace('.',''),
                    #                        str(CDateLocal(i[2][0:10])).zfill(10),
                    #                        str(gl).encode('ascii', 'replace'),
                    #                        str((i[6]).replace(',','')),
                    #                        str((i[7]).replace(',','')),
                    #                        str('1').encode('ascii', 'replace')
                    #                     )
                    #                     ).replace('\n',''))
                    #strContenido.append("\n")

                    strContenido.append(("%8s|%40s|%10s|%s|%24s|%s|%s|%s|%s||||%s|" %
                                         (
                                            str(CDateLocal(i[2][0:10])[6:10] + CDateLocal(i[2][0:10])[3:5]) + '00',
                                            str(i[18]).encode('ascii', 'replace'),
                                            str(str(iif(i[3]=='A',str('A'),str('M')))+str(i[18])).encode('ascii', 'replace'),
                                            str('01').encode('ascii', 'replace'),
                                            str(i[0]).encode('ascii', 'replace').replace('.',''),

                                            str(CDateLocal(i[2][0:10])).zfill(10),
                                            str(i[5]).encode('utf-8', 'replace'),
                                            str((i[6]).replace(',','')),
                                            str((i[7]).replace(',','')),
                                            str('1').encode('ascii', 'replace')
                                         )
                                         ).replace('\n',''))
                    strContenido.append("\n")

                    cta = i[0]
                    CDateLocal
                else:
                    if ncomp > 0:
                        sal = long(tdebe)
                        sal = sal - long(thaber)
                        sal = sal + long(sDebe)
                        sal = sal - long(sHaber)

                        tdebe=0
                        thaber=0

                    sDebe = 0
                    sHaber = 0
                    sql = "Select sum(monto_debe)::numeric, sum(monto_haber)::numeric from ctb.vw_mayor c \
                     where  date_part('year',c.fecha) = date_part('year','"+ str(self.entDesde.get_date()) +"'::date) and (c.fecha < '"+str(self.entDesde.get_date()) +"' or c.cod_tipo_comprobante ='A')  \
                    and c.cod_empresa = " + str(self.cod_empresa) + " and c.num_cuenta ='" + i[0] + "' and estado = '"+ self.estado +"';"
                    self.cursor.execute(sql)
                    saldo = self.cursor.fetchall()

                    if len(saldo) > 0:
                        d = iif(saldo[0][0] == None,0,saldo[0][0])
                        h = iif(saldo[0][1]==None,0,saldo[0][1])

                    total_debe = total_debe + sDebe
                    total_haber = total_haber + sHaber


                    e = e+1
                    ###PLE ANTERIOR
                    #strContenido.append(("%8s|%40s|%24s|%10s|%100s|%s|%s|%s|" %
                    #                     (
                    #                        str(CDateLocal(i[2][0:10])[6:10] + CDateLocal(i[2][0:10])[3:5]) + '00',
                    #                        str(i[4]+i[3]+ str(iif(i[11] not in ('', None,' ','None'), str(i[11]), '')) + str(iif(i[15] not in ('', None,' ','None'), str(i[15]), ''))+ str(iif(i[17] not in ('', None,' ','None'), str(i[17]), ''))).encode('ascii', 'replace'),
                    #                        str(i[0]).encode('ascii', 'replace').replace('.',''),
                    #                        str(CDateLocal(i[2][0:10])).zfill(10),
                    #                        str(gl).encode('ascii', 'replace'),
                    #                        str((i[6]).replace(',','')),
                    #                        str((i[7]).replace(',','')),
                    #                        str('1').encode('ascii', 'replace')
                    #                     )
                    #                     ).replace('\n',''))
                    #strContenido.append("\n")

                    strContenido.append(("%8s|%40s|%10s|%s|%24s|%s|%s|%s|%s||||%s|" %
                                         (
                                            str(CDateLocal(i[2][0:10])[6:10] + CDateLocal(i[2][0:10])[3:5]) + '00',
                                            str(i[18]).encode('ascii', 'replace'),
                                            str(str(iif(i[3]=='A',str('A'),str('M')))+str(i[18])).encode('ascii', 'replace'),
                                            str('01').encode('ascii', 'replace'),
                                            str(i[0]).encode('ascii', 'replace').replace('.',''),

                                            str(CDateLocal(i[2][0:10])).zfill(10),
                                            str(i[5]).encode('utf-8', 'replace'),
                                            str((i[6]).replace(',','')),
                                            str((i[7]).replace(',','')),
                                            str('1').encode('ascii', 'replace')
                                         )
                                         ).replace('\n',''))
                    strContenido.append("\n")

                    cta = comp = i[0]
                    ncomp = ncomp + 1

                tdebe = tdebe + float(i[6])
                thaber = thaber+ float(i[7])
                total_debe = total_debe + float(i[6])
                total_haber = total_haber + float(i[7])
                ucomp = i[1]

            #BARRA DE PROGRESO
            self.pbProgreso.set_fraction(cont / float(max))
            self.pbProgreso.set_text(str(round(((cont / float(max))*100),2)).replace('.0','')+"% COMPLETADO.")
            while gtk.events_pending():
                gtk.mainiteration(gtk.FALSE)
            cont = cont +1

        AA = str(time.localtime().tm_year)[2:4]
        #strFile = "%s%s.txt" % ('CONSTRUCTORA_GALILEA_SAC', AA)
        anio_mes = str(self.entHasta.get_date())[0:4] + str(self.entHasta.get_date())[5:7]
        strFile = "%s%s00060100001111.txt" % ('LE20521119943', anio_mes)
        f=open(strFile, 'w')
        #strContenido[0] = '\n'
        f.writelines(strContenido)
        f.close()
        abreTxt(strFile)

    def sel_cuenta_contable1(self, completion, model, iter):
        self.num_cuenta1_hasta = model.get_value(iter, 1)
        self.txtCuentaHasta.set_text(model.get_value(iter,0))
        self.txtNumCuentaHasta.set_text(self.num_cuenta1_hasta)

    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Libros_Electronicos")

    def on_txtNumCuentaDesde_activate(self,widget,*args):
        self.num_cuenta_desde=None
        if widget.get_text().replace(".","") not in("",None) and widget.get_text().replace(".","").isdigit():
            cuenta=self.mascara_niveles(self.txtNumCuentaDesde)
            self.pecCuentaContableDesde.set_cod(cuenta)
            if self.num_cuenta_desde!=None:
                widget.set_text(cuenta)
                self.txtNumCuentaHasta.grab_focus()
            else:
                self.txtCuentaDesde.set_text('')
        else:
            self.txtCuentaDesde.set_text('')

    def on_txtNumCuentaHasta_activate(self,widget,*args):
        self.num_cuenta1_hasta=None
        if widget.get_text().replace(".","") not in("",None) and widget.get_text().replace(".","").isdigit():
            cuenta=self.mascara_niveles(self.txtNumCuentaHasta)
            self.pecCuentaContableHasta.set_cod(cuenta)
            if self.num_cuenta1_hasta!=None:
                widget.set_text(cuenta)
            else:
                self.txtCuentaHasta.set_text('')
        else:
            self.txtCuentaHasta.set_text('')

    def on_chkCuenta_clicked(self, chk=None):
        if chk.get_active():
            self.txtCuentaDesde.set_sensitive(True)
            self.txtCuentaHasta.set_sensitive(True)
            self.txtNumCuentaDesde.set_sensitive(True)
            self.txtNumCuentaHasta.set_sensitive(True)
        else:
            self.txtCuentaDesde.set_sensitive(False)
            self.txtCuentaHasta.set_sensitive(False)
            self.txtNumCuentaDesde.set_sensitive(False)
            self.txtNumCuentaHasta.set_sensitive(False)

    def on_chkFolio_clicked(self, chk=None):
        if chk.get_active():
            self.spnFolioDesde.set_sensitive(True)
            self.spnFolioHasta.set_sensitive(True)
        else:
            self.spnFolioDesde.set_sensitive(False)
            self.spnFolioHasta.set_sensitive(False)

    def on_btnImprimir_clicked(self, widget, *args):
        active = self.cmbLibro.get_active()
        if active == -1:
            dialogos.error("Debe escoger un Libro Contable!!!")
            return
        if self.libro=='FECU':
            self.genera_fecu()
            return
        if self.libro == 'BALANCE' and self.Nivel!=None:
            if float(self.Nivel)==0:
                dialogos.error("El nivel debe ser mayor que 0")
                return
##            if not self.pecCuentaContableDesde.get_selected() or  not self.pecCuentaContableHasta.get_selected():
##                dialogos.error("Debe seleccionar las cuentas,para generar el Balance")
##                return
            self.lista_balance()
            return
        Filtro_comprobante=''
        filtro=''

        if self.chkOnlyAsientos.get_active():
            filtro="where cod_empresa=%s and periodo='%s'"%(self.cod_empresa,str(self.entHasta.get_date())[0:4])
        else:
            filtro="where cod_empresa=%s and fecha between '%s' and '%s'"%(self.cod_empresa,str(self.entDesde.get_date()),str(self.entHasta.get_date()))

        if self.libro=='LIBRO DIARIO' or self.libro=='LIBRO MAYOR' or self.libro=='LIBRO CAJA Y BANCOS':
            if self.chkCuenta.get_active():
                if (self.num_cuenta_desde) in('',None) or self.num_cuenta1_hasta in('',None):
                    dialogos.error("Debe completar la(s) cuenta contable")
                    return

                else:
                    if self.num_cuenta_desde == self.num_cuenta1_hasta:
                        filtro+=" and num_cuenta like '%s'"%(self.num_cuenta_desde)
                    else:
                        filtro+=" and num_cuenta >= '%s' and num_cuenta<='%s'"%(self.num_cuenta_desde,self.num_cuenta1_hasta)
            Filtro_comprobante    =self.filtro_tipo_comprobante()
            if Filtro_comprobante==' and tipo in ()':
                dialogos.error("Debe seleccionar un <b>Tipo de Comprobante</b>")
                return
            if self.libro == 'LIBRO MAYOR':
                self.estado='V'
                if Filtro_comprobante.find('A') != -1:
                    #Filtro_comprobante=Filtro_comprobante.replace("'A',",'')
                    Filtro_comprobante=Filtro_comprobante

            if self.libro == 'LIBRO CAJA Y BANCOS':
                self.estado='V'
                if Filtro_comprobante.find('A') != -1:
                    Filtro_comprobante=Filtro_comprobante.replace("'A',",'')

            filtro+=Filtro_comprobante
            if self.libro=='LIBRO DIARIO':
                if self.optVigente.get_active():
                    filtro+=" and estado='VIGENTE'"
                    self.estado = 'V'
                else:
                    filtro+=" and estado='PARCIAL'"
                    self.estado = 'P'
            if self.chkFolio.get_active():
                filtro+=" and folio between %s and %s"  %(str(int(self.spnFolioDesde.get_value())), str(int(self.spnFolioHasta.get_value())))

        if self.libro == 'LIBRO DIARIO':
            cuenta = 'ctb.vw_libro_diario_nuevo'
        elif self.libro == 'LIBRO MAYOR':
            cuenta='ctb.vw_libro_mayor_nuevo'
            filtro = filtro.replace("VIGENTE","V").replace("NULO","")
        elif self.libro == 'LIBRO CAJA Y BANCOS':
            cuenta = 'ctb.vw_libro_caja_bancos'

##    cuenta
        sql = "select * from %s %s "%(cuenta, filtro)
        print sql
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            return
        if self.libro=='LIBRO DIARIO':
            self.imprime_libro_diario(r)
        elif self.libro=='LIBRO MAYOR':
            self.imprime_libro_mayor(r)
        elif self.libro=='LIBRO CAJA Y BANCOS':
            self.imprime_libro_caja_bancos(r)

    def on_btnExportar_clicked(self, widget, *args):
        active = self.cmbLibro.get_active()
        if active == -1:
            dialogos.error("Debe escoger un Libro Contable.\nPor favor verifique e intente nuevamente.")
            return
        if self.libro=='FECU':
            self.genera_fecu()
            return
        if self.libro == 'BALANCE' and self.Nivel!=None:
            if float(self.Nivel)==0:
                dialogos.error("El nivel debe ser mayor que 0")
                return
##            if not self.pecCuentaContableDesde.get_selected() or  not self.pecCuentaContableHasta.get_selected():
##                dialogos.error("Debe seleccionar las cuentas,para generar el Balance")
##                return
            self.exportar_balance()
            return
        Filtro_comprobante=''
        filtro=''
        filtro="where cod_empresa=%s and fecha between '%s' and '%s'"%(self.cod_empresa,str(self.entDesde.get_date()),str(self.entHasta.get_date()))

        if self.libro=='LIBRO DIARIO' or self.libro=='LIBRO MAYOR':
            if self.chkCuenta.get_active():
                if (self.num_cuenta_desde) in('',None) or self.num_cuenta1_hasta in('',None):
                    dialogos.error("Debe completar la(s) cuenta contable.\nPor favor verifique e intente nuevamente.")
                    return

                else:
                    if self.num_cuenta_desde == self.num_cuenta1_hasta:
                        filtro+=" and num_cuenta like '%s'"%(self.num_cuenta_desde)
                    else:
                        filtro+=" and num_cuenta >= '%s' and num_cuenta<='%s'"%(self.num_cuenta_desde,self.num_cuenta1_hasta)
##            else:
##                if not self.chkCuenta.get_active():
##                    dialogos.error("Debe seleccionar la(s) cuenta contable")
##                    return
            Filtro_comprobante    =self.filtro_tipo_comprobante()
            if Filtro_comprobante==' and tipo in ()':
                dialogos.error("Debe seleccionar un <b>Tipo de Comprobante</b>")
                return
            if self.libro == 'LIBRO MAYOR':
                self.estado='V'
                if Filtro_comprobante.find('A') != -1:
                    Filtro_comprobante=Filtro_comprobante.replace("'A',",'')

            filtro+=Filtro_comprobante
            if self.libro=='LIBRO DIARIO':
                if self.optVigente.get_active():
                    filtro+=" and estado='VIGENTE'"
                    self.estado = 'V'
                else:
                    filtro+=" and estado='PARCIAL'"
                    self.estado = 'P'
            if self.chkFolio.get_active():
                filtro+=" and folio between %s and %s"  %(str(int(self.spnFolioDesde.get_value())), str(int(self.spnFolioHasta.get_value())))

        if self.libro == 'LIBRO DIARIO':
            cuenta = 'ctb.vw_libro_diario'
        elif self.libro == 'LIBRO MAYOR':
            cuenta='ctb.vw_libro_mayor'
            filtro = filtro.replace("VIGENTE","V").replace("NULO","")
##    cuenta
        sql = "select * from %s %s "%(cuenta, filtro)
        self.cursor.execute(sql)
        print(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            return
        if self.libro=='LIBRO DIARIO':
            self.imprime_libro_diario(r)
        else:
            self.exportar_libro_mayor(r)

    def genera_fecu(self):
        desde=str(self.kdtDesde.children()[0].get_date())
        hasta=str(self.kdtHasta.children()[1].get_date())
        mes1=desde.split("-")[1]
        mes2=hasta.split("-")[1]
        ano1=desde[:4]
        ano2=hasta[:4]
        if self.pecInformeFecus.get_cod() in (None, 'None'):
            dialogos.error("Debe seleccionar el Informe Fecus")
            return
        if int(ano1)==int(ano2):
            dialogos.error("deben ser  distintos periodos")
            return
        if int(mes1)!=int(mes2):
            dialogos.error("deben ser a igual mes los periodos elegidos")
            return
        b=BalanceFecus(self.cnx,self.cod_empresa,int(desde.split("-")[0]),int(hasta.split("-")[0]),int(mes1), self.pecInformeFecus.get_cod())

    def imprime_libro_diario(self,r):
        c = Canvas('LibroDiario.pdf')
        c.setPageCompression(0)
        salto = 0
        lineas = 83
        linea_inicial = 7
        if self.chkFormatoLegal.get_active():
            salto = 7*9
            lineas = 75
            linea_inicial = 13
        def put_linea(pc,linea,strLinea,pagina):
            if linea > 83:
                t = map(str, localtime())
                #if not self.chkFormatoLegal.get_active(): pc.drawString(30,748 -((linea - 4)*9) ,"Fecha Impresion: " +  zfill(t[2],2) + "/" + zfill(t[1],2) + "/" + t[0])
                if not self.chkFormatoLegal.get_active(): pc.drawString(30,748 -((linea - 4)*9) ,"")
                c.showPage()
                pagina = pagina + 1
                cabecera_libro_diario(self,pc,pagina)
                linea = linea_inicial

            pc.drawString(30,748 -((linea -6)*9) ,strLinea.decode('iso8859-7').encode('utf-8'))
            return linea +1,pagina

        def cabecera_libro_diario(self, pc, page_num):

            pc.setFont('Courier', 12)

            w = pc.stringWidth("Libro Diario",'Courier', 12)

            pc.drawString(275 - w/2,800, "Libro Diario")
            if not self.chkFormatoLegal.get_active():
                pc.drawString(30,782,self.descripcion_empresa)
                pc.drawString(30,770,self.rut_empresa)
                pc.drawString(30,758,self.direccion_empresa)
            pc.drawString(370,758,"Pagina: %s"%(zfill(int(page_num), 10)))

            t = map(str, localtime())
            self.entDesde.get_date()
            #pc.drawString(370,782,"Fecha : " +  zfill(t[2],2) + "/" + zfill(t[1],2) + "/" + t[0])
            pc.drawString(370,782,"Desde : " +  CDateLocal(str(self.entDesde.get_date())))
            pc.drawString(370,770,"Hasta : " +  CDateLocal(str(self.entHasta.get_date())))
            pc.setFont('Courier', 7)
            pc.line(30, 756 - (linea_inicial - 7)*9, 550, 756 - (linea_inicial - 7)*9)

            #l = "Fecha".ljust(12) + "Tipo".center(6) + "Folio".rjust(12) + "Glosa Linea".center(30) + "Debe".rjust(20) + "Haber".rjust(20) + "Saldo".rjust(20)
            l = "#".center(5) + "NUM CUENTA".center(15) + " " +  "NOMBRE CUENTA".center(29) + " " + "GLOSA LINEA".center(29) + "DEBE".rjust(20) + "HABER".rjust(20)
            pc.drawString(30,749 - (linea_inicial - 7)*9,l)
            pc.line(30, 748 - (linea_inicial - 7)*9, 550, 748 - (linea_inicial - 7)*9)
            pc.setFont('Courier', 7)


        pagina = 0      # contador de páginas
        comp = -1       #comprobante inicial de la página
        ncomp = 0       # número de comprobante para la impresión de totales
        ucomp = 0
        total_debe = total_haber = 0L
        tdebe = thaber = 0L     # total general

        pagina = 1

        cabecera_libro_diario(self,c,pagina)

        linea = linea_inicial       # linea actual en la página

        max = float(len(r))
        cont = 1

        for i in r:
            d = i[14]
            h = i[15]

            i = map(str, i)     #todo el registro es mapeado como string

            if comp == i[20]:   #si estamos en el detalle del comprobante

                l = i[10].center(5) + i[11].rjust(15) + " " +  i[12][:28].ljust(29) + " " + i[13][:28].ljust(29) + CMon(i[14],0).rjust(20) + CMon(i[15],0).rjust(20)
                linea,pagina = put_linea(c,linea,CISO(l),pagina)
                tdebe += d
                thaber += h
                total_debe += d
                total_haber += h

            else:   #Si no es detalle o si es el primer comprobante
                if ncomp > 0: #si no es el primer comprobante
                    s = ""
                    lc = "Total Comprobante : [ " + ucomp.rjust(12) + " ]"
                    l = lc.rjust(80) + CMon(tdebe,0).rjust(20) + CMon(thaber,0).rjust(20)

                    for n in range(len(l)):
                        s = s + "_"

                    linea,pagina = put_linea(c,linea,s,pagina)
                    linea,pagina = put_linea(c,linea,l,pagina)
                    linea,pagina = put_linea(c,linea,s,pagina)
                    tdebe = thaber = 0

                l = "Fecha".ljust(12) + "Folio".rjust(12) + "Tipo".center(6) + "Estado".rjust(7) + "Glosa Comprobante".center(59)
                linea,pagina = put_linea(c,linea,l,pagina)
                l= i[0].ljust(12)
                l= str(i[1]).ljust(12)
                l= CDateLocal(i[0][0:10]).ljust(12) + (str(i[1]).rjust(12)) + "  " + i[2].center(6) + "  " + i[3].rjust(7) + "  " + i[4].ljust(60) + "  "
                linea,pagina = put_linea(c,linea,l,pagina)
                l = i[10].center(5) + i[11].rjust(15) + " " +  i[12][:28].ljust(29) + " " + i[13][:28].ljust(29) +CMon(i[14],0).rjust(20) + CMon(i[15],0).rjust(20)
                linea,pagina = put_linea(c,linea,CUTF82(l),pagina)
                tdebe += d
                thaber += h
                total_debe += d
                total_haber += h
            comp = i[20]
            ncomp = ncomp + 1
            ucomp = i[1]

            #BARRA DE PROGRESO
            self.pbProgreso.set_fraction(cont / float(max))
            self.pbProgreso.set_text(str(round(((cont / float(max))*100),2)).replace('.0','')+"% COMPLETADO.")
            while gtk.events_pending():
                gtk.mainiteration(gtk.FALSE)
            cont = cont +1

        #tdebe += d
        #thaber += h
        #total_debe += d
        #total_haber += h
        linea = linea + 1
        comp = i[20]
        ncomp = ncomp + 1
        ucomp = i[1]
        s = ""
        if ucomp:
            linea = linea + 1
            lc = "Total Comprobante : [ " + ucomp.rjust(12) + " ]"
            l = lc.rjust(80) + CMon(tdebe,0).rjust(20) + CMon(thaber,0).rjust(20)
            for n in range(len(l)):
                s = s + "_"
            linea,pagina = put_linea(c,linea,s,pagina)
            linea,pagina = put_linea(c,linea,l,pagina)
            l = "Total General : ".rjust(80) + CMon(total_debe,0).rjust(20) + CMon(total_haber,0).rjust(20)
            linea,pagina = put_linea(c,linea,s,pagina)
            linea,pagina = put_linea(c,linea,l,pagina)
        c.showPage()
        c.save()
        Abre_pdf("LibroDiario.pdf")

    def imprime_libro_mayor(self,r):
        lineas = 83
        linea_inicial = 7
        if self.chkFormatoLegal.get_active():
            salto = 7*9
            lineas = 75
            linea_inicial = 13

        def cabecera_libro_mayor(self, pc, page_num):

            pc.setFont('Courier', 12)

            w = pc.stringWidth("Libro Mayor",'Courier', 12)
            pc.drawString(275 - w/2,800, "Libro Mayor")
            if not self.chkFormatoLegal.get_active():
                pc.drawString(30,782,self.descripcion_empresa)
                pc.drawString(30,770,self.rut_empresa)
                pc.drawString(30,758,self.direccion_empresa)

            if not self.chkFormatoLegal.get_active():
                pc.drawString(370,758,"Pagina: %s"%(zfill(int(page_num), 10)))
                t = map(str, localtime())
                pc.drawString(370,782,"Desde : " +  CDateLocal(str(self.entDesde.get_date())))
                pc.drawString(370,770,"Hasta : " + CDateLocal(str(self.entHasta.get_date())))
            else:
                pc.drawString(370,750,"Pagina: %s"%(zfill(int(page_num), 10)))
                t = map(str, localtime())
                pc.drawString(370,774,"Desde : " +  str(self.entDesde.get_date()))
                pc.drawString(370,762,"Hasta : " + str(self.entHasta.get_date()))
            pc.setFont('Courier', 7)
            pc.line(30, 756- (linea_inicial - 7)*9, 550, 756- (linea_inicial - 7)*9)
            l = "Fecha".ljust(12) + "Tipo".center(6) + "Folio".rjust(12) + "Glosa Linea".center(30) + "Debe".rjust(20) + "Haber".rjust(20) + "Saldo".rjust(20)
            pc.drawString(30,749- (linea_inicial - 7)*9,l)

            pc.line(30, 748- (linea_inicial - 7)*9, 550, 748- (linea_inicial - 7)*9)
            pc.setFont('Courier', 7)

        def put_linea(pc,linea,strLinea,pagina):
            if linea > 83:
                t = map(str, localtime())
                #if not self.chkFormatoLegal.get_active(): pc.drawString(30,748 -((linea - 4)*9) ,"Fecha Impresion: " +  zfill(t[2],2) + "/" + zfill(t[1],2) + "/" + t[0])
                if not self.chkFormatoLegal.get_active(): pc.drawString(30,748 -((linea - 4)*9) ,"")
                c.showPage()
                pagina = pagina + 1
                cabecera_libro_mayor(self,pc,pagina)
                linea = linea_inicial
            try:
                pc.drawString(30,748 -((linea -6)*9) ,strLinea.decode('iso8859-7').encode('utf-8'))
            except:
                #print strLinea
                raise
            return linea +1,pagina

        pagina = 0      # contador de páginas
        cta = -1        #cuenta inicial de la página
        ncomp =0
        ncta = 0        # número de cuenta para la impresión de totales
        ucta = 0
        total_debe = 0L      # total general de debe
        total_haber = 0L     # y haber
        tdebe=0L
        thaber=0L
        sDebe = sHaber =0L

        c = Canvas('LibroMayor.pdf')
        c.setPageCompression(0)
        pagina = 1
        cabecera_libro_mayor(self,c,pagina)
        linea = linea_inicial       # linea actual en la página

        max = float(len(r))
        cont = 1

        print len(r)
        for i in r:
            i = map(str, i)     #todo el registro es mapeado como string
            glosa="select glosa from ctb.detalle_comprobante where cod_comprobante=%s and num_linea=%s"%(i[15],i[17])
            self.cursor.execute(glosa)
            glosas=self.cursor.fetchall()
            gl=''
            if cta == i[0]: #si estamos en el detalle del comprobante
                if glosas[0][0] not in (None,''):
                    gl=glosas[0][0]
                else:
                    gl=i[5]
                l = CDateLocal(i[2][0:10]).ljust(12)  + i[3].center(6) + "" +  i[4].rjust(12) + " " + gl[:30].ljust(30) + CMon(i[6],0).rjust(19) + CMon(i[7],0).rjust(20)
                #l = CUTF82(l)
                linea,pagina = put_linea(c,linea,l,pagina)
                l = unicode(l, 'latin-1')
                l.encode('utf-8')

                cta = i[0]

            else:           # estamos listando un comprobante nuevo

                if ncomp > 0:   # si el comprobante de arrastre existe
                    # debemos imprimir su total
                    s = ""
                    lc = "Total Detalle Cuenta : "
                    l = lc.rjust(60)
                    l = l + CMon(long(tdebe + sDebe),0).rjust(20)
                    l = l  + CMon(long(thaber+sHaber),0).rjust(20)
                    sal = long(tdebe)
                    sal = sal - long(thaber)
                    sal = sal + long(sDebe)
                    sal = sal - long(sHaber)
                    l = l  + CMon(sal,0).rjust(20)

                    print str(tdebe)
                    print str(thaber)
                    print str(l)

                    for n in range(len(l)):
                        s = s + "_"

                    linea,pagina = put_linea(c,linea,s,pagina)

                    linea,pagina = put_linea(c,linea,l,pagina)
                    l = unicode(l, 'latin-1')
                    l.encode('utf-8')

                    linea,pagina = put_linea(c,linea,s,pagina)

                    tdebe=0
                    thaber=0

                linea,pagina = put_linea(c,linea,"",pagina)
                l = " Cuenta: " + i[0].rjust(12) + " " + (str(i[1])).rjust(12) + "  "
##                l = "\n Cuenta: " + i[0].rjust(12) + " " + (str(i[1])).rjust(12) + "  "

                linea,pagina = put_linea(c,linea,l,pagina)

                sDebe = 0
                sHaber = 0
                sql = "Select sum(monto_debe)::numeric, sum(monto_haber)::numeric from ctb.vw_mayor c \
                 where  date_part('year',c.fecha) = date_part('year','"+ str(self.entDesde.get_date()) +"'::date) and (c.fecha < '"+str(self.entDesde.get_date()) +"' or c.cod_tipo_comprobante ='A')  \
                and c.cod_empresa = " + str(self.cod_empresa) + " and c.num_cuenta ='" + i[0] + "' and estado = '"+ self.estado +"';"
                try:
                    #print sql
                    self.cursor.execute(sql)
                    saldo = self.cursor.fetchall()
                except:

                    dialogos.error(None, sys.exc_info()[1])

                if len(saldo) > 0:
                    d = iif(saldo[0][0] == None,0,saldo[0][0])
                    #sDebe = sDebe + d
                    h = iif(saldo[0][1]==None,0,saldo[0][1])
                    #sHaber = sHaber + sh

                total_debe = total_debe + sDebe
                total_haber = total_haber + sHaber
                lc = "Saldo Inicial Cuenta : "
                l = lc.rjust(60)+ CMon(str((d)),0).rjust(20) + CMon(str((h)),0).rjust(20) + CMon(str((d-h)),0).rjust(20)

                linea,pagina = put_linea(c,linea,l,pagina)


##                l =  " ".ljust(12) + " ".center(6) + " ".rjust(12) + "Saldo Inicial Cuenta".center(59) + "Debe".ljust(19) + "Haber".ljust(19) + "Saldo".ljust(19) + "\n"
                l =  " ".ljust(12) + " ".center(6) + " ".rjust(12) + "Saldo Inicial Cuenta".center(59) + "Debe".ljust(19) + "Haber".ljust(19) + "Saldo".ljust(19) + " "

                l = CDateLocal(i[2][0:10]).ljust(12) + i[3].center(6) + "" +  i[4].rjust(12) + " " + i[5][:30].ljust(30) + CMon(i[6],0).rjust(19) + CMon(i[7],0).rjust(20)

                linea,pagina = put_linea(c,linea,l,pagina)
                l = unicode(l, 'latin-1')
                l.encode('utf-8')

                cta = comp = i[0]
                ncomp = ncomp + 1

            #BARRA DE PROGRESO
            self.pbProgreso.set_fraction(cont / float(max))
            self.pbProgreso.set_text(str(round(((cont / float(max))*100),2)).replace('.0','')+"% COMPLETADO.")
            while gtk.events_pending():
                gtk.mainiteration(gtk.FALSE)
            cont = cont +1


            tdebe = tdebe + float(i[6])
            thaber = thaber+ float(i[7])
            total_debe = total_debe + float(i[6])
            total_haber = total_haber + float(i[7])
            ucomp = i[1]

        s = ""
        if ucomp:
            #linea = linea + 1
            lc = "Total Detalle Cuenta : "
            l = lc.rjust(60)  + CMon(str((tdebe + sDebe)),0).rjust(20) + CMon(str((thaber+sHaber)),0).rjust(20) + CMon(str((tdebe-thaber+sDebe-sHaber)),0).rjust(20)
            for n in range(len(l)):
                s = s + "_"
            #print str(tdebe)
            #print str(thaber)
            #print str(l)

            linea,pagina = put_linea(c,linea,s,pagina)

            linea,pagina = put_linea(c,linea,l,pagina)

            #l = "Total General : ".rjust(60) + CMon(str(total_debe + float(d)),0).rjust(20) + CMon(str(total_haber + float(h)),0).rjust(20)  + CMon(str((total_debe + float(d)) - (total_haber + float(h))),0).rjust(20)

            l = "Total General : ".rjust(60) + CMon(str(total_debe),0).rjust(20) + CMon(str(total_haber ),0).rjust(20)  + CMon(str((total_debe ) - (total_haber )),0).rjust(20)


            linea,pagina = put_linea(c,linea,l,pagina)

            #linea,pagina = put_linea(c,linea,l,pagina)
        c.showPage()
        c.save()
        Abre_pdf("LibroMayor.pdf")

    def exportar_libro_mayor(self,r):

        #ESTILOS DE XLWT -NO SE USA EN NINGUN LADO
        #style1 = xlwt.easyxf('',num_format_str='DD-MMM-YY')
        #style2 = xlwt.easyxf('',num_format_str='"$"#,##0.00')
        #style6 = xlwt.easyxf('font: name Times New Roman, colour black;align: wrap on, horiz right;border: left thin, top thin,right thin,bottom thin')


        #wb = xlwt.Workbook()
        #ws = wb.add_sheet('Libro_Mayor',cell_overwrite_ok=True)

        wb = xlsxwriter.Workbook('Libro_Mayor.xlsx')
        ws = wb.add_worksheet('Libro_Mayor')
        #ESTILOS EN USO CON XLWT
        #style0 = xlwt.easyxf('font: name Times New Roman, colour black, bold on; align: wrap on, vert centre, horiz center;border: left thin, top thin,right thin,bottom thin')
        #style4 = xlwt.easyxf('font: name Times New Roman, colour black;border: left thin, top thin,right thin,bottom thin')

        #ESTILOS EN USO CON XLSXWRITER
        #TITULOS
        style0 = wb.add_format()
        style0.set_font_color('black')
        style0.set_bold()
        style0.set_align('center')

        style0.set_left()
        style0.set_top()
        style0.set_right()
        style0.set_bottom()

        #CONTENIDO
        style4 = wb.add_format()
        style4.set_font_color('black')
        style4.set_left()
        style4.set_top()
        style4.set_right()
        style4.set_bottom()

        data = []
        i = 1

        ws.write(1, 0, "Fecha".encode('ascii', 'replace'),style0)
        ws.write(1, 1, "Tipo".encode('ascii', 'replace'),style0)
        ws.write(1, 2, "Folio".encode('ascii', 'replace'),style0)
        ws.write(1, 3, "Glosa Linea".encode('ascii', 'replace'),style0)
        ws.write(1, 4, "Debe".encode('ascii', 'replace'),style0)
        ws.write(1, 5, "Haber".encode('ascii', 'replace'),style0)
        ws.write(1, 6, "Saldo".encode('ascii', 'replace'),style0)

        #ws.write_merge(0,0,0,8, label='Libro Mayor', style=style0)
        ws.merge_range(0,0,0,6, 'Libro Mayor', style0)

        #ws.col(0).width = 4000
        #ws.col(1).width = 3000
        #ws.col(2).width = 3000
        #ws.col(3).width = 15000
        #ws.col(4).width = 4000
        #ws.col(5).width = 4000
        #ws.col(6).width = 4000

        ws.set_column(0,0,10)
        ws.set_column(1,1,15)
        ws.set_column(2,2,10)
        ws.set_column(3,3,65)
        ws.set_column(4,4,10)
        ws.set_column(5,5,10)
        ws.set_column(6,6,10)

        cta = -1        #cuenta inicial de la página
        ncomp =0
        ncta = 0        # número de cuenta para la impresión de totales
        ucta = 0
        total_debe = 0L      # total general de debe
        total_haber = 0L     # y haber
        tdebe=0L
        thaber=0L
        sDebe = sHaber =0L
        cuenta = ""
        cont = 0

        e = 1

        max = float(len(r))
        cont = 1

        for i in r:
            #print i
            i = map(str, i)
            glosa="select glosa from ctb.detalle_comprobante where cod_comprobante=%s and num_linea=%s"%(i[15],i[17])
            self.cursor.execute(glosa)
            glosas=self.cursor.fetchall()
            gl=''
            if cta == i[0]:
                if glosas[0][0] not in (None,''):
                    gl=str(glosas[0][0])
                else:
                    gl=str(i[5])

                e = e +1
                ws.write(e, 0, CDateLocal(i[2][0:10]),style4)
                ws.write(e, 1, str(i[3]).encode('ascii', 'replace'),style4)
                ws.write(e, 2, str(i[4]).encode('ascii', 'replace'),style4)
                #ws.write(e, 3, str(gl[:30]).encode('ascii', 'replace'),style4)
                ws.write(e, 3, str(gl).encode('ascii','replace'),style4)
                ws.write(e, 4, float(CMon(i[6],0).replace(',','')),style4)
                ws.write(e, 5, float(CMon(i[7],0).replace(',','')),style4)
                ws.write(e, 6, float(0),style4)

                cta = i[0]

            else:
                if ncomp > 0:
                    sal = long(tdebe)
                    sal = sal - long(thaber)
                    sal = sal + long(sDebe)
                    sal = sal - long(sHaber)

                    e = e +1
                    ws.write(e, 0, " Total Detalle Cuenta ",style4)
                    ws.write(e, 1, "",style4)
                    ws.write(e, 2, "",style4)
                    ws.write(e, 3, "",style4)
                    ws.write(e, 4, float(CMon(long(tdebe + sDebe),0).replace(',','')),style4)
                    ws.write(e, 5, float(CMon(long(thaber+sHaber),0).replace(',','')),style4)
                    ws.write(e, 6, float(CMon(sal,0).replace(',','')),style4)

                    tdebe=0
                    thaber=0

                e=e+1
                ws.write(e, 0, " Cuenta: ",style4)
                ws.write(e, 1, str(i[0]).encode('ascii', 'replace'),style4)
                ws.write(e, 2, str(i[1]).encode('ascii', 'replace'),style4)
                ws.write(e, 3, "",style4)
                ws.write(e, 4, "",style4)
                ws.write(e, 5, "",style4)
                ws.write(e, 6, "",style4)

                sDebe = 0
                sHaber = 0
                sql = "Select sum(monto_debe)::numeric, sum(monto_haber)::numeric from ctb.vw_mayor c \
                 where  date_part('year',c.fecha) = date_part('year','"+ str(self.entDesde.get_date()) +"'::date) and (c.fecha < '"+str(self.entDesde.get_date()) +"' or c.cod_tipo_comprobante ='A')  \
                and c.cod_empresa = " + str(self.cod_empresa) + " and c.num_cuenta ='" + i[0] + "' and estado = '"+ self.estado +"';"
                print sql
                self.cursor.execute(sql)
                saldo = self.cursor.fetchall()


                if len(saldo) > 0:
                    #print "hay datos"
                    d = iif(saldo[0][0] == None,0,saldo[0][0])
                    #print "debe" + str(d)
                    h = iif(saldo[0][1]==None,0,saldo[0][1])
                    #print "haber" + str(h)

                total_debe = total_debe + sDebe
                total_haber = total_haber + sHaber

                e=e+1
                ws.write(e, 0, "",style4)
                ws.write(e, 1, "",style4)
                ws.write(e, 2, "",style4)
                ws.write(e, 3, "Saldo Inicial Cuenta".encode('ascii', 'replace'),style4)
                ws.write(e, 4, float(CMon(str((d)),0).replace(',','')),style4)
                ws.write(e, 5, float(CMon(str((h)),0).replace(',','')),style4)
                ws.write(e, 6, float(CMon(str((d-h)),0).replace(',','')),style4)

                e = e+1
                ws.write(e, 0, CDateLocal(i[2][0:10]),style4)
                ws.write(e, 1, str(i[3]).encode('ascii', 'replace'),style4)
                ws.write(e, 2, str(i[4]).encode('ascii', 'replace'),style4)
                ws.write(e, 3, str(i[5]).encode('ascii','replace'),style4)
                ws.write(e, 4, float(CMon(i[6],0).replace(',','')),style4)
                ws.write(e, 5, float(CMon(i[7],0).replace(',','')),style4)
                ws.write(e, 6, float(0),style4)

                cta = comp = i[0]
                ncomp = ncomp + 1

            tdebe = tdebe + float(i[6])
            thaber = thaber+ float(i[7])
            total_debe = total_debe + float(i[6])
            total_haber = total_haber + float(i[7])
            ucomp = i[1]

            #BARRA DE PROGRESO
            self.pbProgreso.set_fraction(cont / float(max))
            self.pbProgreso.set_text(str(round(((cont / float(max))*100),2)).replace('.0','')+"% COMPLETADO.")
            while gtk.events_pending():
                gtk.mainiteration(gtk.FALSE)
            cont = cont +1

        #print "valor D "+ str(d)
        #print "valor H "+ str(h)

        if ucomp:
            e=e+1
            ws.write(e, 0, "",style4)
            ws.write(e, 1, "",style4)
            ws.write(e, 2, "",style4)
            ws.write(e, 3, "Total Detalle Cuenta".encode('ascii', 'replace'),style4)
            ws.write(e, 4, float(CMon(str((tdebe + sDebe)),0).replace(',','')),style4)
            ws.write(e, 5, float(CMon(str((thaber+sHaber)),0).replace(',','')),style4)
            ws.write(e, 6, float(CMon(str((tdebe-thaber+sDebe-sHaber)),0).replace(',','')),style4)

            e=e+1
            ws.write(e, 0, "",style4)
            ws.write(e, 1, "",style4)
            ws.write(e, 2, "",style4)
            ws.write(e, 3, "Total General".encode('ascii', 'replace'),style4)
            #ws.write(e, 4, float(CMon(str(total_debe + float(d)),0).replace(',','')),style4)
            ws.write(e, 4, float(CMon(str(total_debe),0).replace(',','')),style4)
            #ws.write(e, 5, float(CMon(str(total_haber + float(h)),0).replace(',','')),style4)
            ws.write(e, 5, float(CMon(str(total_haber),0).replace(',','')),style4)
            ws.write(e, 6, float(CMon(str((total_debe) - (total_haber)),0).replace(',','')),style4)


        #wb.save('Libro_Mayor.xlsx')
        wb.close()
        print "workbook"
        Abre_excel('Libro_Mayor.xlsx');

    def imprime_libro_caja_bancos(self,r):
        lineas = 83
        linea_inicial = 7
        if self.chkFormatoLegal.get_active():
            salto = 7*9
            lineas = 75
            linea_inicial = 13

        def cabecera_libro_caja_bancos(self, pc, page_num):

            pc.setFont('Courier', 12)

            w = pc.stringWidth("Libro Caja y Bancos",'Courier', 12)
            pc.drawString(275 - w/2,800, "Libro Caja y Bancos")
            if not self.chkFormatoLegal.get_active():
                pc.drawString(30,782,self.descripcion_empresa)
                pc.drawString(30,770,self.rut_empresa)
                pc.drawString(30,758,self.direccion_empresa)

            if not self.chkFormatoLegal.get_active():
                pc.drawString(370,758,"Pagina: %s"%(zfill(int(page_num), 10)))
                t = map(str, localtime())
                pc.drawString(370,782,"Desde : " +  CDateLocal(str(self.entDesde.get_date())))
                pc.drawString(370,770,"Hasta : " + CDateLocal(str(self.entHasta.get_date())))
            else:
                pc.drawString(370,750,"Pagina: %s"%(zfill(int(page_num), 10)))
                t = map(str, localtime())
                pc.drawString(370,774,"Desde : " +  str(self.entDesde.get_date()))
                pc.drawString(370,762,"Hasta : " + str(self.entHasta.get_date()))
            pc.setFont('Courier', 7)
            pc.line(30, 756- (linea_inicial - 7)*9, 550, 756- (linea_inicial - 7)*9)
            l = "Fecha".ljust(12) + "Tipo".center(6) + "Folio".rjust(12) + "Glosa Linea".center(30) + "Debe".rjust(20) + "Haber".rjust(20) + "Saldo".rjust(20)
            pc.drawString(30,749- (linea_inicial - 7)*9,l)

            pc.line(30, 748- (linea_inicial - 7)*9, 550, 748- (linea_inicial - 7)*9)
            pc.setFont('Courier', 7)

        def put_linea(pc,linea,strLinea,pagina):
            if linea > 83:
                t = map(str, localtime())
                #if not self.chkFormatoLegal.get_active(): pc.drawString(30,748 -((linea - 4)*9) ,"Fecha Impresion: " +  zfill(t[2],2) + "/" + zfill(t[1],2) + "/" + t[0])
                if not self.chkFormatoLegal.get_active(): pc.drawString(30,748 -((linea - 4)*9) ,"")
                c.showPage()
                pagina = pagina + 1
                cabecera_libro_caja_bancos(self,pc,pagina)
                linea = linea_inicial
            try:
                pc.drawString(30,748 -((linea -6)*9) ,strLinea.decode('iso8859-7').encode('utf-8'))
            except:
                print strLinea
                raise
            return linea +1,pagina

        pagina = 0      # contador de páginas
        cta = -1        #cuenta inicial de la página
        ncomp =0
        ncta = 0        # número de cuenta para la impresión de totales
        ucta = 0
        total_debe = 0L      # total general de debe
        total_haber = 0L     # y haber
        tdebe=0L
        thaber=0L
        sDebe = sHaber =0L

        c = Canvas('LibroCajayBancos.pdf')
        c.setPageCompression(0)
        pagina = 1
        cabecera_libro_caja_bancos(self,c,pagina)
        linea = linea_inicial       # linea actual en la página

        saldoini=0
        det_debe=0
        det_haber=0

        max = float(len(r))
        cont = 1

        for i in r:
            i = map(str, i)     #todo el registro es mapeado como string
            glosa="select glosa from ctb.detalle_comprobante where cod_comprobante=%s and num_linea=%s"%(i[15],i[17])

            self.cursor.execute(glosa)
            glosas=self.cursor.fetchall()
            gl=''

            tdebe = tdebe + float(i[6])
            thaber = thaber + float(i[7])
            saldo2 = (tdebe - thaber)
            saldoesc= saldoini+saldo2

            if cta == i[0]: #si estamos en el detalle del comprobante
                if glosas[0][0] not in (None,''):
                    gl=glosas[0][0]
                else:
                    gl=i[5]

                l = CDateLocal(i[2][0:10]).ljust(12)  + i[3].center(6) + "" +  i[4].rjust(12) + " " + gl[:30].ljust(30) + CMon(i[6],0).rjust(19) + CMon(i[7],0).rjust(20)+ CMon(str(float(saldoesc)),0).rjust(20)

                linea,pagina = put_linea(c,linea,l,pagina)
                l = unicode(l, 'latin-1')
                l.encode('utf-8')

                cta = i[0]

            else:           # estamos listando un comprobante nuevo
                if ncomp > 0:   # si el comprobante de arrastre existe
                    s = ""
                    lc = "Total Detalle Cuenta : "
                    l = lc.rjust(60)
                    l = l + CMon(det_debe,0).rjust(20)
                    l = l  + CMon(det_haber,0).rjust(20)

                    sal = float(tdebe)
                    sal = sal - float(thaber)
                    sal = sal + float(sDebe)
                    sal = sal - float(sHaber)
                    l = l  + str(" ").rjust(20)

                    for n in range(len(l)):
                        s = s + "_"

                    linea,pagina = put_linea(c,linea,s,pagina)

                    linea,pagina = put_linea(c,linea,l,pagina)
                    l = unicode(l, 'latin-1')
                    l.encode('utf-8')

                    linea,pagina = put_linea(c,linea,s,pagina)

                    tdebe=0
                    thaber=0
                    saldo2=0

                    det_debe=0
                    det_haber=0

                tdebe=0
                thaber=0
                saldo2=0

                tdebe = tdebe + float(i[6])
                thaber = thaber + float(i[7])
                saldo2 = tdebe - thaber

                linea,pagina = put_linea(c,linea,"",pagina)
                l = " Cuenta: " + i[0].rjust(12) + " " + (str(i[1])).rjust(12) + "  "

                linea,pagina = put_linea(c,linea,l,pagina)

                sDebe = 0
                sHaber = 0
                sql = "Select sum(monto_debe)::numeric, sum(monto_haber)::numeric from ctb.vw_mayor c \
                 where  date_part('year',c.fecha) = date_part('year','"+ str(self.entDesde.get_date()) +"'::date) and (c.fecha < '"+str(self.entDesde.get_date()) +"' or c.cod_tipo_comprobante ='A')  \
                and c.cod_empresa = " + str(self.cod_empresa) + " and c.num_cuenta ='" + i[0] + "' and estado = '"+ self.estado +"';"
                try:
                    self.cursor.execute(sql)
                    saldo = self.cursor.fetchall()
                except:

                    dialogos.error(None, sys.exc_info()[1])

                if len(saldo) > 0:
                    d = iif(saldo[0][0] == None,0,saldo[0][0])
                    #sDebe = sDebe + float(d)

                    h = iif(saldo[0][1]==None,0,saldo[0][1])
                    #sHaber = sHaber + float(h)

                saldoini = float(d-h) + float(saldo2)

                total_debe = total_debe + sDebe
                total_haber = total_haber + sHaber

                lc = "Saldo Inicial Cuenta : "
                l = lc.rjust(60)+ CMon(str((d)),0).rjust(20) + CMon(str((h)),0).rjust(20) + CMon(str((d-h)),0).rjust(20)

                linea,pagina = put_linea(c,linea,l,pagina)
                l =  " ".ljust(12) + " ".center(6) + " ".rjust(12) + "Saldo Inicial Cuenta".center(59) + "Debe".ljust(19) + "Haber".ljust(19) + "Saldo".ljust(19) + " "

                l = CDateLocal(i[2][0:10]).ljust(12) + i[3].center(6) + "" +  i[4].rjust(12) + " " + i[5][:30].ljust(30) + CMon(i[6],0).rjust(19) + CMon(i[7],0).rjust(20) + CMon(str(saldoini),0).rjust(20)

                linea,pagina = put_linea(c,linea,l,pagina)
                l = unicode(l, 'latin-1')
                l.encode('utf-8')

                cta = comp = i[0]
                ncomp = ncomp + 1

                tdebe=0
                thaber=0
            det_debe = det_debe + float(i[6])
            det_haber = det_haber+ float(i[7])
            total_debe = total_debe + float(i[6])
            total_haber = total_haber + float(i[7])
            ucomp = i[1]

            #BARRA DE PROGRESO
            self.pbProgreso.set_fraction(cont / float(max))
            self.pbProgreso.set_text(str(round(((cont / float(max))*100),2)).replace('.0','')+"% COMPLETADO.")
            while gtk.events_pending():
                gtk.mainiteration(gtk.FALSE)
            cont = cont +1

        s = ""
        if ucomp:
            #linea = linea + 1
            lc = "Total Detalle Cuenta : "
            l = lc.rjust(60)  + CMon(str((det_debe)),0).rjust(20) + CMon(str((det_haber)),0).rjust(20) + str(" ").rjust(20)

            for n in range(len(l)):
                s = s + "_"

            linea,pagina = put_linea(c,linea,s,pagina)
            linea,pagina = put_linea(c,linea,l,pagina)
            l = "Total General : ".rjust(60) + CMon(str(total_debe+float(d)),0).rjust(20) + CMon(str(total_haber+float(h)),0).rjust(20)  + CMon(str((total_debe+float(d)) - (total_haber+float(h))),0).rjust(20)
            linea,pagina = put_linea(c,linea,l,pagina)

            #linea,pagina = put_linea(c,linea,l,pagina)
        c.showPage()
        c.save()
        Abre_pdf("LibroCajayBancos.pdf")

    def exportar_balance(self):
        #style0 = xlwt.easyxf('font: name Times New Roman, colour black, border: left thin, top thin,right thin,bottom thin; bold on; align: wrap on, vert centre, horiz center')
        #style4 = xlwt.easyxf('font: name Times New Roman, colour black; border: left thin, top thin,right thin,bottom thin')
        #style6 = xlwt.easyxf('font: name Times New Roman, colour black; border: left thin, top thin,right thin,bottom thin; align: wrap on, horiz right')
        #style8 = xlwt.easyxf('font: name Times New Roman, colour black, border: left thin, top thin,right thin,bottom thin; bold on; align: wrap on, vert centre, horiz center')


        style0 = xlwt.easyxf('font: name Times New Roman, colour black, bold on; align: wrap on, vert centre, horiz center;border: left thin, top thin,right thin,bottom thin')
        style4 = xlwt.easyxf('font: name Times New Roman, colour black;border: left thin, top thin,right thin,bottom thin')
        style6 = xlwt.easyxf('font: name Times New Roman, colour black;align: wrap on, horiz right;border: left thin, top thin,right thin,bottom thin')
        style9 = xlwt.easyxf('font: name Times New Roman, colour black;align: wrap on, horiz left;border: left thin, top thin,right thin,bottom thin')
        style8 = xlwt.easyxf('font: name Times New Roman, colour black, bold on; align: wrap on, vert centre, horiz left;border: left thin, top thin,right thin,bottom thin')
        style10 = xlwt.easyxf('font: name Times New Roman, colour black, bold on; align: wrap on, vert centre, horiz right;border: left thin, top thin,right thin,bottom thin')

        style1 = xlwt.easyxf('',num_format_str='DD-MMM-YY')
        style2 = xlwt.easyxf('',num_format_str='"$"#,##0.00')
        wb = xlwt.Workbook()
        ws = wb.add_sheet('A Test Sheet',cell_overwrite_ok=True)

        data = []
        ppp = 5

        ws.write(5, 0, "Codigo".encode('ascii', 'replace'),style0)
        ws.write(5, 1, "Cuenta".encode('ascii', 'replace'),style0)
        ws.write(5, 2, "Debitos".encode('ascii', 'replace'),style0)
        ws.write(5, 3, "Creditos".encode('ascii', 'replace'),style0)
        ws.write(5, 4, "Deudor".encode('ascii', 'replace'),style0)
        ws.write(5, 5, "Acreedor".encode('ascii', 'replace'),style0)
        ws.write(5, 6, "Activo".encode('ascii', 'replace'),style0)
        ws.write(5, 7, "Pasivo".encode('ascii', 'replace'),style0)
        ws.write(5, 8, "Perdida".encode('ascii', 'replace'),style0)
        ws.write(5, 9, "Ganancia".encode('ascii', 'replace'),style0)

        #ws.write_merge(0,0,0,8, label='Analisis de Anticipos de Clientes al %s' % (str(int(self.spnFolioDesde.get_value()))), style=style0)
        ws.write_merge(0,0,0,9, label='Balance', style=style0)

        ws.col(0).width = 4000
        ws.col(1).width = 15000
        ws.col(2).width = 4000
        ws.col(3).width = 4000
        ws.col(4).width = 4000
        ws.col(5).width = 4000
        ws.col(6).width = 4000
        ws.col(7).width = 4000
        ws.col(8).width = 4000
        ws.col(9).width = 4000

        cod_emp = self.cod_empresa

        def crea_sql(cod_emp,nivel,f_desde,f_hasta):

            filtro =""

            if self.chkCuenta.get_active() and self.pecCuentaContableDesde.get_selected() and self.pecCuentaContableHasta.get_selected():

                filtro = " and cc.num_cuenta between '"+ self.num_cuenta_desde +"' and '" + self.num_cuenta1_hasta + "'"

            sql = "select sum(digitos) + "+ str(nivel)+ " - 1::int4 as dig from ctb.nivel_cuenta where cod_nivel <= " + str(nivel) + " and cod_empresa = " + str(cod_emp) +";"

            try:
                self.cursor.execute(sql)
                r = self.cursor.fetchall()
            except:
                dialogos.error(None, sys.exc_info()[1])
                return 0

            if self.optVigente.get_active():
                self.estado='V'
            else:
                self.estado='N'

            if len(r) != 0:

                mes_ini = f_desde[5:7]

                mes_fin = f_hasta[5:7]

                digit = int(r[0][0])

                sql = "select \
            substring(dc.num_cuenta,1,"+ str(digit) + ") as cuenta,\
            cc.descripcion_cuenta,\
            sum(dc.monto_debe)::numeric(14,2)::text as debe,\
            sum(dc.monto_haber)::numeric(14,2)::text as haber,\
            cc.tipo_cuenta \
            from \
            (\
                (ctb.detalle_comprobante dc join ctb.comprobante c on c.cod_comprobante = dc.cod_comprobante and c.cod_empresa = dc.cod_empresa and c.fecha between '" + f_desde + "' and '" + f_hasta + "' ) \
            join ctb.cuenta_contable cc on cc.num_cuenta like substring(dc.num_cuenta,1,"+ str(digit) +")||'%' and cc.cod_empresa = dc.cod_empresa) \
            where \
            cc.cod_empresa = " + str(self.cod_empresa) + " \
            and cc.nivel_cuenta =" + str(nivel) + filtro + " and c.estado ='"+ self.estado +"' \
            group by \
            substring(dc.num_cuenta,1,"+ str(digit) +")  ,cc.descripcion_cuenta,cc.tipo_cuenta \
            order by substring(dc.num_cuenta,1,"+ str(digit) +")"

                return sql

        filtro = "where cod_empresa = " + str(self.cod_empresa) + " and fecha between '" + CDateLocal(str(self.entDesde.get_date())) + "' and '" + CDateLocal(str(self.entHasta.get_date())) +"'"

        try:
            sql = crea_sql(self.cod_empresa,self.Nivel,str(self.entDesde.get_date()),str(self.entHasta.get_date()))
##            print sql
            self.cursor.execute(sql)
            r = self.cursor.fetchall()
            if len(r) == 0:
                dialogos.error("No hay datos para generar el Balance con estos parametros")
                return

        except:
            print sys.exc_info()[1]
            return 0

        ws.write_merge(1,1,0,5, label=self.descripcion_empresa.encode('ascii', 'replace'), style=style8)
        ws.write_merge(2,2,0,5, label=self.rut_empresa.encode('ascii', 'replace'), style=style8)
        ws.write_merge(3,3,0,5, label=self.direccion_empresa.encode('ascii', 'replace'), style=style8)
        ws.write_merge(1,1,6,9, label=("Desde : " +  CDateLocal(str(self.entDesde.get_date()))).encode('ascii', 'replace'), style=style8)
        ws.write_merge(2,2,6,9, label=("Hasta : " +  CDateLocal(str(self.entHasta.get_date()))).encode('ascii', 'replace'), style=style8)
        ws.write_merge(3,3,6,9, label=("Hasta : " +  CDateLocal(str(self.entHasta.get_date()))).encode('ascii', 'replace'), style=style8)

        ws.write_merge(4,4,4,5, label='S A L D O', style=style0)
        ws.write_merge(4,4,6,7, label='I N V E N T A R I O', style=style0)
        ws.write_merge(4,4,8,9, label='R E S U L T A D O', style=style0)

        ucomp =0
        tdebe =     0   # Total de Columna Debitos
        thaber =    0   # Total de Columna Creditos
        tsdeudor =  0   # Total de Columna Saldo Deudor
        tsacreedor= 0   # Total de Columna Saldo Acreedor
        tiactivo=   0   # Total de Columna Inventario Activo
        tipasivo =  0   # Total de Columna Inventario Pasivo
        trperdida=  0   # Total de Columna Resultado Perdida
        trganancia= 0   # Total de Columna Resultado Ganancia

        for i in r:
            ppp = ppp + 1
            slinea = long(i[2]) - long(i[3])
            i = map(str, i)     #todo el registro es mapeado como string

            ws.write(ppp, 0, i[0],style9)
            ws.write(ppp, 1, i[1][:38].encode('ascii', 'replace'), style9)
            ws.write(ppp, 2, float(i[2]),style6)
            ws.write(ppp, 3, float(i[3]),style6)
            ws.write(ppp, 4, iif(slinea >= 0,float(slinea),0),style6)
            ws.write(ppp, 5, iif(slinea < 0,float(abs(slinea)),0),style6)
            ws.write(ppp, 6, iif(i[4]=="A",float(slinea),0),style6)
            ws.write(ppp, 7, iif(i[4]=="P",float(slinea * -1),0),style6)
            ws.write(ppp, 8, iif(i[4]=="E",float(slinea),0),style6)
            ws.write(ppp, 9, iif(i[4]=="I",float(slinea * -1 ),0),style6)

            ucomp = 1
            tdebe       = tdebe + float(i[2])
            thaber      = thaber + float(i[3])
            tsdeudor    = tsdeudor + float(iif(slinea >= 0,((slinea)),0))
            tsacreedor  = tsacreedor + float(iif(slinea < 0,((abs(slinea))),0))
            slinea = (slinea)

            if i[4] == "A":
                tiactivo    = tiactivo + float(iif(i[4]=="A",((slinea)),"0"))
            elif i[4] == "E":
                trperdida   = trperdida + float(iif(i[4]=="E",((slinea)),"0"))
            elif i[4] == "P":
                tipasivo    = tipasivo + float(iif(i[4]=="P",((slinea * -1)),"0"))
            else:
                trganancia  = trganancia + float(iif(i[4]=="I",((slinea * -1)),"0"))

        if ucomp:
            ppp = ppp + 1
            ws.write_merge(ppp,ppp,0,1, label="SubTotal", style=style0)
            ws.write(ppp, 2, float(tdebe),style10)
            ws.write(ppp, 3, float(thaber),style10)
            ws.write(ppp, 4, float(tsdeudor),style10)
            ws.write(ppp, 5, float(tsacreedor),style10)
            ws.write(ppp, 6, float(tiactivo),style10)
            ws.write(ppp, 7, float(tipasivo),style10)
            ws.write(ppp, 8, float(trperdida),style10)
            ws.write(ppp, 9, float(trganancia),style10)

            ppp = ppp + 1
            ws.write_merge(ppp,ppp,0,1, label="Utilidad/Perdida", style=style0)
            ws.write(ppp, 2, float(0),style10)
            ws.write(ppp, 3, float(0),style10)
            ws.write(ppp, 4, float(0),style10)
            ws.write(ppp, 5, float(0),style10)
            ws.write(ppp, 6, iif(tiactivo>tipasivo,0,float(tipasivo - tiactivo)),style10)
            ws.write(ppp, 7, iif(tiactivo>tipasivo,float(tiactivo-tipasivo),0),style10)
            ws.write(ppp, 8, iif(trperdida>trganancia,0,float(trganancia - trperdida)),style10)
            ws.write(ppp, 9, iif(trperdida>trganancia,float(trperdida-trganancia),0),style10)

            tiactivo = tiactivo + iif(tiactivo>tipasivo,0,float(tipasivo - tiactivo))
            tipasivo = tipasivo + iif(tiactivo>tipasivo,float(tiactivo - tipasivo),0)
            trperdida = trperdida + iif(trperdida>trganancia,0,float(trganancia - trperdida))
            trganancia = trganancia + iif(trperdida>trganancia,float(trperdida-trganancia),0)

            ppp = ppp + 1
            ws.write_merge(ppp,ppp,0,1, label="Totales", style=style0)
            ws.write(ppp, 2, float(tdebe),style10)
            ws.write(ppp, 3, float(thaber),style10)
            ws.write(ppp, 4, float(tsdeudor),style10)
            ws.write(ppp, 5, float(tsacreedor),style10)
            ws.write(ppp, 6, float(tiactivo),style10)
            ws.write(ppp, 7, float(tipasivo),style10)
            ws.write(ppp, 8, float(trperdida),style10)
            ws.write(ppp, 9, float(trganancia),style10)

        wb.save('reporte.xls')
        Abre_excel('reporte.xls');

    def exportar_balance2(self):
        #style0 = xlwt.easyxf('font: name Times New Roman, colour black, border: left thin, top thin,right thin,bottom thin; bold on; align: wrap on, vert centre, horiz center')
        #style4 = xlwt.easyxf('font: name Times New Roman, colour black; border: left thin, top thin,right thin,bottom thin')
        #style6 = xlwt.easyxf('font: name Times New Roman, colour black; border: left thin, top thin,right thin,bottom thin; align: wrap on, horiz right')
        #style8 = xlwt.easyxf('font: name Times New Roman, colour black, border: left thin, top thin,right thin,bottom thin; bold on; align: wrap on, vert centre, horiz center')


        style0 = xlwt.easyxf('font: name Times New Roman, colour black, bold on; align: wrap on, vert centre, horiz center;border: left thin, top thin,right thin,bottom thin')
        style4 = xlwt.easyxf('font: name Times New Roman, colour black;border: left thin, top thin,right thin,bottom thin')
        style6 = xlwt.easyxf('font: name Times New Roman, colour black;align: wrap on, horiz right;border: left thin, top thin,right thin,bottom thin')
        style9 = xlwt.easyxf('font: name Times New Roman, colour black;align: wrap on, horiz left;border: left thin, top thin,right thin,bottom thin')
        style8 = xlwt.easyxf('font: name Times New Roman, colour black, bold on; align: wrap on, vert centre, horiz left;border: left thin, top thin,right thin,bottom thin')
        style10 = xlwt.easyxf('font: name Times New Roman, colour black, bold on; align: wrap on, vert centre, horiz right;border: left thin, top thin,right thin,bottom thin')

        style1 = xlwt.easyxf('',num_format_str='DD-MMM-YY')
        style2 = xlwt.easyxf('',num_format_str='"$"#,##0.00')
        wb = xlwt.Workbook()
        ws = wb.add_sheet('Balance',cell_overwrite_ok=True)

        data = []
        ppp = 5

        ws.write(5, 0, "Cuenta Contable".encode('ascii', 'replace'),style0)
        ws.write(5, 1, "Saldo_Apertura(Debe)".encode('ascii', 'replace'),style0)
        ws.write(5, 2, "Saldo_Apertura(Haber)".encode('ascii', 'replace'),style0)
        ws.write(5, 3, "Movimientos(Debe)".encode('ascii', 'replace'),style0)
        ws.write(5, 4, "Movimientos(Haber)".encode('ascii', 'replace'),style0)
        ws.write(5, 5, "Saldo (Deudor)".encode('ascii', 'replace'),style0)
        ws.write(5, 6, "Saldo (Acreedor)".encode('ascii', 'replace'),style0)
        ws.write(5, 7, "Cuenta de Balance(Debe)".encode('ascii', 'replace'),style0)
        ws.write(5, 8, "Cuenta de Balance(Haber)".encode('ascii', 'replace'),style0)
        ws.write(5, 9, "Cuenta de Resultados(Perdida)".encode('ascii', 'replace'),style0)
        ws.write(5, 10, "Cuenta de Resultados(Ganancia)".encode('ascii', 'replace'),style0)

        #ws.write_merge(0,0,0,8, label='Analisis de Anticipos de Clientes al %s' % (str(int(self.spnFolioDesde.get_value()))), style=style0)
        #ws.write_merge(0,0,0,9, label='Balance', style=style0)

        ws.col(0).width = 5000
        ws.col(1).width = 5000
        ws.col(2).width = 5000
        ws.col(3).width = 5000
        ws.col(4).width = 5000
        ws.col(5).width = 5000
        ws.col(6).width = 5000
        ws.col(7).width = 6000
        ws.col(8).width = 6000
        ws.col(9).width = 7000
        ws.col(10).width = 7000

        cod_emp = self.cod_empresa

        def crea_sql(cod_emp,nivel,f_desde,f_hasta):

            filtro =""

            if self.chkCuenta.get_active() and self.pecCuentaContableDesde.get_selected() and self.pecCuentaContableHasta.get_selected():

                filtro = " and cc.num_cuenta between '"+ self.num_cuenta_desde +"' and '" + self.num_cuenta1_hasta + "'"

            sql = "select sum(digitos) + "+ str(nivel)+ " - 1::int4 as dig from ctb.nivel_cuenta where cod_nivel <= " + str(nivel) + " and cod_empresa = " + str(cod_emp) +";"

            try:
                self.cursor.execute(sql)
                r = self.cursor.fetchall()
            except:
                dialogos.error(None, sys.exc_info()[1])
                return 0

            if self.optVigente.get_active():
                self.estado='V'
            else:
                self.estado='N'

            if len(r) != 0:

                mes_ini = f_desde[5:7]

                mes_fin = f_hasta[5:7]

                digit = int(r[0][0])

                sql = "select \
            substring(dc.num_cuenta,1,"+ str(digit) + ") as cuenta,\
            cc.descripcion_cuenta,\
            sum(dc.monto_debe)::numeric(14,2)::text as debe,\
            sum(dc.monto_haber)::numeric(14,2)::text as haber,\
            cc.tipo_cuenta \
            from \
            (\
                (ctb.detalle_comprobante dc join ctb.comprobante c on c.cod_comprobante = dc.cod_comprobante and c.cod_empresa = dc.cod_empresa and c.fecha between '" + f_desde + "' and '" + f_hasta + "' ) \
            join ctb.cuenta_contable cc on cc.num_cuenta like substring(dc.num_cuenta,1,"+ str(digit) +")||'%' and cc.cod_empresa = dc.cod_empresa) \
            where \
            cc.cod_empresa = " + str(self.cod_empresa) + " \
            and cc.nivel_cuenta =" + str(nivel) + filtro + " and c.estado ='"+ self.estado +"' \
            group by \
            substring(dc.num_cuenta,1,"+ str(digit) +")  ,cc.descripcion_cuenta,cc.tipo_cuenta \
            order by substring(dc.num_cuenta,1,"+ str(digit) +")"

                return sql

        filtro = "where cod_empresa = " + str(self.cod_empresa) + " and fecha between '" + CDateLocal(str(self.entDesde.get_date())) + "' and '" + CDateLocal(str(self.entHasta.get_date())) +"'"

        try:
            sql = crea_sql(self.cod_empresa,self.Nivel,str(self.entDesde.get_date()),str(self.entHasta.get_date()))
##            print sql
            self.cursor.execute(sql)
            r = self.cursor.fetchall()
            if len(r) == 0:
                dialogos.error("No datos para generar el Balance con estos parametros")
                return

        except:
            print sys.exc_info()[1]
            return 0

        ws.write_merge(2,2,0,5, label=self.descripcion_empresa.encode('ascii', 'replace'), style=style8)
        ws.write_merge(1,1,0,5, label=self.rut_empresa.encode('ascii', 'replace'), style=style8)
        ws.write_merge(3,3,0,5, label=("Desde : " +  CDateLocal(str(self.entDesde.get_date()))).encode('ascii', 'replace') + (" Hasta : " +  CDateLocal(str(self.entHasta.get_date()))).encode('ascii', 'replace'), style=style8)

        ucomp =0
        tdebe =     0   # Total de Columna Debitos
        thaber =    0   # Total de Columna Creditos
        tsdeudor =  0   # Total de Columna Saldo Deudor
        tsacreedor= 0   # Total de Columna Saldo Acreedor
        tiactivo=   0   # Total de Columna Inventario Activo
        tipasivo =  0   # Total de Columna Inventario Pasivo
        trperdida=  0   # Total de Columna Resultado Perdida
        trganancia= 0   # Total de Columna Resultado Ganancia

        for i in r:
            ppp = ppp + 1
            slinea = long(i[2]) - long(i[3])
            i = map(str, i)     #todo el registro es mapeado como string

            ws.write(ppp, 0, i[0],style9)
            ws.write(ppp, 1, str('0').encode('ascii', 'replace'), style9)
            ws.write(ppp, 2, str('0').encode('ascii', 'replace'), style9)
            ws.write(ppp, 3, float(i[2]),style6)
            ws.write(ppp, 4, float(i[3]),style6)
            ws.write(ppp, 5, iif(slinea >= 0,float(slinea),0),style6)
            ws.write(ppp, 6, iif(slinea < 0,float(abs(slinea)),0),style6)
            ws.write(ppp, 7, iif(i[4]=="A",float(slinea),0),style6)
            ws.write(ppp, 8, iif(i[4]=="P",float(slinea * -1),0),style6)
            ws.write(ppp, 9, iif(i[4]=="E",float(slinea),0),style6)
            ws.write(ppp, 10
                     , iif(i[4]=="I",float(slinea * -1 ),0),style6)

            ucomp = 1
            tdebe       = tdebe + float(i[2])
            thaber      = thaber + float(i[3])
            tsdeudor    = tsdeudor + float(iif(slinea >= 0,((slinea)),0))
            tsacreedor  = tsacreedor + float(iif(slinea < 0,((abs(slinea))),0))
            slinea = (slinea)

            if i[4] == "A":
                tiactivo    = tiactivo + float(iif(i[4]=="A",((slinea)),"0"))
            elif i[4] == "E":
                trperdida   = trperdida + float(iif(i[4]=="E",((slinea)),"0"))
            elif i[4] == "P":
                tipasivo    = tipasivo + float(iif(i[4]=="P",((slinea * -1)),"0"))
            else:
                trganancia  = trganancia + float(iif(i[4]=="I",((slinea * -1)),"0"))

        #if ucomp:
        #    ppp = ppp + 1
        #    ws.write_merge(ppp,ppp,0,1, label="SubTotal", style=style0)
        #    ws.write(ppp, 2, float(tdebe),style10)
        #    ws.write(ppp, 3, float(thaber),style10)
        #    ws.write(ppp, 4, float(tsdeudor),style10)
        #    ws.write(ppp, 5, float(tsacreedor),style10)
        #    ws.write(ppp, 6, float(tiactivo),style10)
        #    ws.write(ppp, 7, float(tipasivo),style10)
        #    ws.write(ppp, 8, float(trperdida),style10)
        #    ws.write(ppp, 9, float(trganancia),style10)
        #
        #    ppp = ppp + 1
        #    ws.write_merge(ppp,ppp,0,1, label="Utilidad/Perdida", style=style0)
        #    ws.write(ppp, 2, float(0),style10)
        #    ws.write(ppp, 3, float(0),style10)
        #    ws.write(ppp, 4, float(0),style10)
        #    ws.write(ppp, 5, float(0),style10)
        #    ws.write(ppp, 6, iif(tiactivo>tipasivo,0,float(tipasivo - tiactivo)),style10)
        #    ws.write(ppp, 7, iif(tiactivo>tipasivo,float(tiactivo-tipasivo),0),style10)
        #    ws.write(ppp, 8, iif(trperdida>trganancia,0,float(trganancia - trperdida)),style10)
        #    ws.write(ppp, 9, iif(trperdida>trganancia,float(trperdida-trganancia),0),style10)
        #
        #    tiactivo = tiactivo + iif(tiactivo>tipasivo,0,float(tipasivo - tiactivo))
        #    tipasivo = tipasivo + iif(tiactivo>tipasivo,float(tiactivo - tipasivo),0)
        #    trperdida = trperdida + iif(trperdida>trganancia,0,float(trganancia - trperdida))
        #    trganancia = trganancia + iif(trperdida>trganancia,float(trperdida-trganancia),0)
        #
        #    ppp = ppp + 1
        #    ws.write_merge(ppp,ppp,0,1, label="Totales", style=style0)
        #    ws.write(ppp, 2, float(tdebe),style10)
        #    ws.write(ppp, 3, float(thaber),style10)
        #    ws.write(ppp, 4, float(tsdeudor),style10)
        #    ws.write(ppp, 5, float(tsacreedor),style10)
        #    ws.write(ppp, 6, float(tiactivo),style10)
        #    ws.write(ppp, 7, float(tipasivo),style10)
        #    ws.write(ppp, 8, float(trperdida),style10)
        #    ws.write(ppp, 9, float(trganancia),style10)

        wb.save('reporte.xls')
        Abre_excel('reporte.xls');

    def lista_balance(self):
        cod_emp = self.cod_empresa

        def crea_sql(cod_emp,nivel,f_desde,f_hasta):

            filtro =""

            if self.chkCuenta.get_active() and self.pecCuentaContableDesde.get_selected() and self.pecCuentaContableHasta.get_selected():

                filtro = " and cc.num_cuenta between '"+ self.num_cuenta_desde +"' and '" + self.num_cuenta1_hasta + "'"

            sql = "select sum(digitos) + "+ str(nivel)+ " - 1::int4 as dig from ctb.nivel_cuenta where cod_nivel <= " + str(nivel) + " and cod_empresa = " + str(cod_emp) +";"

            try:
                self.cursor.execute(sql)
                r = self.cursor.fetchall()
            except:
                dialogos.error(None, sys.exc_info()[1])
                return 0

            if self.optVigente.get_active():
                self.estado='V'
            else:
                self.estado='N'

            if len(r) != 0:

                mes_ini = f_desde[5:7]

                mes_fin = f_hasta[5:7]

                digit = int(r[0][0])

                sql = "select \
            substring(dc.num_cuenta,1,"+ str(digit) + ") as cuenta,\
            cc.descripcion_cuenta,\
            sum(dc.monto_debe)::numeric(14,2)::text as debe,\
            sum(dc.monto_haber)::numeric(14,2)::text as haber,\
            cc.tipo_cuenta \
            from \
            (\
                (ctb.detalle_comprobante dc join ctb.comprobante c on c.cod_comprobante = dc.cod_comprobante and c.cod_empresa = dc.cod_empresa and c.fecha between '" + f_desde + "' and '" + f_hasta + "' ) \
            join ctb.cuenta_contable cc on cc.num_cuenta like substring(dc.num_cuenta,1,"+ str(digit) +")||'%' and cc.cod_empresa = dc.cod_empresa) \
            where \
            cc.cod_empresa = " + str(self.cod_empresa) + " \
            and cc.nivel_cuenta =" + str(nivel) + filtro + " and c.estado ='"+ self.estado +"' \
            group by \
            substring(dc.num_cuenta,1,"+ str(digit) +")  ,cc.descripcion_cuenta,cc.tipo_cuenta \
            order by substring(dc.num_cuenta,1,"+ str(digit) +")"

                return sql

        filtro = "where cod_empresa = " + str(self.cod_empresa) + " and fecha between '" + CDateLocal(str(self.entDesde.get_date())) + "' and '" + CDateLocal(str(self.entHasta.get_date())) +"'"

        try:
            sql = crea_sql(self.cod_empresa,self.Nivel,str(self.entDesde.get_date()),str(self.entHasta.get_date()))
            print sql
            self.cursor.execute(sql)
            r = self.cursor.fetchall()
            if len(r) == 0:
                dialogos.error("No datos para generar el Balance con estos parametros")
                return

        except:
            print sys.exc_info()[1]
            return 0

        def cabecera_Balance(self, pc, page_num):
            pc.setFont('Courier', 12)
            w = pc.stringWidth("Balance",'Courier', 12)
            pc.drawString((726 /2) - w/2,582, "Balance")
            pc.drawString(30,570,self.descripcion_empresa)
            pc.drawString(30,559,self.rut_empresa)
            pc.drawString(30,549,self.direccion_empresa)
            l ="Pagina: %s"%(zfill(int(page_num), 10))
            w = pc.stringWidth(l,'Courier', 12)
            pc.drawString(796 - w,570,l)
            t = map(str, localtime())
            pc.drawString(796 -w ,559,"Desde : " +  CDateLocal(str(self.entDesde.get_date())))
            pc.drawString(796 -w ,549,"Hasta : " +  CDateLocal(str(self.entHasta.get_date())))
            pc.setFont('Courier', 7)
            pc.line(30, 548, 798, 548)
##            lc = " ".ljust(16) + " ".center(29) + " ".rjust(16) + " ".rjust(16) +"|" + "S A L D O".center(31) +"|" + "I N V E N T A R I O".center(31) +"|"+ "R E S U L T A D O".center(30) + "|\n"
##            pc.drawString(30,537,lc)
            lc = " ".ljust(16) + " ".center(39) + " ".rjust(16) + " ".rjust(16) +"|" + "S A L D O".center(31) +"|" + "I N V E N T A R I O".center(31) +"|"+ "R E S U L T A D O".center(30) + "|"
            pc.drawString(30,537,lc)
##            l = "Codigo".ljust(16) + "Cuenta".center(29) + "Debitos".rjust(16) + "Creditos".rjust(16) + "Deudor".rjust(16) + "Acreedor".rjust(16) + "Activo".rjust(16) + "Pasivo".rjust(16) + "Perdida".rjust(16) + "Ganancia".rjust(16) + "\n"
            l = "Codigo".ljust(16) + "Cuenta".center(39) + "Debitos".rjust(16) + "Creditos".rjust(16) + "Deudor".rjust(16) + "Acreedor".rjust(16) + "Activo".rjust(16) + "Pasivo".rjust(16) + "Perdida".rjust(16) + "Ganancia".rjust(16) + ""
            ##TODO: ajustar tamaños del balance
            pc.drawString(30,529,l)
            pc.line(30, 528, 798, 528)
            pc.setFont('Courier', 7)

        def put_linea(pc,linea,strLinea,pagina):
            if linea > 60:
                t = map(str, localtime())
                #pc.drawString(30,528 -((linea - 4)*9) ,"Fecha Impresion: " +  zfill(t[2],2) + "/" + zfill(t[1],2) + "/" + t[0])
                pc.drawString(30,528 -((linea - 4)*9) ,"")
                c.showPage()
                pagina = pagina + 1
                cabecera_Balance(self,pc,pagina)
                linea = 7
            pc.drawString(30,528 -((linea -6)*9) ,strLinea.decode('iso8859-7').encode('utf-8'))
            return linea +1,pagina

        pagina = 0      # contador de páginas
        cta = -1        #cuenta inicial de la página
        ncomp = 0
        ucomp =0
        ncta = 0        # número de cuenta para la impresión de totales
        ucta = 0
        tdebe =     0   # Total de Columna Debitos
        thaber =    0   # Total de Columna Creditos
        tsdeudor =  0   # Total de Columna Saldo Deudor
        tsacreedor= 0   # Total de Columna Saldo Acreedor
        tiactivo=   0   # Total de Columna Inventario Activo
        tipasivo =  0   # Total de Columna Inventario Pasivo
        trperdida=  0   # Total de Columna Resultado Perdida
        trganancia= 0   # Total de Columna Resultado Ganancia
        c = Canvas('Balance.pdf',pagesize=landscape(A4))
        c.setPageCompression(0)

        pagina = 1
        cabecera_Balance(self,c,pagina)

        linea = 8       # linea actual en la página

        for i in r:

            slinea = long(i[2]) - long(i[3])
            i = map(str, i)     #todo el registro es mapeado como string
            l = i[0].ljust(15) + " " + i[1][:45].ljust(45) + " " +  CMon(str(float(i[2])),0).rjust(16) + CMon(str(float(i[3])),0).rjust(16) + iif(slinea >= 0,ctb_formato_moneda(str(slinea)),"0").rjust(16) + iif(slinea < 0,ctb_formato_moneda(str(abs(slinea))),"0").rjust(16) + iif(i[4]=="A",ctb_formato_moneda(str((slinea))),"0").rjust(16) + iif(i[4]=="P",ctb_formato_moneda(str((slinea * -1))),"0").rjust(16) + iif(i[4]=="E",ctb_formato_moneda(str((slinea))),"0").rjust(16) + iif(i[4]=="I",ctb_formato_moneda(str((slinea * -1 ))),"0").rjust(16) + ""
            linea,pagina = put_linea(c,linea,l,pagina)

            ucomp = 1
            tdebe       = tdebe + float(i[2])
            thaber      = thaber + float(i[3])
            tsdeudor    = tsdeudor + float(iif(slinea >= 0,((slinea)),0))
            tsacreedor  = tsacreedor + float(iif(slinea < 0,((abs(slinea))),0))
            slinea = (slinea)

            if i[4] == "A":
                tiactivo    = tiactivo + float(iif(i[4]=="A",((slinea)),"0"))
            elif i[4] == "E":
                trperdida   = trperdida + float(iif(i[4]=="E",((slinea)),"0"))
            elif i[4] == "P":
                tipasivo    = tipasivo + float(iif(i[4]=="P",((slinea * -1)),"0"))
            else:
                trganancia  = trganancia + float(iif(i[4]=="I",((slinea * -1)),"0"))

        s = ""
        if ucomp:
            l = "".ljust(15) + " " + "SubTotal".ljust(38) + " " +  ctb_formato_moneda(tdebe).rjust(16) + ctb_formato_moneda(thaber).rjust(16)  + ctb_formato_moneda(tsdeudor).rjust(16)  + ctb_formato_moneda(tsacreedor).rjust(16)  + ctb_formato_moneda(tiactivo).rjust(16)  + ctb_formato_moneda(tipasivo).rjust(16)  + ctb_formato_moneda(trperdida).rjust(16)  + ctb_formato_moneda(trganancia).rjust(16) + ""
            for n in range(len(l)):
                s = s + "_"

            linea,pagina = put_linea(c,linea,s,pagina)

            linea,pagina = put_linea(c,linea,l,pagina)

            l = "".ljust(15) + " " + "Utilidad/Perdida".ljust(38) + " " +  ctb_formato_moneda("0").rjust(16) + ctb_formato_moneda("0").rjust(16)  + ctb_formato_moneda("0").rjust(16)  + ctb_formato_moneda("0").rjust(16)  + \
                    ctb_formato_moneda(iif(tiactivo>tipasivo,"0",tipasivo - tiactivo)).rjust(16)  + ctb_formato_moneda(iif(tiactivo>tipasivo,str(tiactivo-tipasivo),"0")).rjust(16)  + \
                    ctb_formato_moneda(iif(trperdida>trganancia,"0",trganancia - trperdida)).rjust(16)  + ctb_formato_moneda(iif(trperdida>trganancia,str(trperdida-trganancia),"0")).rjust(16) + ""

            linea,pagina = put_linea(c,linea,l,pagina)


            tiactivo = tiactivo + iif(tiactivo>tipasivo,0,tipasivo - tiactivo)
            tipasivo = tipasivo + iif(tiactivo>tipasivo,tiactivo - tipasivo,0)
            trperdida = trperdida + iif(trperdida>trganancia,0,trganancia - trperdida)
            trganancia = trganancia + iif(trperdida>trganancia,trperdida-trganancia,0)

            linea,pagina = put_linea(c,linea,s,pagina)

            l = "".ljust(15) + " " + "Totales".ljust(38) + " " +  ctb_formato_moneda(tdebe).rjust(16) + ctb_formato_moneda(thaber).rjust(16)  + ctb_formato_moneda(tsdeudor).rjust(16)  + ctb_formato_moneda(tsacreedor).rjust(16)  + ctb_formato_moneda(tiactivo).rjust(16)  + ctb_formato_moneda(tipasivo).rjust(16)  + ctb_formato_moneda(trperdida).rjust(16)  + ctb_formato_moneda(trganancia).rjust(16) + ""

            linea,pagina = put_linea(c,linea,l,pagina)

        c.showPage()
        c.save()
        Abre_pdf("Balance.pdf")

    """def filtro_tipo_comprobante(self):
        Filtro_comprobante=' and tipo in (%s)'
        filtro=''
        model = self.lstTipo.get_model()

        for i in self.tipos_comprobantes:
            if model[i][1]=="TODOS":
                Filtro_comprobante=" and tipo in('A','E','I','T','C','Z','R','V')"
                return Filtro_comprobante
            if model[i][1]=="INGRESO":
                filtro+='I'
            if model[i][1]=="EGRESO":
                filtro+=' E'
            if model[i][1]=="TRASPASO":
                filtro+=' T'
            if model[i][1]=="APERTURA":
                filtro+=' A'
            if model[i][1]=="CIERRE":
                filtro+=' C'
            if model[i][1]=="CENTRALIZACION":
                filtro+=' Z'
            if model[i][1]=="CONSOLIDACION":
                filtro+='R'
            ##################CREADO#######
            if model[i][1]=="INVENTARIO":
                filtro+='V'
            ###############################
        filtros=filtro.split()
        return Filtro_comprobante %(str(filtros).replace("[","").replace("]",""))
        """
        ### FUNCIÓN NUEVA - PARA AGREGAR
    """def cargar_tipo_libro_electronico(self):

        try:
            self.cursor.execute("select False, desc_tipo_libro from ctb.tipo_libro")
            r = self.cursor.fetchall()
        except:
            dialogos.error(None, sys.exc_info()[1])
            return 0
        self.modelo_libro_electronico = gtk.ListStore(bool, str)
        indices =[]
        indices.append([0,"OK","bool",self.fixed_toggled])
        indices.append([1,"Tipo Libro Electrónico","str"])
        SimpleTree.GenColsByModel(self.modelo_libro_electronico,indices,self.lstTipo)
        if len(r)==0:
            return
        #iter=self.modelo_libro_electronico.append()
        #self.modelo_libro_electronico.set(iter,0,False,1,"TODOS")
        for item in r:
            self.modelo_libro_electronico.append(item)
        return 0"""
        ### función a eliminar
    """def carga_tipo_comprobante(self):
        try:
            self.cursor.execute("select False, descripcion_tipo from ctb.tipo_comprobante")
            r = self.cursor.fetchall()
        except:
            dialogos.error(None, sys.exc_info()[1])
            return 0

        self.modelo_comprobante=gtk.ListStore(bool,str)
        indices=[]
        indices.append([0,"OK","bool",self.fixed_toggled])
        indices.append([1,"Tipo Comprobante","str"])
        SimpleTree.GenColsByModel(self.modelo_comprobante,indices,self.lstTipo)
        if len(r)==0:
            return
        iter=self.modelo_comprobante.append()
        self.modelo_comprobante.set(iter,0,False,1,"TODOS")
        for item in r:
            self.modelo_comprobante.append(item)
        return 0"""

    def fixed_toggled(self,cell, path, model):
        iter = model.get_iter((int(path),))
        fixed = model.get_value(iter, 0)
        fixed = not fixed
        if model.get_value(iter,1)=='TODOS' and fixed==True:
            self.tipos_comprobantes=[]
            for i in self.modelo_comprobante:
                it=self.modelo_comprobante.get_iter((int(i.path[0]),))
                if i[1]!='TODOS' and i[0]==True:
                    self.modelo_comprobante.set(it,0,False)
        else:
            it=self.modelo_comprobante.get_iter_first()
            if self.modelo_comprobante.get_value(it,0)==True:
                self.tipos_comprobantes.remove(0)
            self.modelo_comprobante.set(it,0,False)
        model.set(iter, 0, fixed)
        if fixed==True:
            self.tipos_comprobantes.append(int(path[0]))
        else:
            if int(path[0]) in [(i) for i in self.tipos_comprobantes]:
                self.tipos_comprobantes.remove(int(path[0]))
        #print "hola"

    ##AÑO BISIESTO
    
    def esBisiesto(self):
        if self.anioHasta != None: 
            if self.anioHasta % 4 == 0:
                if self.anioHasta % 100 == 0:
                    if self.anioHasta % 400 == 0:
                        return True
                    else:
                        return False
                else:
                    return True
            else:
               return False
    ###FUNCIÓN NUEVA
    def on_cmbPeriodoDesde_changed(self,widget,*args):

        iter = self.cmbPeriodoDesde.get_active_iter()
        model = self.cmbPeriodoDesde.get_model()
        row = model.get_path(iter)
        self.periodoDesde = model[row][0]
        if self.periodoDesde == 'ENERO':
            self.entPeriodoDesde = '01'
        if self.periodoDesde == 'FEBRERO':
            self.entPeriodoDesde = '02'
        if self.periodoDesde == 'MARZO':
            self.entPeriodoDesde = '03'
        if self.periodoDesde == 'ABRIL':
            self.entPeriodoDesde = '04'
        if self.periodoDesde == 'MAYO':
            self.entPeriodoDesde = '05'
        if self.periodoDesde == 'JUNIO':
            self.entPeriodoDesde = '06'
        if self.periodoDesde == 'JULIO':
            self.entPeriodoDesde = '07'
        if self.periodoDesde == 'AGOSTO':
            self.entPeriodoDesde = '08'
        if self.periodoDesde == 'SETIEMBRE':
            self.entPeriodoDesde = '09'
        if self.periodoDesde == 'OCTUBRE':
            self.entPeriodoDesde = '10'
        if self.periodoDesde == 'NOVIEMBRE':
            self.entPeriodoDesde = '11'
        if self.periodoDesde == 'DICIEMBRE':
            self.entPeriodoDesde = '12'
        print self.entPeriodoDesde

    def on_cmbAnioDesde_changed(self,widget,*args):

        iter = self.cmbAnioDesde.get_active_iter()
        model = self.cmbAnioDesde.get_model()
        row = model.get_path(iter)
        self.anioDesde = model[row][0]
        print self.anioDesde

    def on_cmbPeriodoHasta_changed(self,widget,*args):

        iter = self.cmbPeriodoHasta.get_active_iter()
        model = self.cmbPeriodoHasta.get_model()
        row = model.get_path(iter)
        self.periodoHasta = model[row][0]
        if self.periodoHasta == 'ENERO':
            self.entPeriodoHasta = '01'
            self.ultimoDia = '31'
        if self.periodoHasta == 'FEBRERO':
            self.entPeriodoHasta = '02'
            if self.esBisiesto() == True:
                self.ultimoDia = '29'
            else:
                self.ultimoDia = '28'
            
        if self.periodoHasta == 'MARZO':
            self.entPeriodoHasta = '03'
            self.ultimoDia = '31'
        if self.periodoHasta == 'ABRIL':
            self.entPeriodoHasta = '04'
            self.ultimoDia = '30'
        if self.periodoHasta == 'MAYO':
            self.entPeriodoHasta = '05'
            self.ultimoDia = '31'
        if self.periodoHasta == 'JUNIO':
            self.entPeriodoHasta = '06'
            self.ultimoDia = '30'
        if self.periodoHasta == 'JULIO':
            self.entPeriodoHasta = '07'
            self.ultimoDia = '31'
        if self.periodoHasta == 'AGOSTO':
            self.entPeriodoHasta = '08'
            self.ultimoDia = '31'
        if self.periodoHasta == 'SETIEMBRE':
            self.entPeriodoHasta = '09'
            self.ultimoDia = '30'
        if self.periodoHasta == 'OCTUBRE':
            self.entPeriodoHasta = '10'
            self.ultimoDia = '31'
        if self.periodoHasta == 'NOVIEMBRE':
            self.entPeriodoHasta = '11'
            self.ultimoDia = '39'
        if self.periodoHasta == 'DICIEMBRE':
            self.entPeriodoHasta = '12'
            self.ultimoDia = '31'
        print self.entPeriodoHasta

    def on_cmbAnioHasta_changed(self,widget,*args):

        iter = self.cmbAnioHasta.get_active_iter()
        model = self.cmbAnioHasta.get_model()
        row = model.get_path(iter)
        self.anioHasta = model[row][0]
        print self.anioHasta

    def on_cmbLibro_changed(self,widget,*args):
        iter = self.cmbLibro.get_active_iter()
        model = self.cmbLibro.get_model()
        row = model.get_path(iter)
        self.libro = model[row][0]
        print self.libro
        #self.hbFecus.hide()
        """self.Nivel=None
        self.spnNivel.set_text('')

        if self.libro == 'LIBRO DIARIO':
            self.hbEstadoComprobante.show()
            self.hbNivel.hide()
        elif self.libro=='LIBRO MAYOR':
            self.hbEstadoComprobante.hide()
            self.hbNivel.hide()
        elif self.libro=='LIBRO CAJA Y BANCOS':
            self.hbEstadoComprobante.hide()
            self.hbNivel.hide()
        elif self.libro=='BALANCE':
            self.hbEstadoComprobante.hide()
            self.hbNivel.show_all()
        elif self.libro=='FECU':
            self.hbEstadoComprobante.hide()
            #self.hbFecus.show_all()
            self.hbNivel.hide()"""

    def mascara_niveles(self,entrycuenta):
        if not entrycuenta.get_text().replace(".","").isdigit():
            entrycuenta.grab_focus()
            return
        sql=strSelectEmpresaNivel %(self.cod_empresa,'')
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            return
        mask=''
        digitos=0
        cuenta_contable=entrycuenta.get_text().replace(".","")
        inicio=0
        digitos=0
        for i in r:
            mask+=cuenta_contable[inicio:i[0]+inicio]+"."
            inicio=inicio+i[0]
        return mask[0:len(mask)-1]

if __name__ == '__main__':
    cnx = connect("dbname=scc")
##    cnx.autocommit()
    sys.excepthook = debugwindow.show
    d = wnLibrosElectronicos(cnx)

    gtk.main()
