#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnTipoNumeracion -- formulario de ingreso, eliminación y edición, de los distintos tipos de Numeraciones Contables.

# (C)    José Luis Álvarez Morales    2006, 2007.
#           josealvarezm@gmail.com

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
from strSQL import strSelectTipoNumeracion

(CODIGO,
 DESCRIPCION) = range(2)

class wnTipoNumeracion(GladeConnect):
    def __init__(self, conexion=None, padre=None, root="wnTipoNumeracion"):
        GladeConnect.__init__(self, "glade/wnTipoNumeracion.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnTipoNumeracion.maximize()
            self.frm_padre = self.wnTipoNumeracion
            self.padre
        else:
            self.frm_padre = padre.frm_padre
            
        self.crea_columnas()
        self.carga_datos()
    
    def crea_columnas(self):
        self.modelo=gtk.ListStore(str,str)
        columnas = []
        columnas.append([CODIGO, "Numeración", "str"])
        columnas.append([DESCRIPCION, "Descripción Numeración", "str"])      
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeTipoNumeracion)
        self.carga_datos()
    def carga_datos(self):
        self.cursor.execute(strSelectTipoNumeracion)
        r=self.cursor.fetchall()
        self.modelo.clear()
        for i in r:
            self.modelo.append(i)
    
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgTipoNumeracion(self.cnx, self.frm_padre, False)
        dlg.editando = False
        band = False
        while not band:
            response = dlg.dlgTipoNumeracion.run()
            if response == gtk.RESPONSE_OK:
                if dlg.response != None:
                    self.modelo.append(dlg.response)
                    band = True
            else:
                band = True
        dlg.dlgTipoNumeracion.destroy()
            
    def on_btnQuitar_clicked(self, btn=None):
        model, it = self.treeTipoNumeracion.get_selection().get_selected()
        if model is None or it is None:
            return
        codigo = model.get_value(it, CODIGO)
        descripcion = model.get_value(it, DESCRIPCION)
        
        if dialogos.yesno("¿Desea eliminar el Tipo de Numeración <b>%s</b>?\nEsta acción no se puede deshacer!!!"%descripcion, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'cod_numeracion':codigo}
            sql = ifd.deleteFromDict('ctb.tipo_numeracion', llaves)
            
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()[1]
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>\nEl Tipo de Numeración <b>%s</b>, se encuentra relacionado.")
                
    def on_btnPropiedades_clicked(self, btn=None):
        model, it = self.treeTipoNumeracion.get_selection().get_selected()
        if model is None or it is None:
            return
        
        dlg = dlgTipoNumeracion(self.cnx, self.frm_padre, False)
        dlg.entCodigo.set_text(model.get_value(it, CODIGO))
        dlg.entDescripcion.set_text(model.get_value(it, DESCRIPCION))
        
        dlg.entCodigo.set_sensitive(False)
        
        dlg.editando = True
        band=False
        while not band:            
            response = dlg.dlgTipoNumeracion.run()
            if response == gtk.RESPONSE_OK:
                if dlg.commit==True:
                    del model[it]
                    self.modelo.append(dlg.response)
                    band=True
            else:
                band=True
        dlg.dlgTipoNumeracion.destroy()
            
    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Tipo_Numeración")
            
    def on_treeTipoNumeracion_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()
        
class dlgTipoNumeracion(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=None):
        GladeConnect.__init__(self, "glade/wnTipoNumeracion.glade", "dlgTipoNumeracion")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.editando = editando
        self.entCodigo.grab_focus()
        self.response = None
        self.dlgTipoNumeracion.show_all()
        self.commit=False
    def on_btnAceptar_clicked(self, btn=None):
        if self.entCodigo.get_text() == "" or len(self.entCodigo.get_text()) > 1:
            dialogos.error("El campo <b>Código Numeración</b> no puede estar vacío o exceder el largo de un caracter.") 
            self.entCodigo.grab_focus()
            return
        
        if self.entDescripcion.get_text() == "" or len(self.entDescripcion.get_text().replace(" ",""))==0:
            dialogos.error("El campo <b>Descripción Numeración</b> no puede estar vacío.")
            self.entDescripcion.grab_focus()
            return
        
        campos = {}
        llaves = {}
        
        campos ['descripcion_numeracion'] = self.entDescripcion.get_text().upper()  
        
        if not self.editando:
            campos ['cod_numeracion'] = self.entCodigo.get_text().upper()  
            sql = ifd.insertFromDict("ctb.tipo_numeracion", campos)
        else:
            llaves['cod_numeracion'] = self.entCodigo.get_text()
            sql, campos = ifd.updateFromDict("ctb.tipo_numeracion", campos, llaves)
        
        try:
            self.cursor.execute(sql,campos)
            self.dlgTipoNumeracion.hide()
            
            self.response = [self.entCodigo.get_text().upper(),
                                    self.entDescripcion.get_text().upper()]
            self.commit=True
        except:
            print sys.exc_info()[1]
            cod_numeracion = self.entCodigo
            dialogos.error("En la Base de Datos ya existe el Tipo Numeración <b>%s</b>"%campos['cod_numeracion'])
            self.entCodigo.grab_focus()
            
                
    def on_btnCancelar_clicked(self, btn=None):
        self.dlgTipoNumeracion.hide()
        
if __name__ == '__main__':
    cnx = connect("host=localhost dbname=scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    d = wnTipoNumeracion(cnx)
    
    gtk.main()
