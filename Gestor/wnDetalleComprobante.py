#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnDetalleComprobante -- formulario de ingreso, modificación, eliminación de DetalleComprobantes.
# (C)   José Luis Álvarez Morales 2006
#           jalvarez@galilea.cl

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
import comunes
from strSQL import strSelectDetalleComprobante

(CODIGO,
 NUM_CUENTA,
 DETALLE_CUENTA,
 CODIGO_EMPRESA,
 DESCRIPCION_EMPRESA,
 CODIGO_DOCUMENTO,
 MONTO_DEBE,
 MONTO_HABER,
 GLOSA,
 FOLIO_DOCUMENTO,
 FECHA_DOCUMENTO,
 CODIGO_DOC_CONTABLE,
 DESCRIPCION_DOCUMENTO,
 RUT,
 DESCRIPCION_FICHA,
 NUMERO_LINEA,
 CONCILIADO) = range(17)

class wnDetalleComprobante(GladeConnect):
    def __init__(self, conexion=None, padre=None, root="wnDetalleComprobante"):
        GladeConnect.__init__(self, "glade/wnDetalleComprobante.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnDetalleComprobante.maximize()
            self.frm_padre = self.wnDetalleComprobante
            self.padre
        else:
            self.frm_padre = padre.frm_padre
            
        self.crea_columnas()
        self.carga_datos()
    
    def crea_columnas(self):
      
        columnas = []
        columnas.append([CODIGO, "Código", "str"])
        columnas.append([DETALLE_CUENTA, "Detalle Cuenta", "str"])
        columnas.append([DESCRIPCION_EMPRESA, "Empresa", "str"])
        columnas.append([CODIGO_DOCUMENTO, "Código Documento", "dte"])
        columnas.append([MONTO_DEBE, "Monto Debe", "str"])
        columnas.append([MONTO_HABER, "Monto Haber", "str"])
        columnas.append([GLOSA, "Glosa", "str"])
        columnas.append([FOLIO_DOCUMENTO, "Folio Documento", "str"])
        columnas.append([FECHA_DOCUMENTO, "Fecha Documento", "str"])
        columnas.append([DESCRIPCION_DOCUMENTO, "Descripción Documento", "str"])
        columnas.append([DESCRIPCION_FICHA, "Ficha", "str"])
        columnas.append([NUMERO_LINEA, "Número Línea", "str"])
        columnas.append([CONCILIADO, "Conciliado", "str"])
                
        self.carga_datos()
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeDetalleComprobante)
        
    def carga_datos(self):
        self.modelo = ifd.ListStoreFromSQL(self.cnx, strSelectDetalleComprobante)
        self.treeDetalleComprobante.set_model(self.modelo)
    
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgDetalleComprobante(self.cnx, self.frm_padre, False)
        dlg.editando = False
        response = dlg.dlgDetalleComprobante.run()
        if response == gtk.RESPONSE_OK:
            self.carga_datos()
            
    def on_btnQuitar_clicked(self, btn=None):
        model, it = self.treeDetalleComprobante.get_selection().get_selected()
        if model is None or it is None:
            return
        codigo = model.get_value(it, CODIGO)
        #codigo_comprobante
        cuenta = model.get_value(it, DETALLE_CUENTA)
        #número_cuenta
        empresa = model.get_value(it, DESCRIPCION_EMPRESA)
        #empresa
        
        if dialogos.yesno("¿Desea eliminar Detalle Comprobante <b>%s</b>, con Número Cuenta <b>%s</b>?\nEsta acción no se puede deshacer!"%codigo %cuenta, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'cod_comprobante':codigo}
            sql = ifd.deleteFromDict('ctb.detalle_comprobante', llaves)
            
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()[1]
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>")
                
    def on_btnPropiedades_clicked(self, btn=None):
        model, it = self.treeDetalleComprobante.get_selection().get_selected()
        if model is None or it is None:
            return
                 
        dlg = dlgDetalleComprobante(self.cnx, self.frm_padre, False)
        dlg.entCodigo.set_text(model.get_value(it, CODIGO))
        dlg.numero_cuenta = dlg.pecNumCuenta.select(model.get_value(it, DETALLE_CUENTA),1)
        dlg.codigo_empresa = dlg.pecEmpresa.select(model.get_value(it, DESCRIPCION_EMPRESA),1)
        dlg.spnNumero.set_value(float(model.get_value(it, CODIGO_DOCUMENTO)))
        dlg.entDebe.set_text(model.get_value(it, MONTO_DEBE))
        dlg.entHaber.set_text(model.get_value(it, MONTO_HABER))
        dlg.entGlosa.set_text(model.get_value(it, GLOSA))
        dlg.entFolio.set_text(model.get_value(it, FOLIO_DOCUMENTO))
        
        fecha = comunes.GetDateFromModel(model.get_value(it, FECHA_DOCUMENTO).split()[0])
        dlg.entFecha.set_date(fecha)
        
        dlg.codigo_documento = dlg.pecTipoDocumentoContable.select(model.get_value(it, DESCRIPCION_DOCUMENTO),1)
        dlg.codigo_ficha = dlg.pecFicha.select(model.get_value(it, DESCRIPCION_FICHA),1)
        dlg.entNumero.set_text(model.get_value(it, NUMERO_LINEA))
        dlg.entConciliado.set_text(model.get_value(it, CONCILIADO))
        
        dlg.editando = True
        response = dlg.dlgDetalleComprobante.run()
        if response == gtk.RESPONSE_OK:
            self.modelo.clear()
            self.carga_datos()
            
    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Detalle_Comprobante")
            
    def on_treeDetalleComprobante_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()
        
class dlgDetalleComprobante(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=None):
        GladeConnect.__init__(self, "glade/wnDetalleComprobante.glade", "dlgDetalleComprobante")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.editando = editando
        self.entCodigo.grab_focus()
        self.dlgDetalleComprobante.show_all()
        
        self.numero_cuenta = None
        self.codigo_empresa = None
        self.codigo_documento = None
        self.codigo_ficha = None
        
        self.pecNumCuenta = completion.CompletionNumCuenta(self.entNumCuenta,
                self.sel_cuenta,
                self.cnx)
        
        self.pecEmpresa = completion.CompletionEmpresa(self.entEmpresa,
                self.sel_empresa,
                self.cnx)
        
        self.pecTipoDocumentoContable = completion.CompletionTipoDocumentoContable(self.entDocumento,
                self.sel_documento,
                self.cnx)
       
        self.pecFicha = completion.CompletionFicha(self.entRut,
                self.sel_ficha,
                self.cnx)
        
    def sel_cuenta(self, completion, model, it):
        self.numero_cuenta = model.get_value(it,1)
        
    def sel_empresa(self, completion, model, it):
        self.codigo_empresa = model.get_value(it,1)
        
    def sel_documento(self, completion, model, it):
        self.codigo_documento = model.get_value(it,1)
        
    def sel_documento(self, completion, model, it):
        self.codigo_ficha = model.get_value(it, 1)
        
    def on_btnAceptar_clicked(self, btn=None):
        if self.entCodigo.get_text() == "":
            dialogos.error("El campo <b>Código Comprobante</b> no puede estar vacío.")
            return
        
        if self.entNumCuenta.get_text() == "":
            dialogos.error("El campo <b>Número Cuenta</b> no puede estar vacío")
            return
        
        if self.entEmpresa.get_text() == "":
            dialogos.error("El campo <b>Empresa</b> no puede estar vacío.")
            return
        
        if self.entNumero.get_text() == "":
            dialogos.error("El campo <b>Número Línea</b> no puede estar vacío.")
            return
 
        campos = {}
        llaves = {}
        
        campos ['cod_comprobante'] = self.entCodigo.get_text().upper()
        campos ['num_cuenta'] = self.numero_cuenta
        campos ['cod_empresa'] = self.codigo_empresa
        campos ['cod_documento'] = self.spnNumero.get_value()
        campos ['monto_debe'] = self.codigo_empresa
        campos ['monto_haber'] = self.entHaber.get_text().upper()
        campos ['glosa'] = self.entGlosa.get_text().upper()
        campos ['folio_documento'] = self.entFolio.get_text().upper()
        campos ['fecha_doc'] = self.entRetencion.get_text().upper()
        campos ['cod_doc_contable'] = self.codigo_documento
        campos ['rut'] = self.codigo_ficha
        campos ['num_linea'] = self.entNumero.get_text().upper()
        campos ['conciliado'] = self.entConciliado.get_text().upper()
   
        if not self.editando:
            sql = ifd.insertFromDict("ctb.DetalleComprobante", campos)
            
        else:
            llaves['cod_DetalleComprobante'] = self.entCodigo.get_text()
            sql, campos = ifd.updateFromDict("ctb.DetalleComprobante", campos, llaves)  
            
        self.cursor.execute(sql, campos)
        self.dlgDetalleComprobante.hide()
        
    def on_btnCancelar_clicked(self, btn=None):
        self.dlgDetalleComprobante.hide()
        
if __name__ == '__main__':
    cnx = connect("host=localhost dbname=scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    d = wnDetalleComprobante(cnx)
    
    gtk.main()
