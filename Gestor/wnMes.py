#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnMes -- formulario de ingreso, eliminación y edición, de los distintos Meses.
# (C)    José Luis Álvarez Morales 2006
#           jalvarez@galilea.cl

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
from strSQL import strSelectMes

(CODIGO,
 DESCRIPCION) = range(2)

class wnMes(GladeConnect):
    def __init__(self, conexion=None, padre=None, root="wnMes"):
        GladeConnect.__init__(self, "glade/wnMes.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnMes.maximize()
            self.frm_padre = self.wnMes
            self.padre
        else:
            self.frm_padre = padre.frm_padre
            
        self.crea_columnas()
        self.carga_datos()
    
    def crea_columnas(self):
        columnas = []
        columnas.append([CODIGO, "Código Mes", "int"])
        columnas.append([DESCRIPCION, "Descripción Mes", "str"])
        
        self.carga_datos()
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeMes)
        
    def carga_datos(self):
        self.modelo = ifd.ListStoreFromSQL(self.cnx, strSelectMes)
        self.treeMes.set_model(self.modelo)
    
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgMes(self.cnx, self.frm_padre, False)
        dlg.editando = False
        response = dlg.dlgMes.run()
        if response == gtk.RESPONSE_OK:
            self.carga_datos()
            
    def on_btnQuitar_clicked(self, btn=None):
        model, it = self.treeMes.get_selection().get_selected()
        if model is None or it is None:
            return
        codigo = model.get_value(it, CODIGO)
        descripcion = model.get_value(it, DESCRIPCION)
        
        if dialogos.yesno("¿Desea eliminar el Mes <b>%s</b>?\nEsta acción no se puede deshacer"%descripcion, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'cod_mes':codigo}
            sql = ifd.deleteFromDict('ctb.mes', llaves)
            
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()[1]
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>\nEl Mes <b>%s</b>, se encuentra relacionado.")
                
    def on_btnPropiedades_clicked(self, btn=None):
        model, it = self.treeMes.get_selection().get_selected()
        if model is None or it is None:
            return
        
        dlg = dlgMes(self.cnx, self.frm_padre, False)
        dlg.entCodigo.set_text(model.get_value(it, CODIGO))
        dlg.entDescripcion.set_text(model.get_value(it, DESCRIPCION))
        
        dlg.editando = True
        response = dlg.dlgMes.run()
        if response == gtk.RESPONSE_OK:
            self.modelo.clear()
            self.carga_datos()
            
    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Mes")
            
    def on_treeMes_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()
        
class dlgMes(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=None):
        GladeConnect.__init__(self, "glade/wnMes.glade", "dlgMes")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        
        self.editando = editando
        self.entCodigo.grab_focus()
        self.dlgMes.show_all()
        
    def on_btnAceptar_clicked(self, btn=None):
        if self.entCodigo.get_text() == "":
            dialogos.error("El campo <b>Código Mes</b> no puede estar vacío.")
        
        if self.entDescripcion.get_text() == "":
            dialogos.error("El campo <b>Descripción Mes</b> no puede estar vacío.")
            return
        
        campos = {}
        llaves = {}
        campos ['cod_mes'] = self.entCodigo.get_text().upper()
        campos ['descripcion_mes'] = self.entDescripcion.get_text().upper()  
        
        if not self.editando:
            sql = ifd.insertFromDict("ctb.mes", campos)
        else:
            llaves['cod_mes'] = self.entCodigo.get_text()
            sql, campos = ifd.updateFromDict("ctb.mes", campos, llaves)
            
        self.cursor.execute(sql, campos)
        self.dlgMes.hide()
        
    def on_btnCancelar_clicked(self, btn=None):
        self.dlgMes.hide()
        
if __name__ == '__main__':
    cnx = connect("host=192.168.0.105 dbname=scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    d = wnMes(cnx)
    
    gtk.main()
