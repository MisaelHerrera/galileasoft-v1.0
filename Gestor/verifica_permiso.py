#!/usr/bin/env python
# -*- coding: utf-8 -*-
#~ from coneccion_pg import *
from dialogos import dlgError
import gettext; _ = gettext.gettext 
import sys

def VerificaPermiso(self, NumFuncion,muestra = True):

    if self.cursor == None:
        self.cursor = self.cnx.cursor()
    try:
        sql ="select * from ctb.cargo_funcion  where cod_funcion = " + str(NumFuncion) + " and cod_cargo = " + str(sys.__getattribute__('cod_cargo'))
    except:
        sql ="select * from ctb.cargo_funcion join ctb.usuario using (cod_cargo) where cod_funcion = " + str(NumFuncion) + " and login_usuario= current_user" 
    self.cursor.execute(sql)
    r = self.cursor.fetchall()
    if len(r) == 0:
        if muestra:
            dlgError(self.ventana, "Usted no está habilitado para realizar esta operación")
        return False
    else:
        return True
