#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnHonorario -- formulario de ingreso, eliminacion y edición, de los distintos Honorarios contables.
# (C)    José Luis Álvarez Morales 2006
#           jalvarez@galilea.cl

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
from strSQL import strSelectHonorario

(CODIGO,
  CODIGO_EMPRESA,
  DESCRIPCION_EMPRESA,
  CODIGO_COMPROBANTE,
  FECHA_BOLETA,
  RUT_FICHA,
  DESCRIPCION_FICHA,
  MONTO_HONORARIO,
  CUENTA_HONORARIO,
  DESCRIPCION_CUENTA,
  MONTO_RETENCION,
  MONTO_LIQUIDO) = range(12) 

class wnHonorario (GladeConnect):
    def __init__ (self, conexion = None, padre= None, root="wnHonorario"):
        GladeConnect.__init__ (self, "glade/wnHonorario.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnHonorario.maximize()
            self.frm_padre = self.wnHonorario
            self.padre
        else:
            self.frm_padre = self.padre.frm_padre

        self.crea_columnas()
        self.carga_datos()

    def crea_columnas (self):
        columnas = []
        columnas.append([CODIGO, "Código", "str"])
        columnas.append([DESCRIPCION_EMPRESA,"Nombre Empresa", "str"])
        columnas.append([CODIGO_COMPROBANTE, "Código Comprobante", "str"])
        columnas.append([FECHA_BOLETA, "Fecha Boleta", "str"])
        columnas.append([DESCRIPCION_FICHA, "Ficha", "str"])
        columnas.append([MONTO_HONORARIO, "MontoHonorario", "str"])
        columnas.append([DESCRIPCION_CUENTA, "Número de Cuenta", "str"])
        columnas.append([MONTO_RETENCION, "Monto Retención", "str"])
        columnas.append([MONTO_LIQUIDO, "Monto Líquido", "str"])
    
        self.carga_datos()
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeHonorario)
        
    def carga_datos(self):
        self.modelo = ifd.ListStoreFromSQL(self.cnx, strSelectHonorario)
        self.treeHonorario.set_model(self.modelo)
    
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgHonorario(self.cnx, self.frm_padre, False)
        dlg.editando =False
        response = dlg.dlgHonorario.run()
        if response == gtk.RESPONSE_OK:
            self.carga_datos()
            
    def on_btnQuitar_clicked (self, btn=None):
        model, it = self.treeHonorario.get_selection().get_selected()
        if model is None or it is None:
            return
        
        descripcion = model.get_value(it, CODIGO_COMPROBANTE)
        codigo = model.get_value(it, CODIGO)
        
        if dialogos.yesno("Desea eliminar el Honorario, <b>%s</b>\nEsta acción no se puede deshacer!"% descripcion, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'cod_honorario':codigo}
            sql = ifd.deleteFromDict("ctb.honorario", llaves)
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()[1]
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>.")
    
    def on_btnPropiedades_clicked (self, btn=None):
        model, it = self.treeHonorario.get_selection().get_selected()
        if model is None or it is None:
            return
        dlg = dlgHonorario(self.cnx, self.frm_padre, False)
      
        dlg.entCodigo.set_text(model.get_value(it, CODIGO))
        dlg.codigo_empresa = dlg.pecEmpresa.select(model.get_value(it, DESCRIPCION_EMPRESA),1)
        dlg.codigo_mes = dlg.pecMes.select(model.get_value(it, DESCRIPCION_MES),1)
        dlg.entDescripcion.set_text(model.get_value(it, DESCRIPCION_PERIODO))
        dlg.codigo_estado = dlg.pecEstado.select(model.get_value(it, DESCRIPCION_ESTADO),1)
        
        dlg.editando= True
        response = dlg.dlgHonorario.run()
        if response == gtk.RESPONSE_OK:
            self.modelo.clear()
            self.carga_datos()
        
    def on_btnCerrar_clicked (self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Honorario")
    
    def on_treeHonorario_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()


class dlgHonorario(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=None):
        GladeConnect.__init__(self, "glade/wnHonorario.glade", "dlgHonorario")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.editando = editando
        self.entPeriodo.grab_focus()
        self.dlgHonorario.show_all()
        
        self.codigo_empresa = None
        self.codigo_mes = None
        self.codigo_estado = None
        
        self.pecEmpresa = completion.CompletionEmpresa(self.entEmpresa,
                self.sel_empresa,
                self.cnx)
        
        self.pecMes = completion.CompletionMes(self.entMes,
                self.sel_mes,
                self.cnx)
        
        self.pecEstado = completion.CompletionEstado(self.entEstado,
                self.sel_estado,
                self.cnx)
        
                
    def sel_empresa(self, completion, model, it):
        self.codigo_empresa = model.get_value(it,1)
        
    def sel_mes(self, completion, model, it):
        self.codigo_mes = model.get_value(it,1)
        
    def sel_estado(self, completion, model, it):
        self.codigo_estado = model.get_value(it,1) 
    
    def on_btnAceptar_clicked(self, btn=None):
        if self.entPeriodo.get_text() == "":
            dialogos.error ("El Campo <b>Periodo</b> no puede estar vacío.")
            return
        
        if self.entEmpresa.get_text() == "":
            dialogos.error ("El Campo <b>Empresa</b> no puede estar vacío.")
            return
        
        if self.entMes.get_text() == "":
            dialogos.error ("El Campo <b>Mes</b> no puede estar vacío.")
            return
        
        if self.entDescripcion.get_text() == "":
            dialogos.error ("El Campo <b>Descripción</b> no puede estar vacío.")
            return
        
        if self.entEstado.get_text() == "":
            dialogos.error ("El Campo <b>Estado</b> no puede estar vacío.")
            return
        
        campos = {}
        llaves = {}
        campos['periodo'] = self.entPeriodo.get_text().upper()
        campos ['cod_empresa']=self.codigo_empresa
        campos ['mes']=self.codigo_mes
        campos ['descripcion_periodo']=self.entDescripcion.get_text().upper()
        campos['estado'] = self.codigo_estado    
        
        if not self.editando:
            sql = ifd.insertFromDict("ctb.periodo_contable", campos)
            
        else:
#            que pasa si las llaves primarias son tres: periodo, mes, cod_empresa??? Qué llave utilizo???
            llaves['periodo'] = self.entPeriodo.get_text()
            sql, campos = ifd.updateFromDict("ctb.periodo_contable", campos, llaves)
            
        self.cursor.execute(sql, campos)
        self.dlgHonorario.hide()
        
    def on_btnCancelar_clicked (self, btn=None):
        self.dlgHonorario.hide()
        

if __name__ == "__main__":
    cnx = connect("host=localhost dbname = scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    w = wnHonorario(cnx)
    
    gtk.main()
