#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnTipoCuenta -- formulario de ingreso, eliminación y edición, de los distintos Tipos de Cuentas contables.

# (C)    José Luis Álvarez Morales 2006
#           josealvarezm@gmail.com

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
from strSQL import strSelectTipoCuenta

(CODIGO,
 DESCRIPCION) = range(2)

class wnTipoCuenta(GladeConnect):
    def __init__(self, conexion=None, padre=None, root="wnTipoCuenta"):
        GladeConnect.__init__(self, "glade/wnTipoCuenta.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnTipoCuenta.maximize()
            self.frm_padre = self.wnTipoCuenta
            self.padre
        else:
            self.frm_padre = padre.frm_padre
            
        self.crea_columnas()
        self.carga_datos()
    
    def crea_columnas(self):
        columnas = []
        columnas.append([CODIGO, "Código", "int"])
        columnas.append([DESCRIPCION, "Descripción Cuenta", "str"])
        
        self.carga_datos()
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeTipoCuenta)
        
    def carga_datos(self):
        self.modelo = ifd.ListStoreFromSQL(self.cnx, strSelectTipoCuenta)
        self.treeTipoCuenta.set_model(self.modelo)
    
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgTipoCuenta(self.cnx, self.frm_padre, False)
        dlg.editando = False
        band = False
        while not band:
            response = dlg.dlgTipoCuenta.run()
            if response == gtk.RESPONSE_OK:
                if dlg.resp ==True:
                    self.modelo.append(dlg.response)
                    band = True
            else:
                band = True
        dlg.dlgTipoCuenta.destroy()
                        
    def on_btnQuitar_clicked(self, btn=None):
        model, it = self.treeTipoCuenta.get_selection().get_selected()
        if model is None or it is None:
            return
        codigo = model.get_value(it, CODIGO)
        descripcion = model.get_value(it, DESCRIPCION)
        
        if dialogos.yesno("¿Desea eliminar el Tipo de Cuenta <b>%s</b>?\nEsta acción no se puede deshacer!!!"%descripcion, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'cod_tipo_cuenta':codigo}
            sql = ifd.deleteFromDict('ctb.tipo_cuenta', llaves)
            
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()[1]
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>\nEl Tipo de Cuenta <b>%s</b>, se encuentra relacionado.")
                
    def on_btnPropiedades_clicked(self, btn=None):
        model, it = self.treeTipoCuenta.get_selection().get_selected()
        if model is None or it is None:
            return
        
        dlg = dlgTipoCuenta(self.cnx, self.frm_padre, False)
        dlg.entCodigo.set_text(model.get_value(it, CODIGO))
        dlg.entDescripcion.set_text(model.get_value(it, DESCRIPCION))
        
        dlg.entCodigo.set_sensitive(False)
        
        dlg.editando = True
        band=False
        while not band:
            response = dlg.dlgTipoCuenta.run()
            if response == gtk.RESPONSE_OK:
                if dlg.resp==True:
                    del model[it]
                    self.modelo.append(dlg.response)
                    band=True
            else:
                band=True
        dlg.dlgTipoCuenta.destroy()
                        
    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Tipo_de_Cuenta")
            
    def on_treeTipoCuenta_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()
        
class dlgTipoCuenta(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=None):
        GladeConnect.__init__(self, "glade/wnTipoCuenta.glade", "dlgTipoCuenta")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        
        self.editando = editando
        self.response = None
        self.entCodigo.grab_focus()
        self.dlgTipoCuenta.show_all()
        self.resp=False
    def on_btnAceptar_clicked(self, btn=None):
        if self.entCodigo.get_text() == "" or len(self.entCodigo.get_text()) > 1:
            dialogos.error("El campo <b>Código</b> no puede estar vacío o exceder el largo de un caracter.")
            return        
        
        if self.entDescripcion.get_text() == "" or len(self.entDescripcion.get_text().replace(" ",""))==0:
            dialogos.error("El campo <b>Descripción Tipo Cuenta</b> no puede estar vacío.")
            return
        
        campos = {}
        llaves = {}
        
        campos ['descripcion_tipo_cuenta'] = self.entDescripcion.get_text().upper()  
        
        if not self.editando:
            campos ['cod_tipo_cuenta'] = self.entCodigo.get_text().upper()
            sql = ifd.insertFromDict("ctb.tipo_cuenta", campos)
        else:
            llaves['cod_tipo_cuenta'] = self.entCodigo.get_text().upper()
            sql, campos = ifd.updateFromDict("ctb.tipo_cuenta", campos, llaves)
                
        try:
            self.cursor.execute(sql,campos)
            self.dlgTipoCuenta.hide()
            self.resp=True
            self.response = [self.entCodigo.get_text().upper(),
                                    self.entDescripcion.get_text().upper()]
        
        except:
            print sys.exc_info()[1]
            cod_tipo_cuenta = self.entCodigo
            dialogos.error("En la Base de Datos ya existe el Tipo de Cuenta <b>%s</b>."%campos['cod_tipo_cuenta'])
            self.entCodigo.grab_focus()
       
    def on_btnCancelar_clicked(self, btn=None):
        self.dlgTipoCuenta.hide()
        
if __name__ == '__main__':
    cnx = connect("host=localhost dbname=scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    d = wnTipoCuenta(cnx)
    
    gtk.main()
