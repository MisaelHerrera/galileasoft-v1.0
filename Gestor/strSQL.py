strSelectFicha = """SELECT
                        f.rut,
                        f.nombre,
                        f.tipo_ficha,
                        tf.descripcion_tipo_ficha,
                        case when f.direccion!='null' then f.direccion else '' end,
                        case when f.telefono!='null' then f.telefono else '' end,
                        f.cod_empresa,
                        e.descripcion_empresa,
                        f.cod_banco,
                        b.nombre_banco,
                        case when f.numero_cuenta!='null' then f.numero_cuenta else '' end
                FROM ctb.ficha f
                JOIN ctb.tipo_ficha tf
                on f.tipo_ficha = tf.cod_tipo_ficha
                JOIN ctb.empresa e
                on f.cod_empresa = e.cod_empresa
                LEFT JOIN ctb.banco b
                on f.cod_banco = b.cod_banco
                %s
                ORDER BY f.rut"""

strSelectTipoFicha = """SELECT
                            cod_tipo_ficha,
                            descripcion_tipo_ficha
                    FROM ctb.tipo_ficha
                    ORDER BY cod_tipo_ficha"""

strSelectTipoAnalisisCuentaContable = """select tipo_analisis from ctb.cuenta_contable
                    where cod_empresa = %s and num_cuenta = '%s'"""

strSelectEmpresa = """SELECT
                            e.cod_empresa,
                            e.descripcion_empresa,
                            e.tipo_numeracion,
                            t.descripcion_numeracion,
                            e.cod_mes_cierre,
                            m.descripcion_mes,
                            e.nivel_cuenta,
                            e.cod_moneda,
                            c.descripcion_moneda,
                            e.rut_empresa,
                            e.direccion_empresa,
                            e.periodos_abiertos,
                            e.cuenta_retencion,
                            v.descripcion_cuenta,
                            e.cuenta_liquido,
                            l.descripcion_cuenta,
                            e.telefono_empresa
                    FROM ctb.empresa e
                    left JOIN ctb.tipo_numeracion t
                    ON e.tipo_numeracion = t.cod_numeracion
                    left JOIN ctb.mes m
                    ON e.cod_mes_cierre = m.cod_mes
                    left JOIN ctb.moneda c
                    ON e.cod_moneda = c.cod_moneda
                    left JOIN ctb.cuenta_contable v 
                    ON e.cuenta_retencion = v.num_cuenta and e.cod_empresa=v.cod_empresa
                    left JOIN ctb.cuenta_contable l
                    ON e.cuenta_liquido = l.num_cuenta and e.cod_empresa = l.cod_empresa
                    %s
                    ORDER BY e.cod_empresa"""

strSelectEmpresaNivel = """ SELECT
                                    digitos,
                                    cod_nivel
                            FROM ctb.nivel_cuenta 
                            WHERE cod_empresa = %s%s  ORDER BY cod_nivel """

strSelectTipoDocumento = """SELECT
                                    cod_doc_contable,
                                    descripcion_documento,
                                    maneja_saldo
                            FROM ctb.tipo_doc_contable
                            ORDER BY cod_doc_contable"""

strSelectTipoDocContable = """SELECT
                                    False,
                                    cod_doc_contable,
                                    descripcion_documento                                 
                            FROM ctb.tipo_doc_contable
                            ORDER BY cod_doc_contable"""
                            
strSelectFamilia = """SELECT f.codigo_familia,
                             f.descripcion_familia,
                             f.num_cuenta_compra,
                             cc.descripcion_cuenta as cuenta_compra,
                             f.num_cuenta_venta,
                             cv.descripcion_cuenta as cuenta_venta,
                             f.impuesto,
                             f.porcentaje_impuesto,
                             f.cod_empresa
                      from inventario.familia f
                      join ctb.cuenta_contable cc
                      on f.num_cuenta_compra = cc.num_cuenta
                      join ctb.cuenta_contable cv
                      on f.num_cuenta_compra = cv.num_cuenta
                      order by f.descripcion_familia"""
                      
strSelectMoneda =   """SELECT
                            cod_moneda,
                            descripcion_moneda,
                            simbolo_moneda,
                            decimales,
                            ''
                    FROM ctb.moneda 
                    ORDER BY cod_moneda"""

strSelectTipoResultado1 = """ SELECT
                                    e.cod_tipo_resultado,
                                    e.descripcion_resultado,
                                    e.cod_empresa,
                                    d.descripcion_empresa
                            FROM ctb.tipo_resultado e
                            JOIN ctb.empresa d
                            on e.cod_empresa = d.cod_empresa
                            ORDER BY cod_tipo_resultado"""

strSelectEstadoPContable = """ SELECT
                                            cod_estado,
                                            descripcion_estado
                                    FROM ctb.estado_periodo_contable
                                    ORDER BY cod_estado"""

strSelectTipoNumeracion = """ SELECT
                                            cod_numeracion,
                                            descripcion_numeracion
                                    FROM ctb.tipo_numeracion
                                    ORDER BY cod_numeracion"""

strSelectTipoCuenta = """ SELECT
                                        cod_tipo_cuenta,
                                        descripcion_tipo_cuenta
                                FROM ctb.tipo_cuenta
                                """

strSelectMes = """ SELECT
                        cod_mes,
                        descripcion_mes
                FROM ctb.mes
                ORDER BY cod_mes"""

strSelectTipoComprobante = """ SELECT
                                        cod_tipo_comprobante,
                                        descripcion_tipo
                                FROM ctb.tipo_comprobante
                                ORDER BY cod_tipo_comprobante"""
                                
    
strSelectSistema = """ SELECT
                                cod_sistema,
                                nombre_sistema
                        FROM ctb.sistema
                        ORDER BY cod_sistema"""

strSelectBanco = """ SELECT
                                cod_banco,
                                nombre_banco,
                                cod_sbif
                        FROM ctb.banco 
                        ORDER BY cod_banco"""

strSelectSucursalBanco = """SELECT
                                e.codigo_sucursal,
                                e.descripcion_sucursal,
                                e.cod_banco,
                                m.nombre_banco
                        FROM ctb.sucursal_banco e
                        JOIN ctb.banco m
                        on e.cod_banco = m.cod_banco
                        ORDER BY e.descripcion_sucursal"""

strSelectNivelCuenta = """SELECT
                                m.cod_nivel,
                                m.cod_empresa,
                                s.descripcion_empresa,
                                m.digitos
                        FROM ctb.nivel_cuenta m
                        JOIN ctb.empresa s
                        on m.cod_empresa = s.cod_empresa
                        ORDER BY m.cod_nivel"""

strSelectPeriodoContable =  """SELECT
                                p.periodo,
                                p.cod_empresa,
                                d.descripcion_empresa,
                                p.mes,
                                m.descripcion_mes,
                                p.descripcion_periodo,
                                p.estado,
                                e.descripcion_estado
                        FROM ctb.periodo_contable p
                        JOIN ctb.empresa d
                        on p.cod_empresa = d.cod_empresa
                        JOIN ctb.mes m
                        on p.mes = m.cod_mes     
                        JOIN ctb.estado_periodo_contable e
                        on p.estado = e.cod_estado                        
                        ORDER BY p.periodo"""
                        
strSelectComprobante = """ SELECT
                                c.cod_comprobante,
                                c.cod_tipo_comprobante,
                                t.descripcion_tipo,
                                c.centralizado,
                                c.periodo,
                                p.periodo,
                                c.mes,
                                m.descripcion_mes,
                                c.estado,
                                c.origen,
                                n.nombre_sistema,
                                c.fecha,
                                c.fecha_digitacion,
                                c.debe,
                                c.haber,
                                c.glosa,
                                c.cod_empresa,
                                e.descripcion_empresa,
                                c.folio_comprobante
                        FROM ctb.comprobante c
                        JOIN ctb.tipo_comprobante t
                        on c.cod_tipo_comprobante = t.cod_tipo_comprobante
                        JOIN ctb.periodo_contable p
                        on c.periodo = p.periodo
                        JOIN ctb.mes m
                        on c.mes = m.cod_mes
                        JOIN ctb.sistema n
                        on c.origen = n.cod_sistema
                        JOIN ctb.empresa e
                        on c.cod_empresa = e.cod_empresa
                        ORDER BY c.cod_comprobante"""


strSelectComprobante2 = """ SELECT
                                c.cod_comprobante,
                                c.cod_tipo_comprobante,
                                t.descripcion_tipo,
                                c.centralizado,
                                c.periodo,
                                p.periodo,
                                c.mes,
                                m.descripcion_mes,
                                c.estado,
                                c.origen,
                                n.nombre_sistema,
                                c.fecha,
                                c.fecha_digitacion,
                                c.debe,
                                c.haber,
                                c.glosa,
                                c.cod_empresa,
                                e.descripcion_empresa,
                                c.folio_comprobante
                        FROM ctb.comprobante c
                        JOIN ctb.tipo_comprobante t
                        on c.cod_tipo_comprobante = t.cod_tipo_comprobante
                        JOIN ctb.periodo_contable p
                        on c.periodo = p.periodo
                        JOIN ctb.mes m
                        on c.mes = m.cod_mes
                        JOIN ctb.sistema n
                        on c.origen = n.cod_sistema
                        JOIN ctb.empresa e
                        on c.cod_empresa = e.cod_empresa
                        WHERE c.cod_comprobante in (%s)
                        ORDER BY c.cod_comprobante"""

#strSelectCuentaContable = """SELECT
#                                        c.num_cuenta,
#                                        c.tipo_cuenta,
#                                        t.descripcion_tipo_cuenta,
#                                        c.descripcion_cuenta,
#                                        c.cod_empresa,
#                                        d.descripcion_empresa,
#                                        c.estado_cuenta,
#                                        c.tiene_hijos,
#                                        c.nivel_cuenta,
#                                        c.estado_resultado,
#                                        c.tipo_analisis,
#                                        c.num_padre,
#                                        c.aplica_resultado,
#                                        c.cod_tipo_resultado
#                                FROM ctb.cuenta_contable c
#                                JOIN ctb.tipo_cuenta t
#                                on c.tipo_cuenta = t.cod_tipo_cuenta
#                                JOIN ctb.empresa d
#                                on c.cod_empresa = d.cod_empresa
#                                 %s
#                                ORDER BY c.num_cuenta"""
 
strPlanCuentas="""select 
                                        c.num_cuenta, 
                                        c.estado_cuenta, 
                                        c.tiene_hijos, 
                                        c.nivel_cuenta, 
                                        c.estado_resultado, 
                                        c.tipo_analisis, 
                                        c.tipo_cuenta, 
                                        c.descripcion_cuenta, 
                                        c.num_padre, 
                                        c.cod_empresa, 
                                        c.aplica_resultado, 
                                        c.cod_tipo_resultado, 
                                        t.descripcion_tipo_cuenta
                                       
                            from 
                                    ctb.cuenta_contable c
                                    inner join ctb.tipo_cuenta t
                                    on c.tipo_cuenta = t.cod_tipo_cuenta                                    
                                    where
                                    c.cod_empresa = %s %s
                                order by num_cuenta """
  
strCuentaContable="""select 
                                        c.num_cuenta, 
                                       c.estado_cuenta, 
                                       c.tiene_hijos, 
                                       c.nivel_cuenta, 
                                       c.estado_resultado, 
                                       c.tipo_analisis, 
                                       c.tipo_cuenta, 
                                       c.descripcion_cuenta, 
                                       c.num_padre, 
                                       c.cod_empresa, 
                                       c.aplica_resultado, 
                                       c.cod_tipo_resultado, 
                                       t.descripcion_tipo_cuenta
                                from 
                                        ctb.cuenta_contable c 
                                        inner join ctb.tipo_cuenta t 
                                        on c.tipo_cuenta = t.cod_tipo_cuenta \
                                        
                                where 
                                        cod_empresa = %s
                                and 
                                        num_cuenta = '%s'
                                and estado_cuenta='V'
                                order by num_cuenta"""

strCuentaContableAll="""select 
                                        c.num_cuenta, 
                                       c.estado_cuenta, 
                                       c.tiene_hijos, 
                                       c.nivel_cuenta, 
                                       c.estado_resultado, 
                                       c.tipo_analisis, 
                                       c.tipo_cuenta, 
                                       c.descripcion_cuenta, 
                                       c.num_padre, 
                                       c.cod_empresa, 
                                       c.aplica_resultado, 
                                       c.cod_tipo_resultado, 
                                       t.descripcion_tipo_cuenta
                                from 
                                        ctb.cuenta_contable c 
                                        inner join ctb.tipo_cuenta t 
                                        on c.tipo_cuenta = t.cod_tipo_cuenta \
                                        
                                where 
                                        cod_empresa = %s
                                and 
                                        num_cuenta = '%s'
                                order by num_cuenta"""

strSelectTipoCuenta =""" select * from ctb.tipo_cuenta order by descripcion_tipo_cuenta"""

strSelectCuentaContable="""select 
                                                    nivel_cuenta, count(*) 
                                            from 
                                                    ctb.cuenta_contable 
                                            where 
                                                    num_cuenta = '%s' 
                                            and 
                                                cod_empresa = %s
                                        group by 
                                                    nivel_cuenta"""
                                                    
strSelectTipoResultado = """SELECT
                                    cod_tipo_resultado,
                                    descripcion_resultado,
                                    cod_empresa
                        FROM ctb.tipo_resultado
                        where cod_empresa=%s and cod_tipo_resultado=%s"""
                                
strAnular="""update ctb.comprobante set estado='N' where cod_comprobante=%s"""

strCentralizadoComprobante="""select c.centralizado from ctb.comprobante c join ctb.sistema s on c.origen=s.cod_sistema where cod_comprobante=%s"""

strInsertDetalleComprobanteSA="""INSERT INTO ctb.detalle_comprobante 
                                                        (cod_comprobante,
                                                        num_cuenta,
                                                        cod_empresa,
                                                        monto_debe,
                                                        monto_haber,
                                                        glosa,
                                                        num_linea,
                                                        codigo_cc,
                                                        cod_sucursal,
                                                        cod_area_funcional)
                                                        VALUES(%s,'%s',%s,%s,%s,'%s',%s,%s,%s,%s)"""
                                                        
strInsertDetalleComprobanteAD="""INSERT INTO ctb.detalle_comprobante 
                                                        (cod_comprobante,
                                                        num_cuenta,
                                                        cod_empresa,
                                                        monto_debe,
                                                        monto_haber,
                                                        glosa,
                                                        serie,
                                                        folio_documento,
                                                        fecha_doc,
                                                        cod_doc_contable,
                                                        rut,
                                                        num_linea,
                                                        codigo_cc,
                                                        cod_sucursal,
                                                        cod_area_funcional) 
                                                    VALUES(%s,'%s',%s,%s,%s,'%s','%s',%s,'%s',%s,'%s',%s,%s,%s,%s)"""
                                                    
strInsertDetalleComprobanteSSA="""INSERT INTO ctb.detalle_comprobante 
                                                        (cod_comprobante,
                                                        num_cuenta,
                                                        cod_empresa,
                                                        monto_debe,
                                                        monto_haber,
                                                        glosa,
                                                        serie,
                                                        folio_documento,
                                                        fecha_doc,
                                                        cod_doc_contable,
                                                        rut,
                                                        num_linea,
                                                        codigo_cc) 
                                                    VALUES(%s,'%s',%s,%s,%s,'%s','%s',%s,'%s',%s,'%s',%s,%s)"""

strInsertDetalleComprobanteAS="""INSERT INTO ctb.detalle_comprobante 
                                                        (cod_comprobante,
                                                        num_cuenta,
                                                        cod_empresa,
                                                        monto_debe,
                                                        monto_haber,
                                                        glosa,
                                                        serie,
                                                        folio_documento,
                                                        fecha_doc,
                                                        cod_doc_contable,
                                                        num_linea,
                                                        codigo_cc,
                                                        cod_sucursal,
                                                        cod_area_funcional) 
                                                    VALUES(%s,'%s',%s,%s,%s,'%s','%s',%s,'%s',%s,%s,%s,%s,%s)"""

strSelectFolio="""SELECT 
                                        c.folio_comprobante,
                                        c.periodo,
                                        c.mes,
                                        c.cod_empresa,
                                        e.tipo_numeracion,
                                        t.cod_tipo_comprobante
                            from        
                                        ctb.comprobante c join ctb.empresa e on c.cod_empresa=e.cod_empresa 
                                        join ctb.tipo_comprobante t on t.cod_tipo_comprobante=c.cod_tipo_comprobante
                            where   
                                        c.cod_empresa=%s 
                            and 
                                        c.periodo=%s
                            and 
                                        c.mes=%s
                            and     
                                        c.folio_comprobante=%s
                            and
                                        e.tipo_numeracion='%s'
                            and     
                                        c.cod_tipo_comprobante='%s'"""
strSelectMaxFolio="""select max(folio_comprobante) +1 from ctb.comprobante c %s """
strInsertCabeceraComprobante="""INSERT into ctb.comprobante
                                                            (cod_tipo_comprobante,
                                                            cod_empresa,
                                                            periodo, 
                                                            mes,
                                                            origen,
                                                            fecha,
                                                            debe,
                                                            haber,
                                                            glosa,
                                                            folio_comprobante,
                                                            estado) 
                                                            VALUES('%s',%s,%s,%s,'%s','%s',%s,%s,'%s',%s,'%s')"""
strInsertCabeceraComprobanteN="""INSERT into ctb.comprobante
                                                            (cod_tipo_comprobante,
                                                            cod_empresa,
                                                            periodo, 
                                                            mes,
                                                            origen,
                                                            fecha,
                                                            debe,
                                                            haber,
                                                            glosa,
                                                            folio_comprobante,
                                                            estado,cod_distribucion_centro_costo) 
                                                            VALUES('%s',%s,%s,%s,'%s','%s',%s,%s,'%s',%s,'%s',%s)"""
strInsertCabeceraComprobanteNull="""INSERT into ctb.comprobante
                                                            (cod_tipo_comprobante,
                                                            cod_empresa,
                                                            periodo, 
                                                            mes,
                                                            origen,
                                                            fecha,
                                                            debe,
                                                            haber,
                                                            glosa,
                                                            folio_comprobante,
                                                            estado) 
                                                            VALUES('%s',%s,%s,%s,'%s','%s',%s,%s,'%s',%s,'%s')"""                                                        
                                                    
strSelectComprobanteM="""select 
                                                        cod_comprobante 
                                            from 
                                                        ctb.comprobante 
                                            where 
                                                    cod_tipo_comprobante = '%s'  
                                            and 
                                                    folio_comprobante = %s 
                                            and 
                                                    periodo = %s 
                                            and 
                                                    mes = %s 
                                            and 
                                                    cod_empresa =%s"""
                                                    
strSelectTipoAnalisisCuentaContable = """select tipo_analisis from ctb.cuenta_contable
                    where cod_empresa = %s and num_cuenta = '%s'"""
                    
strSelectPeriodo= """SELECT
                        descripcion_periodo,
                        mes,
                        periodo,
                        cod_empresa
                FROM
                        ctb.periodo_contable
                WHERE cod_empresa = %s
                ORDER BY
                        descripcion_periodo DESC"""
                        
strFicha="""SELECT 
                                    f.rut,
                                    f.nombre 
                    from 
                                    ctb.ficha f 
                    where 
                                    f.cod_empresa = %s
                    and
                                    f.rut='%s'"""
                                    
strCargaDatos="""select
                                            False,
                                            c.cod_comprobante,
                                            c.folio_comprobante,
                                            c.fecha,
                                            c.cod_tipo_comprobante,
                                            e.descripcion_empresa,
                                            c.origen,
                                            c.estado, 
                                            p.descripcion_periodo,
                                            c.glosa,
                                            c.debe,
                                            c.haber,
                                            e.cod_empresa,
                                            p.periodo,
                                            p.mes,
                                            p.estado,
                                            dic.desc_distribucion_centro_costo,
                                            c.cod_distribucion_centro_costo
                            from 
                                            ctb.comprobante c 
                                            join ctb.empresa e on e.cod_empresa = c.cod_empresa 
                                            join ctb.periodo_contable p on p.mes=c.mes and c.periodo=p.periodo and c.cod_empresa=p.cod_empresa
                                            left join distribucion_centro_costo dic on c.cod_distribucion_centro_costo = dic.codigo
                            where
                                            c.cod_empresa=%s %s
                            order by c.cod_tipo_comprobante,c.folio_comprobante"""
                            
strCargaComprobante="""SELECT
                                                    dc.num_linea, 
                                                    dc.num_cuenta,
                                                    c.descripcion_cuenta,
                                                    dc.monto_debe, 
                                                    dc.monto_haber, 
                                                    dc.glosa,
                                                    dc.serie,
                                                    dc.folio_documento,                                                    
                                                    dc.fecha_doc::text,
                                                    c.tipo_analisis,
                                                    tdc.descripcion_documento,
                                                    dc.rut,
                                                    f.nombre,
                                                    tdc.cod_doc_contable,
                                                    dc.codigo_cc,
                                                    dcc.desc_distribucion_centro_costo,
                                                    s.nom_sucursal,
                                                    ae.nom_area_funcional,
                                                    s.cod_sucursal,
                                                    ae.cod_area_funcional                                                    
                                            from 
                                                    (ctb.detalle_comprobante dc join ctb.cuenta_contable c on dc.num_cuenta = c.num_cuenta and dc.cod_comprobante = %s and c.cod_empresa=dc.cod_empresa) 
                                                    left join ctb.tipo_doc_contable tdc on dc.cod_doc_contable = tdc.cod_doc_contable                                                     
                                                    left join ctb.ficha f on dc.rut=f.rut and f.cod_empresa=c.cod_empresa
                                                    left join distribucion_centro_costo dcc on dcc.codigo=dc.codigo_cc
                                                    left join sucursal s on dc.cod_sucursal = s.cod_sucursal 
                                                    left join comun.area_funcional ae on dc.cod_area_funcional = ae.cod_area_funcional 
                                            order by dc.num_linea"""

strCargaComprobanteMasivo="""SELECT
                                                    dc.num_linea, 
                                                    dc.num_cuenta,
                                                    c.descripcion_cuenta,
                                                    dc.monto_debe, 
                                                    dc.monto_haber, 
                                                    dc.glosa,
                                                    dc.serie,
                                                    dc.folio_documento,                                                    
                                                    dc.fecha_doc::text,
                                                    c.tipo_analisis,
                                                    tdc.descripcion_documento,
                                                    dc.rut,
                                                    f.nombre,
                                                    tdc.cod_doc_contable,
                                                    dc.codigo_cc,
                                                    dcc.desc_distribucion_centro_costo
                                            from 
                                                    (ctb.detalle_comprobante dc join ctb.cuenta_contable c on dc.num_cuenta = c.num_cuenta and dc.cod_comprobante in (%s) and c.cod_empresa=dc.cod_empresa) 
                                                    left join ctb.tipo_doc_contable tdc on dc.cod_doc_contable = tdc.cod_doc_contable                                                     
                                                    left join ctb.ficha f on dc.rut=f.rut and f.cod_empresa=c.cod_empresa
                                                    left join distribucion_centro_costo dcc on dcc.codigo=dc.codigo_cc
                                            """


strDeleteCabeceraComprobante="""delete from ctb.comprobante where cod_comprobante =%s"""

strDeleteDetalleComprobante="""delete from ctb.detalle_comprobante where cod_comprobante=%s and cod_empresa=%s"""

strSelectEmpresaPeriodo="""select * from ctb.periodo_contable where cod_empresa =%s and periodo=%s and mes=%s"""

strSelectCuentaEmpresa="""select * from ctb.cuenta_contable where num_cuenta='%s' and cod_empresa=%s"""
strMaxPeriodo= """select descripcion_periodo,periodo,mes,cod_empresa from ctb.periodo_contable where cod_empresa=%s  and estado='A' order by mes desc limit 1"""

strMaxLibro= """select desc_libro,cod_libro,periodo,mes from ctb.libro where cod_empresa=%s  and estado=True  and cod_tipo_libro=%s order by (periodo ||'-'||mes||'-01')::date desc limit 1"""

strSelectBuscaBoleta=""" select
                                            --case when contabilizado='S' or h.cod_comprobante is not Null then True else False end,
                                            False,
                                            h.numero_boleta, 
                                            h.monto_honorario,                                        
                                            h.rut_ficha,
                                            f.nombre,                                        
                                            case when h.electronica=True then 'E' else 'N' end as electronica,
                                            h.fecha_boleta::text,
                                            case 
                                                    when h.tipo_servicio is not Null then h.tipo_servicio 
                                                    when h.tipo_servicio is Null and h.monto_retencion is Null then 'P'
                                                    when h.tipo_servicio is Null and h.monto_retencion is not Null then 'B'
                                            end as tipo_servicio,					
                                            case when contabilizado='S' or h.cod_comprobante is not Null then 'S' else 'N' end,
                                            h.cod_comprobante,                                                                                
                                            h.cod_honorario,
                                            h.periodo,
                                            h.mes,
                                            case when monto_honorario is not Null and h.monto_liquido is Null then round(monto_honorario-round(monto_honorario*10/100)) else h.monto_liquido end::numeric as neto,
                                            case when monto_honorario is not null and h.monto_retencion is Null then round(round(monto_honorario*10/100)) else monto_retencion end::numeric as retencion                                        from ctb.honorario h join ctb.ficha f on h.cod_empresa=f.cod_empresa and h.rut_ficha=f.rut		
                                        %s
    order by h.numero_boleta,
                    h.rut_ficha"""
strCargaDetalleHonorario="""select 
                                                        h.cod_honorario,
                                                        d.num_cuenta,
                                                        c.descripcion_cuenta,
                                                        d.rut_ficha,
                                                        f.nombre,
                                                        d.monto,
                                                        d.numero_boleta,
                                                       glosa,
                                                      d.num_linea_comprobante 
                                            from 
                                                        ctb.detalle_honorario d join ctb.honorario h on d.cod_honorario=h.cod_honorario
                                                        join ctb.cuenta_contable c on c.num_cuenta=d.num_cuenta and h.cod_empresa=c.cod_empresa
                                                        join ctb.ficha f on f.rut=h.rut_ficha and f.cod_empresa=h.cod_empresa  %s"""
strSelectDocumento="""select 
                                                sum(monto_debe)::int4,
                                                sum(monto_haber)::int4,
                                                dc.fecha_doc::text
                                        from 
                                                ctb.comprobante c join                                                    
                                                ctb.detalle_comprobante dc on dc.cod_comprobante=c.cod_comprobante and c.cod_tipo_comprobante!='A'
                                                join ctb.tipo_doc_contable tdc 
                                                on tdc.cod_doc_contable = dc.cod_doc_contable  and
                                                dc.rut ='%s' and tdc.cod_doc_contable =%s   and dc.folio_documento = %s
                                                and dc.cod_empresa=%s  and dc.num_cuenta='%s'
                                                group by dc.fecha_doc"""
                                        
strSelectCuentaGrupo="""SELECT 
                                                num_cuenta,
                                                tipo_cuenta
                                            from 
                                                ctb.tipo_cuenta tc join ctb.cuenta_contable c on c.tipo_cuenta=tc.cod_tipo_cuenta where (num_padre is null or num_padre = 'Null' or num_padre='') and cod_empresa=%s and num_cuenta ilike '%s'"""
strPlanCuentasFecus="""select 
                                        c.num_cuenta, 
                                        c.estado_cuenta, 
                                        c.tiene_hijos, 
                                        c.nivel_cuenta, 
                                        c.estado_resultado, 
                                        c.tipo_analisis, 
                                        c.tipo_cuenta, 
                                        c.descripcion_cuenta, 
                                        c.num_padre, 
                                        c.cod_empresa, 
                                        c.aplica_resultado, 
                                        c.cod_tipo_resultado, 
                                        t.descripcion_tipo_cuenta,
                                        case when f.cod_fecus is not Null then f.cod_fecus end as fecus,
                                                                                                                case when fe.identificador is Null then '' else fe.identificador end as identificador,
                                                                                                                case when fe.desc_fecus is Null then '' else fe.desc_fecus end as descripcion_fecus
                                       
                            from 
                                    ctb.cuenta_contable c
                                    inner join ctb.tipo_cuenta t
                                    on c.tipo_cuenta = t.cod_tipo_cuenta
                                    left join ctb.cuenta_contable_fecus f on f.num_cuenta=c.num_cuenta and  f.cod_empresa=c.cod_empresa
                                    left join ctb.fecus fe on fe.cod_fecus=f.cod_fecus 
                                    where
                                    c.cod_empresa = %s %s
                                order by num_cuenta """
strSelectBuscaFactura=""" select
                                            --case when contabilizado='S' or h.cod_comprobante is not Null then True else False end,
                                            False,
                                            d.num_documento, 
                                            d.total,                                        
                                            d.rut_documento,
                                            f.nombre,                                        
                                            case when d.electronica=True then 'E' else 'N' end as electronica,
                                            d.fecha_documento::text,
                                            '',
                                            centralizado,
                                            '',                                                                      
                                            d.cod_documento,
                                          -- p.periodo,
                                           -- p.mes,
                                            neto,
                                            exento,
                                            iva,
                                            ie,
                                            cod_comprobante,
                                            td.descripcion_documento
                                            from ctb.documento d join ctb.libro l on l.cod_libro=d.cod_libro join ctb.ficha f on d.cod_empresa=f.cod_empresa and d.rut_documento=f.rut	join ctb.tipo_doc_contable td on td.cod_doc_contable=d.cod_tipo_documento	
                                        %s
    order by d.num_documento,
                    d.rut_documento"""
strNivelEmpresaCuentaContable="""select 
                                        c.num_cuenta, 
                                       c.estado_cuenta, 
                                       c.tiene_hijos, 
                                       c.nivel_cuenta, 
                                       c.estado_resultado, 
                                       c.tipo_analisis, 
                                       c.tipo_cuenta, 
                                       c.descripcion_cuenta, 
                                       c.num_padre, 
                                       c.cod_empresa, 
                                       c.aplica_resultado, 
                                       c.cod_tipo_resultado, 
                                       t.descripcion_tipo_cuenta
                                from 
                                        ctb.cuenta_contable c 
                                        inner join ctb.tipo_cuenta t 
                                        on c.tipo_cuenta = t.cod_tipo_cuenta \
                                        join ctb.empresa e on e.cod_empresa=c.cod_empresa and c.nivel_cuenta=e.nivel_cuenta
                                where 
                                        c.cod_empresa = %s
                                and 
                                        c.num_cuenta = '%s'
                                and c.estado_cuenta='V'
                                order by c.num_cuenta"""
strSelectMaxNumEntrega="""select cod_empresa, max(num_entrega_rendir) + 1 
                            from ctb.entrega_rendir 
                            %s 
                            group by 1"""
strCargaEntregaRendir="""%s select 
                                er.cod_entrega_rendir,
                                er.num_entrega_rendir,
                                er.fecha_entrega,
                                pc.descripcion_periodo,
                                f.rut,
                                f.nombre,
                                er.num_cuenta,
                                c.descripcion_cuenta,
                                com.cod_comprobante, 
                                com.folio_comprobante,                                                        
                                er.glosa,
                                er.mes,
                                er.periodo,
                                er.monto,	
                                %s,
                                er.monto_reembolso,
                                er.cod_comprobante_reembolso,
                                er.reembolso,
                                ' ' as colum,
                                (case when(er.monto - coalesce(sum(r.total), sum(r.total), 0)) > 0 then (er.monto - coalesce(sum(r.total), sum(r.total), 0)) else 0 end) - (coalesce(nr.monto_rendicionA, nr.monto_rendicionA, 0)) as saldo_por_rendir_real,
                                (case when(er.monto - coalesce(sum(r.total), sum(r.total), 0) + er.monto_reembolso) < 0 then (er.monto - coalesce(sum(r.total), sum(r.total), 0)) * -1 else 0 end) - (coalesce(nr.monto_reembolsoA, nr.monto_reembolsoA, 0)) as saldo_por_reembolsar_real,                                
                                coalesce(nr.monto_rendicionA, nr.monto_rendicionA, 0) as monto_rendicionA,
                                coalesce(nr.monto_reembolsoA, nr.monto_reembolsoA, 0) as monto_reembolsoA
                            from 
                                ctb.entrega_rendir er 
                                join ctb.cuenta_contable c on c.num_cuenta=er.num_cuenta and er.cod_empresa=c.cod_empresa
                                join ctb.ficha f on f.rut=er.rut and f.cod_empresa=er.cod_empresa
                                join ctb.comprobante com on com.cod_comprobante = er.cod_comprobante
                                join  ctb.periodo_contable pc on pc.cod_empresa=com.cod_empresa and pc.mes=com.mes and pc.periodo=com.periodo
                                left join ctb.rendicion r on r.cod_entrega_rendir = er.cod_entrega_rendir
                                left join (
                                    select cod_entrega_origen,sum(case when tipo = 'D' then monto else 0 end) as monto_rendicionA,sum(case when tipo = 'B' then monto else 0 end) as monto_reembolsoA
                                    from ctb.neteo_rendicion_reembolso
                                    group by cod_entrega_origen
                                    ) nr on er.num_entrega_rendir = nr.cod_entrega_origen
                                %s 
                                group by  er.cod_entrega_rendir,
                                er.num_entrega_rendir,
                                er.fecha_entrega,
                                pc.descripcion_periodo,
                                f.rut,
                                f.nombre,
                                er.num_cuenta,
                                c.descripcion_cuenta,
                                com.cod_comprobante, 
                                com.folio_comprobante,                                                        
                                er.glosa,
                                er.mes,
                                er.periodo,
                                er.monto,
                                er.monto_reembolso,
                                er.cod_comprobante_reembolso,
                                er.reembolso,nr.monto_rendicionA,nr.monto_reembolsoA %s  """

strCargaRendiciones="""select
                            r.cod_rendicion, 
                            r.num_cuenta,
                            c.descripcion_cuenta,
                            f.rut,
                            f.nombre,
                            r.fecha_documento,
                            r.serie_documento,
                            r.num_documento,
                            r.cod_tipo_documento,
                            tdc.descripcion_documento,
                            pc.descripcion_periodo,
                            r.cod_entrega_rendir,
                            fe.nombre || ', Entrega ' || er.num_entrega_rendir || ', Comprobante ' || er.cod_comprobante || ', Folio ' || cr.folio_comprobante,
                            com.cod_comprobante,
                            com.folio_comprobante,                                                        
                            r.glosa,
                            r.neto,
                            r.igv,
                            r.total,
                            r.mes,
                            r.periodo,
                            r.fecha_digitacion,
                            dcc.desc_distribucion_centro_costo
                        from
                            ctb.rendicion r 
                            join ctb.cuenta_contable c on c.num_cuenta=r.num_cuenta and r.cod_empresa=c.cod_empresa
                            join ctb.ficha f on f.rut=r.rut and f.cod_empresa=r.cod_empresa
                            join ctb.comprobante com on com.cod_comprobante = r.cod_comprobante
                            join ctb.periodo_contable pc on pc.cod_empresa=com.cod_empresa and pc.mes=com.mes and pc.periodo=com.periodo
                            join ctb.tipo_doc_contable tdc on tdc.cod_doc_contable = r.cod_tipo_documento
                            join ctb.entrega_rendir er on er.cod_entrega_rendir = r.cod_entrega_rendir
                            join ctb.comprobante cr on cr.cod_comprobante = er.cod_comprobante
                            join ctb.ficha fe on fe.rut=er.rut and fe.cod_empresa=er.cod_empresa
                            left join distribucion_centro_costo dcc on r.cod_dist_centro_costo=dcc.codigo
                            %s"""
                            
strCargaRendicionesEntrega="""select
                            r.num_cuenta,
                            c.descripcion_cuenta,
                            f.rut,
                            f.nombre,
                            tdc.descripcion_documento,
                            r.fecha_documento,
                            r.fecha_digitacion,
                            r.serie_documento,
                            r.num_documento,                            
                            r.neto,
                            r.igv, 
                            r.total,
                            r.mes,
                            r.periodo
                        from
                            ctb.rendicion r 
                            join ctb.cuenta_contable c on c.num_cuenta=r.num_cuenta and r.cod_empresa=c.cod_empresa
                            join ctb.ficha f on f.rut=r.rut and f.cod_empresa=r.cod_empresa
                            join ctb.tipo_doc_contable tdc on tdc.cod_doc_contable = r.cod_tipo_documento 
                            %s"""

strCargaNeteos="""select
                            nr.cod_entrega_origen,
                            nr.cod_entrega_destino,
                            nr.monto,
                            nr.tipo,
                            nr.fecha_neteo,
                            nr.relacion
                        from
                            ctb.neteo_rendicion_reembolso nr                             
                            %s"""

strCargaReporteMovBancario="""
                            select
                                    *
                            from
                                ctb.vw_reporte_mov_bancario                            
                                %s"""

strCargaDetalleReporteMovBancario="""select  
                                            de.cod_egreso
                                            ,de.cod_documento
                                            ,de.index
                                            ,de.rut_proveedor
                                            ,de.fecha_emision as "Fecha"
                                            ,p.nom_proveedor as "Responsable"
                                            ,de.monto_debe as "Ingreso"
                                            ,de.monto_haber as "Egreso"
                                            ,de.glosa as "Concepto"
                                            ,de.tipo_doc::text||'-'||num_documento::text as "Documento"
                                            
                                    from detalle_egreso de
                                    left join proveedor p using (rut_proveedor)
                                    where 
                                            de.index <>0 and de.cod_egreso=%s
                                    order by 3"""

strVerificarPeriodoContable="""
                            select
                                    estado::text
                            from
                                ctb.periodo_contable
                            where
                                periodo=%s and mes=%s"""
                                
strUsuarioActual="""select current_user"""

strFechaHoraActual="""select current_timestamp"""

strCabeceraExportar="""select 
                            tc.descripcion_tipo,
                            e.descripcion_empresa,
                            e.rut_empresa,
                            e.direccion_Empresa,
                            c.folio_comprobante,
                            c.fecha,
                            c.glosa
                        from ctb.comprobante c
                        join ctb.tipo_comprobante tc using (cod_tipo_comprobante) 
                        join ctb.empresa e using (cod_empresa)
                        where cod_comprobante=%s"""