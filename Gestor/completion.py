#!/usr/bin/env python
# -*- coding: utf-8 -*-
# completion -- Objectos de autocompletado del sistema
# (c) Fernando San Martín Woerner 2003, 2004, 2005
# snmartin@galilea.cl

# This file is part of Gestor.
#
# Gestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#~ from pyPgSQL.PgSQL import connect

from PixEntryCompletion import PixEntryCompletion
import gtk
from codificacion import CUTF8
from constantes import MESES
from constantes import SUCURSAL_MATRIZ
import ifd
#from spg import connect

class GenericCompletion(PixEntryCompletion):

    def __init__(self, entry = gtk.Entry(), sel_func = None,
                    cnx = None, sql = None, modelo=None):

        PixEntryCompletion.__init__(self, entry, selfunc = sel_func,
                                        match_all = False)

        self.cnx = cnx
        self.cursor = self.cnx.cursor()
        self.sql = sql
        if modelo is None:
            self.carga_modelo()
        else:
            self.set_model(modelo)
            self.set_select_column(0)

    def carga_modelo(self):
        self.cursor.execute(self.sql)
        r = self.cursor.fetchall()
        self.modelo.clear()
        if len(r) == 0:
            return
        l = map(ifd.type_to_str, r[0])
        self.modelo = gtk.ListStore(*map(type, l))
        for i in r:
            self.modelo.append(map(CUTF8,i))
        self.set_model(self.modelo)
        self.set_select_column(0)

    def reload(self,sql=None):        
        self.modelo.clear()
        if sql!=None:
            self.sql=sql
        self.cursor.execute(self.sql)
        r = self.cursor.fetchall()
        for i in r:
            self.modelo.append(map(CUTF8,i))

class CompletionCuentaContable(GenericCompletion):   
    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = 1,estado=None, desc=None):
        self.cod_empresa = e
        sqlWhere=''
        if desc!=None:
            sqlWhere+=" and descripcion_cuenta ilike '%" + desc+ "%'"
        if estado!=None:
            sqlWhere+=" and estado_cuenta='%s'"%estado
        s = """SELECT
                        descripcion_cuenta,
                        num_cuenta
                FROM
                        ctb.cuenta_contable join ctb.empresa e using(cod_empresa)
                WHERE
                    cod_empresa = %s and ctb.cuenta_contable.nivel_cuenta=e.nivel_cuenta
                    %s
                ORDER BY
                        descripcion_cuenta""" % (self.cod_empresa,sqlWhere)
        print s
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)

class CompletionDistribucionCentroCosto(GenericCompletion):
    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = 1):
        #PixEntryCompletion.__init__(self, entry, selfunc = sel_func, match_all = False)
        
       
        sql = """select 
                rtrim(ltrim(coalesce(ds1.desc_distribucion_centro_costo,'')
                || coalesce((' - ' || ds2.desc_distribucion_centro_costo),'')
                || coalesce((' - ' || ds3.desc_distribucion_centro_costo),''))) as descripcion
                , (case when ds3.desc_distribucion_centro_costo is not null then ds3.codigo else (case when ds2.desc_distribucion_centro_costo is not null then ds2.codigo else ds1.codigo end) end) as codigo
                from 
                (select d1.codigo,d1.desc_distribucion_centro_costo 
                from distribucion_centro_costo d1
                where length(rtrim(ltrim(d1.codigo::text))) = 2) ds1 
                left join 
                (select d2.codigo,d2.desc_distribucion_centro_costo 
                from distribucion_centro_costo d2
                where length(rtrim(ltrim(d2.codigo::text))) = 3) ds2 
                on rtrim(ltrim(ds1.codigo::text)) = substring(rtrim(ltrim(ds2.codigo::text)),0,3)
                left join 
                (select d3.codigo,d3.desc_distribucion_centro_costo 
                from distribucion_centro_costo d3
                where length(rtrim(ltrim(d3.codigo::text))) = 4) ds3 
                on rtrim(ltrim(ds2.codigo::text)) = substring(rtrim(ltrim(ds3.codigo::text)),0,4) order by 1"""

        print sql
        GenericCompletion.__init__(self,entry,sel_func = f,cnx = c,sql = s)


class CompletioninformeFecus(GenericCompletion):   
    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = 1):
        self.cod_empresa = e
        sqlWhere=''
        s = """SELECT
                        desc_informe_fecus,
                        cod_informe_fecus
                FROM
                        ctb.informe_fecus
                WHERE
                    cod_empresa = %s 
                ORDER BY
                        desc_informe_fecus""" % (self.cod_empresa)
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)

class CompletionFicha(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = 1):
        self.cod_empresa = e
        s = """SELECT
                        nombre,
                        rut
                FROM
                        ctb.ficha
                WHERE
                    cod_empresa = %s
                ORDER BY
                        nombre""" % self.cod_empresa
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)

class CompletionFichaTipo(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = 1, t = None):
        self.cod_empresa = e
        tipo_ficha = ""
        if t not in(None,''):
            tipo_ficha = " and tipo_ficha = '%s' " % t
        s = """SELECT
                        nombre,
                        rut
                FROM
                        ctb.ficha
                WHERE
                    cod_empresa = %s %s 
                ORDER BY
                        nombre""" % (self.cod_empresa, tipo_ficha)
                        
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)
        
class CompletionConsultaCuenta(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = 1):
        self.cod_empresa = e
        s = """SELECT
                        num_cuenta,
                        descripcion_cuenta
                FROM
                        ctb.cuenta_contable
                WHERE
                    cod_empresa = %s
                ORDER BY
                        descripcion_cuenta""" % self.cod_empresa
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)

class CompletionTipoFicha(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e=None):

        s = """SELECT
                        descripcion_tipo_ficha,
                        cod_tipo_ficha
                FROM
                        ctb.tipo_ficha
                ORDER BY
                        descripcion_tipo_ficha"""
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)        


class CompletionTipoDocumentoContable(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = 1):
        self.cod_empresa = e
        s = """SELECT
                        descripcion_documento,
                        cod_doc_contable
                FROM
                        ctb.tipo_doc_contable
                
                ORDER BY
                        descripcion_documento""" 
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)

class CompletionTipoDocumentoContableRendicion(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = 1):
        self.cod_empresa = e
        s = """SELECT
                        descripcion_documento,
                        cod_doc_contable
                FROM
                        ctb.tipo_doc_contable
                WHERE cod_doc_contable in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,
                                            26,27,28,29,30,31,32,34,35,36,37,40,50,52,53,54,87,88,91,96,97,98,99,201) 
                ORDER BY
                        descripcion_documento""" 
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)

#completion Periodo_Contable
class CompletionPeriodo(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = 1):
        self.cod_empresa = e
        s = """SELECT
                        descripcion_periodo,
                        mes,
                        periodo
                FROM
                        ctb.periodo_contable
                WHERE
                        estado='A' and cod_empresa=1
                ORDER BY
                        descripcion_periodo""" 
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)

#class CompletionPeriodoContable(PixEntryCompletion):
#
#    def __init__(self, entry = gtk.Entry(), f = None, c = None, e=1):
#        PixEntryCompletion.__init__(self, entry, selfunc = f, match_all = False)
#        self.cnx = c
#        self.cursor = self.cnx.cursor()
#        self.cod_empresa = e
#        self.carga_modelo()
#     
#    def carga_modelo(self):
#        sql = """SELECT
#                        descripcion_periodo,
#                        mes,
#                        periodo
#                FROM
#                        ctb.periodo_contable
#                WHERE cod_empresa = %s
#                ORDER BY
#                        descripcion_periodo DESC""" % self.cod_empresa
#        self.cursor.execute(sql)
#        r = self.cursor.fetchall()
#        modelo = gtk.ListStore(str, str, str)
#        for i in r:
#            modelo.append(i)
#        self.set_model(modelo)
#        self.set_select_column(0)


class CompletionTipoNumeracion(PixEntryCompletion):

    def __init__(self, entry, sel_func = None, cnx = None):
        PixEntryCompletion.__init__(self, entry, selfunc = sel_func, match_all = False)
        self.cnx = cnx
        self.cursor = self.cnx.cursor()
        self.carga_modelo()

    def carga_modelo(self):
        sql = """SELECT
                    descripcion_numeracion,
                    cod_numeracion
                FROM ctb.tipo_numeracion
                ORDER BY descripcion_numeracion"""
        self.cursor.execute(sql)
        r = self.cursor.fetchall()
        modelo = gtk.ListStore(str, str)
        for i in r:
            modelo.append(i)

        self.set_model(modelo)
        self.set_select_column(0)
        
class CompletionEntregaNetear(PixEntryCompletion):

    def __init__(self, entry, sel_func = None, cnx = None):
        PixEntryCompletion.__init__(self, entry, selfunc = sel_func, match_all = False)
        self.cnx = cnx
        self.cursor = self.cnx.cursor()
        self.carga_modelo()

    def carga_modelo(self):
        sql = """select b.num_entrega_rendir,b.glosa,b.saldo_por_rendir,b.saldo_por_reembolsar from (  select 
        er.cod_entrega_rendir,
        er.num_entrega_rendir,
        er.fecha_entrega,
        pc.descripcion_periodo,
        f.rut,
        f.nombre,
        er.num_cuenta,
        c.descripcion_cuenta,
        com.cod_comprobante, 
        com.folio_comprobante,                                                        
        er.glosa,
        er.mes,
        er.periodo,
        er.monto,	
        coalesce(sum(r.total), sum(r.total), 0) as rendido,
        (case when(er.monto - coalesce(sum(r.total), sum(r.total), 0)) > 0 then (er.monto - coalesce(sum(r.total), sum(r.total), 0)) else 0 end) - (case when nr.tipo = 'D' then nr.monto else 0 end) - (case when nr.tipo = 'B' then 0 else coalesce(nr.monto, nr.monto, 0) end) as saldo_por_rendir,
        (case when(er.monto - coalesce(sum(r.total), sum(r.total), 0) + er.monto_reembolso) < 0 then (er.monto - coalesce(sum(r.total), sum(r.total), 0)) * -1 else 0 end) - (case when nr.tipo = 'B' then nr.monto else 0 end) - (case when nr.tipo = 'B' then coalesce(nr.monto, nr.monto, 0) else 0 end) as saldo_por_reembolsar ,
        er.monto_reembolso,
        er.cod_comprobante_reembolso,
        er.reembolso,
        fecha_reembolso,
        (case when(er.monto - coalesce(sum(r.total), sum(r.total), 0)) > 0 then (er.monto - coalesce(sum(r.total), sum(r.total), 0)) else 0 end) - (case when nr.tipo = 'D' then nr.monto else 0 end) - (case when nr.tipo = 'B' then 0 else coalesce(nr.monto, nr.monto, 0) end) as saldo_por_rendir_real,
        (case when(er.monto - coalesce(sum(r.total), sum(r.total), 0) + er.monto_reembolso) < 0 then (er.monto - coalesce(sum(r.total), sum(r.total), 0)) * -1 else 0 end) - (case when nr.tipo = 'B' then nr.monto else 0 end) - (case when nr.tipo = 'B' then coalesce(nr.monto, nr.monto, 0) else 0 end) as saldo_por_reembolsar_real,                                
        coalesce(nr.monto, nr.monto, 0) as monto_neteo,
        nr.cod_entrega_destino
        from 
        ctb.entrega_rendir er 
        join ctb.cuenta_contable c on c.num_cuenta=er.num_cuenta and er.cod_empresa=c.cod_empresa
        join ctb.ficha f on f.rut=er.rut and f.cod_empresa=er.cod_empresa
        join ctb.comprobante com on com.cod_comprobante = er.cod_comprobante
        join  ctb.periodo_contable pc on pc.cod_empresa=com.cod_empresa and pc.mes=com.mes and pc.periodo=com.periodo
        left join ctb.rendicion r on r.cod_entrega_rendir = er.cod_entrega_rendir
        left join ctb.neteo_rendicion_reembolso nr on er.cod_entrega_rendir = nr.cod_entrega_origen
        group by  er.cod_entrega_rendir,
        er.num_entrega_rendir,
        er.fecha_entrega,
        pc.descripcion_periodo,
        f.rut,
        f.nombre,
        er.num_cuenta,
        c.descripcion_cuenta,
        com.cod_comprobante, 
        com.folio_comprobante,                                                        
        er.glosa,
        er.mes,
        er.periodo,
        er.monto,
        er.monto_reembolso,
        er.cod_comprobante_reembolso,
        er.reembolso,
        fecha_reembolso,nr.tipo,nr.monto,nr.cod_entrega_destino,nr.fecha_neteo,nr.cod_entrega_destino  ) b where 1 = 1   
        and (saldo_por_rendir <> 0  or saldo_por_reembolsar <> 0) order by b.cod_entrega_rendir,b.num_entrega_rendir"""
        self.cursor.execute(sql)
        r = self.cursor.fetchall()
        modelo = gtk.ListStore(str, str, str, str)
        for i in r:
            modelo.append(i)

        self.set_model(modelo)
        self.set_select_column(0)

class CompletionMes(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None):
        s = """SELECT
                        descripcion_mes,
                        cod_mes
                FROM
                        ctb.mes
                ORDER BY
                        cod_mes"""
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)

class CompletionFormulaValidacion(PixEntryCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e=1):
        PixEntryCompletion.__init__(self, entry, selfunc = f, match_all = False)
        self.cnx = c
        self.cursor = self.cnx.cursor()
        self.carga_modelo()
    def carga_modelo(self):
        r = [['RUT (Chile)', 'rut'], 
            ['RUC (PERU)', 'ruc'], 
            ['DNI (PERU)', 'dni'], 
            ['SIN VALIDACION', 'sin']]
        modelo = gtk.ListStore(str, str)
        for i in r:
            modelo.append(i)
        self.set_model(modelo)
        self.set_select_column(0)
    

class CompletionMoneda(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None):
        s = """ SELECT
                        descripcion_moneda,
                        cod_moneda
                FROM
                        ctb.moneda
                ORDER BY
                        cod_moneda;"""
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)

class CompletionBanco(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = None):
        s = """SELECT
                        nombre_banco,
                        cod_banco
                FROM
                        ctb.banco
                ORDER BY
                        nombre_banco"""
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)

class CompletionSistema(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = None):
        s = """SELECT
                        nombre_sistema,
                        cod_sistema
                FROM
                        ctb.sistema
                ORDER BY
                        nombre_sistema"""
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)


class CompletionTipoComprobante(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = None):
        s = """SELECT
                        descripcion_tipo,
                        cod_tipo_comprobante
                FROM
                        ctb.tipo_comprobante
                ORDER BY
                        descripcion_tipo"""
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)

class CompletionPeriodoContable(PixEntryCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e=1):
        PixEntryCompletion.__init__(self, entry, selfunc = f, match_all = False)
        self.cnx = c
        self.cursor = self.cnx.cursor()
        self.cod_empresa = e
        self.carga_modelo()
    def carga_modelo(self):
        sqlWhere=''
        if self.cod_empresa!=None:
            sqlWhere=" where cod_empresa=%s"%(self.cod_empresa)
        sql = """SELECT
                        descripcion_periodo,
                        mes,
                        periodo,
                        cod_empresa
                FROM
                        ctb.periodo_contable
                %s
                ORDER BY
                        descripcion_periodo DESC""" % sqlWhere
        self.cursor.execute(sql)
        r = self.cursor.fetchall()
        modelo = gtk.ListStore(str, str, str,str)
        for i in r:
            modelo.append(i)
        self.set_model(modelo)
        self.set_select_column(0)
    def reload(self,sql):
        self.modelo.clear()
        self.cursor.execute(sql)
        r = self.cursor.fetchall()
        for i in r:
            self.modelo.append(map(CUTF8,i))

class CompletionDistribucionCentroCosto(PixEntryCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e=1):
        PixEntryCompletion.__init__(self, entry, selfunc = f, match_all = False)
        self.cnx = c
        self.cursor = self.cnx.cursor()
        self.cod_empresa = e
        self.carga_modelo()
    def carga_modelo(self):
        sqlWhere=''
        if self.cod_empresa!=None:
            sqlWhere=" where cod_empresa=%s"%(self.cod_empresa)
        sql = """
        select
                desc_distribucion_centro_costo,
                codigo
        from
                distribucion_centro_costo 
        where
                codigo in (10,11,112,113,114,115,116,117,12,49,52,53,20,126,13,14,15,16,51,54,55,56,57,61,62,63,64,65,00)
        order by
                codigo"""
        self.cursor.execute(sql)
        r = self.cursor.fetchall()
        modelo = gtk.ListStore(str, str)
        for i in r:
            modelo.append(i)
        self.set_model(modelo)
        self.set_select_column(0)
    def reload(self,sql):
        self.modelo.clear()
        self.cursor.execute(sql)
        r = self.cursor.fetchall()
        for i in r:
            self.modelo.append(map(CUTF8,i))
class CompletionEmpresa(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None):

        s = """SELECT
                        descripcion_empresa,
                        cod_empresa
                FROM
                        ctb.empresa                
                ORDER BY
                        descripcion_empresa"""
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)

class CompletionSucursal(GenericCompletion):
    
    def __init__(self, entry = gtk.Entry(), selfunc = None, c = None):

        s = """SELECT
                        nom_sucursal ,
                        s.cod_sucursal
                FROM
                        usuario u join usuario_sucursal us using (cod_usuario)
                        join sucursal s on  s.cod_sucursal = us.cod_sucursal
                WHERE
                        u.en_uso = 'True' and s.en_uso='True' and login_usuario = current_user
                ORDER BY
                        nom_sucursal"""

        GenericCompletion.__init__(self, entry, sel_func = selfunc, cnx = c, sql = s)
    def set_cod_sucursal(self, cod_sucursal):
        self.sql = """select
                        nom_proyecto,        
                        cod_proyecto
                from
                        proyecto
                where 
                        cod_sucursal =%s
                order by nom_proyecto"""%(cod_sucursal)
        self.carga_modelo()
        
class CompletionAreaFuncional(GenericCompletion):
    
    def __init__(self, entry = gtk.Entry(), selfunc = None, c = None):

        s = """select
                    af.nom_area_funcional
                    ,af.cod_area_funcional
                from
                    comun.area_funcional af
                order by 2"""

        GenericCompletion.__init__(self, entry, sel_func = selfunc, cnx = c, sql = s)

class CompletionTipoCuenta(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = None):
        
        s = """SELECT
                        descripcion_tipo_cuenta,
                        cod_tipo_cuenta
                FROM
                        ctb.tipo_cuenta
                ORDER BY
                        descripcion_tipo_cuenta"""
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)
        
class CompletionEstado(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = None):
        s = """SELECT
                        descripcion_estado,
                        cod_estado
                FROM
                        ctb.estado_periodo_contable
                ORDER BY
                        cod_estado"""
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)
        
#class CompletionTipoNumeracion(GenericCompletion):
#    
#    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = None):
#        s = """SELECT
#                        descripcion_numeracion,
#                        cod_numeracion
#                FROM
#                        ctb.tipo_numeracion
#                ORDER BY cod_numeracion"""
#        GenericCompletion.__init__(self, entry, sel_fun = f, cnx = c, sql = s)
        
class CompletionRetencion(GenericCompletion):
    
    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = None):
        s = """SELECT
                        descripcion_cuenta,
                        num_cuenta
                FROM
                        ctb.cuenta_contable
                ORDER BY num_cuenta"""
        GenericCompletion.__init__(self, entry, f, c, s)
        
class CompletionLiquido(GenericCompletion):
    
    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = None):
        s = """SELECT
                        descripcion_cuenta,
                        num_cuenta
                FROM
                        ctb.cuenta_contable
                ORDER BY num_cuenta"""
        GenericCompletion.__init__(self, entry, f,c,s)
       
class CompletionResultado(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = 1):
        self.cod_empresa = e
        s="""select
                                    descripcion_resultado,
                                    cod_tipo_resultado
                            from 
                                    ctb.tipo_resultado 
                            where 
                                    cod_empresa = %s
                            order by 
                                    descripcion_resultado"""%int(self.cod_empresa)
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)
class CompletionLibro(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = 1):
        self.cod_empresa = e
        s="""select
                                    desc_libro,
                                    cod_libro
                            from 
                                    ctb.libro
                            where 
                                    cod_empresa = %s
                            order by 
                                    desc_libro"""%int(self.cod_empresa)
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)
class CompletionEntregaRendir(GenericCompletion):

    def __init__(self, entry = gtk.Entry(), f = None, c = None, e = 1):
        self.cod_empresa = e
        s = """select
                    fe.nombre || ', Entrega ' || er.num_entrega_rendir || ', Comprobante ' || er.cod_comprobante || ', Folio ' || cr.folio_comprobante,
                    fe.rut,
                    er.cod_entrega_rendir,                    
                    er.fecha_entrega,                    
                    er.monto
                from 
                    ctb.entrega_rendir er                     
                    join ctb.ficha fe on fe.rut=er.rut and fe.cod_empresa=er.cod_empresa
                    join ctb.comprobante cr on cr.cod_comprobante = er.cod_comprobante
                ORDER BY 1""" 
        GenericCompletion.__init__(self, entry, sel_func = f, cnx = c, sql = s)

if __name__ == "__main__":
    w = gtk.Window()
    e = gtk.Entry()
    cnx = connect("dbname=scc")
    cnx.autocommit()
    p = CompletionTipoFicha(e, None, cnx, 1)    
    w.add(e)
    w.show_all()
     
    gtk.main()
    

