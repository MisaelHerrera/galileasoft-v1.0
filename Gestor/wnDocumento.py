#!/usr/bin/env python
# -*- coding: utf-8 -*-
# wnDocumento -- formulario de ingreso, modificacion, eliminacion de Documentos Contables
#


from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys

(COD_DOC_CONTABLE,
 DESCRIPCION_DOCUMENTO,
 MANEJA_SALDO) = range(3)

class wnDocumento(GladeConnect):
    
    def __init__(self, conexion=None, documento=None, padre=None):
        GladeConnect.__init__(self, "glade/wnDocumento.glade", "wnDocumento")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        if padre is None:
            self.wnDocumento.maximize()
            self.padre = self.wnDocumento
        else:
            self.padre = padre            
        self.crea_columnas()

    def crea_columnas(self):
        columnas = []        
        columnas.append([COD_DOC_CONTABLE,"Codigo","str"])        
        columnas.append([DESCRIPCION_DOCUMENTO,"Descripcion Documento","str"])
        columnas.append([MANEJA_SALDO,"Maneja Saldo","str"])
        self.modelo = gtk.ListStore(*(3*[str]))
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeDocumento)
                
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgDocumento(conexion=self.cnx)
        
        
    def on_btnBuscar_clicked(self, btn=None):
        try:
            self.cursor.execute("""SELECT * 
                                FROM ctb.tipo_doc_contable 
                                ORDER BY cod_doc_contable""")
            records = self.cursor.fetchall()
            self.modelo.clear()
            for record in records:
                self.modelo.append(record)
            self.treeDocumento.set_model(self.modelo)
        except:
            print sys.exc_info()[1]
            dialogos.error("Error al obtener los datos.")
                
    def on_btnCerrar_clicked(self, btn=None, data=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.wnDocumento.hide()

class dlgDocumento(GladeConnect):
    
    def __init__(self, conexion=None, empresa=None, padre=None):
        GladeConnect.__init__(self, "glade/wnDocumento.glade", "dlgDocumento")
        self.cnx=conexion
        self.cursor = self.cnx.cursor()
        self.entTipo.grab_focus()
        self.dlgDocumento.show()
    
    def on_btnCancelar_clicked(self, btn=None):
        self.dlgDocumento.hide()
        
    def on_btnAceptar_clicked(self, btn=None):
        if self.entTipo.get_text() == "" :
                dialogos.error("El campo Tipo Documento no debe estar vacio")
                return
    
    
if __name__ == "__main__":
    cnx = connect("dbname=ctb")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    w = wnDocumento(cnx)    
    gtk.main()
