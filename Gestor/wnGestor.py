#!/usr/bin/env python
# -*- coding: UTF8 	-*-

#wnGestor.py: Formulario Principal de la Aplicación
##==========================================================
##     G   E   S   T  O  R
##==========================================================
#       (C) 2003, 2004, 2005 Galilea S.A.
#       Fernando San Martín Woerner    snmartin@galilea.cl
#       Claudio Salgado Morales             csalgado@galilea.cl
#       Víctor Benitez Tejos                    vbenitez@galilea.cl

#       Gracias a Leonardo Soto por sus tips para el utf8 coding y una linea huacha que habia en plan de cuentas

# This file is part of pyGestor.
#
# pyGestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# (C)    José Luis Álvarez Morales    2006, 2007
#           josealvarezm@gmail.com
from gettext import gettext as _

from GladeConnect import GladeConnect
import gtk
import gobject
import sys
from dialogos import dlgError
import debugwindow
from spg import connect
from ctb_rutinas import ctb_coneccion
from wnEmpresa import wnEmpresa
from wnMoneda import wnMoneda

from wnTipoResultado import wnTipoResultado
from wnPlandeCuentas import wnPlandeCuentas
from wnFicha import wnFicha
from wnTipoDocumento import wnTipoDocumento
from wnPeriodoContable import wnPeriodoContable
##tipo comprobante
from wnTipoCuenta import wnTipoCuenta
from wnTipoNumeracion import wnTipoNumeracion

from wnTipoFicha import wnTipoFicha
from wnEstadoPContable import wnEstadoPContable
from wnBanco import wnBanco
from wnSistema import wnSistema
##-------------------------------
from wnComprobante import wnComprobante
from wnLibrosContables import wnLibrosContables
from wnLibrosElectronicos import wnLibrosElectronicos
from wnConsultaCuenta import wnConsultaCuenta
from frm_libro_contable import FrmLibroContable
from frm_libroHonorario import FrmLibroHonorario
from frm_EntregaRendir import FrmEntregaRendir
from frm_Rendicion import FrmRendicion
from frm_fecus import FrmInformeFecus as FrmFecus
from frm_fecus import FrmParametrizacionInformeFecus as FrmAsignacionFecus
#from frm_asignacion_fecus import FrmAsignacionFecus
from frm_seleccion_empresa import dlgSeleccionEmpresa
##----------------Libros Contables
from frm_mantenedor import FrmParametrizacion
from frm_tipo_libro import FrmTipoLibro
from frm_creacion_libros import FrmCreacionLibros
from frm_mantenedor import FrmDocumentos
from frm_centralizador_libros import FrmLibrosContables
from frm_activo_fijo import FrmActivoFijo
from wn_conciliacion import wnConciliacion
from frm_reporte_mov_bancario import FrmReporteMovBancario

class Gestor(GladeConnect):
        
    def __init__(self, argumentos):
        GladeConnect.__init__(self, "./glade/wnGestor.glade")
        self.args = argumentos
        self.wnGestor.hide()
        self.cnx=None
    
    def arranque(self):
        self.cod_empresa = self.sel_empresa()
        self.wnGestor.show_all()
        self.wnGestor.maximize()
        self.frm_padre = self.wnGestor
        self.wins = {}
        self.ntbGestor.set_scrollable(1)
        self.ntbGestor.popup_enable()
        return self.cod_empresa
    
    def sel_empresa(self):
        self.wins={}
        d = dlgSeleccionEmpresa(self.cnx, self.wnGestor)
        self.cod_empresa = d.seleccionar()
        if self.cod_empresa > -1:
            formula = sys.__setattr__('rut', d.formula_rut)
            formula = sys.__setattr__('decimales', d.decimales)
            self.wnGestor.set_title(d.desc_empresa + " - Contabilidad General")
            return self.cod_empresa
        else:
            formula = sys.__setattr__('rut', '')
            formula = sys.__setattr__('decimales', 0)
            return self.cod_empresa
    
    def run(self):
        self.mnuActivarMenu(False)
        if self.on_mnuConectar_activate():            
            self.mnuActivarMenu(True)
        else:
            if self.cnx==None:
                gtk.main_quit()
            return None
    
    def mnuActivarMenu(self,estado=False):
        self.mnuEmpresa.set_sensitive(estado)
        self.mnuMoneda.set_sensitive(estado)
        self.mnuCentroUtilidad.set_sensitive(estado)
        self.mnuPlanCuenta.set_sensitive(estado)
        self.mnuFicha.set_sensitive(estado)
        self.mnuPeriodoCon.set_sensitive(estado)
        self.mnuTipoDocCon.set_sensitive(estado)
        self.mnuTipoCuenta.set_sensitive(estado)
        self.mnuTipoNumeracion.set_sensitive(estado)
        self.mnuTipoFicha.set_sensitive(estado)
        self.mnuEstPContable.set_sensitive(estado)
        self.mnuBanco.set_sensitive(estado)
        self.mnuSistema.set_sensitive(estado)
        self.mnuComContables.set_sensitive(estado)
        self.mnuLibrosContables.set_sensitive(estado)
        self.mnuCueContables.set_sensitive(estado)
        self.mnuLibHonorarios.set_sensitive(estado)
        self.mnuCentralizacionHonorarios.set_sensitive(estado)
        self.mnuMaestroFecu.set_sensitive(estado)
        self.mnuAsignacionFecu.set_sensitive(estado)
        self.mnuLibros.set_sensitive(estado)
##    conectar con los distintos sistemas

    def on_mnuDesconectar_activate(self,s=None,e=None):
        if self.cnx!=None:
            self.mnuActivarMenu(False)
            if len(self.wins)>0:                
                wins=self.wins.values()                                   
                for i in wins:             
                    self.ntbGestor.remove(i[0])
                self.wins={}
            self.cnx=None
        self.mnuActivarMenu(False)

    def on_mnuConectar_activate(self, s=None, e=None):
        db = None
        rc = None
        
        if not len(self.args) == 0:
            for i in self.args:
                if i[:2] == "-s":
                    x = i.find('x')
                    a = int(i[2:x])
                    b = int(i[x+1:])
                    self.wnGestor.set_resizable(1)
                    self.wnGestor.set_default_size(a, b)
                    self.wnGestor.resize(a, b)

                if i[:2] == "-d":
                    db = i[2:]

                if i[:2] == "-r":
                    rc = i[2:]

        self.cnx = None
        self.cnx, self.cursor = ctb_coneccion(db, rc)
        if self.cnx == None:
            return 0
        sql = """SELECT
                        cod_cargo 
                FROM ctb.usuario u 
                WHERE login_usuario = (CURRENT_USER)::text and u.en_uso = True""" 
        self.cursor.execute(sql)
        r = self.cursor.fetchall()

        if len(r)==0:
            dlgError(None,_('Error al cargar datos del usuario'))
            return
        else:
            sys.__setattr__('cod_cargo', r[0][0])
        
        self.cursor.execute("select * from pg_catalog.pg_namespace where nspname = 'ctb'")
        r = self.cursor.fetchall()
        if len(r) == 0:
            print "no existe el esquema"
            f = open("sql/gestor_demo.sql", 'r')
            sql = f.read()
            self.cursor.execute(sql)
        
        if not self.arranque():
            return False
        return True
        
    def add_tab(self, widget, label):
        p = -1
        if not self.wins.has_key(label):
            l = gtk.Label('')
            l.set_text_with_mnemonic(label)
            self.ntbGestor.append_page(widget, l)
            widget.show()
            self.wins[label] = (widget, len(self.wins))
##        else:
##            self.ntbGestor.show_all()
##            self.ntbGestor.set_current_page(self.wins[label][1])
##            a = self.ntbGestor.get_current_page()
        p = len(self.wins) - 1
        self.ntbGestor.set_current_page(-1)
            
    def remove_tab(self, label):
        self.ntbGestor.remove(self.wins[label][0])
        del self.wins[label]
    
    #empresa
    def on_mnuEmpresa_activate(self, widget, *args):
        def_empresa = wnEmpresa(self.cnx, self, self.cod_empresa,"vboxEmpresa")
        self.add_tab(def_empresa.vboxEmpresa, "Empresa")
        return
    
    #moneda
    def on_mnuMoneda_activate(self, widget, *args):
        moneda = wnMoneda(self.cnx, self, "vboxMoneda")
        self.add_tab(moneda.vboxMoneda, "Moneda")
    
    #centros de utilidad
    #wnTipoResultado
    def on_mnuCentroUtilidad_activate(self, widget, *args):
        tipo_resultado = wnTipoResultado(self.cnx, self, "vboxTipoResultado")
        self.add_tab(tipo_resultado.vboxTipoResultado, "Centros_de_Utilidad")

    #plan de cuentas
    def on_mnuPlanCuenta_activate(self, widget, *args):
        plan_cuenta = wnPlandeCuentas(self.cnx, self, self.cod_empresa, "vboxPlandeCuentas")
        self.add_tab(plan_cuenta.vboxPlandeCuentas, "Plan_de_Cuentas")
    
    #fichas
    def on_mnuFicha_activate(self, widget, *args):
        ficha = wnFicha(self.cnx, self, self.cod_empresa, "vboxFicha")
        self.add_tab(ficha.vboxFicha, "Ficha")

    #periodos contables
    def on_mnuPeriodoCon_activate(self, widget, *args):
        periodo = wnPeriodoContable(self.cnx, self, self.cod_empresa, "vboxPeriodoContable")
        self.add_tab(periodo.vboxPeriodoContable, "Periodo_Contable")
    
    
    #tipo documento contable
    def on_mnuTipoDocCon_activate(self, widget, *args):
        tipo_documento = wnTipoDocumento(self.cnx, self, "vboxTipoDocumento")
        self.add_tab(tipo_documento.vboxTipoDocumento, "Tipo_Documento_Contable")

#==================================================

    #wnNivelCuenta
    #def on_mnuNivelCuenta_activate(self, widget, *args):
    #    nivel = wnNivelCuenta(self.cnx, self, "vboxNivelCuenta")
    #    self.add_tab(nivel.vboxNivelCuenta, "Nivel_Cuenta")
        
    #wnTipoCuenta
    def on_mnuTipoCuenta_activate(self, widget, *args):
        tipo_cuenta = wnTipoCuenta(self.cnx, self, "vboxTipoCuenta")
        self.add_tab(tipo_cuenta.vboxTipoCuenta, "Tipo_de_Cuenta")
    
    #wnTipoNumeracion
    def on_mnuTipoNumeracion_activate(self, widget, *args):
        tipo_num = wnTipoNumeracion(self.cnx, self, "vboxTipoNumeracion")
        self.add_tab(tipo_num.vboxTipoNumeracion, "Tipo_Numeración")
          
    #wnTIpoFicha
    def on_mnuTipoFicha_activate(self, widget, *args):
        tipo_ficha = wnTipoFicha(self.cnx, self, "vboxTipoFicha")
        self.add_tab(tipo_ficha.vboxTipoFicha, "Tipo_Ficha")
    
    #wnEstadoPContable
    def on_mnuEstPContable_activate(self, widget, *args):
        estado_periodo = wnEstadoPContable(self.cnx, self, "vboxEstadoPContable")
        self.add_tab(estado_periodo.vboxEstadoPContable, "Estado_Periodo_Contable")
    
    #wnBanco
    def on_mnuBanco_activate(self, widget, *args):
        banco = wnBanco(self.cnx, self, "vboxBanco")
        self.add_tab(banco.vboxBanco, "Banco")
    def on_btnCambioEmpresa_clicked(self,widget,*args):
        if self.cnx:
            if len(self.wins)>0:                
                wins=self.wins.values()                                   
                for i in wins:             
                    self.ntbGestor.remove(i[0])
                self.wins={}
            if not self.sel_empresa():                    
                self.mnuActivarMenu(False)
            
    #wnSistema
    def on_mnuSistema_activate(self, widget, *args):
        sistema = wnSistema(self.cnx, self, "vboxSistema")
        self.add_tab(sistema.vboxSistema, "Sistema")
    def on_mnuCargo_activate(self, widget, *args):
        from frm_cargo import frmCargo
        cargo = frmCargo(self.cnx, self, self.cod_empresa, 'vboxCargo')
        cargo.etiqueta = "Parametrizacion de Cargos"
        self.add_tab(cargo.vboxCargo, "Parametrizacion de Cargos")
        print "OK"
        return 
        Fecu=FrmFecus(self.cnx,self,self.cod_empresa, 'vboxFecu')
        self.add_tab(Fecu.vboxFecu,"Maestro de Fecu")          
#**************************************************************************
#Comprobantes Contables
    def on_mnuComContables_activate(self, widget, *args):
        com_contable = wnComprobante(self.cnx, self, self.cod_empresa, "vboxComprobante")
        self.add_tab(com_contable.vboxComprobante, "Comprobante")
        
#Libros y Balances
    def on_mnuLibrosContables_activate(self, widget, *args):
        libros = wnLibrosContables(self.cnx, self, self.cod_empresa, "vboxLibrosContables")
        self.add_tab(libros.vboxLibrosContables, "Libros_Contables")
        
#Libros Electrónicos
    def on_libros_electronicos_activate(self, widget, *args):
        librosElectronicos = wnLibrosElectronicos(self.cnx,self, self.cod_empresa, "vboxLibrosElectronicos")
        self.add_tab(librosElectronicos.vboxLibrosElectronicos, "Libros_Electronicos")
#Cuentas Contables---- Consulta de Cuentas
    def on_mnuCueContables_activate(self, widget, *args):
        consulta_cta = wnConsultaCuenta(self.cnx, self, self.cod_empresa, "vboxCuenta")
        self.add_tab(consulta_cta.vboxCuenta, "Consulta_de_Cuentas")
    
#Libro de Honorarios
    def on_mnuLibHonorarios_activate(self,widget,*args):
        honorarios=FrmLibroHonorario(self.cnx, self, self.cod_empresa,"vboxLibroHonorario")
        self.add_tab(honorarios.vboxLibroHonorario,"Boleta de Honorarios")
    def on_mnuCentralizacionHonorarios_activate(self,widget,*args):
        centraliza_honorario=FrmLibroContable(self.cnx, self, self.cod_empresa,"vboxLibroContable")
        self.add_tab(centraliza_honorario.vboxLibroContable,"Centralizacion de Honorarios")          
    def on_mnuMaestroFecu_activate(self,widget,*args):
        Fecu=FrmFecus(self.cnx,self,self.cod_empresa, 'vboxFecu')
        self.add_tab(Fecu.vboxFecu,"Maestro de Fecu")          
    def on_mnuAsignacionFecu_activate(self,widget,*args):
        asignacion_fecu=FrmAsignacionFecus(self.cnx,self,self.cod_empresa,"vbox")
        self.add_tab(asignacion_fecu.vbox,"Asignacion de Fecu")          
    def on_mnuTipoLibro_activate(self,widget,*args):
        Tipo_Libro=FrmTipoLibro(self.cnx,self,self.cod_empresa,"vboxTipoLibro")
        self.add_tab(Tipo_Libro.vboxTipoLibro,"Tipos de Libros")
    def on_mnuCreacionLibros_activate(self,widget,*args):
        creacion_libros=FrmCreacionLibros(self.cnx,self,self.cod_empresa,"vboxCreacionLibros")
        self.add_tab(creacion_libros.vboxCreacionLibros,"Creacion de Libros")
        creacion_libros.etiqueta="Creacion de Libros"
    def on_mnuLibroCompra_activate(self,widget,*args):
        Documento=FrmDocumentos(self.cnx,self,self.cod_empresa,"vboxMantenedorDocumento")
        self.add_tab(Documento.vboxMantenedorDocumento,"Mantenedor de Documentos")
        Documento.etiqueta="Mantenedor de Documentos"
    def on_mnuCentralizacionLibros_activate(self,widget,*args):
        LibroContable=FrmLibrosContables(self.cnx,self,self.cod_empresa,1,"vboxLibroContable")
        self.add_tab(LibroContable.vboxLibroContable,"Centralizacion Libro Compra")
        LibroContable.etiqueta="Centralizacion Libro Compra"
    def on_mnuActivoFijo_activate(self,widget,*args):
        ActivoFijo=FrmActivoFijo(self.cnx,self,self.cod_empresa,"vboxActivoFijo")
        self.add_tab(ActivoFijo.vboxActivoFijo,"Activos Fijos")
        self.etiqueta="Activos Fijos"
    def on_mnuParametrizacion_activate(self,widget,*args):
        ParametrizacionLibros=FrmParametrizacion(self.cnx,self,self.cod_empresa,"vboxParametrizacion")
        self.add_tab(ParametrizacionLibros.vboxParametrizacion,"Parametrizacion Libros")
        self.etiqueta="Parametrizacion Libros"
    def on_mnuConciliacionBancaria_activate(self,widget,*args):
        Conciliacion=wnConciliacion(self.cnx,self,self.cod_empresa,"vboxConciliacion")
        self.add_tab(Conciliacion.vboxConciliacion,"Conciliacion Bancaria")
        self.etiqueta="Conciliacion Bancaria"
    def on_mnuEntregasRendir_activate(self,widget,*args):
        entrega_rendir=FrmEntregaRendir(self.cnx, self, self.cod_empresa,"vboxEntregaRendir")
        self.add_tab(entrega_rendir.vboxEntregaRendir,"Entrega a Rendir")
    def on_mnuRendiciones_activate(self,widget,*args):
        rendicion=FrmRendicion(self.cnx, self, self.cod_empresa,"vboxRendicion")
        self.add_tab(rendicion.vboxRendicion,"Rendicion de Gastos")
    def on_mnuReporteMovBancario_activate(self,widget,*args):
        print "Mov.Bancario"
        ReporteMovBancario=FrmReporteMovBancario(self.cnx, self, self.cod_empresa,"vboxReporteMovBancario")
        self.add_tab(ReporteMovBancario.vboxReporteMovBancario,"Reporte de Movimiento Bancario")
    
if __name__ == "__main__":
    t = Gestor(sys.argv)
    gobject.idle_add(t.run)
    sys.excepthook = debugwindow.show
    
    gtk.main()
