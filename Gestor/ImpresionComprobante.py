#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2005 by Async Open Source and Sicem S.L.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import sys
import os
import types
from string import *
from time import *
import sys, string, os
import gtk
from comunes import CDateLocal,CDateDB,iif,CMon,Abre_pdf
from reportlab.pdfbase.ttfonts import *
from reportlab.pdfgen.canvas import Canvas
from strSQL import strSelectComprobante,strCargaComprobante,strCargaComprobanteMasivo
from Documento import PdfCustomDetail
from codificacion import CISO,CUTF82


def imprimir_comprobante(dlgDialog):
        pag = 1
        global paginas
        paginas=[]
        def cabecera_Comprobante(pc, page_num):

            pc.setFont('Courier', 10)
            w = pc.stringWidth("Comprobante Contable",'Courier', 10)
            pc.drawString(275 - w/2,800, "Comprobante Contable - ")
            model=dlgDialog.cmbTipoComprobante.get_model()
            w = pc.stringWidth(model.get_value(dlgDialog.cmbTipoComprobante.get_active_iter(),1),'Courier', 12)
            pc.drawString(375 - w/2,800, model.get_value(dlgDialog.cmbTipoComprobante.get_active_iter(),1))
            model1=dlgDialog.cmbEmpresa.get_model()
            pc.drawString(30,782,model1.get_value(dlgDialog.cmbEmpresa.get_active_iter(),1))
            pc.drawString(445,782,"Pagina: %s"%(zfill(int(page_num), 10)))

            pc.drawString(30,770,model1.get_value(dlgDialog.cmbEmpresa.get_active_iter(),9))
            pc.drawString(445,770,"Folio : %s"%(zfill(int(dlgDialog.spnFolio.get_text()), 10)))

            pc.drawString(30,758,model1.get_value(dlgDialog.cmbEmpresa.get_active_iter(),10))
            t = map(str, localtime())
            pc.drawString(445,758,"Fecha : " + CDateLocal(str(dlgDialog.entFecha.get_date())))

            pc.setFont('Courier', 7)
            pc.drawString(30,748,"GLOSA : " + dlgDialog.entGlosa.get_text())
            pc.line(30, 746, 550, 746)
            l = "Linea" + "Cuenta".rjust(15) + "Descripcion Cuenta".center(30) + "Glosa Linea".center(41) + "Debe".ljust(19) + "Haber".ljust(19)  #+ "\n"
            pc.drawString(30,739,l)
            pc.line(30, 738, 550, 738)
            pc.setFont('Courier', 7)

        def put_linea(pc,linea,strLinea,pagina):
            if linea > 75:
                c.showPage()
                pagina = pagina + 1
                cabecera_Comprobante(pc,pagina)
                linea = 7

            pc.drawString(30,738 -((linea -6)*9) ,strLinea)
            return linea +1,pagina

        def nueva_hoja():
            global textbuffer
            textbuffer= gtk.TextBuffer(None)
            paginas.append(textbuffer)

            tag = textbuffer.create_tag('courier10bold')
            tag.set_property('font', 'Courier Bold 10')
            tag.set_property('pixels_above_lines', 0)
            tag.set_property('pixels_below_lines', 0)

            tag = textbuffer.create_tag('courier12bold')
            tag.set_property('font', 'Courier Bold 12')
            tag.set_property('pixels_above_lines', 0)
            tag.set_property('pixels_below_lines', 0)

            tag = textbuffer.create_tag('courier8')
            tag.set_property('font', 'Courier 8')
            tag.set_property('pixels_above_lines', 0)
            tag.set_property('pixels_below_lines', 0)

            tag = textbuffer.create_tag('courier8bold')
            tag.set_property('font', 'Courier Bold 8')
            tag.set_property('pixels_above_lines', 0)
            tag.set_property('pixels_below_lines', 0)

            tag = textbuffer.create_tag('courier8boldundeline')
            tag.set_property('font', 'Courier Bold 8')
            tag.set_property('underline',1)
            tag.set_property('pixels_above_lines', 0)
            tag.set_property('pixels_below_lines', 0)
        model=dlgDialog.cmbTipoComprobante.get_model()
        global tipo_comprobante
        tipo_comprobante=model.get_value(dlgDialog.cmbTipoComprobante.get_active_iter(),0)
        model1=dlgDialog.cmbEmpresa.get_model()
        global tipo_numeracion
        tipo_numeracion=model1.get_value(dlgDialog.cmbEmpresa.get_active_iter(),2)

        def cabecera_pagina():
            nueva_hoja()

        cabecera_pagina()
        linea = 1

        c = Canvas('Comprobante.pdf')
        c.setPageCompression(0)
        pagina = 1
        cabecera_Comprobante(c,pagina)
        linea = 7       # linea actual en la página
        totaldebe = 0
        totalhaber = 0
        j = 0
        numpagina = 1
        strAUX =""
        for i in dlgDialog.model:
            strAUX = str(i[0]).rjust(5) + "  "
            strAUX = strAUX + i[1].ljust(15) + "  "
            strAUX = strAUX + iif(len(i[2]) >= 29,i[2][0:28],i[2].ljust(28)) + "  "
            if i[5] not in('',None):
                strAUX = strAUX + iif(len(i[5]) >= 25,i[5][0:24],i[5].ljust(24)) + "  "
            strAUX = strAUX + CMon(i[3],0).rjust(20) + " "
            strAUX = strAUX + CMon(i[4],0).rjust(20)
            linea,pagina = put_linea(c,linea,strAUX,pagina)
            totaldebe = totaldebe + float(i[3])
            totalhaber = totalhaber + float(i[4])
            end_iter = textbuffer.get_end_iter()
            strAUX = unicode(strAUX,'latin-1').encode('utf-8')
            textbuffer.insert(end_iter,strAUX + "\n")
            end_iter = textbuffer.get_end_iter()
            j = j +1

        start = textbuffer.get_iter_at_line_offset(8,0)
        textbuffer.apply_tag_by_name('courier8', start, end_iter)
        strAUX =  ""
        l = "_________________________________________________________________________________________________________________________"
        linea,pagina = put_linea(c,linea,l,pagina)

        strAUX = strAUX + str("").ljust(5) + "  "
        strAUX = strAUX + "".ljust(15) + "  "
        strAUX = strAUX + "".ljust(28) + "  "
        strAUX = strAUX + "TOTAL COMPROBANTE".ljust(24) + "  "
        strAUX = strAUX + CMon(totaldebe,0).rjust(20) + " "
        strAUX = strAUX + CMon(totalhaber,0).rjust(20)
        textbuffer.insert(end_iter,l + strAUX + "\n")
        linea,pagina = put_linea(c,linea,strAUX,pagina)

        end_iter = textbuffer.get_end_iter()
        start = textbuffer.get_iter_at_line_offset(0,0)
        textbuffer.apply_tag_by_name('courier8', start, end_iter)

        c.drawString(30,738 -((79 -6)*9) ,l)

        strAux = "CONTABILIDAD               TESORERIA               V° B° Gerencia          V° B° Gerencia          Recibi Conforme"
        c.drawString(30,738 -((80 -6)*9) , strAux)
        strAux = "                                                                                                   R.U.T.:"
        c.drawString(30,738 -((81 -6)*9) ,strAux)
        c.drawString(30,738 -((82 -6)*9) ,l)
        c.showPage()
        c.save()
##        self.btnVistaPrevia.set_sensitive(0)
        Abre_pdf("Comprobante.pdf")
        
        
def imprimir_detalle_comprobante(cod_comprobante,cnx):
        sql=strSelectComprobante.replace("""JOIN ctb.empresa e
                        on c.cod_empresa = e.cod_empresa""","""JOIN ctb.empresa e
                        on c.cod_empresa = e.cod_empresa where c.cod_comprobante=%s""")
        sql=sql %(cod_comprobante)
        cnx.execute(sql)
        r=cnx.fetchall()
        if len(r)==0:
            return
        
        tipo_comprobante=r[0][2]
        folio_comprobante=r[0][18]
        fecha_comprobante=r[0][11]
        empresa=r[0][17]
        
        sql=strCargaComprobante %(cod_comprobante)
        cnx.execute(sql)
        r=cnx.fetchall()
        if len(r)==0:
            return
        
        info = []
        info.append([CUTF82("Cod.Comprobante: ")+str(CUTF82(cod_comprobante)),0])
        info.append([CUTF82("NºFolio         : ") + str(CUTF82(folio_comprobante)),1])
        info.append([CUTF82("Fecha Comprobante: ") +str(CUTF82(fecha_comprobante)), 2])
        info.append([CUTF82("Empresa:")+empresa,3])
        
        a = PdfCustomDetail(
            title="Comprobante %s"%(tipo_comprobante),
            filename="inf_comprobante_%s.pdf"%(tipo_comprobante),
            companyInfo=info,
            folio="",
            logo=False
            )
        data=[]
        fila=[]
        fila.append(CUTF82("N°"))
        fila.append(CUTF82("N° Cuenta"))
        fila.append(CUTF82("Descripcion"))
        fila.append(CUTF82("Debe"))
        fila.append(CUTF82("Haber"))
        fila.append(CUTF82("Glosa"))
        data.append(fila)
        a.drawData(data, ["","","","","",""],
                                    [30,60,130,80,80,150],
                                    ["LEFT","LEFT","LEFT","RIGHT","RIGHT","LEFT"],row_height=10)
        fila=[]
        data=[]
        debe=0L
        haber=0L
        for i in r:
            fila=[]
            fila.append(CUTF82(i[0]))
            fila.append(CUTF82(i[1]))
            fila.append(CUTF82(i[2]))
            fila.append(CUTF82(CMon(i[3],0).rjust(20)))
            fila.append(CUTF82(CMon(i[4],0).rjust(20)))
            debe+=float(i[3])
            haber+=float(i[4])
            fila.append(CUTF82(i[5].lower()))
            data.append(fila)
            fila=[]
            fila.append(CUTF82("Folio"))
            fila.append(CUTF82("Fecha"))
            fila.append(CUTF82("Documento"))
            fila.append(CUTF82("Ficha"))
            fila.append("")
            fila.append("")
            data.append(fila)
            fila=[]
            fila.append(CUTF82(i[6]))
            fila.append(CUTF82(i[7]))
            if i[9] in ('',None):
                fila.append(CUTF82(''))
            else:
                fila.append(CUTF82(i[9].lower()))
            if i[11] in('',None):
                fila.append(CUTF82(''))
            else:
                fila.append(CUTF82(i[11].lower()))
            fila.append("")
            fila.append("")
            data.append(fila)
            fila=[]
            fila.append('')
            fila.append('')
            fila.append('')
            fila.append('')
            fila.append('')
            fila.append('')
            data.append(fila)
        a.drawData(data, ["","","","","",""],
                                    [30,60,130,80,80,150],
                                    ["LEFT","LEFT","LEFT","RIGHT","RIGHT","LEFT"],row_height=10)
        fila=[]
        data=[]
        fila.append("")
        fila.append("")
        fila.append(CUTF82("Total Comprobante:"))
        fila.append(CUTF82(CMon(debe,0).rjust(20)))
        fila.append(CUTF82(CMon(haber,0).rjust(20)))
        fila.append("")
        data.append(fila)
        a.drawData(data, ["","","","","",""],
                                    [30,60,130,80,80,150],
                                    ["LEFT","LEFT","LEFT","RIGHT","RIGHT","LEFT"],row_height=10)
        data=[]
        fila=[]
        fila.append('')
        fila.append('')
        fila.append('')
        fila.append('')
        fila.append('')
        data.append(fila)
        a.drawDataBlanco(data, ["","","","","",""],
                                    [90,90,90,90,90],
                                    ["CENTER","CENTER","CENTER","CENTER","CENTER"],row_height=10)
        data=[]
        fila=[]
        fila.append(CUTF82('CONTABILIDAD'))
        fila.append(CUTF82('TESORERIA'))
        fila.append(CUTF82('V° B° Gerencia'))
        fila.append(CUTF82('V° B° Gerencia'))
        fila.append(CUTF82('Recibi Conforme'))
        data.append(fila)
        fila=[]
        fila.append('')
        fila.append('')
        fila.append('')
        fila.append('')
        fila.append(CUTF82('R.U.C.:'))
        data.append(fila)
        a.drawData(data, ["","","","","",""],
                                    [90,90,90,90,90],
                                    ["CENTER","CENTER","CENTER","CENTER","CENTER"],row_height=10)
        a.go()

def imprimir_detalle_comprobante_masivo(cod_comprobante,cnx):
        
        results = map(int,cod_comprobante)
        results2=str(results).strip('[]')
        print results2
        
        #print results
        
        sql=strSelectComprobante.replace("""JOIN ctb.empresa e
                        on c.cod_empresa = e.cod_empresa""","""JOIN ctb.empresa e
                        on c.cod_empresa = e.cod_empresa where c.cod_comprobante in (%s)""")
        sql=sql %(results2)
        cnx.execute(sql)
        r=cnx.fetchall()
        if len(r)==0:
            return
        tipo_comprobante=r[0][2]
        folio_comprobante=r[0][18]
        fecha_comprobante=r[0][11]
        empresa=r[0][17]
        sql=strCargaComprobanteMasivo%(results2)
        print sql
        cnx.execute(sql)
        r=cnx.fetchall()
        if len(r)==0:
            return
        
        info = []
        info.append([CISO("NºFolio         : ") + str(CISO(folio_comprobante)),0])
        info.append([CISO("Fecha Comprobante: ") +str(CISO(fecha_comprobante)), 1])
        info.append([CISO("Empresa:")+empresa,2])
        a = PdfCustomDetail(
            title="Comprobante %s"%(tipo_comprobante),
            filename="inf_comprobante_%s.pdf"%(tipo_comprobante),
            companyInfo=info,
            folio="",
            logo=False
            )
        data=[]
        fila=[]
        fila.append(CISO("N°"))
        fila.append(CISO("N° Cuenta"))
        fila.append(CISO("Descripcion"))
        fila.append("Debe")
        fila.append("Haber")
        fila.append("glosa")
        data.append(fila)
        a.drawData(data, ["","","","","",""],
                                    [30,60,130,80,80,150],
                                    ["LEFT","LEFT","LEFT","RIGHT","RIGHT","LEFT"],row_height=10)
        fila=[]
        data=[]
        debe=0L
        haber=0L
        for i in r:
            fila=[]
            fila.append(CISO(i[0]))
            fila.append(CISO(i[1]))
            fila.append(CISO(i[2]))
            fila.append(CISO(CMon(i[3],0).rjust(20)))
            fila.append(CISO(CMon(i[4],0).rjust(20)))
            debe+=float(i[3])
            haber+=float(i[4])
            fila.append(CISO(i[5].lower()))
            data.append(fila)
            fila=[]
            fila.append("Folio")
            fila.append("Fecha")
            fila.append("Documento")
            fila.append("Ficha")
            fila.append("")
            fila.append("")
            data.append(fila)
            fila=[]
            fila.append(CISO(i[6]))
            fila.append(CISO(i[7]))
            if i[9] in ('',None):
                fila.append(CISO(''))
            else:
                fila.append(CISO(i[9].lower()))
            if i[11] in('',None):
                fila.append(CISO(''))
            else:
                fila.append(CISO(i[11].lower()))
            fila.append("")
            fila.append("")
            data.append(fila)
            fila=[]
            fila.append('')
            fila.append('')
            fila.append('')
            fila.append('')
            fila.append('')
            fila.append('')
            data.append(fila)
        a.drawData(data, ["","","","","",""],
                                    [30,60,130,80,80,150],
                                    ["LEFT","LEFT","LEFT","RIGHT","RIGHT","LEFT"],row_height=10)
        fila=[]
        data=[]
        fila.append("")
        fila.append("")
        fila.append(CISO("Total Comprobante:"))
        fila.append(CISO(CMon(debe,0).rjust(20)))
        fila.append(CISO(CMon(haber,0).rjust(20)))
        fila.append("")
        data.append(fila)
        a.drawData(data, ["","","","","",""],
                                    [30,60,130,80,80,150],
                                    ["LEFT","LEFT","LEFT","RIGHT","RIGHT","LEFT"],row_height=10)
        data=[]
        fila=[]
        fila.append('')
        fila.append('')
        fila.append('')
        fila.append('')
        fila.append('')
        data.append(fila)
        a.drawDataBlanco(data, ["","","","","",""],
                                    [90,90,90,90,90],
                                    ["CENTER","CENTER","CENTER","CENTER","CENTER"],row_height=10)
        data=[]
        fila=[]
        fila.append('CONTABILIDAD')
        fila.append('TESORERIA')
        fila.append('V° B° Gerencia')
        fila.append('V° B° Gerencia')
        fila.append('Recibi Conforme')
        data.append(fila)
        fila=[]
        fila.append('')
        fila.append('')
        fila.append('')
        fila.append('')
        fila.append('R.U.T.:')
        data.append(fila)
        a.drawData(data, ["","","","","",""],
                                    [90,90,90,90,90],
                                    ["CENTER","CENTER","CENTER","CENTER","CENTER"],row_height=10)
        a.go()
