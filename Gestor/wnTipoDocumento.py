#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnTipoDocumento -- formulario de ingreso, eliminación y edición, de los diferentes Tipos de Documentos contables.

# (C)   Fernando San Martín Woerner (c) 2003, 2004
#       snmartin@galilea.cl

#This file is part of pyGestor.
#
# pyGestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# (C)    José Luis Álvarez Morales     2006, 2007.
#           josealvarezm@gmail.com

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
from strSQL import strSelectTipoDocumento

(CODIGO,
 TIPO_DOC_CONTABLE,
 MANEJA_SALDO) = range(3)

class wnTipoDocumento(GladeConnect):
    def __init__(self, conexion=None, padre=None, root="wnTipoDocumento"):
        GladeConnect.__init__ (self, "glade/wnTipoDocumento.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnTipoDocumento.maximize()
            self.frm_padre = self.wnTipoDocumento
        else:
            self.frm_padre = self.padre.frm_padre
        
##        self.pecSucursal=completion.CompletionSucursal(self.entSucursal,
##                self.sel_sucursal,
##                self.cnx)  
        
        self.crea_columnas()
        self.carga_datos()
            
    def crea_columnas(self):
        self.modelo=gtk.ListStore(str,str,str)
        columnas = []
        columnas.append([CODIGO,"Código","str"])
        columnas.append([TIPO_DOC_CONTABLE, "Tipo de Documento", "str"])
        columnas.append([MANEJA_SALDO, "Maneja saldos", "str"])      
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeTipoDocumento)
        self.carga_datos()
    def carga_datos(self):
##        
        self.cursor.execute(strSelectTipoDocumento)
        r=self.cursor.fetchall()
        self.modelo.clear()
        for i in r:
            self.modelo.append(i)
    
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgTipoDocumento(self.cnx, self.frm_padre, False)
        dlg.editando = False
        band = False
        while not band:
            response = dlg.dlgTipoDocumento.run()
            if response == gtk.RESPONSE_OK:
                if dlg.commit == True:
                    self.modelo.append(dlg.response)
                    band = True
            else:
                band = True
        
        dlg.dlgTipoDocumento.destroy()
            
    def on_btnQuitar_clicked(self, btn=None):
        model, it = self.treeTipoDocumento.get_selection().get_selected()
        if model is None or it is None:
            return
        tipo_doc = model.get_value(it, TIPO_DOC_CONTABLE)
        codigo = model.get_value(it, CODIGO)
        
        if dialogos.yesno("¿Desea eliminar el Tipo de Documento Contable <b>%s</b>?\nEsta acción no se puede deshacer!!!"%tipo_doc, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'cod_doc_contable': codigo}
            sql = ifd.deleteFromDict("ctb.tipo_doc_contable",llaves)
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>\nEl Tipo de Documento <b>%s</b>, se encuentra relacionado."%(model.get_value(it,TIPO_DOC_CONTABLE)))

    def on_btnPropiedades_clicked(self, btn=None):
        model, it = self.treeTipoDocumento.get_selection().get_selected()
        if model is None or it is None:
            return
        
        dlg = dlgTipoDocumento(self.cnx, self.frm_padre, False)
        dlg.entCodigo.set_text(model.get_value(it, CODIGO))
        dlg.entTipoDocumento.set_text(model.get_value(it, TIPO_DOC_CONTABLE))
        dlg.maneja_saldo = (model.get_value(it, MANEJA_SALDO))
                
        if dlg.maneja_saldo == 'N':
            dlg.cmbManejaSaldo.set_active(0)
        else:
            dlg.cmbManejaSaldo.set_active(1)
            
        dlg.editando = True
        band=False
        while not band:
            response = dlg.dlgTipoDocumento.run()
            if response == gtk.RESPONSE_OK:
                if dlg.commit==True:
                    del model[it]
                    self.modelo.append(dlg.response)
                    band=True
            else:
                band=True
            
        dlg.dlgTipoDocumento.destroy()

    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Tipo_Documento_Contable")
    
    def on_treeTipoDocumento_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()
        
class dlgTipoDocumento(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=None):
        GladeConnect.__init__(self, "glade/wnTipoDocumento.glade", "dlgTipoDocumento")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.entTipoDocumento.grab_focus()
        self.dlgTipoDocumento.show()
        self.editando=editando
        self.response = None
        self.cmbManejaSaldo.set_active(0)
        self.commit=False
    def on_btnAceptar_clicked(self,btn=None):
        if self.entTipoDocumento.get_text() == "":
            dialogos.error("El campo <b>Tipo Documento</b> no puede estar vacio!")
            self.entTipoDocumento.grab_focus()
            return
  
        else:
            campos = {}
            llaves = {}
         
            active = self.cmbManejaSaldo.get_active()
            if  active == -1:
                return
            
            modelo = self.cmbManejaSaldo.get_model()
            ManejaSaldo = modelo[active] [0]
                
            campos['descripcion_documento']= self.entTipoDocumento.get_text().upper()
            campos['maneja_saldo'] = ManejaSaldo
            
            if not self.editando:
                self.cursor.execute("select nextval('ctb.tipo_doc_contable_cod_doc_contable_seq')")
                r = self.cursor.fetchall()
                self.entCodigo.set_text(str(r[0][0]))
                campos['cod_doc_contable'] = self.entCodigo.get_text()
                sql = ifd.insertFromDict("ctb.tipo_doc_contable", campos)
            else:
                llaves['cod_doc_contable'] = self.entCodigo.get_text()
                sql, campos = ifd.updateFromDict("ctb.tipo_doc_contable", campos, llaves)
            
            try:
                self.cursor.execute(sql, campos)
                self.dlgTipoDocumento.hide()
                
                self.response = [self.entCodigo.get_text(),
                                        self.entTipoDocumento.get_text().upper(),
                                        ManejaSaldo]
                self.commit=True
            except:
                print sys.exc_info()[1]
                descripcion_documento = self.entTipoDocumento
                dialogos.error("En la Base de  Datos ya existe el Tipo Documento Contable <b>%s</b>."%campos['descripcion_documento'])
                self.entTipoDocumento.grab_focus()
            
               
    def on_btnCancelar_clicked(self, btn=None):
        self.dlgTipoDocumento.hide()

if __name__ == "__main__":
    cnx = connect("host=localhost dbname = scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    w = wnTipoDocumento(cnx)
    
    gtk.main()
