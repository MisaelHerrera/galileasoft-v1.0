#!/usr/bin/env python
# -*- coding: UTF8 	-*-

import os
import gtk
from SimpleGladeApp import SimpleGladeApp
from spg import connect
from comunes import *
from completion import CompletionEmpresa
import SimpleTree
from strSQL import strPlanCuentas,strPlanCuentasFecus

class dlgSeleccionEmpresa(gtk.Dialog):

    def __init__(self, cnx, window):
    
        gtk.Dialog.__init__(self, 
                            "Selección de empresa", 
                            None, 
                            0,
                            (gtk.STOCK_OK, 
                                gtk.RESPONSE_OK))
        
        if not window is None:
            self.set_transient_for(window)
        if cnx == None:
            error(window, 'No hay una conección activa.')
            return 
            
        self.desc_empresa = ""
        self.cod_empresa = -1
        self.formula_rut = 'rut'
        self.decimales = 0
        self.set_size_request(500, 200)
        hbox = gtk.HBox(False, 8)
        hbox.set_border_width(8)
        self.vbox.pack_start(hbox, True, True)
        stock = gtk.Image()
        stock.set_from_stock(gtk.STOCK_DIALOG_QUESTION, gtk.ICON_SIZE_DIALOG)
        hbox.pack_start(stock, False, False)

        model = gtk.ListStore(str, str, str, str, str)

        lista = gtk.TreeView()
        col = gtk.TreeViewColumn("Empresa", gtk.CellRendererText(), text=0)
        lista.append_column(col)
        col = gtk.TreeViewColumn("Código", gtk.CellRendererText(), text=1)
        lista.append_column(col)
        col = gtk.TreeViewColumn("R.U.T.", gtk.CellRendererText(), text=2)
        lista.append_column(col)
        lista.set_model(model)
        lista.connect('row-activated', self.on_list_row_activated)
        self.lista = lista
        try:
            r = cnx.cursor()
            r.execute("select descripcion_empresa, cod_empresa, rut_empresa,coalesce(formula_rut,'rut'), decimales from ctb.empresa join ctb.moneda using (cod_moneda) order by descripcion_empresa")
            l = r.fetchall()
        except :
            try:
                r = cnx.cursor()
                r.execute("select descripcion_empresa, cod_empresa, rut_empresa,'rut', 0 from ctb.empresa order by descripcion_empresa")
                l = r.fetchall()
            except :
                error(window,sys.exc_info()[1])
                return 
        
        for record in l:
            model.append(record)
        
        hbox.pack_start(lista, True, True)

        self.set_default_response(gtk.RESPONSE_OK)
        self.show_all()
        lista.grab_focus()

    def seleccionar(self):
        response = self.run()
        if response == gtk.RESPONSE_OK:
            return self.select()
        else:
            return self.cod_empresa

    def select(self):
        try:
            m, i = self.lista.get_selection().get_selected()
            code = m[i][1]

            self.cod_empresa = code
            self.desc_empresa = m[i][0]
            self.formula_rut  = m[i][3]
            self.decimales = m[i][4]
            self.destroy()
            return code
        except:
            print sys.exc_info()[1]
            self.destroy()
            return -1    
            
    def on_list_row_activated(self, tree, path, col):
        self.select()

if __name__ == "__main__":
    cnx = connect("dbname = gestor")
    cnx.autocommit()
    print dlgSeleccionEmpresa(cnx, None).seleccionar()                
