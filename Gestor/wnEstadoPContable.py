#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnEstadoPContable -- formulario de ingreso, eliminación y edición, de los distintos tipos de Períodos Contables,
# para cada Empresa en el sistema.

# (C)   Claudio Salgado Morales 2003, 2004

#This file is part of pyGestor.
#
# pyGestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# (C)    José Luis Álvarez Morales   2006, 2007.
#           josealvarezm@gmail.com

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
from strSQL import strSelectEstadoPContable

(CODIGO,
 DESCRIPCION) = range(2)

class wnEstadoPContable(GladeConnect):
    def __init__(self, conexion=None, padre=None, root="wnEstadoPContable"):
        GladeConnect.__init__(self, "glade/wnEstadoPContable.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnEstadoPContable.maximize()
            self.frm_padre = self.wnEstadoPContable
            self.padre
        else:
            self.frm_padre = padre.frm_padre
            
        self.crea_columnas()
        self.carga_datos()
    
    def crea_columnas(self):
        self.modelo=gtk.ListStore(str,str)
        columnas = []
        columnas.append([CODIGO, "Código", "int"])
        columnas.append([DESCRIPCION, "Descripción Estado", "str"])        
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeEstadoPContable)
        self.carga_datos()
    def carga_datos(self):
        self.cursor.execute(strSelectEstadoPContable)
        r=self.cursor.fetchall()
        self.modelo.clear()
        for i in r:
            self.modelo.append(i)
    
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgEstadoPContable(self.cnx, self.frm_padre, False)
        dlg.editando = False
        band = False
        while not band:
            response = dlg.dlgEstadoPContable.run()
            if response == gtk.RESPONSE_OK:
                if dlg.commit == True:                    
                    self.modelo.append(dlg.response)
                    band = True
            else:
                band = True
                
##        dlg.commit=False
##        band=False
##        while ((band==False) and (dlg.dlgEstadoPContable.run() != gtk.RESPONSE_CANCEL)):
##            if dlg.commit==True:
##                self.carga_datos()                
##                band=True
        dlg.dlgEstadoPContable.destroy()
    
    def on_btnQuitar_clicked(self, btn=None):
        model, it = self.treeEstadoPContable.get_selection().get_selected()
        if model is None or it is None:
            return
        codigo = model.get_value(it, CODIGO)
        descripcion = model.get_value(it, DESCRIPCION)
        
        if dialogos.yesno("¿Desea eliminar el Estado Periodo Contable <b>%s</b>?\nEsta acción no se puede deshacer!!!"%descripcion, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'cod_estado':codigo}
            sql = ifd.deleteFromDict('ctb.estado_periodo_contable', llaves)
            
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()[1]
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>\nEl Estado Periodo Contable<b>%s</b>, se encuentra relacionado.")
                
    def on_btnPropiedades_clicked(self, btn=None):
        model, it = self.treeEstadoPContable.get_selection().get_selected()
        if model is None or it is None:
            return
        
        dlg = dlgEstadoPContable(self.cnx, self.frm_padre, False)
        dlg.entCodigo.set_text(model.get_value(it, CODIGO))
        dlg.entDescripcion.set_text(model.get_value(it, DESCRIPCION))
        
        dlg.entCodigo.set_sensitive(False)
        
        dlg.editando = True
        
        band=False
        while not band:
            response = dlg.dlgEstadoPContable.run()
            if response == gtk.RESPONSE_OK:
                if dlg.commit==True:
                    del model[it]
                    self.modelo.append(dlg.response)
                    band=True
            else:
                band=True
        dlg.dlgEstadoPContable.destroy()
            
    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Estado_Periodo_Contable")
            
    def on_treeEstadoPContable_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()
        
class dlgEstadoPContable(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=None):
        GladeConnect.__init__(self, "glade/wnEstadoPContable.glade", "dlgEstadoPContable")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.editando = editando
        self.entCodigo.grab_focus()
        self.response = None
        self.dlgEstadoPContable.show_all()
        self.commit=False
        
    def on_btnAceptar_clicked(self, btn=None):
        if self.entCodigo.get_text() ==""  or  len(self.entCodigo.get_text()) >1 or len(self.entCodigo.get_text().replace(" ",""))==0:
            dialogos.error("El campo <b>Código</b> no puede estar vacío o exceder el largo de un caracter.")            
            return
        
        if self.entDescripcion.get_text() == "" or len(self.entDescripcion.get_text().replace(" ",""))==0:
            dialogos.error("El campo <b>Descripción Estado</b> no puede estar vacío.")
            return
        
        campos = {}
        llaves = {}
       
        campos ['descripcion_estado'] = self.entDescripcion.get_text().upper()  
      
        if not self.editando:
            campos ['cod_estado'] = self.entCodigo.get_text().upper()
            sql = ifd.insertFromDict("ctb.estado_periodo_contable", campos)
        else:
            llaves['cod_estado'] = self.entCodigo.get_text()
            sql, campos = ifd.updateFromDict("ctb.estado_periodo_contable", campos, llaves)
        try:
            self.cursor.execute(sql,campos)
            self.dlgEstadoPContable.hide()
            
            self.response = [self.entCodigo.get_text().upper(),
                                    self.entDescripcion.get_text().upper()]
            self.commit = True
        except:
            print sys.exc_info()[1]
            cod_estado = self.entCodigo
            dialogos.error("En la Base de Datos ya existe el Código Estado Periodo Contable <b>%s</b>."%campos['cod_estado'])
            self.entCodigo.grab_focus()
            
       
                
    def on_btnCancelar_clicked(self, btn=None):
        self.dlgEstadoPContable.hide()
        
if __name__ == '__main__':
    cnx = connect("host=localhost dbname = scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    d = wnEstadoPContable(cnx)
    
    gtk.main()
 
