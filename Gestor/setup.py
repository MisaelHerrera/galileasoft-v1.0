from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages = [], excludes = [])

executables = [
    Executable('/home/antonny/E', 'Win32GUI', targetName = 'wnGestor.py')
]

setup(name='maxsecurity',
      version = '1.0',
      description = 'elevacion de seguridad 1.0',
      options = dict(build_exe = buildOptions),
      executables = executables)
