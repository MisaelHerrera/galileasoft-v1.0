#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ctb_rutinas -- Libreria de rutinas generales usadas en los demás mdulos

# (C)   Fernando San Martín Woerner 2003, 2004
# (C)   Victor M. Benitez Tejos 2003, 2004

#This file is part of pyGestor.
#
# pyGestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sys
import string
import gobject
import gtk
import time

import os
from types import StringType
from decimal import Decimal
from spg import connect
DB_MODULE = "psycopg"
	

def ceros(s, n=0):
    return string.zfill(s,n)


def Abre_pdf(arch):
    if sys.platform=="win32":
        os.startfile(arch)
        return
        acrord = 'c:\\Archivos de programa\\Adobe\\Acrobat 5.0\\Reader\\AcroRd32.exe'

        #~ acrord = "explorer.exe"
        #~ acrord = os.getcwd() + "\\pdfreader\\pdfreader.exe"

        args = [acrord, "AcroRd32.exe",arch]

        try:
            os.spawnv(os.P_NOWAIT, args[0], args[1:])
        except:
            acrord = os.getcwd() + "\\pdfreader\\pdfreader.exe"
            args = [acrord, "pdfreader.exe",arch]
            try:
                os.spawnv(os.P_NOWAIT, args[0], args[1:])
            except:
                print "no hay un visor registrado."


    else:
        if os.spawnv(os.P_NOWAIT, '/usr/bin/xpdf', ['xpdf', arch]) != 0 :
            pass
        elif os.spawnv(os.P_NOWAIT, '/usr/bin/acroread', ['acroread', arch]) != 0 :
            pass
        elif os.spawnv(os.P_NOWAIT, '/usr/bin/gpdf', ['gpdf', arch]) != 0 :
            pass
        elif os.spawnv(os.P_NOWAIT, '/usr/bin/evince', ['evince', arch]) != 0 :
            pass

def ctb_aviso(self, window, mensaje):
    dialog = gtk.MessageDialog(window, gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,gtk.MESSAGE_INFO, gtk.BUTTONS_OK, mensaje)
    dialog.set_default_response(gtk.BUTTONS_OK)
    dialog.connect('response', lambda dialog, response: dialog.destroy())
    dialog.show()

def ctb_error(window, mensaje):
    dialog = gtk.MessageDialog(window, gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, mensaje)
    dialog.set_default_response(gtk.BUTTONS_OK)
    dialog.connect('response', lambda dialog, response: dialog.destroy())
    dialog.show()

def ctb_pregunta_si_no(self, window, mensaje, titulo):
    dialog = gtk.Dialog(titulo, window, 0,(gtk.STOCK_NO, gtk.RESPONSE_NO, gtk.STOCK_YES, gtk.RESPONSE_YES))
    hbox = gtk.HBox(False, 8)
    hbox.set_border_width(8)
    dialog.vbox.pack_start(hbox, False, False, 0)

    stock = gtk.image_new_from_stock(
                                    gtk.STOCK_DIALOG_QUESTION,
                                    gtk.ICON_SIZE_DIALOG)

    hbox.pack_start(stock, False, False, 0)
    label = gtk.Label(mensaje)
    hbox.pack_start(label, True, True, 0)
    dialog.show_all()
    response = dialog.run()
    dialog.destroy()
    return response

def ctb_barra_progreso(self, window, mensaje, titulo,progreso):
    m = unicode(mensaje, 'latin-1')
    t = unicode(titulo, 'latin-1')
    self.dialog = gtk.Dialog(t.encode('utf-8'),
                            window, 0,
                            (gtk.STOCK_NO, gtk.RESPONSE_NO, gtk.STOCK_YES, gtk.RESPONSE_YES))

    hbox = gtk.HBox(False, 8)
    hbox.set_border_width(8)
    self.dialog.vbox.pack_start(hbox, False, False, 0)
    stock = gtk.image_new_from_stock(
                                    gtk.STOCK_DIALOG_QUESTION,
                                    gtk.ICON_SIZE_DIALOG)

    hbox.pack_start(stock, False, False, 0)
    self.barra = gtk.ProgressBar()
    self.barra.set_text(m.encode('utf-8'))
    self.barra.set_fraction(progreso)
    hbox.pack_start(self.barra, True, True, 0)
    self.dialog.show_all()
    response = self.dialog.run()
    return response

def ctb_coneccion(window=None, db=None, rc=None):
    if rc == None:
        try:
            f = open('.pygestor.rc','r')
        except:
            try:
                f = open('/etc/pygestor.rc','r')
            except:
                try:
                    f = open('/opt/pyGestor/pygestor.rc')
                except:
                    ctb_error(None, "No se encuentra el archivo /etc/pygestor.rc")
    else:
        try:
            f = open(rc,'r')
        except:
            ctb_error(None, "No se encuentra el archivo " + rc)
    l = f.readlines()
    seccion = ""
    db = ""
    host = ""
    port = ""
    user = ""
    secciones = []
    
    print "port "+str(port)
    model = gtk.ListStore(str, str, str, str, str)
    for i in l:
        if i[-1] == "\n":
            linea = i[:-1]
        else:
            linea = i
        if not "#" in linea:
            if '[' in i and ']' in i:
                if seccion == "":
                    seccion = i[1:-2]
                else:
                    if db.strip() != "":
                        model.append((seccion, db, host, port, user))
                    db = ""
                    host = ""
                    port = ""
                    user = ""
                    seccion = i[1:-2]
            else:
                opciones = linea.split('=')
                if opciones[0].upper().strip() == "DATABASE":
                    db = opciones[1].strip()
                if opciones[0].upper().strip() == "HOST":
                    host = opciones[1].strip()
                if opciones[0].upper().strip() == "PORT":
                    port = opciones[1].strip()
                if opciones[0].upper().strip() == "USER":
                    user = opciones[1].strip()
    if not db == "":
        model.append((seccion, db, host, port, user))
    f.close()
    
    print "port "+str(port)
    dialog = gtk.Dialog("Bienvenido a Gestor", window, 0,
            (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_OK, gtk.RESPONSE_OK))

    hbox = gtk.HBox(False, 8)
    hbox.set_border_width(8)
    dialog.vbox.pack_start(hbox, False, False, 0)
    stock = gtk.Image()
    stock.set_from_stock(gtk.STOCK_DIALOG_AUTHENTICATION, gtk.ICON_SIZE_DIALOG)
    hbox.pack_start(stock, False, False, 0)
    table = gtk.Table(3, 2)
    table.set_row_spacings(4)
    table.set_col_spacings(4)
    hbox.pack_start(table, True, True, 0)
    label = gtk.Label("Conección:")
    label.set_use_underline(True)
    table.attach(label, 0, 1, 0, 1)
    combo = gtk.ComboBox(model)
    cell = gtk.CellRendererText()
    combo.pack_start(cell, True)
    combo.add_attribute(cell, 'text', 0) 

    local_entry1 = gtk.Entry()
    table.attach(combo, 1, 2, 0, 1)
    label = gtk.Label("Usuario:")
    label.set_use_underline(True)
    table.attach(label, 0, 1, 1, 2)
    local_entry1.set_text(user)
    table.attach(local_entry1, 1, 2, 1, 2)
    label.set_mnemonic_widget(local_entry1)
    label = gtk.Label("Contraseña:")
    label.set_use_underline(True)
    table.attach(label, 0, 1, 2, 3)
    local_entry2 = gtk.Entry()
    local_entry2.set_visibility(0)
    local_entry2.set_invisible_char("*")
    local_entry2.set_text("")
    local_entry2.set_activates_default(1)
    table.attach(local_entry2, 1, 2, 2, 3)
    label.set_mnemonic_widget(local_entry2)
    dialog.set_default_response(gtk.RESPONSE_OK)
    def combo_changed_cb(combo):
        active = combo.get_active()
        model = combo.get_model()
        user = model[active][4]
        local_entry1.set_text(user)
    combo.connect_after('changed', combo_changed_cb)
    combo.set_active(0)        

    dialog.show_all()

    local_entry2.grab_focus()
    response = dialog.run()
    usuario = local_entry1.get_text()
    password = local_entry2.get_text()
    cnx, cursor = (None, None)
    if response == gtk.RESPONSE_OK:
        try:
            model = combo.get_model()
            active = combo.get_active()
            db = model[active][1]
            host = model[active][2]
            port = model[active][3]
            user = model[active][4]
	    
	    print "port "+str(port)
            if host == "":
                cnx = connect(database=db)
            #cnx = connect(host+":"+port+":"+db+":"+usuario+":"+password)
            if DB_MODULE == "pypglsq":
                cnx = connect(host+":"+port+":"+db+":"+usuario+":"+password)
                cnx.autocommit = 'True'

            if DB_MODULE == "psycopg":
                #if db.strip().upper() in('scp','scc','SCC','SCP'):
                #    db='gsa'
                cnx = connect("host=%s port=%s dbname=%s user=%s password=%s" % (host,port, db, usuario, password))
                cnx.autocommit()
            cursor = cnx.cursor()	        
            cursor.execute("set client_encoding = utf8")
        except:
            print sys.exc_info()[1]
            cnx, cursor = (None, None)
        dialog.destroy()
##        return cnx, cursor
    else:
        dialog.destroy()
    return cnx, cursor

def ctb_selecciona_empresa(cnx=None, window=None):
    if cnx == None:
        ctb_error(window, 'No hay una conección activa.')
        return 0

    lbl = unicode("Selección de Empresa", "latin-1")
    dialog = gtk.Dialog("Selección de empresa", window, 0,(gtk.STOCK_NEW, gtk.RESPONSE_CANCEL, gtk.STOCK_OK, gtk.RESPONSE_OK))
    dialog.set_size_request(500, 100)
    hbox = gtk.HBox(False, 8)
    hbox.set_border_width(8)
    dialog.vbox.pack_start(hbox, 1, False, 0)
    stock = gtk.Image()
    stock.set_from_stock(gtk.STOCK_EDIT, gtk.ICON_SIZE_MENU)
    hbox.pack_start(stock, False, False, 0)
    label = gtk.Label("Empresa")
    label.set_use_underline(True)
    hbox.pack_start(label, 0, 0, 0)
    model = gtk.ListStore(str, str)
    combo = gtk.ComboBoxEntry(model, 0)
    #~ combo.set_text_column(0)
    #~ cell = gtk.CellRendererText()
    #~ combo.pack_start(cell, True)
    #~ combo.add_attribute(cell, 'text', 0) 
    try:
        r = cnx.cursor()
        r.execute("select descripcion_empresa, cod_empresa from ctb.empresa order by descripcion_empresa")
        l = r.fetchall()
    except :
        ctb_error(window,sys.exc_info()[1])
        return 0
    for record in l:
        model.append(record)
    if len(l) > 0:
        combo.set_active(0)
    hbox.pack_start(combo, True, True, 0)
    label.set_mnemonic_widget(combo)
    dialog.set_default_response(gtk.RESPONSE_OK)
    dialog.show_all()
    combo.grab_focus()
    response = dialog.run()
    if response == gtk.RESPONSE_OK:
        try:
            window.set_title(combo.child.get_text() + " - Contabilidad General")
            code = model[combo.get_active()][1]
            dialog.destroy()
            return code
        except:
            print sys.exc_info()[1]
            return -1
    else:
        if response == gtk.RESPONSE_CANCEL:
##            cursor = cnx.cursor()
##            cursor.execute("insert into ctb.empresa (descripcion_empresa) values ('" + combo.child.get_text().upper() + "')")
##            cursor.execute("select cod_empresa from ctb.empresa where descripcion_empresa = '" + combo.child.get_text().upper() + "'")
##            r = cursor.fetchall()
            window.set_title("Contabilidad General  " + combo.child.get_text() )
            
            dialog.destroy()
##            return r[0][0]
        else:
            dialog.destroy()
            return -1

def ctb_cambia_folio(self=None, window=None):
    lbl = unicode("Ingrese el Nuevo Folio", "latin-1")
    dialog = gtk.Dialog(lbl.encode("utf-8"), window, 0,(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,gtk.STOCK_OK, gtk.RESPONSE_OK))
    hbox = gtk.HBox(False, 8)
    hbox.set_border_width(8)
    dialog.vbox.pack_start(hbox, 1, False, 0)
    stock = gtk.Image()
    stock.set_from_file("pixmaps/gnome-gmenu.png")
    hbox.pack_start(stock, False, False, 0)
    hbox.pack_start(stock, False, False, 0)
    label = gtk.Label("Folio")
    label.set_use_underline(True)
    hbox.pack_start(label, 0, 0, 0)
    combo = gtk.Entry()
    combo.set_size_request(300,25)
    hbox.pack_start(combo, True, True, 0)
    label.set_mnemonic_widget(combo)
    dialog.set_default_response(gtk.RESPONSE_CANCEL)
    dialog.show_all()
    combo.grab_focus()
    response = dialog.run()
    if response == gtk.RESPONSE_OK:
        dialog.destroy()
        return combo.get_text()

    else:
        dialog.destroy()
        return -1

def ctb_calendar(self=None, window=None, fecha=None):
    def on_calendar_double_click(self=None, window=None):
        dialog.response(gtk.RESPONSE_OK)

    lbl = unicode("Calendario", "latin-1")
    dialog = gtk.Dialog(lbl.encode("utf-8"), window, 0,)#(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,gtk.STOCK_OK, gtk.RESPONSE_OK))
    dialog.set_position(gtk.WIN_POS_MOUSE)
    hbox = gtk.HBox(False, 8)
    hbox.set_border_width(8)
    dialog.vbox.pack_start(hbox, 1, False, 0)
    combo = gtk.Calendar()
    combo.connect('day_selected_double_click', on_calendar_double_click)

    if fecha <> None and fecha <> "":
        combo.select_day(int(fecha[0:2]))
        combo.select_month(int(fecha[3:5])-1,int(fecha[6:10]))

    hbox.pack_start(combo, True, True, 0)
    dialog.set_default_response(gtk.RESPONSE_CANCEL)
    dialog.show_all()
    combo.grab_focus()
    response = dialog.run()
    if response == gtk.RESPONSE_OK:
        dialog.destroy()
        fecha = combo.get_date()
        return str(ceros(fecha[2],2)) +"/"+ str(ceros(fecha[1] +1,2))+"/"+ str(ceros(fecha[0],4))

    else:
        dialog.destroy()
        return fecha

def ctb_list_elimina_ultima_fila(self, tree):
    store = tree.get_model()
    if store.iter_n_children(None) > 0:
        p = (store.iter_n_children(None)-1),
        store.remove(store.get_iter(p))
    return

def ctb_tree_row_activate(self, tree, cadena, columna):
    model = tree.get_model()
    iter =  model.get_iter_first()
    i = 0
    if model.get_value(iter, columna) == cadena:
        tree.set_cursor(model.get_path(iter))
        return 0

    while iter:
        i=i+1
        iter = model.iter_next(iter)
        if not iter is None:
            if model.get_value(iter, columna) == cadena:
                tree.set_cursor(model.get_path(iter))
                return 0
    return

def cmb_carga_tipo_numeracion(cmb, cnx):
    try:
        cursor = cnx.cursor()
        cursor.execute("select cod_numeracion, descripcion_numeracion from ctb.tipo_numeracion order by cod_numeracion")
        r = cursor.fetchall()

    except:
        ctb_error(self.padre.ctb_frm_main, "Error al consultar los niveles de resultado.")
        return 1

    if len(r) == 0:
        return 0
    st = []

    for i in r:
        l = unicode(i[0] + " - " + i[1],"latin-1")
        st.append(l.encode("utf-8"))

    cmb.set_popdown_strings(st)
    return  0

def cmb_carga_tipo_numeracion(cmb, cnx):
    try:
        cursor= cnx.cursor()
        cursor.execute("select cod_numeracion, descripcion_numeracion from ctb.tipo_numeracion order by cod_numeracion")
        r = cursor.fetchall()
    except:
        ctb_error(self.padre.ctb_frm_main, "Error al consultar los niveles de resultado.")
        print sys.exc_info()
        return 1

    if len(r) == 0:
        return 0

    st = []
    for i in r:
        l = unicode(i[0] + " - " + i[1],"latin-1")
        st.append(l.encode("utf-8"))

    cmb.set_popdown_strings(st)
    return  0

def cmb_carga_mes_cierre(cmb, cnx):
    try:
        cursor= cnx.cursor()
        cursor.execute("select cod_mes, descripcion_mes from ctb.mes order by cod_mes desc")
        r = cursor.fetchall()

    except:
        ctb_error(self.padre.ctb_frm_main, "Error al consultar los niveles de resultado.")
        return 1

    if len(r) == 0: return 0
    st = []
    for i in r:
        l = unicode(str(i[0]) + " - " + i[1], "latin-1")
        st.append(l.encode("utf-8"))

    cmb.set_popdown_strings(st)
    return  0

def cmb_carga_moneda(cmb, cnx):
    try:
        cursor = cnx.cursor()
        cursor.execute("select cod_moneda, descripcion_moneda, simbolo_moneda from ctb.moneda order by cod_moneda")
        r = cursor.fetchall()

    except:
        ctb_error(self.padre.ctb_frm_main, "Error al consultar los niveles de resultado.")
        return 1

    if len(r) == 0: return 0
    st = []
    for i in r:
        l = unicode(str(i[0]) + " - " + i[1] + " (" + i[2] + ")", "latin-1")
        st.append(l.encode("utf-8"))

    cmb.set_popdown_strings(st)
    return  0

def cmb_carga_niveles(self):
    try:
        self.cursor.execute("select nivel_cuenta from ctb.empresa \
                            where cod_empresa = "+ str(self.cod_empresa) + "")
        r = self.cursor.fetchall()

    except:
        ctb_error(self.padre.ctb_frm_main, "Error al consultar las empresas.")
        return 1

    if len(r) == 0: return   0
    self.spnNivel.set_range(1,r[0][0])
    return 0

def cmb_carga_empresas(self):
    try:
        self.cursor.execute("select descripcion_empresa from ctb.empresa \
                            order by descripcion_empresa")
        r = self.cursor.fetchall()

    except:
        ctb_error(self.padre.ctb_frm_main, "Error al consultar las empresas.")
        return 1

    if len(r) == 0: return   0
    st = []
    for i in r:
        st.append(i[0])

    self.cmbEmpresa.set_popdown_strings(st)
    return 0

def cmb_carga_tipo_comprobante(self):
    try:
        self.cursor.execute("select descripcion_tipo from ctb.tipo_comprobante \
                            order by descripcion_tipo")
        r = self.cursor.fetchall()

    except:
        ctb_error(self.padre.ctb_frm_main, "Error al consultar los tipos de comprobantes.")
        return 1

    if len(r) == 0: return   0
    st = []
    for i in r:
        st.append(i[0])

    self.cmbTipoComprobante.set_popdown_strings(st)
    return 0

def cmb_carga_tipo_cuentas(self):
    if not self.cmbTipoCuenta is None:
        try:
            self.cursor.execute("select descripcion_tipo_cuenta from \
            ctb.tipo_cuenta order by descripcion_tipo_cuenta")
            r = self.cursor.fetchall()

        except:
            ctb_error(self.padre.ctb_frm_main, "Error al consultar los tipos de cuentas.")
            return 1

        if len(r) == 0: return 0
        st = []
        for i in r:
            st.append(i[0])

        self.cmbTipoCuenta.set_popdown_strings(st)
        return 0

def cmb_carga_tipo_documento(self):
    if not self.cmbTipoDocumento is None:
        try:
            self.cursor.execute("select descripcion_documento from \
            ctb.tipo_doc_contable order by descripcion_documento")
            r = self.cursor.fetchall()

        except:
            ctb_error(self.padre.ctb_frm_main, "Error al consultar los tipos de cuentas.")
            return 1

        if len(r) == 0: return 0
        st = []
        for i in r:
            st.append(i[0])

        self.cmbTipoDocumento.set_popdown_strings(st)
        return 0

def tree_carga_plan_de_cuentas(self, empresa=None,Vigentes = None):
    if self.treeCuentas.get_model() == None:
        model = gtk.TreeStore(str, str)
        cell = gtk.CellRendererText()
        column = gtk.TreeViewColumn("Cuenta", cell, text=0)
        self.treeCuentas.append_column(column)
        column = gtk.TreeViewColumn("Descripcion", cell, text=1)
        self.treeCuentas.append_column(column)

    else:
        model = self.treeCuentas.get_model()
        model.clear()

    if empresa is None:
        return 0

    if Vigentes is None:
        Vigentes = 'V'

    try:
        if Vigentes == 'V':
            self.cursor.execute("select c.num_cuenta, \
                                c.estado_cuenta, \
                                c.tiene_hijos, \
                                c.nivel_cuenta, \
                                c.estado_resultado, \
                                c.tipo_analisis, \
                                c.tipo_cuenta, \
                                c.descripcion_cuenta, \
                                c.num_padre, \
                                c.cod_empresa, \
                                c.aplica_resultado, \
                                c.cod_tipo_resultado, \
                                t.descripcion_tipo_cuenta\
                                from ctb.cuenta_contable c \
                                inner join ctb.tipo_cuenta t \
                                on c.tipo_cuenta = t.cod_tipo_cuenta \
                                where cod_empresa = " + str(empresa) + " and estado_cuenta = '" + Vigentes + "' \
                                order by num_cuenta")
            r = self.cursor.fetchall()
        else:

            self.cursor.execute("select c.num_cuenta, \
                                c.estado_cuenta, \
                                c.tiene_hijos, \
                                c.nivel_cuenta, \
                                c.estado_resultado, \
                                c.tipo_analisis, \
                                c.tipo_cuenta, \
                                c.descripcion_cuenta, \
                                c.num_padre, \
                                c.cod_empresa, \
                                c.aplica_resultado, \
                                c.cod_tipo_resultado, \
                                t.descripcion_tipo_cuenta\
                                from ctb.cuenta_contable c \
                                inner join ctb.tipo_cuenta t \
                                on c.tipo_cuenta = t.cod_tipo_cuenta \
                                where cod_empresa = " + str(empresa) + " \
                                order by num_cuenta")
            r = self.cursor.fetchall()

    except:
        return 0

    iter = padre = raiz = model.append(None)
    model.set_value(raiz, 0, 'Plan de Cuentas')
    n = 1

    for i in r:
        desc = unicode(i[7],'latin-1')
        if i[3] == 1:
            iter = model.append(raiz)
            padre = raiz
            n = 1
            model.set(iter, 0, i[0], 1, desc.encode('utf-8'))

        else:
            if n < i[3]:
                padre = iter
                iter = model.append(padre)
                model.set(iter, 0, i[0], 1, desc.encode('utf-8'))
                n = i[3]

            else:
                if n == i[3]:
                    iter = model.append(padre)
                    model.set(iter, 0, i[0], 1, desc.encode('utf-8'))

                else:
                    if n > i[3]:
                        path = model.get_path(iter)
                        path = path[:int(i[3])]
                        padre = model.get_iter(path)
                        iter = model.append(padre)
                        model.set(iter, 0, i[0], 1, desc.encode('utf-8'))
                        n = i[3]

    selection = self.treeCuentas.get_selection()
    selection.set_mode('single')
    selection.connect('changed', self.treeCuentas_changed_cb)
    self.treeCuentas.set_model(model)
    self.treeCuentas.expand_row(0, 0)

def ctb_cod_empresa(self, empresa):
    try:
        r = self.cursor.execute("select cod_empresa from ctb.empresa \
                                where descripcion_empresa = '" + empresa + "'")
        r = self.cursor.fetchall()
        cod_empresa = r[0][0]

    except:
        ctb_error(self.padre.ctb_frm_main, "Error al consultar la empresa.")
        return -1

    return cod_empresa



def ctb_cod_tipo_cuenta(self, tipo_cuenta):
    try:
        self.cursor.execute("select cod_tipo_cuenta from ctb.tipo_cuenta \
                            where descripcion_tipo_cuenta = '" + tipo_cuenta + "'")
        r = self.cursor.fetchall()
        return r[0][0]

    except:
        ctb_error(self.padre.ctb_frm_main, "Error al consultar el tipo de cuenta.")
        return -1

def ctb_nivel_cuenta(self, empresa):
    try:
        self.cursor.execute("select nivel_cuenta from ctb.empresa where cod_empresa = " + str(empresa))
        r = self.cursor.fetchall()
        nivel = r[0][0]

    except:
        ctb_error(self.padre.ctb_frm_main, "Error al consultar el nivel de cuentas.")
        return -1

    return nivel

def ctb_seccion_padre(self, cuenta, nivel):
    n = 1

    cp = ca =  ""
    cf = "."
    for i in cuenta:
        if n < nivel:
            cp = cp + i

        if n == nivel and not (i!="."):
            ca = ca + i

        if n > nivel:
            cf = cf + i

        if i == ".":
            n = n + 1

    return (cp, ca, cf)


def ctb_seccion_hijo(self, cuenta, nivel):
    n = 1
    cp = ci = ""
    for i in cuenta:
        if n <= nivel:
            cp = cp + i

        else:
            ci = ci + i

        if i == ".":
            n = n + 1

    return (cp, ci)

def ctb_limpia_seccion(self, cuenta):
    cp = ""
    for i in cuenta:
        if i == ".":
            cp = cp + i

        else:
            cp = cp + "0"

    return cp

def ctb_verifica_cuenta(self, cuenta, cod_empresa):
    try:
        self.cursor.execute("select cod_nivel, digitos from ctb.nivel_cuenta \
                            where cod_empresa = " + str(cod_empresa) + " \
                            order by cod_nivel ")
        r = self.cursor.fetchall()

    except:
        ctb_error(self.padre.ctb_frm_main, "Error al consultar el nivel de cuentas.")
        return 0

    c = 0
    ret = 1
    for i in r:
        j = c

        while j >= c and j < c + i[1]:
            if not int(cuenta[j]) >= 0 and int(cuenta[j]) <= 9:
                ret = 0

            j = j + 1

        c = c + i[1] + 1

    return ret
def moneyfmt(value, places=2, curr='', sep=',', dp='.',
             pos='', neg='-', trailneg=''):
    """Convert Decimal to a money formatted string.

    places:  required number of places after the decimal point
    curr:    optional currency symbol before the sign (may be blank)
    sep:     optional grouping separator (comma, period, space, or blank)
    dp:      decimal point indicator (comma or period)
             only specify as blank when places is zero
    pos:     optional sign for positive numbers: '+', space or blank
    neg:     optional sign for negative numbers: '-', '(', space or blank
    trailneg:optional trailing minus indicator:  '-', ')', space or blank

    >>> d = Decimal('-1234567.8901')
    >>> moneyfmt(d, curr='$')
    '-$1,234,567.89'
    >>> moneyfmt(d, places=0, sep='.', dp='', neg='', trailneg='-')
    '1.234.568-'
    >>> moneyfmt(d, curr='$', neg='(', trailneg=')')
    '($1,234,567.89)'
    >>> moneyfmt(Decimal(123456789), sep=' ')
    '123 456 789.00'
    >>> moneyfmt(Decimal('-0.02'), neg='<', trailneg='>')
    '<.02>'

    """
    q = Decimal((0, (1,), -places))    # 2 places --> '0.01'
    sign, digits, exp = value.quantize(q).as_tuple()
    assert exp == -places    
    result = []
    digits = map(str, digits)
    build, next = result.append, digits.pop
    if sign:
        build(trailneg)
    for i in range(places):
        if digits:
            build(next())
        else:
            build('0')
    build(dp)
    i = 0
    while digits:
        build(next())
        i += 1
        if i == 3 and digits:
            i = 0
            build(sep)
    build(curr)
    if sign:
        build(neg)
    else:
        build(pos)
    result.reverse()
    return ''.join(result)
def CNumDb(s):
    try:
        s = float(s)
    except:
        print "not float"
    if type(s) == float:
        return str(s)
    s = str(s)
    p = len(s)
    t = ""
    for i in range(p):
        c = s[i]
        if c == ',':
            t = t + "."
        elif not c == ".":
            t = t + c
    return t
def CMon(s, dec = 1):
    if dec == 0:
        dec = int(sys.__getattribute__("decimales"))
    try:
        A = float(s)
        s = CNumDb(s)
    except:
        #print "ret", s
        return s
    d = Decimal(str(s))
    if s == "0.0":
        return "0." + ("0"*dec)
    if dec >= 1:
        return moneyfmt(d, places=dec, sep=",", dp=".")
    else:
        return moneyfmt(d, places=dec, sep=",", dp=".")[:-1]
def ctb_formato_moneda(s):
    return CMon(s, 0)

def ctb_formato_cuenta(cnx,cod_empresa,cuenta):
    cuenta= string.replace(cuenta,".","")
    cuenta_aux =""
    try:
        cursor = cnx.cursor()
        cursor.execute("select nivel_cuenta from ctb.empresa where cod_empresa = "+ str(cod_empresa)+";")
        r = cursor.fetchall()

        if len(r) == 0 :
            return cuenta_aux

        else:
            niveles = r[0][0]
            i = 1
            contador = 0
            while i <= niveles:
                cursor.execute("select digitos from ctb.nivel_cuenta where cod_empresa = "+ str(cod_empresa)+" and cod_nivel = "+ str(i) +";")
                r = cursor.fetchall()
                cuenta_aux = cuenta_aux + "." + cuenta[contador:contador+r[0][0]]
                contador = contador + r[0][0]
                i = i +1

    except:
        print "ERROR"

    return cuenta_aux[1:]

def ctb_formato_numero_db(s):
    s = str(s)
    p = len(s)
    t = ""
    for i in range(p):
        c = s[i]
        if c == ',':
            t = t + "."

        elif not c == ".":
            t = t + c

    return t

def iif(cond,siTrue,siFalse):
    if cond:
        return siTrue

    else:
        return siFalse

def ctb_alinea_derecha(s,largo):
    s = ctb_formato_moneda(ctb_formato_numero_db((s)))
    l = len(s)
    if s.find(".") == -1:
        l = l

    else:
        l=l-1

    return s.rjust(largo -l)

def valida_rut(rut=None):
    if not rut: return 0
        
    es_rut = 0
    cadena = "234567234567"
    dig_rut = rut[-1]
    
    rut = string.replace(rut, ".", "")
    rut = rut[:rut.find("-")]
    rut = string.replace(rut, " ", '0')
            
    j, suma, i = 0, 0, len(rut) -1
    
    while i >= 0:
        try:
            suma = suma + (int(rut[i]) * int(cadena[j]))
        except:
            return 0
        
        i = i - 1
        j = j + 1
    
    divid = int(suma/11)
    mult = int(divid*11)
    dife = suma - mult
    digito = 11 - dife
    
    if digito == 10:
        caract = "K"
    elif digito == 11:
        caract = "0"
    else:
        caract = string.replace(str(digito), " ", "")
    
        
    if caract == dig_rut: es_rut = 1
    
    return es_rut    
def valida_dni(dni=None):
    if len(dni) == 8 and str(dni).isdigit():
        return True
    else:
        return False
def valida_ruc(lcNroRuc=None):
    if len(lcNroRuc) != 11:
        return valida_dni(lcNroRuc)
        return False
    elif lcNroRuc.startswith("000"):
        return valida_dni(lcNroRuc[-8:])
    arrayRuc = [11*[0], 11*[0], 11*[0]]
    for i in range(11):
        arrayRuc[0][i] = int(lcNroRuc[i])
    mult = '54327654320'
    for i in range(11):
        arrayRuc[1][i] = int(mult[i])
    for i in range(10):
        arrayRuc[2][i] = arrayRuc[0][i] * arrayRuc[1][i]
        arrayRuc[2][10] = arrayRuc[2][10] + arrayRuc[2][i]
    lnResiduo = arrayRuc[2][10].__mod__(11)
#    lnResiduo   = MOD(aArrayRuc(3,11),11)
    lnUltDigito = 11 - lnResiduo
    if lnUltDigito in (11, 1):
        lnUltDigito = 1
    elif lnUltDigito in (10, 0):
        lnUltDigito = 0
        
    return lnUltDigito == arrayRuc[0][10]  

def es_rut(rut=None):
    formula = sys.__getattribute__('rut')
    if formula == 'rut':
        return valida_rut(rut)
    elif formula == 'ruc':
        return valida_ruc(rut)
    else:
        return True



def formato_rut(rut):
    if rut == "":
        return rut

    rut = string.replace(rut,".","")
    rut = string.replace(rut,"-","")
    rut = "0000000000"+ rut
    l = len(rut)
    rut_aux = "-" + rut[l-1:l]
    l = l-1
    while 2 < l:
        rut_aux = "."+ rut[l-3:l] +rut_aux
        l = l-3

    rut_aux = rut[0:l] +rut_aux
    l = len(rut_aux)
    rut_aux = rut_aux[l-12:l]
    return rut_aux
def formato_ruc(rut):
    if rut == "":
        return rut
    return rut.zfill(11)
def CRut(rut):
    formula = sys.__getattribute__('rut')
    if formula == 'rut':
        return formato_rut(rut)
    elif formula in ('ruc', 'dni'):
        return formato_ruc(rut)
    else:
        return rut

def ctb_set_fecha(fecha):
    f = (int(fecha[0:4]),int(fecha[5:7]),int(fecha[8:10]),0,0,0,1,252,0)
    return time.mktime(f)

def ctb_formato_fecha_db(fecha):
    f =""
    if fecha <> "":
        f = fecha[6:10] +"/"+ fecha[3:5] +"/" + fecha[0:2]

    return f

def ctb_formato_fecha_local(fecha):
    f =""
    if fecha <> "":
        f = str(fecha)[8:10] +"/"+ str(fecha)[5:7] +"/" + str(fecha)[0:4]

    return f

def ctb_get_fecha(dte):
    fecha = dte.get_time()
    a = time.localtime(fecha)
    return  time.strftime("%Y-%m-%d",a)

class ProgressBar:
    def destroy_progress(self, widget = None, data=None):
        gtk.main_quit()
        self.termina = False

    def __init(self,titulo):
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.set_resizable(True)
        self.window.connect("destroy", self.destroy_progress)
        self.window.set_title(titulo)
        self.window.set_border_width(0)
        vbox = gtk.VBox(False, 5)
        vbox.set_border_width(10)
        self.window.add(vbox)
        vbox.show()
        align = gtk.Alignment(0.5, 0.5, 0, 0)
        vbox.pack_start(align, False, False, 5)
        align.show()
        self.pbar = gtk.ProgressBar()
        align.add(self.pbar)
        self.pbar.show()
        button = gtk.Button("close")
        button.connect("clicked", self.destroy_progress)
        vbox.pack_start(button, False, False, 0)
        button.set_flags(gtk.CAN_DEFAULT)
        button.grab_default ()
        button.show()
        self.window.show()

def ctb_existe_comprobante(self, num_comprobante, tipo_comprobante):
    if self.tipo_numeracion == "T":
        sql = "and periodo = "+ self.periodo +" and cod_tipo_comprobante ='"+ tipo_comprobante +"'"
    else:
        if self.tipo_numeracion =="C":
            sql = "and periodo = "+ str(self.periodo) +" and mes = "+ str(self.mes) +" and cod_tipo_comprobante ='"+ tipo_comprobante +"'"
        else:
            if self.tipo_numeracion=="M":
                sql = "and periodo = "+ str(self.periodo) +" and mes = "+ str(self.mes) +""
            else:
                sql = "and periodo = "+ str(self.periodo) +" "
        sql = "select cod_comprobante,glosa,debe,haber, fecha, estado from ctb.comprobante c join ctb.empresa e on e.cod_empresa = c.cod_empresa and c.folio_comprobante = " + str(num_comprobante) + " and c.cod_empresa = " + str(self.cod_empresa) + " " + sql
        try:
            self.cursor.execute(sql)
            r = self.cursor.fetchall()
        except:
            self.aviso(self.padre.ctb_frm_main,sys.exc_info()[1])
            return False
        if len(r) == 0:
            return False
        else:
            return r[0][0]
if __name__ == "__main__":
    print ctb_es_rut('00.000.000-0')
