#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2005 by Async Open Source and Sicem S.L.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import os, gtk
from spg import connect
from codificacion import CISO
from reportlab.pdfbase.ttfonts import *
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import A4, landscape
from strSQL import strSelectEmpresa
from Documento import PdfComprobanteContable
from comunes import CMon,iif
PdfBalanceFecu=PdfComprobanteContable
class BalanceFecus:
    def __init__(self, cnx=None,  empresa=1,desde=2006,hasta=2007,meses=12, cod_informe = 1):
        self.cod_empresa=empresa
        self.cnx=cnx
        self.cursor=self.cnx.cursor()
        self.desde=desde
        self.hasta=hasta
        self.meses=meses
        self.cod_informe = cod_informe
        self.datos_empresa()
        self.cod_fecu=gtk.ListStore(str,str)
        self.generar_subcolumnas()
        self.imprimir_fecu()
        
    def datos_empresa(self):
        sql=strSelectEmpresa %("WHERE e.cod_empresa=%s"%(self.cod_empresa))
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        self.razon_social=r[0][1]
        self.tipo_moneda=r[0][8]        
    def generar_subcolumnas(self):
        self.cod_fecu.append(["5.11.00.00"," ACTIVOS CIRCULANTES"])        
        self.cod_fecu.append(["5.12.00.00"," ACTIVOS FIJOS"])
        self.cod_fecu.append(["5.13.00.00","OTROS ACTIVOS"])
        self.cod_fecu.append(["5.10.00.00", "TOTAL ACTIVOS"])
        self.cod_fecu.append(["5.21.00.00","PASIVOS CIRCULANTES"])
        self.cod_fecu.append(["5.22.00.00","PASIVOS A LARGO PLAZO"])
        self.cod_fecu.append(["5.24.00.00","TOTAL PATRIMONIO"])
        self.cod_fecu.append(["5.20.00.00" ,"TOTAL PASIVOS Y PATRIMONIO"])
##        self.cod_fecu.append(["5.31.11.10", "MARGEN DE EXPLOTACION"])
##        self.cod_fecu.append(["5.31.11.00", "RESULTADO DE EXPLOTACION"])
##        self.cod_fecu.append(["5.31.12.00", "RESULTADO FUERA DE EXPLOTACION"])
        self.cod_fecu.append(["5.31.10.00", "RESULTADO ANTES DE IMPUESTO A LA RENTA E ITEMES EXTRAORDINARIOS"])
        self.cod_fecu.append(["5.31.20.00", "IMPUESTO A LA RENTA"])
        self.cod_fecu.append(["5.31.30.00","ITEMES EXTRAORDINARIOS"])
        self.cod_fecu.append(["5.31.40.00", "UTILIDAD (PERDIDA) ANTES INTERÉS MINORITARIO"])
        self.cod_fecu.append(["5.31.50.00", "INTERES MINORITARIO"])
        self.cod_fecu.append(["5.31.00.00", "UTILIDAD (PERDIDA) LIQUIDA"])
        
    def imprimir_fecu(self):
        
        periodo="""select distinct periodo as periodo from ctb.comprobante where periodo between %s and %s"""%(self.desde,self.hasta)
        self.cursor.execute(periodo)
        per=self.cursor.fetchall()
        if len(per)>1:
            sql="""select * 
                                    from ctb.fecu(%s,%s,%s,%s,%s) as (cod text, descrpcion_fecu text, identificador text,"%s" numeric, "%s" numeric) 
                            order by identificador"""%(self.cod_empresa,self.desde,self.hasta,self.meses, self.cod_informe,str(self.hasta),str(self.desde))
        else:
            sql="""select * 
                                    from ctb.fecu(%s,%s,%s,%s,%s) as (cod text, descrpcion_fecu text, identificador text,"%s" numeric) 
                        order by identificador"""%(self.cod_empresa,self.desde,self.hasta,self.meses, self.cod_informe,per[0][0])
        self.cursor.execute(sql)       
        r=self.cursor.fetchall()
        if len(r)==0:
            return
        a=PdfBalanceFecu("F.E.C.U",
            "Fecu.pdf",
            "",[["EMPRESA    :" + self.razon_social, 0],
                ["MONEDA  :"+self.tipo_moneda,1]])
        fila=[]
        fila.append("")
        fila.append("ACTIVOS")        
        fila.append("")
        if len(per)>1:
            fila.append(self.hasta)
            fila.append(self.desde)        
        else:
            fila.append(self.desde)
            fila.append(per[0][0])
        a.drawHeaders([],fila,[100,100,200,150,150])
        
        band=False
        umbral_codido_activo=[10,19]        
        data=self.imprimir_fecu_por_grupo(umbral_codido_activo,r,1)        
        a.drawData(data,["","","",""],[100,250,100,100], ["LEFT","LEFT","RIGHT","RIGHT"])
        fila=[]
        fila.append("")
        fila.append("TOTAL ACTIVOS")        
        fila.append("")
        fila.append(CMon(self.sum_grupo1,0).replace(",00",""))
        fila.append(CMon(self.sum_grupo2,0).replace(",00",""))        
        a.drawHeaders([],fila,[100,150,200,150,150])
        a.go()
        a=PdfBalanceFecu("F.E.C.U",
            "Pasivos.pdf",
            "",[["EMPRESA    :" + self.razon_social, 0],
                ["MONEDA  :"+self.tipo_moneda,1]])
        
        fila=[]
        data=[]
        fila.append("")
        fila.append("PASIVOS")        
        fila.append("")
        if len(per)>1:
            fila.append(self.hasta)
            fila.append(self.desde)        
        else:
            fila.append(self.desde)
            fila.append(per[0][0])
        a.drawHeaders([],fila,[100,150,200,150,150])
        umbral_codido_pasivo=[20,29]
        self.sum_grupo1=0L
        self.sum_grupo2=0L
        data=self.imprimir_fecu_por_grupo(umbral_codido_pasivo,r,1,-1)
        
        a.drawData(data,["","","",""],[100,250,100,100], ["LEFT","LEFT","RIGHT","RIGHT"])
       
        fila=[]
        fila.append("")
        fila.append("TOTAL PASIVOS")        
        fila.append("")
        fila.append(CMon(self.sum_grupo1,0).replace(",00",""))
        fila.append(CMon(self.sum_grupo2,0).replace(",00",""))        
        a.drawHeaders([],fila,[100,150,200,150,150])
        a.go()
        umbral_codido_resultado=[30,32]
        a=PdfBalanceFecu("F.E.C.U",
            "Resultado.pdf",
            "",[["EMPRESA    :" + self.razon_social, 0],
                ["MONEDA  :"+self.tipo_moneda,1]])
        
        fila=[]
        data=[]
        fila.append("")
        fila.append("ESTADO DE RESULTADO")        
        fila.append("")
        if len(per)>1:
            fila.append(self.hasta)
            fila.append(self.desde)        
        else:
            fila.append(self.desde)
            fila.append(per[0][0])
        
        a.drawHeaders([],fila,[100,150,200,150,150])
        umbral_codido_pasivo=[20,29]
        data=self.imprimir_fecu_por_grupo(umbral_codido_resultado,r,2)
        data1=[]
        fila1=[]
        if len(data)>1:            
            i=0        
            identificador=data[i][0]
            fecu=data[i][1]
            if data[i][2] not in('',None):
                monto1=iif(float(data[i][2].replace(".",""))<0,-1*float(data[i][2].replace(".","")),float(data[i][2].replace(".","")))
            else:
                monto1=0
            if data[i][3] not in('',None):
                monto2=iif(float(data[i][3].replace(".",""))<0,-1*float(data[i][3].replace(".","")),float(data[i][3].replace(".","")))
            else:
                monto2=0
                
            ##ingresos de explotacion
            data1.append(data[i][0])
            data1.append(data[i][1])
            if data[i][2] not in('',None):
                data1.append(CMon(iif(float(data[i][2].replace(".",""))<0,str(-1*float(data[i][2].replace(".",""))),str(float(data[i][2].replace(".","")))),0).replace(",00",""))
            else:
                data1.append('')
            if data[i][3] not in('',None):
                data1.append(CMon(iif(float(data[i][3].replace(".",""))<0,str(-1*float(data[i][3].replace(".",""))),str(float(data[i][3].replace(".","")))),0).replace(",00",""))
            else:
                data1.append('')
            fila1.append(data1)
            data1=[]
            ## costos de explotacion
            data1.append(data[i+1][0])
            data1.append(data[i+1][1])
            if data[i+1][2] not in('',None):
                data1.append("("+CMon(iif(float(data[i+1][2].replace(".",""))<0,str(-1*float(data[i+1][2].replace(".",""))),str(float(data[i+1][2].replace(".","")))),0).replace(",00","")+")")
            else:
                data1.append('')
            if data[i+1][3] not in('',None):
                data1.append("("+CMon(iif(float(data[i+1][3].replace(".",""))<0,str(-1*float(data[i+1][3].replace(".",""))),str(float(data[i+1][3].replace(".","")))),0).replace(",00","")+")")
            else:
                data1.append('')
            fila1.append(data1)
            data1=[]
            if data[i+1][2] not in('',None):
                margen_explotacion1=monto1-float(iif(int(data[i+1][2].replace(".",""))<0,-1*int(data[i+1][2].replace(".","")),data[i+1][2].replace(".","")))
            else:
                margen_explotacion1=monto1
            if data[i+1][3] not in('',None):
                margen_explotacion2=monto2-float(data[i+1][3].replace(".",""))
            else:
                margen_explotacion2=monto2
            data1.append("5.31.11.10")
            data1.append("MARGEN DE EXPLOTACION")
            data1.append(CMon(margen_explotacion1,0).replace(",00",""))
            data1.append(CMon(margen_explotacion2,0).replace(",00",""))            
            fila1.append(data1)
            data1=[]
            if len(data)>2:
                if data[i+2][0] not in('',None) and data[i+2][1] not in('',None):
                    data1.append(data[i+2][0])
                    data1.append(data[i+2][1])
                data1.append("("+data[i+2][2]+")")
                data1.append("("+data[i+2][3]+")")                
                fila1.append(data1)
                data1=[]
                data1.append("5.31.11.00")
                data1.append("RESULTADO DE EXPLOTACION")
                data1.append(CMon(margen_explotacion1-float(iif(data[i+2][2].replace(".","")=='',0,data[i+2][2].replace(".",""))),0).replace(",00",""))
##                data1.append(CMon((margen_explotacion1-float(data[i+2][2].replace(".",""))),0).replace(",00",""))
                data1.append(CMon(margen_explotacion2-float(iif(data[i+2][3].replace(".","")=='',0,data[i+2][3].replace(".",""))),0).replace(",00",""))
##                data1.append(CMon((margen_explotacion2-float(data[i+2][3].replace(".",""))),0).replace(",00",""))
                fila1.append(data1)
                pivote=None
                sum_grupo1=0L
                sum_grupo2=0L
            
                for j in range(i+3,len(data)):
                    data1=[]
                    if data[j][0] not in(None,'None'):
                        if pivote==data[j][0].split(".")[:3] or pivote==None:
                            sum_grupo1+=float(iif(data[j][2].replace(".","")=='',0,data[j][2].replace(".","")))
                            sum_grupo2+=float(iif(data[j][3].replace(".","")=='',0,data[j][3].replace(".","")))
                            pivote=data[j][0].split(".")[:3]
                            data1.append(data[j][0])
                            data1.append(data[j][1])
                        
                            if float(iif(data[j][2].replace(".","")=='',0,data[j][2].replace(".","")))<0:
                                data1.append(CMon(-1*(float(data[j][2].replace(".",""))),0).replace(",00",""))
                            else:
                                data1.append("("+CMon(float(iif(data[j][2].replace(".","")=='',0,data[j][2].replace(".",""))),0).replace(",00","")+")")
                            if float(iif(data[j][3].replace(".","")=='',0,data[j][3].replace(".","")))<0:
                                data1.append(CMon(-1*(float(data[j][3].replace(".",""))),0).replace(",00",""))
                            else: 
                                if float(iif(data[j][3].replace(".","")=='',0,data[j][3].replace(".","")))!=0:
                                    data1.append("("+CMon(float(data[j][3].replace(".","")),0).replace(",00","")+")")                       
                            fila1.append(data1)
                        
                    else:
                        if pivote==['5','31','12']:
                            data1=[]
                            data1.append("5.31.12.00") 
                            data1.append("RESULTADO FUERA DE EXPLOTACION")
                            data1.append(CMon(iif(sum_grupo1>0,-1*sum_grupo1,sum_grupo1),0).replace(",00",""))
                            data1.append(CMon(iif(sum_grupo2>0,-1*sum_grupo2,sum_grupo2),0).replace(",00",""))
                            sum_grupo1=0L
                            sum_grupo2=0L
                            fila1.append(data1)
                            if len(data)<j:
                                if data[j+1][0] in(None,'None'):
                                    data1=[]
                                    data1.append(data[j+1][0])
                                    data1.append(data[j+1][1])
                                    data1.append(CMon(iif(float(data[j+1][2].replace(".",""))>0,-1*float(data[j+1][2].replace(".","")),float(data[j+1][2].replace(".",""))),0).replace(",00",""))
                                    data1.append(CMon(iif(float(data[j+1][3].replace(".",""))>0,-1*float(data[j+1][2].replace(".","")),float(data[j+1][3].replace(".",""))),0).replace(",00",""))
                                    fila1.append(data1)
                
        print fila1
        
        a.drawData(fila1,["","","",""],[100,250,100,100], ["LEFT","LEFT","RIGHT","RIGHT"])
        a.go()
    def imprimir_fecu_por_grupo(self,umbral,r,indice=1,factor=1):
        fila=[]
        data=[]
        pivote=None
        sum_grupo_eje1=0L
        sum_grupo_eje2=0L
        self.sum_grupo1=0L
        self.sum_grupo2=0L
        for i in r:            
            band=False
            i=[CISO(x) for x in i]
            if len(i)>4:
                fila = [i[2],CISO(i[1]),CMon(i[4]*factor,0).replace(",00",""),CMon(i[3]*factor,2).replace(",00","")]
            else:
                fila = [i[2],CISO(i[1]),'',CMon(i[3]*factor,2).replace(",00","")]
           ## indice mayor que 1 indica que es resultado  
            if indice>1:
                if i[2] not in(None,'None') and int(i[2].split(".")[indice-1]) in range(umbral[0],umbral[1]):
                    band=True
            else:
                if i[2] not in(None,'None') and int(i[2].split(".")[indice]) in range(umbral[0],umbral[1]):
                    band=True
            if band==True:
                    if i[2] not in(None,'None') :
                        if i[2].split(".")[:indice+1]==pivote or pivote==None:
                            if len(i)>4:
                                sum_grupo_eje1+=float(i[4])*factor
                            sum_grupo_eje2+=float(i[3])*factor                            
                            data.append(fila)      
                        else:
                            fila1=[]
                            fila1=fila
                            fila=[]                
                            for j in self.cod_fecu:                    
                                if pivote==j[0].split(".")[:indice+1]:
                                    fila.append("%s"%(j[0]))
                                    fila.append("TOTAL %s"%(j[1]))                    
                                    fila.append(CMon(sum_grupo_eje1,0).replace(",00",""))
                                    fila.append(CMon(sum_grupo_eje2,0).replace(",00",""))
                                    self.cod_fecu.remove(j.iter)
                                    data.append(fila)
                                    self.sum_grupo1+=sum_grupo_eje1
                                    self.sum_grupo2+=sum_grupo_eje2
                                    sum_grupo_eje1=0L
                                    sum_grupo_eje2=0L
                                    if len(i)>4:
                                        sum_grupo_eje1+=float(i[4])*factor
                                    sum_grupo_eje2+=float(i[3])*factor
                                    fila=[]
                            data.append(fila1)                    
                        fila=[] 
                        if i[2] not in(None,'None'):
                            pivote=i[2].split(".")[:indice+1]
                        else:
                            pivote=None
            else:
                band1=False
                if indice>1:
                    if i[2] not in(None,'None') and int(i[2].split(".")[indice-1]) in range(umbral[0],umbral[1]):
                        band1=True
                else:
                    if i[2] in(None,'None') or int(i[2].split(".")[indice]) >umbral[1]: 
                        band1=True
                if band1==True or i[2] in(None,'None'):                
                    fila1=[]
                    fila1=fila
                    fila=[]                
                    for j in self.cod_fecu:                    
                        if pivote==j[0].split(".")[:indice+1]:
                            fila.append("%s"%(j[0]))
                            fila.append("TOTAL %s"%(j[1]))                    
                            fila.append(CMon(sum_grupo_eje1,0).replace(",00",""))
                            fila.append(CMon(sum_grupo_eje2,0).replace(",00",""))
                            if i[2] not in(None,'None'):
                                self.sum_grupo1+=sum_grupo_eje1                                    
                                self.sum_grupo2+=sum_grupo_eje2
                            self.cod_fecu.remove(j.iter)
                            data.append(fila)
                            sum_grupo_eje=0L
                            sum_grupo_eje=0L
                    if len(i)>4:
                        sum_grupo_eje1+=float(i[4])
                    sum_grupo_eje2+=float(i[3])
                    if i[2] in (None,'None'):
                        data.append(fila1)
        return data
##                else:
##                    if i[2]==None:
##                        return data
        
if __name__ == "__main__":
    cnx = connect("dbname=asp")    
    cod_empresa=2
    w = BalanceFecus(cnx,cod_empresa)    
    gtk.main()
