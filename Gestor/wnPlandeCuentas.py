#!/usr/bin/env python
# -*- coding: UTF8 -*-

# Python module wnplandecuentas.py
# Autogenerated from wnplandecuentas.glade
# Generated on Mon Oct 30 17:25:36 2006

# Warning: Do not delete or modify comments related to context
# They are required to keep user's code

import os, gtk
from comunes import *
from GladeConnect import GladeConnect
from gobject import *
##from ctb_rutinas import  *
from spg import connect
import completion
import SimpleTree
import xlwt
from dialogos import  dlgAviso,dlgSiNo,dlgError
import ifd
import fechas
from strSQL import strPlanCuentas
from strSQL import strSelectEmpresa
from strSQL import strCuentaContable
from strSQL import strCuentaContableAll
from strSQL import strSelectEmpresaNivel
from strSQL import strSelectTipoCuenta 
from strSQL import strSelectCuentaContable
from strSQL import strSelectTipoResultado
from strSQL import strSelectCuentaGrupo

from Documento import PdfComprobanteContable
from wnComprobante import carga_combo
from wnComprobante import combo_set_active
from wnComprobante import combo_get_active
glade_dir = ""

# Put your modules and data here

# From here through main() codegen inserts/updates a class for
# every top-level widget in the .glade file.
  
class wnPlandeCuentas(GladeConnect):
    def __init__(self,conexion=None, padre=None, empresa=1, root="wnPlandeCuentas"):
        GladeConnect.__init__(self, "glade/wnPlandeCuentas.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.cod_empresa = empresa               
        self.padre = padre
        
        if padre is None:
            self.wnPlandeCuentas.maximize()
            self.frm_padre = self.wnPlandeCuentas
            self.ventana=self.frm_padre
            self.padre
        else:
            self.frm_padre = padre.frm_padre 
            self.ventana=self.frm_padre
      
        
        self.modelo=None
        self.nivel_cuenta=None
        self.crea_modelo()
        self.carga_cuentas()
        self.datos_empresa()
    
    def datos_empresa(self):
        sqlWhere='where e.cod_empresa=%s'%int(self.cod_empresa)
        sql=strSelectEmpresa %(sqlWhere)
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            return
##        if self.nivel_cuenta in('',None):
##            return
        self.nivel_cuenta=int(r[0][6])  
    
    def crea_modelo(self):
        columnas=[]
        columnas.append([0,"N° Cuenta","str"])
        columnas.append([1,"Descripcion","str"])
        columnas.append([4,"Activa","bool", False])
        self.modelo = gtk.TreeStore(*(4*[str])+[bool])
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeComprobante)                
    
    def carga_cuentas(self,Vigente=None):
        if Vigente==None:
            sqlWhere=''
        else:   
            sqlWhere=Vigente
        if len(self.modelo)>0:
            self.modelo.clear()
        sql=strPlanCuentas % (int(self.cod_empresa),sqlWhere)
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            raiz=self.modelo.append(None)
            iter=raiz
            padre=iter
            self.modelo.set_value(raiz,0,"PLAN DE CUENTAS")
            return
        raiz=self.modelo.append(None)
        iter=raiz
        padre=iter
        self.modelo.set_value(raiz,0,"PLAN DE CUENTAS")
        n=1
        for i in r:
            desc = unicode(i[7],'latin-1')
            activa = i[1] =='V'
            if i[3] == 1:
                iter = self.modelo.append(raiz)
                padre = raiz
                n = 1
                self.modelo.set(iter, 0, i[0], 1, desc.encode('utf-8'),2,i[5],3,i[6],4,  activa)
    
            else:
                if n < i[3]:
                    padre = iter
                    iter = self.modelo.append(padre)
                    self.modelo.set(iter, 0, i[0], 1, desc.encode('utf-8'),2,i[5],3,i[6],4,  activa)
                    n = i[3]
    
                else:
                    if n == i[3]:
                        iter = self.modelo.append(padre)
                        self.modelo.set(iter, 0, i[0], 1, desc.encode('utf-8'),2,i[5],3,i[6],4,  activa)
    
                    else:
                        if n > i[3]:
                            path = self.modelo.get_path(iter)
                            path = path[:int(i[3])]
                            padre = self.modelo.get_iter(path)
                            iter = self.modelo.append(padre)
                            self.modelo.set(iter, 0, i[0], 1, desc.encode('utf-8'),2,i[5],3,i[6],4,  activa)
                            n = i[3]
        self.treeComprobante.expand_row(0, 0)               

    def on_btnAnadir_clicked(self, widget, *args):
        if not VerificaPermiso(self,201):
            return
        model,iter = self.treeComprobante.get_selection().get_selected()                
        
        if iter == None:
            return 0
        
        self.num_cuenta = model.get_value(iter,0)
        self.descripcion_cuenta = model.get_value(iter,1)
        
        if self.num_cuenta == 'PLAN DE CUENTAS':
            n=1
            self.descripcion_cuenta=''            
        else:                  
            sql=strCuentaContable%(int(self.cod_empresa),self.num_cuenta)
            self.cursor.execute(sql)
            r=self.cursor.fetchall()
            if len(r)==0:
                return
            n=int(r[0][3])+1            
        nivel_cuenta=self.nivel_cuenta
        
        ##nivel de cuenta de la empresa no puede ser superior al nivel de cuenta a agregar
        if n > nivel_cuenta:
            dlgAviso(None,"No puede agregarse una Cuenta de nivel inferior.")
            return
        
        dlg = dlgPlandeCuentas(self.cnx, self.frm_padre, self.cod_empresa, False)
        dlg.txtDescripcion.set_text(self.descripcion_cuenta)
        dlg.lblNivel.set_text(str(n))
        sqlWhere=''
        if n==1:##Nivel de Cuentas es Plan de Cuentas
            try:
                sql=strSelectEmpresaNivel % (int(self.cod_empresa),sqlWhere)
                self.cursor.execute(sql)
                r=self.cursor.fetchall()
            except:
                error('Ha ocurrido al seleccionar los datos de la empresa.',str(sys.exc_info()[1]))
                return
            if len(r)==0:
                return
            l=r[0][0]
            m = "."
            _n = 2
            for i in r[1:]:
                m = m + zfill("0",i[0])                
                if _n < nivel_cuenta:
                    m = m + "."
                _n+=1
            if m == ".": 
                m = ""            
            dlg.lblCuenta.set_text("")
            dlg.lblCuenta2.set_text(m)
            dlg.entNumeroCuenta.set_text(zfill("0", l))
            dlg.entNumeroCuenta.set_max_length(l)
        else:
            try:
                sqlWhere='and cod_nivel=%s'%(n)
                sql=strSelectEmpresaNivel.replace("ORDER BY cod_nivel","" ) %(self.cod_empresa,sqlWhere)+"ORDER BY cod_nivel"
                self.cursor.execute(sql)
                r=self.cursor.fetchall()
            except:
                error('Ha ocurrido al seleccionar la empresa.',str(sys.exc_info()[1]))
                return
            l = r[0][0]
            c = self.seccion_padre(self.num_cuenta,n)
            dlg.lblCuenta.set_text(c[0])
            dlg.entNumeroCuenta.set_width_chars(l)
            dlg.entNumeroCuenta.set_max_length(l)
            dlg.entNumeroCuenta.set_text(zfill("0", l))
            if c[2]==".":
                dlg.lblCuenta2.set_text("")                
            else:
                dlg.lblCuenta2.set_text(c[2])        
            sql=strSelectCuentaGrupo %(self.cod_empresa,c[0][0:1]+"%")
            self.cursor.execute(sql)
            r=self.cursor.fetchall()
            if len(r)==0:
                return
            tipo_cuenta=r[0][1]
            combo_set_active(dlg.cmbTipoCuenta,tipo_cuenta,0)
        dlg.entNumeroCuenta.show()
        dlg.entNumeroCuenta.set_sensitive(1)
        dlg.chkVigente.set_sensitive(1)
        dlg.entNumeroCuenta.grab_focus()
        if n == 1:            
            dlg.cmbTipoCuenta.set_sensitive(1)            
        else:
            dlg.cmbTipoCuenta.set_sensitive(0)
        dlg.chkVigente.set_active(1)
        dlg.chkVigente.set_sensitive(0)
        if int(dlg.lblNivel.get_text()) < self.nivel_cuenta:
            dlg.chkResultado.set_sensitive(0)
            dlg.txtResultado.set_sensitive(0)
            dlg.hbAnalisis.set_sensitive(0)
            
        else:
            dlg.chkResultado.set_sensitive(1)
            dlg.hbAnalisis.set_sensitive(1)
        dlg.num_padre=self.num_cuenta
        band=False
        while band<>True and dlg.dlgPlandeCuentas.run()!=gtk.RESPONSE_CANCEL:
            if dlg.response==1:
                iter1=self.modelo.append(iter)
                self.modelo.set(iter1,0,dlg.num_cuenta,1,dlg.txtDescripcion.get_text())
                band=True
                dlg.dlgPlandeCuentas.destroy()
    
    def tipo_cuenta(self,codigo):
        if codigo==1:
            return 'A'
        elif codigo==2:
            return 'P'
        elif codigo==5:
            return 'I'
        elif codigo==6:
            return 'E'            
    
    def seccion_padre(self, cuenta, nivel):
        n = 1
        cp = ca =  ""
        cf = "."
        for i in cuenta:
            if n < nivel:
                cp = cp + i    
            if n == nivel and not (i!="."):
                ca = ca + i    
            if n > nivel:
                cf = cf + i    
            if i == ".":
                n = n + 1
        return (cp, ca, cf)
    
    def on_btnQuitar_clicked(self, widget, *args):
        if not VerificaPermiso(self,203):
            return
        model,iter=self.treeComprobante.get_selection().get_selected()        
        if model is None or iter is None:
            return
        self.num_cuenta=model.get_value(iter,0)
        self.descripcion_cuenta=model.get_value(iter,1)
        if self.num_cuenta=='PLAN DE CUENTAS':
           return
        resp=dlgSiNo(None,"Desea Eliminar la Cuenta N° %s"%(self.num_cuenta),"Eliminar").response
        if resp==gtk.RESPONSE_NO:
            return            
        llaves={}
        llaves['cod_empresa']=self.cod_empresa
        llaves['num_cuenta']=self.num_cuenta
        try:            
            self.cursor.execute('begin')
            sql=ifd.deleteFromDict('ctb.cuenta_contable_fecus',llaves)
            self.cursor.execute(sql,llaves)
            
            sql=ifd.deleteFromDict('ctb.cuenta_contable',llaves)
            self.cursor.execute(sql,llaves)
            
            self.cursor.execute('commit')            
        except:
            self.cursor.execute('rollback')
            error('Ha ocurrido al eliminar los datos de la cuenta.',str(sys.exc_info()[1]))
            return
        dlgAviso(self.ventana,"La cuenta Ha sido eliminada de manera satifactoria")
        self.modelo.remove(iter)
        
       
    def on_btnCuentasNVigente_clicked(self, widget, *args):
        Vigente=" and estado_cuenta ='N'"
        self.carga_cuentas(Vigente)

    def on_btnDescargar_clicked(self, widget, *args):
        sql= """SELECT
            descripcion_cuenta,
            num_cuenta,
            ctb.cuenta_contable.nivel_cuenta
            FROM
            ctb.cuenta_contable
            join ctb.empresa e using(cod_empresa)
            WHERE
            cod_empresa = 1 
            ORDER BY num_cuenta,descripcion_cuenta """
        
        print(sql)
        self.cursor.execute(sql)
        r = self.cursor.fetchall()
        
        if len(r)==0:
            return
        #r['id_detalle']
                
        style0 = xlwt.easyxf('font: name Times New Roman, colour black, bold on; align: wrap on, vert centre, horiz center;border: left thin, top thin,right thin,bottom thin')
        style4 = xlwt.easyxf('font: name Times New Roman, colour black;border: left thin, top thin,right thin,bottom thin')
        style6 = xlwt.easyxf('font: name Times New Roman, colour black;align: wrap on, horiz right;border: left thin, top thin,right thin,bottom thin')
        style1 = xlwt.easyxf('',num_format_str='DD-MMM-YY')
        style2 = xlwt.easyxf('',num_format_str='"$"#,##0.00')
        wb = xlwt.Workbook()
        ws = wb.add_sheet('A Test Sheet',cell_overwrite_ok=True)        

        data = []
        i = 1
        ws.write(1, 0, "Descripcion Cuenta".encode('ascii', 'replace'),style0)
        ws.write(1, 1, "Numero Cuenta".encode('ascii', 'replace'),style0)
        ws.write(1, 2, "Nivel".encode('ascii', 'replace'),style0)                        
        
        ws.write_merge(0,0,0,2, label='Plan de Cuentas', style=style0)
        
        ws.col(0).width = 22000
        ws.col(1).width = 6000
        ws.col(2).width = 4000

        for e in r:
            i = i + 1
            ws.write(i, 0, str(e[0]).encode('ascii', 'replace'),style4)
            ws.write(i, 1, str(e[1]).encode('ascii', 'replace'),style4)
            ws.write(i, 2, str(e[2]).encode('ascii', 'replace'),style4)
        
        wb.save('reporte.xls')        
        Abre_excel('reporte.xls');
    
    def on_btnPropiedades_clicked(self, widget, *args):
        self.on_treeComprobante_row_activated(None)
        
    def on_btnBuscar_clicked(self, widget, *args):
        self.carga_cuentas()

    def on_btnImprimir_clicked(self, widget, *args):        
        sqlWhere="WHERE e.cod_empresa=%s"%self.cod_empresa
        sql=strSelectEmpresa %sqlWhere
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            return
        a = PdfComprobanteContable(
            "Plan de Cuentas",
            "Plan de Cuentas.pdf",
            "",[["EMPRESA    :" + r[0][1], 0],
                ["FECHA     :"+ND(time.localtime()).formatESCentury(), 1]                
                ])
        fila=[]
        data=[]
        sqlWhere=''
        sql=strPlanCuentas % (self.cod_empresa,sqlWhere)
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            return
        for i in r:
            i=[CISO(x) for x in i]
            if i[5]=='S':
                analisis='SIMPLE'
            else:
                if i[5]=='D':                
                    analisis='DETALLADO'                    
                else:
                    analisis='SIN ANALISIS'
            fila = [CISO(i[0]),CISO(i[7]),i[6],CISO(analisis)]
            data.append(fila)                                 
        a.drawData(data,[CISO("N°CUENTA"),CISO("DESCRIPCION CUENTA."),CISO("TIPO"),CISO("TIPO ANALISIS")],[100,200,50,140], ["LEFT","LEFT","LEFT","LEFT"])                
        a.go()

    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Plan_de_Cuentas")
    
    def on_treeComprobante_row_activated(self, widget, *args):
        if not VerificaPermiso(self,202):
            return
        model,iter=self.treeComprobante.get_selection().get_selected()        
        if model is None or iter is None:
            return
        self.num_cuenta=model.get_value(iter,0)
        self.descripcion_cuenta=model.get_value(iter,1)
        
        if self.num_cuenta=='PLAN DE CUENTAS':
           return       
        sql=strCuentaContableAll%(int(self.cod_empresa),self.num_cuenta)
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            return            
        dlg=dlgPlandeCuentas(self.cnx,self.padre,self.cod_empresa)
        dlg.edit=True
        nivel_cuenta=self.nivel_cuenta        
        dlg.lblNivel.set_text(str(nivel_cuenta))
        dlg.lblCuenta.set_text(self.num_cuenta)
        dlg.txtDescripcion.set_text(r[0][7])
        
        if r[0][1]=='V':
            dlg.chkVigente.set_active(1)      
        if r[0][10]=='S':
            dlg.chkResultado.set_active(1)
            sql=strSelectTipoResultado %(self.cod_empresa,r[0][11])
            self.cursor.execute(sql)
            r1=self.cursor.fetchall()
            
            if len(r1)==0:
                dlgAviso(self.ventana,"No tiene Resultado asignado")
                return
            dlg.txtResultado.set_text(r1[0][1])
        if r[0][5]=='S':
            dlg.optConAnalisis.set_active(1)
        else:
            if r[0][5]=='D':
                dlg.optDetallado.set_active(1)
            else:
                dlg.optSinAnalisis.set_active(1)
        if int(dlg.lblNivel.get_text()) < self.nivel_cuenta:
            dlg.chkResultado.set_sensitive(0)
            dlg.txtResultado.set_sensitive(0)
            dlg.hbAnalisis.set_sensitive(0)
            
        else:
            dlg.chkResultado.set_sensitive(1)
            dlg.hbAnalisis.set_sensitive(1)
        combo_set_active(dlg.cmbTipoCuenta,r[0][6],0)
        dlg.cmbTipoCuenta.set_sensitive(False)
        band=False
        while band<>True and dlg.dlgPlandeCuentas.run()!=gtk.RESPONSE_CANCEL:
            if dlg.response==1:                
                self.modelo.set(iter,0,dlg.num_cuenta,1,dlg.txtDescripcion.get_text())
                band=True
                dlg.dlgPlandeCuentas.destroy()


class dlgPlandeCuentas(GladeConnect):
    def __init__(self, conexion=None, padre=None, empresa=1, editando=None):
        GladeConnect.__init__(self, "glade/wnPlandeCuentas.glade", "dlgPlandeCuentas")
        #self.dlgPlandeCuentas.resize(750, 500)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.ventana=padre
        self.cod_empresa = empresa                                
        carga_combo(self.cmbTipoCuenta,self.cnx,strSelectTipoCuenta)
#        self.pecFecu=PixEntryCompletion(self.txtFecu, self.genera_modelo_fecu(),0, self.sel_fecu)
        self.pecResultado=completion.CompletionResultado(self.txtResultado,self.sel_resultado,self.cnx,self.cod_empresa)
        self.edit=False
        self.num_padre=None
        self.response=0
        self.cod_fecus=None
        self.identificador=None
        self.cod_fecus_ant=None
    def genera_modelo_fecu(self):
       
        sql="select desc_fecus,cod_fecus,identificador from ctb.fecus where cod_empresa=%s"%self.cod_empresa
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            return None
        modelo_fecu=gtk.ListStore(str,str,str)
        for i in r:
            modelo_fecu.append(i)
        return modelo_fecu
    def sel_fecu(self,completion,model,iter):
        self.cod_fecus=model.get_value(iter,1)
        self.identificador=model.get_value(iter,2)
    def sel_resultado(self,completion,model,iter):
        self.cod_tipo_resultado=model.get_value(iter,1)
        self.txtResultado.set_text(model.get_value(iter,0))        

    def on_btnCancelar_clicked(self, widget, *args):
        self.dlgPlandeCuentas.destroy()

    def on_btnAceptar_clicked(self, widget, *args):
        num_cuenta = self.lblCuenta.get_text() + self.entNumeroCuenta.get_text() + self.lblCuenta2.get_text()
        if not self.verifica_cuenta(num_cuenta):
            dlgError(None,"El formato de la cuenta no corresponde.",None,False)
            return 0            
        desc = self.txtDescripcion.get_text()        
       
        if len(desc)==0:
            dlgError(None,'La cuenta no tiene descripción',None,False)
            return 0            
        cod_tipo_cuenta = combo_get_active(self.cmbTipoCuenta,False)
       
        if self.chkVigente.get_active():
            vigente = 'V'            
        else:
            vigente = 'N'
        nivel = self.lblNivel.get_text()
       
        if self.chkResultado.get_active():
            aplica_resultado = 'S'
            if self.pecResultado.get_selected():                
                cod_tipo_resultado = self.cod_tipo_resultado                            
            else:
                error("Debe seleccionar el tipo Resultado")
                return
        else:
            aplica_resultado = 'N'
            cod_tipo_resultado = 0

        if self.optSinAnalisis.get_active():
            analisis = 'N'
        else:
            if self.optConAnalisis.get_active():
                analisis = 'S'
            else:
                analisis = 'D'
        
        if self.edit==False:
            try:
                sql=strSelectCuentaContable %(num_cuenta,int(self.cod_empresa))
                self.cursor.execute(sql)
                r=self.cursor.fetchall()
                if len(r)>0:
                    error("La Cuenta Contable ya existe")
                    return
            except:
                error('Ha ocurrido al seleccionar los datos de la cuenta.',str(sys.exc_info()[1]))
                return
        if self.edit==False:
            if self.num_padre=="PLAN DE CUENTAS":
                num_padre='Null'
            else:
                num_padre=self.num_padre
            try:                
                campos={}
                campos['num_cuenta']=num_cuenta
                campos['estado_cuenta']=vigente
                campos['nivel_cuenta']=int(nivel)
                campos['tiene_hijos']='N'
                campos['estado_resultado']=0
                campos['tipo_analisis']=analisis
                campos['tipo_cuenta']=cod_tipo_cuenta
                campos['descripcion_cuenta']=desc
                campos['num_padre']=num_padre
                campos['cod_empresa']=self.cod_empresa
                campos['aplica_resultado'] =aplica_resultado
                campos['cod_tipo_resultado']=cod_tipo_resultado
                self.cursor.execute('begin')
                sql=ifd.insertFromDict('ctb.cuenta_contable',campos)                           
                self.cursor.execute(sql,campos)
                self.cursor.execute('commit')
                self.response=1
            except:
                self.cursor.execute('rollback')
                error('Ha ocurrido al seleccionar los datos de la cuenta.',str(sys.exc_info()[1]))
                return 
        else:
            try:
                campos={}
                llaves={}
                campos['descripcion_cuenta']=desc
                campos['aplica_resultado']=aplica_resultado
                campos['cod_tipo_resultado']=cod_tipo_resultado
                campos['tipo_analisis']=analisis
                campos['estado_cuenta']=vigente
                llaves['num_cuenta']=num_cuenta
                llaves['cod_empresa']=self.cod_empresa
                sql,dic=ifd.updateFromDict('ctb.cuenta_contable',campos,llaves)
                self.cursor.execute(sql,dic)
                if self.cod_fecus_ant!=None:
                    sql="delete from ctb.cuenta_contable_fecus where cod_empresa=%s and num_cuenta='%s' and cod_fecus=%s"%(self.cod_empresa,num_cuenta,self.cod_fecus_ant)
                    self.cursor.execute(sql)
                self.cursor.execute('commit')
                self.response=1
            except:
                self.cursor.execute('rollback')
                error('Ha ocurrido al seleccionar los datos de la cuenta.',str(sys.exc_info()[1]))
        self.num_cuenta=num_cuenta    
    
    def verifica_cuenta(self,cuenta):        
        try:
            sqlWhere=''
            sql=strSelectEmpresaNivel %(int(self.cod_empresa),sqlWhere)
            self.cursor.execute(sql)
            r=self.cursor.fetchall()            
        except:
            error('Ha ocurrido al consultar los niveles de cuenta.',str(sys.exc_info()[1]))
            return 0
        c = 0
        ret = 1
        try:
            for i in r:
                j = c    
                while j >= c and j < c + i[0]:
                    if not int(cuenta[j]) >= 0 and int(cuenta[j]) <= 9:
                        ret = 0    
                    j = j + 1    
                c = c + i[0] + 1    
            return ret
        except:
            return None

if __name__ == '__main__':
    cnx = connect("host=192.168.0.11 dbname=scp user=rmendez")
    #cnx.autocommit()
    #sys.excepthook = debugwindow.show
    d = wnPlandeCuentas(cnx,None,2)
    
    gtk.main()
