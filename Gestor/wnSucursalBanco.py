#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnSucursalBanco -- formulario de ingreso, eliminacion y edición, de Sucursales de Bancos.
# (C)    José Luis Álvarez Morales 2006
#           jalvarez@galilea.cl

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
from strSQL import strSelectSucursalBanco

(CODIGO,
  DESCRIPCION_SUCURSAL,
  CODIGO_BANCO,
  DESCRIPCION_BANCO) = range(4) 

class wnSucursalBanco (GladeConnect):
    def __init__ (self, conexion = None, padre= None, root="wnSucursalBanco"):
        GladeConnect.__init__ (self, "glade/wnSucursalBanco.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnSucursalBanco.maximize()
            self.frm_padre = self.wnSucursalBanco
            self.padre
        else:
            self.frm_padre = self.padre.frm_padre

        self.crea_columnas()
        self.carga_datos()

    def crea_columnas (self):
        columnas = []
        columnas.append([CODIGO, "Código", "str"])
        columnas.append([DESCRIPCION_SUCURSAL,"Nombre Sucursal Banco", "str"])
        columnas.append([DESCRIPCION_BANCO, "Nombre Banco", "str"])
    
        self.carga_datos()
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeSucursalBanco)
        
    def carga_datos(self):
        self.modelo = ifd.ListStoreFromSQL(self.cnx, strSelectSucursalBanco)
        self.treeSucursalBanco.set_model(self.modelo)
    
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgSucursalBanco(self.cnx, self.frm_padre, False)
        dlg.editando =False
        response = dlg.dlgSucursalBanco.run()
        if response == gtk.RESPONSE_OK:
            self.carga_datos()
            
    def on_btnQuitar_clicked (self, btn=None):
        model, it = self.treeSucursalBanco.get_selection().get_selected()
        if model is None or it is None:
            return
        descripcion = model.get_value(it, DESCRIPCION_SUCURSAL)
        codigo = model.get_value(it, CODIGO)
        
        if dialogos.yesno("Desea eliminar el SucursalBanco, <b>%s</b>\nEsta acción no se puede deshacer!"% descripcion, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'codigo_sucursal':codigo}
            sql = ifd.deleteFromDict("ctb.sucursal_banco", llaves)
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()[1]
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>\nLa Sucursal de Banco<b>%s</b>, se encuentra relacionado.")
    
    def on_btnPropiedades_clicked (self, btn=None):
        model, it = self.treeSucursalBanco.get_selection().get_selected()
        if model is None or it is None:
            return
        dlg = dlgSucursalBanco(self.cnx, self.frm_padre, False)
        dlg.entCodigo.set_text(model.get_value(it, CODIGO))
        dlg.entDescripcion.set_text(model.get_value(it, DESCRIPCION_SUCURSAL))
        dlg.codigo_banco = dlg.pecBanco.select(model.get_value(it, DESCRIPCION_BANCO),1)
        
        dlg.editando= True
        response = dlg.dlgSucursalBanco.run()
        if response == gtk.RESPONSE_OK:
            self.modelo.clear()
            self.carga_datos()
        
    def on_btnCerrar_clicked (self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Sucursal_Banco")
    
    def on_treeSucursalBanco_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()


class dlgSucursalBanco(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=None):
        GladeConnect.__init__(self, "glade/wnSucursalBanco.glade", "dlgSucursalBanco")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.editando = editando
        self.codigo_banco = None
        
        self.pecBanco = completion.CompletionBanco(self.entBanco,
                self.sel_banco,
                self.cnx)

        self.entDescripcion.grab_focus()
        self.dlgSucursalBanco.show_all()
        
    def sel_banco(self, completion, model, it):
        self.codigo_banco = model.get_value(it,1)
        
    def on_btnAceptar_clicked(self, btn=None):
        if self.entDescripcion.get_text() == "":
            dialogos.error ("El Campo <b>Nombre Sucursal</b> no puede estar vacío.")
            return
        
        if self.entBanco.get_text() == "":
            dialogos.error ("El Campo <b>Nombre Banco</b> no puede estar vacío.")
            return
        
        campos = {}
        llaves = {}
        campos ['descripcion_sucursal']=self.entDescripcion.get_text().upper()
        campos['cod_banco'] = self.codigo_banco    
        
        if not self.editando:
            sql = ifd.insertFromDict("ctb.sucursal_banco", campos)
            
        else:
            llaves['codigo_sucursal'] = self.entCodigo.get_text()
            sql, campos = ifd.updateFromDict("ctb.sucursal_banco", campos, llaves)
            
        self.cursor.execute(sql, campos)
        self.dlgSucursalBanco.hide()
        
    def on_btnCancelar_clicked (self, btn=None):
        self.dlgSucursalBanco.hide()
        

if __name__ == "__main__":
    cnx = connect("dbname = ctb")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    w = wnSucursalBanco(cnx)
    
    gtk.main()