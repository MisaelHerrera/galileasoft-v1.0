#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnPeriodoContable -- formulario de ingreso, eliminacion y edición, de los distintos tipos de Periodos contables,
#para cada empresa en el sistema.

# (C)   Claudio Salgado Morales 2003, 2004

#This file is part of pyGestor.
#
# pyGestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# (C)    José Luis Álvarez Morales    2006, 2007.
#            josealvarezm@gmail.com

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
import datetime
from time import localtime
from strSQL import strSelectPeriodoContable
from strSQL import strUsuarioActual
from strSQL import strFechaHoraActual
from comunes import VerificaPermiso
(PERIODO,
  CODIGO_EMPRESA,
  DESCRIPCION_EMPRESA,
  MES,
  DESCRIPCION_MES,
  DESCRIPCION_PERIODO,
  CODIGO_ESTADO,
  DESCRIPCION_ESTADO) = range(8) 

class wnPeriodoContable (GladeConnect):
    def __init__ (self, conexion = None, padre= None, empresa=1, root="wnPeriodoContable"):
        GladeConnect.__init__ (self, "glade/wnPeriodoContable.glade", root)
        self.cnx = conexion
        self.ventana = None
        self.cursor = self.cnx.cursor()
        self.padre = padre
        self.cod_empresa = empresa
                        
        if padre is None:
            self.wnPeriodoContable.maximize()
            self.frm_padre = self.wnPeriodoContable
            self.padre
        else:
            self.frm_padre = self.padre.frm_padre
        self.pecPeriodo=completion.CompletionPeriodoContable(self.entBPeriodo,self.sel_periodo_contable,self.cnx,self.cod_empresa)
        self.crea_columnas()
        self.periodo=None
        self.mes=None
##        self.carga_datos()
    def sel_periodo_contable(self,completion,model,iter):
        self.periodo=model.get_value(iter,2)
        self.mes=model.get_value(iter,1)
        
        
    def crea_columnas (self):
        columnas = []
        columnas.append([PERIODO, "Periodo", "str"])
        columnas.append([DESCRIPCION_EMPRESA,"Nombre Empresa", "str"])
        columnas.append([DESCRIPCION_MES, "Mes", "str"])
        columnas.append([DESCRIPCION_PERIODO, "Descripción Periodo", "str"])
        columnas.append([DESCRIPCION_ESTADO, "Descripción Estado", "str"])

        self.modelo = gtk.ListStore(*(8*[str]))
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treePeriodoContable)
        
    def carga_datos(self):
        self.modelo = ifd.ListStoreFromSQL(self.cnx, strSelectPeriodoContable)
        self.treePeriodoContable.set_model(self.modelo)
    
    def on_btnAnadir_clicked(self, btn=None):
        if not VerificaPermiso(self,101):
            return
        dlg = dlgPeriodoContable(self.cnx, self.frm_padre, False, self.cod_empresa)
        dlg.editando = False
        dlg.cod_empresa = self.cod_empresa
        band = False
        while not band:
            response = dlg.dlgPeriodoContable.run()
            if response == gtk.RESPONSE_OK:
                if dlg.response != None:
                    self.modelo.append(dlg.response)
                    band = True
            else:
                band = True
        dlg.dlgPeriodoContable.destroy()
            
    def on_btnQuitar_clicked (self, btn=None):
        if not VerificaPermiso(self,103):
            return
        model, it = self.treePeriodoContable.get_selection().get_selected()
        if model is None or it is None:
            return
        
        periodo = model.get_value(it, PERIODO)
        codigo = model.get_value(it, CODIGO_EMPRESA)
        descripcion = model.get_value(it, DESCRIPCION_EMPRESA)
        mes = model.get_value(it, MES)
        descripcion_mes = model.get_value(it, DESCRIPCION_MES)
                
        if dialogos.yesno("Desea eliminar el Periodo <b>%s</b>, con mes de <b>%s</b> de la empresa <b>%s</b> \nEsta acción no se puede deshacer!!!"% (periodo,descripcion_mes,descripcion), self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'periodo' : periodo,'mes': mes,'cod_empresa' : codigo}
            sql = ifd.deleteFromDict("ctb.periodo_contable", llaves)
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()[1]
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>.")
    
    def on_btnPropiedades_clicked (self, btn=None):
        if not VerificaPermiso(self,102):
            return
        model, it = self.treePeriodoContable.get_selection().get_selected()
        if model is None or it is None:
            return
                
        dlg = dlgPeriodoContable(self.cnx, self.frm_padre, False, self.cod_empresa)
        dlg.cod_empresa = self.cod_empresa
        
        dlg.spnPeriodo.set_text(model.get_value(it, PERIODO))

        sql = "SELECT descripcion_empresa,cod_empresa FROM ctb.empresa WHERE cod_empresa = %s"%(self.cod_empresa) 
        self.cursor.execute(sql)
        r = self.cursor.fetchall()        
        dlg.entEmpresa.set_text(r[0][0])
        dlg.cod_empresa=r[0][1]        
  
        dlg.codigo_mes = dlg.pecMes.select(model.get_value(it, DESCRIPCION_MES),1)
        dlg.entDescripcion.set_text(model.get_value(it, DESCRIPCION_PERIODO))
        dlg.codigo_estado = dlg.pecEstado.select(model.get_value(it, DESCRIPCION_ESTADO),1)
            
        dlg.spnPeriodo.set_sensitive(False)
        dlg.entEmpresa.set_sensitive(False)
        dlg.entMes.set_sensitive(False)
        dlg.entDescripcion.set_sensitive(False)
        
        dlg.editando= True
        band=False
        while band==False:
            response = dlg.dlgPeriodoContable.run()
            if response == gtk.RESPONSE_OK:
                if dlg.respuesta==True:
                    del model[it]
                    self.modelo.append(dlg.response)
                    band=True
            else:
                band=True
        dlg.dlgPeriodoContable.destroy()
        
    def on_btnBuscar_clicked(self, btn=None):
        buscar = ''
        if self.pecPeriodo.get_selected():
            buscar = " and p.periodo = %s and mes=%s" %(int(self.periodo),int(self.mes))
        sql = strSelectPeriodoContable.replace("ORDER BY p.periodo","WHERE d.cod_empresa = %s%s")%(self.cod_empresa, buscar)
        self.cursor.execute(sql)
        r = self.cursor.fetchall()
        self.modelo.clear()
        for i in r:
            self.modelo.append(i)
        
    def on_btnCerrar_clicked (self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Periodo_Contable")
    
    def on_treePeriodoContable_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()

class dlgPeriodoContable(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=None, empresa=1):
        GladeConnect.__init__(self, "glade/wnPeriodoContable.glade", "dlgPeriodoContable")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.editando = editando
        self.spnPeriodo.grab_focus()
        
        self.cod_empresa = empresa        
        self.codigo_mes = None
        self.codigo_estado = None
        self.response = None

        sql = "SELECT descripcion_empresa,cod_empresa FROM ctb.empresa WHERE cod_empresa = %s"%(self.cod_empresa) 
        self.cursor.execute(sql)
        r = self.cursor.fetchall()        
        self.entEmpresa.set_text(r[0][0])
        self.cod_empresa=r[0][1]       
        self.entEmpresa.set_sensitive(False)
        self.pecMes = completion.CompletionMes(self.entMes,
                self.sel_mes,
                self.cnx)
        
        self.pecEstado = completion.CompletionEstado(self.entEstado,
                self.sel_estado,
                self.cnx)
        self.entDescripcion.set_sensitive(False)
        (y,m,d)=localtime()[:3]        
        self.spnPeriodo.set_value(y)
        self.respuesta=False
    def sel_mes(self, completion, model, it):
        self.codigo_mes = model.get_value(it,1)
        self.entDescripcion.set_text(self.entEmpresa.get_text()+" "+self.entMes.get_text()+" "+self.spnPeriodo.get_text())
        
    def sel_estado(self, completion, model, it):
        self.codigo_estado = model.get_value(it,1) 
        
    def on_btnAceptar_clicked(self, btn=None):
        if self.spnPeriodo.get_value <= 0:
            dialogos.error("El Campo <b>Periodo</b> debe ser mayor a cero.")
            self.spnPeriodo.grab_focus()
            return   
  
        if not self.pecMes.get_selected() or self.entMes.get_text() == "":
            dialogos.error ("El Campo <b>Mes</b> no puede estar vacío.")
            self.entMes.grab_focus()
            return
        
        if self.entDescripcion.get_text() == "" or len(self.entDescripcion.get_text().replace(" ",""))==0:
            dialogos.error ("El Campo <b>Descripción</b> no puede estar vacío.")
            self.entDescripcion.grab_focus()
            return
        
        if not self.pecEstado.get_selected() or self.entEstado.get_text() == "":
            dialogos.error("El campo <b>Estado Periodo</b> no puede estar vacío")
            self.entEstado.grab_focus()
            return

        self.cursor.execute(strUsuarioActual)
        r_consulta=self.cursor.fetchall()
        self.current_user=str(r_consulta[0][0])
        
        self.cursor.execute(strFechaHoraActual)
        r_consulta2=self.cursor.fetchall()
        self.current_timestamp=r_consulta2[0][0]
    
        campos = {}
        llaves = {}
        campos['periodo'] = self.spnPeriodo.get_text()
        campos ['cod_empresa'] = self.cod_empresa
        campos ['mes'] = self.codigo_mes
        campos ['descripcion_periodo'] = self.entDescripcion.get_text().upper()
        campos ['estado'] = self.codigo_estado    
        campos ['user_ultima_actualiza'] = self.current_user       
        campos ['fecha_ultima_actualiza'] =  self.current_timestamp
        
        if not self.editando:
            sql="""select  periodo,mes,cod_empresa from ctb.periodo_contable where periodo=%s and mes=%s and cod_empresa=%s"""%(int(campos['periodo']),int(campos['mes']),int(campos['cod_empresa']))
            self.cursor.execute(sql)
            r=self.cursor.fetchall()
            if len(r)>0:
                dialogos.error("El Periodo se encuentra ingresado")
                return
            sql = ifd.insertFromDict("ctb.periodo_contable", campos)
            
        else:
            llaves['periodo'] = self.spnPeriodo.get_text()
            llaves['mes'] = self.codigo_mes
            llaves['cod_empresa'] = self.cod_empresa
            sql, campos = ifd.updateFromDict("ctb.periodo_contable", campos, llaves)
            
        try:
            self.cursor.execute(sql, campos)
            self.dlgPeriodoContable.hide()
            
            self.response = [self.spnPeriodo.get_text(),
                                    self.cod_empresa,
                                    self.entEmpresa.get_text().upper(),
                                    self.codigo_mes,
                                    self.entMes.get_text().upper(),
                                    self.entDescripcion.get_text().upper(),
                                    self.codigo_estado,
                                    self.entEstado.get_text().upper()]
            self.respuesta=True
        except:
            print sys.exc_info()[1]
            dialogos.error("En el sistema ya existen estos datos")
            self.spnPeriodo.grab_focus()
         
    def on_btnCancelar_clicked (self, btn=None):
        self.dlgPeriodoContable.hide()
        
if __name__ == "__main__":
    cnx = connect("host=localhost dbname = scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    w = wnPeriodoContable(cnx)
    
    gtk.main()
