#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnTipoComprobante -- formulario de ingreso, eliminación y edición, de los distintos tipos de Comprobantes contables.
# (C)    José Luis Álvarez Morales 2006, 2007.
#           jalvarez@galilea.cl

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
from strSQL import strSelectTipoComprobante

(CODIGO,
 DESCRIPCION) = range(2)

class wnTipoComprobante(GladeConnect):
    def __init__(self, conexion=None, padre=None, root="wnTipoComprobante"):
        GladeConnect.__init__(self, "glade/wnTipoComprobante.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnTipoComprobante.maximize()
            self.frm_padre = self.wnTipoComprobante
            self.padre
        else:
            self.frm_padre = padre.frm_padre
            
        self.crea_columnas()
        self.carga_datos()
    
    def crea_columnas(self):
        columnas = []
        columnas.append([CODIGO, "Código", "int"])
        columnas.append([DESCRIPCION, "Descripción Tipo Comprobante", "str"])
        
        self.carga_datos()
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeTipoComprobante)
        
    def carga_datos(self):
        self.modelo = ifd.ListStoreFromSQL(self.cnx, strSelectTipoComprobante)
        self.treeTipoComprobante.set_model(self.modelo)
    
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgTipoComprobante(self.cnx, self.frm_padre, False)
        dlg.editando = False
        response = dlg.dlgTipoComprobante.run()
        if response == gtk.RESPONSE_OK:
            self.carga_datos()
            
    def on_btnQuitar_clicked(self, btn=None):
        model, it = self.treeTipoComprobante.get_selection().get_selected()
        if model is None or it is None:
            return
        codigo = model.get_value(it, CODIGO)
        descripcion = model.get_value(it, DESCRIPCION)
        
        if dialogos.yesno("¿Desea eliminar el TipoComprobante <b>%s</b>?\nEsta acción no se puede deshacer!!!"%descripcion, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'cod_tipo_comprobante':codigo}
            sql = ifd.deleteFromDict('ctb.tipo_comprobante', llaves)
            
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()[1]
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>\nEl TipoComprobante <b>%s</b>, se encuentra relacionado.")
                
    def on_btnPropiedades_clicked(self, btn=None):
        model, it = self.treeTipoComprobante.get_selection().get_selected()
        if model is None or it is None:
            return
        
        dlg = dlgTipoComprobante(self.cnx, self.frm_padre, False)
        dlg.entCodigo.set_text(model.get_value(it, CODIGO))
        dlg.entDescripcion.set_text(model.get_value(it, DESCRIPCION))
        
        dlg.entCodigo.set_sensitive(False)
        
        dlg.editando = True
        response = dlg.dlgTipoComprobante.run()
        if response == gtk.RESPONSE_OK:
            self.modelo.clear()
            self.carga_datos()
            
    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Tipo_Comprobante")
            
    def on_treeTipoComprobante_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()
        
class dlgTipoComprobante(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=None):
        GladeConnect.__init__(self, "glade/wnTipoComprobante.glade", "dlgTipoComprobante")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.editando = editando
        self.entCodigo.grab_focus()
        self.dlgTipoComprobante.show_all()
        
    def on_btnAceptar_clicked(self, btn=None):
        if self.entCodigo.get_text() == "" or len(self.entCodigo.get_text()) >1:
            dialogos.error("El campo <b>Código Tipo Comprobante</b> no puede estar vacío o exceder de un caracter.")
            return
        
        if self.entDescripcion.get_text() == "" or len(self.entDescripcion.get_text().replace(" ",""))==0:
            dialogos.error("El campo <b>Descripción Tipo Comprobante</b> no puede estar vacío.")
            return
        
        campos = {}
        llaves = {}
        
        campos ['descripcion_tipo'] = self.entDescripcion.get_text().upper()  
        
        if not self.editando:
            campos ['cod_tipo_comprobante'] = self.entCodigo.get_text().upper()
            sql = ifd.insertFromDict("ctb.tipo_comprobante", campos)
        else:
            llaves['cod_tipo_comprobante'] = self.entCodigo.get_text()
            sql, campos = ifd.updateFromDict("ctb.tipo_comprobante", campos, llaves)
            
        try:
            self.cursor.execute(sql,campos)
            self.dlgTipoComprobante.hide()
        except:
            print sys.exc_info()[1]
            cod_tipo_comprobante = self.entCodigo
            dialogos.error("En la Base de Datos ya existe el TIpo Comprobante <b>%s</b>."%campos['cod_tipo_comprobante'])
            self.entCodigo.grab_focus()
            
               
    def on_btnCancelar_clicked(self, btn=None):
        self.dlgTipoComprobante.hide()
        
if __name__ == '__main__':
    cnx = connect("host=localhost dbname=scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    d = wnTipoComprobante(cnx)
    
    gtk.main()
