#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnTipoResultado -- formulario de ingreso, eliminación y edición, de los distintos tipos de Resultados, 
#define los items de resultado en el sistema.

# (C)  Fernando San Martín Woerner 2003, 2004
#       snmartin@galilea.cl

#This file is part of pyGestor.
#
# pyGestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# (C)    José Luis Álvarez Morales    2006,2007.
#           josealvarezm@gmail.com

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
from strSQL import strSelectTipoResultado1

(CODIGO,
 DESCRIPCION_RESULTADO,
 CODIGO_EMPRESA,
 DESCRIPCION_EMPRESA) = range(4) 

class wnTipoResultado (GladeConnect):
    def __init__ (self, conexion = None, padre= None, root="wnTipoResultado"):
        GladeConnect.__init__ (self, "glade/wnTipoResultado.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnTipoResultado.maximize()
            self.frm_padre = self.wnTipoResultado
            self.padre
        else:
            self.frm_padre = self.padre.frm_padre

        self.crea_columnas()
        self.carga_datos()

    def crea_columnas (self):
        self.modelo=gtk.ListStore(str,str,str,str)
        columnas = []
        columnas.append([CODIGO, "Codigo", "str"])
        columnas.append([DESCRIPCION_RESULTADO,"Descripción Resultado", "str"])
        columnas.append([DESCRIPCION_EMPRESA, "Empresa", "str"])              
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeTipoResultado)        
    def carga_datos(self):
##        self.modelo = ifd.ListStoreFromSQL(self.cnx, strSelectTipoResultado1
        sql=strSelectTipoResultado1
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        self.modelo.clear()
        for i in r:
            self.modelo.append(i)
##        self.treeTipoResultado.set_model(self.modelo)
    
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgTipoResultado(self.cnx, self.frm_padre, False)
        dlg.editando=False
        band = False
        while not band:
            response = dlg.dlgTipoResultado.run()
            if response == gtk.RESPONSE_OK:
                if dlg.commit== True:
                    self.modelo.append(dlg.response)
                    band = True
            else:
                band = True
        dlg.dlgTipoResultado.destroy()
            
    def on_btnQuitar_clicked (self, btn=None):
        model, it = self.treeTipoResultado.get_selection().get_selected()
        if model is None or it is None:
            return
        resultado = model.get_value(it, DESCRIPCION_RESULTADO)
        codigo = model.get_value(it, CODIGO)
        
        if dialogos.yesno("¿Desea eliminar el Tipo Resultado, <b>%s</b>?\nEsta acción no se puede deshacer!!!"% resultado, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'cod_tipo_resultado':codigo}
            sql = ifd.deleteFromDict("ctb.tipo_resultado", llaves)
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()[1]
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>\nEl Tipo Resultado<b>%s</b>, se encuentra relacionado.")
    
    def on_btnPropiedades_clicked (self, btn=None):
        model, it = self.treeTipoResultado.get_selection().get_selected()
        if model is None or it is None:
            return
        dlg = dlgTipoResultado(self.cnx, self.frm_padre, False)
        dlg.entCodigo.set_text(model.get_value(it, CODIGO))
        dlg.entDescripcion.set_text(model.get_value(it, DESCRIPCION_RESULTADO))
        dlg.codigo_empresa = dlg.pecEmpresa.select(model.get_value(it, DESCRIPCION_EMPRESA),1)
        
        dlg.editando= True
        band=False
        while not band:            
            response = dlg.dlgTipoResultado.run()
            if response == gtk.RESPONSE_OK:
                if dlg.commit==True:
                    del model[it]
                    self.modelo.append(dlg.response)
                    band=True
            else:
                band=True
        dlg.dlgTipoResultado.destroy()
        
    def on_btnCerrar_clicked (self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Centros_de_Utilidad")
    
    def on_treeTipoResultado_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked() 

class dlgTipoResultado(GladeConnect):
    
    def __init__(self, conexion=None, padre=None, editando=None):
        GladeConnect.__init__(self, "glade/wnTipoResultado.glade", "dlgTipoResultado")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.editando = editando
        self.codigo_empresa = None
        self.response = None
        
        self.pecEmpresa = completion.CompletionEmpresa(self.entEmpresa,
                self.sel_empresa,
                self.cnx)

        self.entDescripcion.grab_focus()
        self.dlgTipoResultado.show()
        self.commit=False
    def sel_empresa(self, completion, model, it):
        self.codigo_empresa = model.get_value(it,1)
        
    def on_btnAceptar_clicked(self, btn=None):
        if self.entDescripcion.get_text() == "" or len(self.entDescripcion.get_text().replace(" ",""))==0:
            dialogos.error ("El Campo <b>Descripción</b> no puede estar vacío.")
            self.entDescripcion.grab_focus()
            return
        
        if not self.pecEmpresa.get_selected():
            dialogos.error("El Campo <b>Empresa</b> no puede estar vacío.")
            self.entEmpresa.grab_focus()
            return
        
        campos = {}
        llaves = {}
        
        campos ['descripcion_resultado'] = self.entDescripcion.get_text().upper()
        campos['cod_empresa'] = self.codigo_empresa    
        
        if not self.editando:
            self.cursor.execute("select nextval('ctb.tipo_resultado_cod_tipo_resultado_seq')")
            r = self.cursor.fetchall()
            self.entCodigo.set_text(str(r[0][0]))
            campos['cod_tipo_resultado'] = self.entCodigo.get_text()
            sql = ifd.insertFromDict("ctb.tipo_resultado", campos)
        else:
            llaves['cod_tipo_resultado'] = self.entCodigo.get_text()
            sql, campos = ifd.updateFromDict("ctb.tipo_resultado", campos, llaves)
        
        try:
            self.cursor.execute(sql, campos)
            self.dlgTipoResultado.hide()
            
            self.response = [self.entCodigo.get_text(),
                                    self.entDescripcion.get_text().upper(),
                                    self.codigo_empresa,
                                    self.entEmpresa.get_text().upper()
                                    ]
            self.commit=True
        except:
            print sys.exc_info()[1]
            dialogos.error("Estos datos ya existen en el sistema.")
            self.entDescripcion.grab_focus()
        
    def on_btnCancelar_clicked (self, btn=None):
        self.dlgTipoResultado.hide()
        

if __name__ == "__main__":
    cnx = connect("host=localhost dbname=scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    w = wnTipoResultado(cnx)
    
    gtk.main()
