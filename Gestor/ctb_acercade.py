﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ctb_ficha -- Define las fichas del sistema
# (C)   Fernando San Martín Woerner 2003, 2004
#       snmartin@gnome.cl
# (C)   Claudio Salgado Morales 2003, 2004
#       csalgado@galilea.cl
#This file is part of pyGestor.
#
# pyGestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


from GladeConnect import GladeConnect
from ctb_rutinas import *
from string import *
import sys
import gobject
from gtk import *

class ctb_acercade(GladeConnect):

    def __init__(self, c=None,e=None):

        GladeConnect.__init__(self, "glade/ctb_acercade.glade")


