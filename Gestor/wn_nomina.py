#!/usr/bin/env python
# -*- coding: utf-8 -*-
# wn_nomina -- Genera nóminas y comporbantes contables
# (c) Fernando San Martín Woerner 2003, 2004, 2005
# snmartin@galilea.cl

# This file is part of Gestor.
#
# Gestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
import comunes
import completion
import gtk
import empresa
import nomina
import spg as psycopg
import debugwindow

#Constantes para cuentas de fondo fijo (CFF)
NUMERO_CFF = 0
DESCRIPCION_CFF = 1
EMPRESA_CFF = 2
#Constantes para nominas
CHECK_NOMINA = 0
NOMBRE = 1
RUT = 2
FOLIO = 3
FECHA = 4
MONTO = 5
EMPRESA = 6
COMPROBANTE = 7
CUENTA = 8
NUM_CUENTA = 9
TIPO_DOC = 10
LINEA = 11
CTA_CTE = 12
BANCO = 13

class wnNomina(comunes.GladeConnect):
    """Define y crea las empresas a utilizar en el sistema"""

    def __init__(self, c=None, padre=None, e=None):
        """Constructor de la clase incializa las propiedades"""
        comunes.GladeConnect.__init__(self, "./glade/wn_nomina.glade")
        self.cnx = c
        self.ventana_activa = None
        self.cod_empresa = e
        cursor = self.cnx.cursor()
        cursor.execute("""SELECT rut_empresa FROM ctb.empresa
                WHERE cod_empresa = %s""" % e)
        record = cursor.fetchall()
        self.rut_empresa = record[0][0]
        self.num_cuenta = None
        self.num_cuenta_banco = None
        cursor.execute("""SELECT e.num_cuenta, c.descripcion_cuenta
                        FROM nomina.cuenta_egreso e
                        INNER JOIN ctb.cuenta_contable c
                        ON  e.num_cuenta = c.num_cuenta""")
        record = cursor.fetchall()
        if len(record)>0:
            self.num_cuenta_egreso = record[0][0]
            self.desc_cuenta_egreso = record[0][1]
        else:
            self.num_cuenta_egreso = None
            self.desc_cuenta_egreso = None
        self.todos = False
        self.elements = 0
        if padre is None:
            self.padre = None
        else:
            self.padre = padre
        self.treeNomina.set_rules_hint(True)
        self.treeCuentaFF.set_rules_hint(True)
        self.crea_columnas_cff()
        self.crea_modelo_cff()
        self.crea_columnas_nomina()
        self.crea_modelo_nomina()
        self.carga_ff()
        self.pecCuentaFF = completion.CompletionCuenta(self.entCuentaFF,
                    self.sel_cuentaff,
                    self.cnx,
                    self.cod_empresa)
        self.pecCuentaBanco = completion.CompletionCuenta(self.entCuentaBanco,
                    self.sel_cuenta_banco,
                    self.cnx,
                    self.cod_empresa)
        self.pecCuentaEgreso = completion.CompletionCuenta(self.EntCuentaEgreso,
                    self.sel_cuenta_egreso,
                    self.cnx,
                    self.cod_empresa)
        self.pecBancoEgreso = completion.CompletionBanco(self.entBancoEgreso,
                    self.sel_banco_egreso,
                    self.cnx,
                    self.cod_empresa)
        #~ self.add_mandatory(self.spnComprobante)

    def sel_cuentaff(self, completion, model, iter):
        """Recupera el número de cuenta de fondo fijo seleccionada"""
        self.num_cuenta = model.get_value(iter, 1)

    def sel_cuenta_banco(self, completion, model, iter):
        """Recupera el número de cuenta de fondo fijo seleccionada"""
        self.num_cuenta_banco = model.get_value(iter, 1)

    def sel_cuenta_egreso(self, completion, model, iter):
        """Recupera el número de cuenta de fondo fijo seleccionada"""
        self.num_cuenta_egreso = model.get_value(iter, 1)
        self.desc_cuenta_egreso = model.get_value(iter, 0)
        cursor = self.cnx.cursor()
        cursor.execute("begin")
        cursor.execute("""DELETE FROM nomina.cuenta_egreso""")
        cursor.execute("""INSERT INTO nomina.cuenta_egreso
                           VALUES ('%s')""" % self.num_cuenta_egreso)
        cursor.execute("commit")

    def sel_banco_egreso(self, completion, model, iter):
        """Recupera el código del banco para el egreso del fondo fijo."""
        self.cod_banco_egreso = model.get_value(iter, 1)
        self.desc_banco_egreso = model.get_value(iter, 0)
        cursor = self.cnx.cursor()
        cursor.execute("begin")
        cursor.execute("""UPDATE nomina.cuenta_egreso
                           SET cod_banco =  %s""" % self.cod_banco_egreso)
        cursor.execute("commit")

    def crea_columnas_cff(self):
        """Crea las columnas del treeview para cuentas de fondo fijo."""
        render = gtk.CellRendererText()
        column = gtk.TreeViewColumn('Número', render, text=NUMERO_CFF)
        self.treeCuentaFF.append_column(column)
        render = gtk.CellRendererText()
        column = gtk.TreeViewColumn('Descripción', render, text=DESCRIPCION_CFF)
        self.treeCuentaFF.append_column(column)
        column.set_sort_column_id(DESCRIPCION_CFF)

    def fixed_toggled(self, render, path):
        self.modelo_nomina[path][CHECK_NOMINA] = not self.modelo_nomina[path][CHECK_NOMINA]
        if self.modelo_nomina[path][CHECK_NOMINA]:
            self.elements += 1
        else:
            self.elements -= 1
        return

    def on_btnTodos_clicked(self, btn=None, data=None):
        iter = self.modelo_nomina.get_iter_first()
        self.todos = not self.todos
        self.elements = 0
        while not iter is None:
            if self.todos:
                self.elements += 1
            self.modelo_nomina.set(iter, CHECK_NOMINA, self.todos)
            iter = self.modelo_nomina.iter_next(iter)

    def crea_columnas_nomina(self):
        """Crea las columnas del treeview para listar fondos fijos pendientes."""
        renderer = gtk.CellRendererToggle()
        renderer.connect('toggled', self.fixed_toggled)
        column = gtk.TreeViewColumn('Centr.', renderer, active=CHECK_NOMINA)
        column.set_clickable(True)
        column.set_sort_column_id(CHECK_NOMINA)
        self.treeNomina.append_column(column)
        render = gtk.CellRendererText()
        column = gtk.TreeViewColumn('Nombre', render, text=NOMBRE)
        self.treeNomina.append_column(column)
        column.set_cell_data_func(render, comunes.columna_utf8, NOMBRE)
        column.set_sort_column_id(NOMBRE)
        render = gtk.CellRendererText()
        column = gtk.TreeViewColumn('RUT', render, text=RUT)
        self.treeNomina.append_column(column)
        column.set_cell_data_func(render, comunes.columna_rut, RUT)
        column.set_sort_column_id(RUT)
        render = gtk.CellRendererText()
        column = gtk.TreeViewColumn('Folio', render, text=FOLIO)
        self.treeNomina.append_column(column)
        column.set_cell_data_func(render, comunes.columna_numerica, FOLIO)
        column.set_sort_column_id(FOLIO)
        render = gtk.CellRendererText()
        column = gtk.TreeViewColumn('Fecha', render, text=FECHA)
        column.set_sort_column_id(FECHA)
        self.treeNomina.append_column(column)
        column.set_cell_data_func(render, comunes.columna_fecha, FECHA)
        column.set_sort_column_id(FECHA)
        render = gtk.CellRendererText()
        column = gtk.TreeViewColumn('Monto', render, text=MONTO)
        self.treeNomina.append_column(column)
        column.set_cell_data_func(render, comunes.columna_real, MONTO)
        column.set_sort_column_id(MONTO)
        render = gtk.CellRendererText()
        column = gtk.TreeViewColumn('Comprobante', render, text=COMPROBANTE)
        self.treeNomina.append_column(column)
        column.set_cell_data_func(render, comunes.columna_numerica, COMPROBANTE)
        column.set_sort_column_id(COMPROBANTE)
        render = gtk.CellRendererText()
        column = gtk.TreeViewColumn('Cuenta', render, text=CUENTA)
        self.treeNomina.append_column(column)
        column.set_cell_data_func(render, comunes.columna_utf8, CUENTA)
        column.set_sort_column_id(CUENTA)

    def crea_modelo_nomina(self):
        """Crea el modelo de lista para el treeview de los fondos fijos pendientes."""
        self.modelo_nomina = gtk.ListStore(bool, str, str, str,
                                            str, str, str, str, str, str, str, str, str, str)
        self.treeNomina.set_model(self.modelo_nomina)

    def crea_modelo_cff(self):
        """Crea el modelo de lista para el treeview de cuentas de fondo fijo."""
        self.modelo_cff = gtk.ListStore(str, str, str)
        self.treeCuentaFF.set_model(self.modelo_cff)

    def carga_cff(self):
        """Carga las cuentas de fondo fijo en el diálogo de preferencias"""
        try:
            cursor = self.cnx.cursor()
            cursor.execute("""SELECT
                                c.num_cuenta,
                                c.descripcion_cuenta,
                                c.cod_empresa
                              FROM ctb.cuenta_contable c
                              NATURAL JOIN nomina.cuenta_fondo_fijo f
                              ORDER BY c.descripcion_cuenta
                                """ )
            records = cursor.fetchall()
            for record in records:
                self.modelo_cff.append(record)
        except:
            print comunes.sys.exc_info()[1]
            return

    def carga_ff(self):
        """Carga los items de fondos fijos pendientes de proceso."""
        try:
            cursor = self.cnx.cursor()
            cursor.execute("""SELECT
                                0,
                                f.nombre,
                                d.rut,
                                d.folio_documento,
                                d.fecha_doc,
                                d.monto_haber,
                                d.cod_empresa,
                                c.cod_comprobante,
                                cc.descripcion_cuenta,
                                d.num_cuenta,
                                d.cod_doc_contable,
                                d.num_linea,
                                f.numero_cuenta,
                                b.cod_sbif
                              FROM ctb.ficha f
                              INNER JOIN ctb.detalle_comprobante d
                              ON f.rut = d.rut
                              INNER JOIN nomina.cuenta_fondo_fijo ff
                              ON d.num_cuenta = ff.num_cuenta
                              INNER JOIN ctb.cuenta_contable cc
                              ON cc.num_cuenta = d.num_cuenta
                              AND cc.cod_empresa = d.cod_empresa
                              INNER JOIN ctb.comprobante c
                              ON c.cod_comprobante = d.cod_comprobante
                              LEFT JOIN nomina.proceso p
                              ON d.num_linea = p.linea_a
                              AND d.cod_comprobante = p.cod_a
                              LEFT JOIN ctb.banco b
                              ON f.cod_banco = b.cod_banco
                              WHERE p.linea_a is null
                              AND ff.cod_empresa = %s
                              AND d.monto_haber > 0
                              AND c.cod_tipo_comprobante = 'T'
                                """ % self.cod_empresa)
            records = cursor.fetchall()
            self.elements = 0
            self.todos = False
            for record in records:
                self.modelo_nomina.append(record)
        except:
            print comunes.sys.exc_info()[1]
            return

    def on_btnPreferencias_clicked(self, btn=None, data=None):
        """Muestra el diálogo de preferencias"""
        self.crea_modelo_cff()
        self.carga_cff()
        self.entCuentaFF.set_text("")
        cursor = self.cnx.cursor()
        cursor.execute("""SELECT e.num_cuenta,
                                c.descripcion_cuenta,
                                e.cod_banco,
                                b.nombre_banco
                        FROM nomina.cuenta_egreso e
                        INNER JOIN ctb.cuenta_contable c
                        ON e.num_cuenta = c.num_cuenta
                        LEFT JOIN ctb.banco b
                        ON e.cod_banco = b.cod_banco""")
        record = cursor.fetchall()
        if len(record)>0:
            self.num_cuenta_egreso = record[0][0]
            self.EntCuentaEgreso.set_text(record[0][1])
            self.cod_banco_egreso = record[0][2]
            self.entBancoEgreso.set_text(record[0][3])
        else:
            self.num_cuenta_egreso = None
            self.EntCuentaEgreso.set_text("")
            self.cod_banco_egreso = None
            self.entBancoEgreso.set_text("")
        self.entCuentaFF.grab_focus()
        self.dlgPreferencias.show()

    def on_btnAnadirCFF_clicked(self, btn=None, data=None):
        """Añade una cuenta de fondo fijo"""
        if self.num_cuenta is None:
            return
        try:
            cursor = self.cnx.cursor()
            if cursor.execute("""INSERT INTO nomina.cuenta_fondo_fijo
                            VALUES ('%s', %s)""" % (self.num_cuenta,
                                            self.cod_empresa)):
                self.modelo_cff.append((self.num_cuenta,
                    self.entCuentaFF.get_text(),
                    self.cod_empresa))
                self.entCuentaFF.set_text("")
                self.num_cuenta = None
                self.entCuentaFF.grab_focus()
        except:
            print sys.exc_info()[1]
            return

    def on_btnQuitarCFF_clicked(self, btn=None, data=None):
        """Elimina una cuenta de fondo fijo"""
        model, iter = self.treeCuentaFF.get_selection().get_selected()
        if not iter is None:
            row = model.get_path(iter)
            try:
                cursor = cnx.cursor()
                if cursor.execute("""DELETE FROM nomina.cuenta_fondo_fijo
                                WHERE num_cuenta = '%s'
                                AND cod_empresa = %s""" % (model[row][NUMERO_CFF],
                                                    self.cod_empresa)):
                    model.remove(iter)
            except:
                print sys.exc_info()[1]
                return

    def on_btnCerrarPreferencias_clicked(self, btn=None, data=None):
        self.dlgPreferencias.hide()

    def on_btnGenerar_clicked(self, btn=None, data=None):
        if self.elements <= 0:
            return
        if self.desc_cuenta_egreso is None:
            self.entCuentaBanco.set_text("")
            self.num_cuenta_banco = ""
        else:
            self.entCuentaBanco.set_text(self.desc_cuenta_egreso)
            self.num_cuenta_banco = self.num_cuenta_egreso
        self.spnComprobante.set_value(0)
        color = gtk.gdk.color_parse("#FFFFFF")
        self.spnComprobante.modify_base(gtk.STATE_NORMAL, color)
        self.entFichero.set_text("nomina.txt")
        self.spnComprobante.grab_focus()
        self.entFechaComprobante.set_text(comunes.ND().formatESCentury())
        self.entFechaNomina.set_text(comunes.ND().formatESCentury())
        self.dlgGenerar.show()

    def on_btnAceptar_clicked(self, btn=None, data=None):
        if self.entFichero.get_text().strip() == "":
            return
        if self.entFechaComprobante.get_text().strip() == "":
            return
        if self.entFechaNomina.get_text().strip() == "":
            return
        glosa = "FONDOS FIJOS"
        mes = self.entFechaComprobante.get_text()[3:5]
        periodo = self.entFechaComprobante.get_text()[6:]
        cursor = self.cnx.cursor()
        cursor.execute("""SELECT cod_comprobante
                        FROM ctb.comprobante WHERE folio_comprobante = %s
                        AND periodo = %s
                        AND mes = %s """ % (self.spnComprobante.get_value(),
                            periodo,
                            mes))
        records = cursor.fetchall()
        if len(records)>0:
            color = gtk.gdk.color_parse("#FF6B6B")
            self.spnComprobante.modify_base(gtk.STATE_NORMAL, color)
            return
        else:
            color = gtk.gdk.color_parse("#FF6B6B")
            self.spnComprobante.modify_base(gtk.STATE_NORMAL, color)
        try:
            cursor.execute("begin")
            #Cabecera del comprobante
            sql = """INSERT INTO ctb.comprobante
                        (cod_tipo_comprobante,
                         periodo,
                         mes,
                         fecha,
                         glosa,
                         cod_empresa,
                         folio_comprobante
                         )
                         VALUES
                         ('E',
                         %s,
                         %s,
                         '%s',
                         '%s',
                         %s,
                         %s)""" % (periodo,
                             mes,
                             comunes.CDateDB(self.entFechaComprobante.get_text()),
                             glosa,
                             self.cod_empresa,
                             self.spnComprobante.get_value())
            cursor.execute(sql)
            #Código del nuevo comprobante
            cursor.execute("""SELECT cod_comprobante
                        FROM ctb.comprobante WHERE folio_comprobante = %s
                        AND periodo = %s
                        AND mes = %s """ % (self.spnComprobante.get_value(),
                            periodo,
                            mes))
            records = cursor.fetchall()
            codigo = records[0][0]
            #Registro de nomina
            cursor.execute("""INSERT INTO nomina.nomina (descripcion_nomina, fecha)
                            VALUES ('Nomina generada desde comprobante %s', '%s')
                            """ % (self.spnComprobante.get_value(),
                            comunes.CDateDB(self.entFechaComprobante.get_text())))
            cursor.execute("""select currval('nomina.seq_cod_nomina')""")
            records = cursor.fetchall()
            cod_nomina = records[0][0]
            iter = self.modelo_nomina.get_iter_first()
            linea = 1
            total = 0.0
            listado = []
            #Bucle para insertar lineas en el detalle del comprobante
            while not iter is None:
                if self.modelo_nomina.get_value(iter, CHECK_NOMINA):
                    row = self.modelo_nomina.get_path(iter)
                    fecha = self.modelo_nomina[row][FECHA][:10]
                    #Inserción de registros al Debe
                    sql = """INSERT INTO ctb.detalle_comprobante VALUES
                        (%s,
                        '%s',
                        %s,
                        NULL,
                        %s,
                        0,
                        '%s',
                        %s,
                        '%s',
                        %s,
                        '%s',
                        %s) """ % (codigo,
                            self.modelo_nomina[row][NUM_CUENTA],
                            self.cod_empresa,
                            self.modelo_nomina[row][MONTO],
                            'EGRESO',
                            self.modelo_nomina[row][FOLIO],
                            fecha,
                            self.modelo_nomina[row][TIPO_DOC],
                            self.modelo_nomina[row][RUT],
                            linea)
                    cursor.execute(sql)
                    #Inserción de la linea procesada en la tabla
                    #nomina.proceso
                    cursor.execute("""INSERT INTO nomina.proceso
                                        VALUES (%s, %s, %s, %s)
                                        """ % (self.modelo_nomina[row][LINEA],
                                                self.modelo_nomina[row][COMPROBANTE],
                                                linea,
                                                codigo))
                    total += float(self.modelo_nomina[row][MONTO])
                    linea += 1
                    print self.modelo_nomina[row][CTA_CTE], self.modelo_nomina[row][BANCO]
                    if self.modelo_nomina[row][CTA_CTE] is None:
                        medio_pago = "12"
                        cta_cte = " "
                        banco = "001"
                    else:
                        if self.modelo_nomina[row][BANCO] == "001":
                            medio_pago = "01"
                        else:
                            medio_pago = "07"
                        cta_cte = self.modelo_nomina[row][CTA_CTE]
                        banco = self.modelo_nomina[row][BANCO]

                    listado.append([self.rut_empresa[:-2].replace('.', ''),
                                    self.rut_empresa[-1],
                                    '002',
                                    cod_nomina,
                                    'NOMINA %s' % cod_nomina,
                                    self.entFechaNomina.get_text(),
                                    self.modelo_nomina[row][MONTO],
                                    '2201',
                                    self.modelo_nomina[row][RUT],
                                    self.modelo_nomina[row][NOMBRE],
                                    self.modelo_nomina[row][MONTO],
                                    'NOMINA DE FONDOS FIJOS',
                                    self.modelo_nomina[row][FOLIO],
                                    medio_pago,
                                    cta_cte,
                                    banco])
                iter = self.modelo_nomina.iter_next(iter)
            #Inserción de registro al haber
            cursor.execute("""INSERT INTO ctb.detalle_comprobante VALUES
                        (%s,
                        '%s',
                        %s,
                        NULL,
                        0,
                        %s,
                        '%s',
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        %s)""" % (codigo,
                            self.num_cuenta_banco,
                            self.cod_empresa,
                            str(total),
                            'EGRESO',
                            linea))
            #Actualización de la cabecera
            cursor.execute("""UPDATE ctb.comprobante
                                SET debe = %s, haber = %s
                                WHERE cod_comprobante = %s """ % (total, total, codigo))
            for i in listado:
                i[nomina.MONTO_PAGO] = total
            nomina.GeneraNomina(listado, self.entFichero.get_text().strip())
            cursor.execute("commit")
            self.crea_modelo_nomina()
            self.carga_ff()
        except:
            cursor.execute("rollback")
            print comunes.sys.exc_info()[1]
        self.dlgGenerar.hide()

    def on_btnCancelar_clicked(self, btn=None, data=None):
        self.dlgGenerar.hide()

    def on_btnGuardarComo_clicked(self, btn=None, data=None):
        self.dlgFichero.show()

    def on_btnCancelaFichero_clicked(self, btn=None, data=None):
        self.dlgFichero.hide()

    def on_btnGuardaFichero_clicked(self, btn=None, data=None):
        if not self.dlgFichero.get_filename() is None:
            self.entFichero.set_text(self.dlgFichero.get_filename())
            self.dlgFichero.hide()

    def on_btnFecha_clicked(self, btn = None, widget=None):
        if btn.name == "btnFechaComprobante":
            widget = self.entFechaComprobante
        elif btn.name == "btnFechaNomina":
            widget = self.entFechaNomina
        else:
            return
        fecha = comunes.dlgCalendario(None, widget.get_text())
        widget.set_text(fecha.date)

if __name__ == '__main__':
    cnx = psycopg.connect("host=192.168.0.11 dbname=g-2002 user=fernando password=1020")
    cnx.autocommit()
    v = wnNomina(cnx, e=1)
    v.wnNomina.show()
    v.wnNomina.maximize()
    sys.excepthook = debugwindow.show
    gtk.main()