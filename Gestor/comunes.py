#!/usr/bin/env python
# -*- coding: utf-8 -*-
# comunes -- Libreería de objetos y constantes comunes para todo el proyecto
# (c) Fernando San Martín Woerner 2003, 2004, 2005
# snmartin@galilea.cl


from GladeConnect import GladeConnect
import sys
import string, re
import gobject
import gtk
import time
import os
from types import StringType
import math
from pyBusqueda import *
from dialogos import *
from fechas import ND, CDateLocal, CDateDB
from completion import *
from codificacion import *
from constantes import *
from spg import connect
import locale
from verifica_permiso import VerificaPermiso
from ctb_rutinas import es_rut,  CMon,  CNumDb, Decimal,  moneyfmt

if sys.platform=="win32":
    impresora = 'lpt1:'
else:
    impresora = 'lpr:'
            

def columna_numerica(tree, cell, model, iter, data = 0):
    pyobj = model.get_value(iter, data)
    pyobj = str(pyobj)
    cell.set_property('text', CMon((pyobj.replace(".", ",")), 0))
    cell.set_property('xalign', 1)

def columna_real(tree, cell, model, iter, data = 0):
    pyobj = model.get_value(iter, data)
    if not  pyobj is None:
        cell.set_property('text', CMon(pyobj.replace(".", ","), 2))
        cell.set_property('xalign', 1)
    else:
        cell.set_property('text', CMon(pyobj, 2))
        cell.set_property('xalign', 1)


def columna_rut(tree, cell, model, iter, data = 0):
    pyobj = model.get_value(iter, data)
    cell.set_property('text', CRut(pyobj))
    cell.set_property('xalign', 1)

def columna_fecha(tree, cell, model, iter, data = 0):

    pyobj = model.get_value(iter, data)
    if pyobj is None:
        cell.set_property('text', "Sin Fecha")
        cell.set_property('xalign', 1)
    else:
        cell.set_property('text', CDateLocal(pyobj))
        cell.set_property('xalign', 1)

def columna_utf8(tree, cell, model, iter, data = 0):
    pyobj = model.get_value(iter, data)
    try:
        cell.set_property('text', CUTF8(pyobj))
    except:
        cell.set_property('text', CUTF8(pyobj, 'latin1'))

def columna_vacia(tree, cell, model, iter, data = ''):
    cell.set_property('text', ' ')
    cell.set_property('xalign', 1)

def CRut(rut):
    if rut == "":
        return rut
    rut = string.replace(rut,".","")
    rut = string.replace(rut,"-","")
    rut = "0000000000"+ rut
    l = len(rut)
    rut_aux = "-" + rut[l-1:l]
    l = l-1
    while 2 < l:
        rut_aux = "."+ rut[l-3:l] +rut_aux
        l = l-3
    rut_aux = rut[0:l] +rut_aux
    l = len(rut_aux)
    rut_aux = rut_aux[l-12:l]
    return rut_aux


def end_match(completion=None, key=None, iter=None, column=None):
    model = completion.get_model()
    text = model.get_value(iter, column)
    key = unicode(key,'latin1').encode('utf-8')
    if unicode(text,'latin1').encode('utf-8').upper().find(key.upper()) <> -1:
        return True
    return False


def Abre_pdf(arch):
    if sys.platform=="win32":
        os.startfile(arch)
        return
        acrord = 'c:\\Archivos de programa\\Adobe\\Acrobat 5.0\\Reader\\AcroRd32.exe'

        #~ acrord = "explorer.exe"
        #~ acrord = os.getcwd() + "\\pdfreader\\pdfreader.exe"

        args = [acrord, "AcroRd32.exe",arch]

        try:
            os.spawnv(os.P_NOWAIT, args[0], args[1:])
            print "acro"
        except:
            acrord = os.getcwd() + "\\pdfreader\\pdfreader.exe"
            args = [acrord, "pdfreader.exe",arch]
            try:
                os.spawnv(os.P_NOWAIT, args[0], args[1:])
                print "PDFReader"
            except:
                print "no hay un visor registrado."
    else:
        if os.spawnv(os.P_NOWAIT, '/usr/bin/evince', ['evince', arch]) != 0 :
            return 
        if os.spawnv(os.P_NOWAIT, '/usr/bin/xpdf', ['xpdf', arch]) != 0 :
            return
        if os.spawnv(os.P_NOWAIT, '/usr/bin/acroread', ['acroread', arch]) != 0 :
            return
        if os.spawnv(os.P_NOWAIT, '/usr/bin/gpdf', ['gpdf', arch]) != 0 :
            return

def Abre_excel(arch):
    if sys.platform=="win32":
        os.startfile(arch)
    else:
        try:
            os.system("gnome-open " + arch) # Para Gnome
        except:
            if os.spawnv(os.P_WAIT, '/usr/bin/libreoffice', ['libreoffice', arch]) == 0:
                return
            if os.spawnv(os.P_WAIT, '/usr/bin/openoffice', ['openoffice', arch]) == 0:
                return
            if os.spawnv(os.P_WAIT, '/usr/bin/gnumeric', ['gnumeric', arch]) == 0:
                return        

def abreTxt(arch):
    if sys.platform=="win32":
        os.startfile(arch)
    else:
        try:
            os.system("gnome-open " + arch) # Para Gnome
        except:
            if os.spawnv(os.P_WAIT, '/usr/bin/mousepad', ['mousepad', arch]) == 0:
                return
            if os.spawnv(os.P_WAIT, '/usr/bin/geany', ['geany', arch]) == 0:
                return
            
def IsNull(valor):
    if valor == None:
        return True
    else:
        return False
    
def Numlet(tyCantidad):
    _ret = None
    tyCantidad = round(tyCantidad)
    lyCantidad = int(tyCantidad)
    lyCentavos = ( tyCantidad - lyCantidad )  * 100
    laUnidades = ('UNA', 'DOS', 'TRES', 'CUATRO', 'CINCO', 'SEIS', 'SIETE', 'OCHO', 'NUEVE', 'DIEZ', 'ONCE', 'DOCE', 'TRECE', 'CATORCE', 'QUINCE', 'DIESISEIS', 'DIESISIETE', 'DIESIOCHO', 'DIESINUEVE', 'VEINTE', 'VEINTIUN', 'VEINTIDOS', 'VEINTITRES', 'VEINTICUATRO', 'VEINTICINCO', 'VEINTISEIS', 'VEINTISIETE', 'VEINTIOCHO', 'VEINTINUEVE')
    laDecenas = ('DIEZ', 'VEINTE', 'TREINTA', 'CUARENTA', 'CINCUENTA', 'SESENTA', 'SETENTA', 'OCHENTA', 'NOVENTA')
    laCentenas =('CIENTO', 'DOSCIENTOS', 'TRESCIENTOS', 'CUATROCIENTOS', 'QUINIENTOS', 'SEISCIENTOS', 'SETECIENTOS', 'OCHOCIENTOS', 'NOVECIENTOS')
    lnNumeroBloques = 1
    while 1:
        lnPrimerDigito = 0
        lnSegundoDigito = 0
        lnTercerDigito = 0
        lcBloque = ''
        lnBloqueCero = 0
        for i in range(1,4):
            lnDigito = lyCantidad % 10
            if lnDigito <> 0:
                _select0 = i
                if (_select0 == 1):
                    lcBloque = ' ' + laUnidades[lnDigito - 1]
                    lnPrimerDigito = lnDigito
                elif (_select0 == 2):
                    if lnDigito <= 2:
                        lcBloque = ' ' + laUnidades[( lnDigito * 10 )  + lnPrimerDigito - 1]
                    else:
                        lcBloque = ' ' + laDecenas[lnDigito - 1] + lcBloque
                    lnSegundoDigito = lnDigito
                elif (_select0 == 3):
                    if lnDigito == 1 and lnPrimerDigito == 0 and lnSegundoDigito == 0:
                        lcBloque = ' ' + 'CIEN'+ lcBloque
                    else:
                        lcBloque = ' ' + laCentenas[lnDigito - 1] + lcBloque
                    lnTercerDigito = lnDigito
            else:
                lnBloqueCero = lnBloqueCero + 1
            lyCantidad = int(lyCantidad / 10)
            if lyCantidad == 0:
                break
        _select1 = lnNumeroBloques
        if (_select1 == 1):
            _ret = lcBloque
        elif (_select1 == 2):
            _ret = lcBloque + IIf(lnBloqueCero == 3, '', ' MIL') + _ret
        elif (_select1 == 3):
            _ret = lcBloque + IIf(lnPrimerDigito == 1 and lnSegundoDigito == 0 and lnTercerDigito == 0, ' MILLON', ' MILLONES') + _ret
        lnNumeroBloques = lnNumeroBloques + 1
        if lyCantidad == 0:
            break
    _ret = _ret + IIf(tyCantidad > 1, ' PESOS ', ' PESO ') #+ Format(Str(lyCentavos), '00') + '/100 M.N. )'
    return _ret

def iif(cond, si, no):
    if cond:
        return si
    else:
        return no


def isNull(valor):
    if valor == None:
        return True
    else:
        return False

def ADer(pal,largo):
    try:
        pal = str(pal)
    except:
        pal = pal
    return pal.rjust(largo)

def AIzq(pal,largo):
    try:
        pal = str(pal)
    except:
        pal = pal
    return pal.ljust(largo)

def Ceros(pal, largo):
    try:
        pal = str(pal)
    except:
        pal = pal
    return pal.zfill(largo)

def Mid(pal, desde, hasta = 0):
    if hasta == 0:
        return pal[desde:]
    else:
        return pal[desde:hasta + desde]

def UCase(pal):
    return pal.upper()
Long = long
def long(arg):
    return float(arg)
if __name__ == '__main__':

    print CMon("100000.0".replace(".",","))
    #Abre_pdf("comprobante_ingreso.pdf")
