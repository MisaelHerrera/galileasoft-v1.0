#!/usr/bin/env python
# -*- coding: utf-8 -*-
# wnComprobante -- formulario de ingreso, modificacion, eliminacion de comprobantes 
# contables
# (C)   Fernando San Martín Woerner 2006
#       fernando@gnome.cl
#       Ricardo Mendez L.
#       rmendez@galilea.cl
#This file is part of pyGestor.

# (C)   José Luis Álvarez Morales    2006, 2007.
#           josealvarezm@gmail.com

import gtk
from GladeConnect import GladeConnect
from gobject import *
from spg import connect
from fechas import CDateDB,CDateLocal
import sys
import os
import types
import xlrd
import xlwt
from string import *
from time import *
from comunes import iif
##from ctb_busqueda import *
import sys, string, os
import datetime
from comunes import CMon,CRut,CNumDb,  VerificaPermiso
import completion
import SimpleTree
from ModelProxy import ListStoreProxy
from kiwi.ui.dateentry import DateEntry
from kiwi.ui.entry import KiwiEntry
from ImpresionComprobante import imprimir_comprobante,imprimir_detalle_comprobante,imprimir_detalle_comprobante_masivo
import  dialogos 
from strSQL import strSelectTipoAnalisisCuentaContable
from strSQL import strSelectTipoComprobante
from strSQL import strSelectSistema
from strSQL import strInsertCabeceraComprobanteNull
from strSQL import strSelectEmpresa
from strSQL import strInsertCabeceraComprobanteN
from strSQL import strInsertDetalleComprobanteAD
from strSQL import strInsertDetalleComprobanteAS
from strSQL import strInsertDetalleComprobanteSA
from strSQL import strInsertDetalleComprobanteSSA
from strSQL import strSelectFolio
from strSQL import strSelectComprobanteM
from strSQL import strSelectPeriodo
from strSQL import strFicha
from strSQL import strCargaDatos
from strSQL import strCargaComprobante
from strSQL import strDeleteCabeceraComprobante
from strSQL import strDeleteDetalleComprobante
from strSQL import strAnular
from strSQL import strSelectEmpresaPeriodo
from strSQL import strSelectCuentaEmpresa
from strSQL import strCentralizadoComprobante 
from strSQL import strSelectMaxFolio
from strSQL import strMaxPeriodo
from strSQL import strCuentaContable
from strSQL import strSelectEmpresaNivel
from strSQL import strSelectDocumento
from strSQL import strNivelEmpresaCuentaContable
from strSQL import strVerificarPeriodoContable
from strSQL import strCabeceraExportar
from spg import connect
from comunes import Abre_excel

import ifd
#Para poder imprimir en documento pdf
from Documento import PdfComprobanteContable
from codificacion import CISO
from reportlab.pdfbase.ttfonts import *
from reportlab.pdfgen.canvas import Canvas

import datetime

(NUM_LINEA,
 NUM_CUENTA,
 DESCRIPCION_CUENTA,
 MONTO_DEBE,
 MONTO_HABER,
 GLOSA,
 SERIE,
 FOLIO_ANALISIS,
 FECHA_ANALISIS,
 TIPO_ANALISIS,
 DOCUMENTO_ANALISIS,
 RUT_ANALISIS,
 NOMBRE_ANALISIS,
 COD_TIPO_DOCUMENTO,
 COD_CC,
 CENTRO_COSTO,
 SUCURSAL,
 AREA_FUNCIONAL,
 COD_SUCURSAL,
 COD_AREA_FUNCIONAL) = range(20)

class wnComprobante(GladeConnect):
    def __init__(self, conexion=None, padre=None, empresa=1, root="wnComprobante"):
        GladeConnect.__init__(self, "glade/wnComprobante.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        
        self.cod_empresa = empresa
        self.periodo = None
        self.mes = None
        
        self.cod_compr=None
        
        self.f=[]
        
        if padre is None:
            self.wnComprobante.maximize()
            self.frm_padre = self.wnComprobante
            self.padre
        else:
            self.frm_padre = padre.frm_padre
            self.ventana = self.frm_padre
            
        self.pecTipoComprobante = completion.CompletionTipoComprobante(self.entTipoComprobante,
                self.sel_tipo_comprobante,
                self.cnx, self.cod_empresa)
        
        self.pecTipoComprobante1 = completion.CompletionTipoComprobante(self.txtTipoComprobante,
                self.sel_tipo_comprobante,
                self.cnx, self.cod_empresa)
        
        self.pecPeriodo = completion.CompletionPeriodoContable(self.entPeriodo,
                self.sel_periodo,
                self.cnx, self.cod_empresa)
               
        self.crea_modelo_cuenta()
        self.crea_modelo_ficha()
        self.pecPeriodo.completion.get_popup_completion()
              
        self.crea_modelo()
        self.cod_tipo_comprobante = None
        self.wins = {}

    def crea_modelo_cuenta(self):
        self.modelo_cuenta =gtk.ListStore(str,str)
        sql="""SELECT
                        descripcion_cuenta,
                        num_cuenta
                FROM
                        ctb.cuenta_contable   join ctb.empresa e using(cod_empresa)
                WHERE
                    cod_empresa = %s and ctb.cuenta_contable.nivel_cuenta=e.nivel_cuenta and estado_cuenta='V'
                ORDER BY
                        descripcion_cuenta""" % self.cod_empresa
        self.cursor.execute(sql)
        r= self.cursor.fetchall()
        for i in r:
            self.modelo_cuenta.append(i)
   
    def crea_modelo_ficha(self):
        self.modelo_ficha =gtk.ListStore(str,str)
        sql = """SELECT
                        nombre,
                        rut
                FROM
                        ctb.ficha
                WHERE
                    cod_empresa = %s
                ORDER BY
                        nombre""" % self.cod_empresa
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        for i in r:
            self.modelo_ficha.append(i)
  
    def crea_modelo(self):
        columnas=[]
        columnas.append([0,"OK","bool",self.fixed_toggled_ddp])
        columnas.append([1,"N° Comprobante","str"])
        columnas.append([2,"Folio","str"])
        columnas.append([3,"Fecha Comprobante","dte"])
        columnas.append([4,"Tipo Comprobante","str"])
        columnas.append([5,"Empresa","str"])
        columnas.append([6,"Origen","str"])
        columnas.append([7,"Estado","str"])
        columnas.append([8,"Periodo","str"])
        columnas.append([9,"Glosa","str"])
        columnas.append([10,"Total Debe","int"])
        columnas.append([11,"Total Haber","int"])
        
        self.modelo = gtk.ListStore(bool,
                                    str,
                                    str,
                                    str,
                                    str,
                                    str,
                                    str,
                                    str,
                                    str,
                                    str,
                                    str,
                                    str,
                                    str,
                                    str,
                                    str,
                                    str,
                                    str,
                                    str)
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeComprobante)
    
    def fixed_toggled_ddp(self, cell, path, model):
        model = self.treeComprobante.get_model()
        iter = model.get_iter((int(path),))
        fixed = model.get_value(iter, 0)
        #mon = model.get_value(iter, 3)
        #ret = model.get_value(iter, 4)
                                
        fixed = not fixed
        if fixed:
            print "marcado"
            self.f.append(model.get_value(iter,1))
        else:
            print "desmarcado"
            self.f.remove(model.get_value(iter,1))
 
        model.set(iter, 0, fixed)   
    
    def carga_datos(self,args=None):
        print strCargaDatos
        sqlWhere=''
        if self.txtGlosa.get_text() not in('',None):
            desc = self.txtGlosa.get_text()
            sqlWhere=" and c.glosa ilike '%" + desc + "%'"
        
        if (self.txtDocBodega.get_text() not in('',None)):
            doc_bod = self.txtDocBodega.get_text()
            sqlWhere=" and c.cod_comprobante in (select distinct cod_comprobante from ctb.detalle_comprobante where serie ilike '%" + doc_bod + "%')"
            
        if self.optComprobante.get_active():
            if self.entCodigoComprobante.get_text() not in('',None) and self.entCodigoComprobante.get_text().isdigit()==True:
                sqlWhere+=" and c.cod_comprobante=%s"%int(self.entCodigoComprobante.get_text())
            #else:
            #    dialogos.dlgAviso(None,"Debe ingresar un N° de Comprobante")
            #    return
        elif self.optTipoComprobante.get_active():           
            if self.cod_tipo_comprobante not in('',None):
                sqlWhere+=" and c.cod_tipo_comprobante='%s'"%(self.cod_tipo_comprobante)
                if self.entFechaDesde.get_text() not in ('',None) and self.entFechaHasta.get_text() not in('',None):
                    sqlWhere+=" and c.fecha between '%s' and '%s'" %(CDateDB(self.entFechaDesde.get_text()),CDateDB(self.entFechaHasta.get_text()))
        elif self.optPeriodo.get_active():
            if self.mes  not in('',None) and self.periodo not in('',None):
                sqlWhere+=" and c.mes=%s and c.periodo=%s"%(int(self.mes),int(self.periodo))
            if self.entFolio.get_text() not in ('',None) and self.entFolio.get_text().isdigit():
               sqlWhere+="  and c.folio_comprobante=%s"%int(self.entFolio.get_text())
            if self.entTipoComprobante.get_text() not in('',None) and self.pecTipoComprobante.get_selected()==True:
                sqlWhere+= " and c.cod_tipo_comprobante='%s'" %(self.cod_tipo_comprobante)
        
        print sqlWhere        
        try:            
            if sqlWhere=='':    
                return
            sql=strCargaDatos %(self.cod_empresa,sqlWhere)
            print sql
            self.cursor.execute(sql)
            r=self.cursor.fetchall()
            self.modelo.clear()
            if len(r)==0:
                return
            
            for i in r:
                self.modelo.append(i)            
        except:
            dialogos.error('Ha ocurrido un error al cargar los datos del comprobante contable.',str(sys.exc_info()[1]))
    
    def sel_tipo_comprobante(self, completion, model, iter):
        self.cod_tipo_comprobante = model.get_value(iter, 1)
        self.entTipoComprobante.set_text(model.get_value(iter, 0))
   
    def sel_periodo(self, completion, model, iter):
        self.entPeriodo.set_text(model.get_value(iter,0))
        self.mes = model.get_value(iter, 1)
        self.periodo = model.get_value(iter, 2)
        self.cod_empresa=model.get_value(iter,3)
          
    def add_tab(self, widget, label):
        p = -1
        if not self.wins.has_key(label):
            l = gtk.Label('')
            l.set_text_with_mnemonic(label)
            self.ntbComprobante.append_page(widget, l)
            widget.show()
            self.wins[label] = (widget, len(self.wins))
        p = len(self.wins) - 1

    def remove_tab(self, label):
        self.ntbComprobante.remove(self.wins[label][0])
        del self.wins[label]
    
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgComprobante(self.cnx, self.wins,self.cod_empresa,None,self.modelo_cuenta,self.modelo_ficha,self)        
        band = False
        page=self.ntbComprobante.get_n_pages()
        j=0
        if self.wins.has_key("Comprobante Contable %s"%(page)):            
            for i in self.wins:
                label="Comprobante Contable %s"%page
                if self.wins.has_key(label):
                    page+=1
        self.add_tab(dlg.vboxDetalleComprobante, "Comprobante Contable %s"%(page))
        self.ntbComprobante.set_current_page(self.ntbComprobante.get_n_pages()-1)
    
    def on_btnPropiedades_clicked(self, btn=None):                
        self.on_treeComprobante_row_activated(None)      

    def on_btnImportar_clicked(self, btn=None):                
        print('importar')
        
        archivo =  dialogos.dlgAbrirArchivo("Selecccione el archivo a importar", self.ventana, ("*.xls","*.XLS"))
        
        if archivo == None:
            return
        
        #falta validar archivo xls
        book = xlrd.open_workbook(archivo)
        print(book.sheet_names()[0])
        sheet = book.sheet_by_name(book.sheet_names()[0])
                    
        print(sheet.cell(0, 0).value)
        cod_empresa = sheet.cell(0, 0).value
        
        print(sheet.cell(0, 1).value)
        tipo_comprobante = sheet.cell(0, 1).value
        
        print(sheet.cell(0, 2).value)
        periodo = sheet.cell(0, 2).value
        
        print(sheet.cell(0, 3).value)
        mes = sheet.cell(0, 3).value
        
        cod_sistema = "CON"
        
        print(sheet.cell(0, 4).value)
        fecha= int(sheet.cell(0, 4).value)
        print "fecha"
        print fecha
      
        ###LA FECHA OBTENIDA EN EXCEL ESTA EN UN FORMATO DISTINTO,CON ESTOS PROCEDIMIENTOS SE CONVIERTE A FECHA
        seconds = (fecha - 25569) * 86400.0
        fecha2=datetime.datetime.fromtimestamp(seconds).date()
        
        print "fecha2"
        print fecha2
        
        print(sheet.cell(0, 5).value)
        debe = float(sheet.cell(0, 5).value)
        
        #print(debe)
        
        print(sheet.cell(0, 6).value)
        haber = float(sheet.cell(0, 6).value)
        
        #print(haber)
        print(sheet.cell(0,7).value)
        glosa = str(sheet.cell(0, 7).value)
        
        print "cabecera"
        
        sql_consulta=strVerificarPeriodoContable%(periodo,mes)
        self.cursor.execute(sql_consulta)
        print sql_consulta
        r_consulta=self.cursor.fetchall()

        
        if len(r_consulta)<=0:
            dialogos.dlgAviso(None,"El Periodo Contable del Comprobante a importar no esta creado.\nPor favor contacte al Dpto de Contabilidad.")
            return
        else:
            print str(r_consulta[0][0])
            if r_consulta[0][0]=='C':
                dialogos.dlgAviso(None,"El Periodo Contable del Comprobante a importar esta cerrado.\nPor favor contacte al Dpto de Contabilidad.")
                return
        
        try:
            progreso = dialogos.dlgBarraProgreso("Importando comprobante...", "Progreso", 0)
                        
            self.cursor.execute("begin")
        
            sqlWhere="where c.cod_empresa=%s and c.periodo=%s and c.mes=%s and c.cod_tipo_comprobante='%s'"%(cod_empresa,periodo,mes,tipo_comprobante)
            sql=strSelectMaxFolio %sqlWhere
            self.cursor.execute(sql)
            print sql
            r=self.cursor.fetchall()
            
            folio = iif(r[0][0] not in('',None),str(r[0][0]),1)
            print "folio"+" "+ str(folio)
            
            estado = "V"
    
            #se crea el comprobante
            sql= strInsertCabeceraComprobanteN % (tipo_comprobante,
                                                int(cod_empresa),
                                                int(periodo),
                                                int(mes),
                                                cod_sistema,
                                                fecha2,
                                                float(debe),
                                                float(haber),
                                                str(glosa),
                                                long(folio),
                                                estado,9)
                                                                        
            print(sql.upper())
            self.cursor.execute(sql.upper())
            
            sql = strSelectComprobanteM %(tipo_comprobante,long(folio),int(periodo),int(mes),int(cod_empresa))
            print sql
            self.cursor.execute(sql.upper())
            r = self.cursor.fetchall()
            
            cod_comprobante = r[0][0]
            print cod_comprobante
            #dialogos.error(str(sheet.nrows))
            cantidad_asientos = int(sheet.nrows) - 1
            max = float(cantidad_asientos)
            cont = 1
                
            for i in range(int(cantidad_asientos)):
                
                # print 're'
                # print type(sheet.cell(i + 1 , 1).value)
                # print type(sheet.cell(i + 1 , 2).value)
                # print type(sheet.cell(i + 1 , 3).value)
                # print type(sheet.cell(i + 1 , 4).value)
                # print type(sheet.cell(i + 1 , 5).value)
                # print type(sheet.cell(i + 1 , 6).value)
                # print type(sheet.cell(i + 1 , 7).value)
                # print type(sheet.cell(i + 1 , 9).value)
                # print type(sheet.cell(i + 1 , 10).value)
                # print type(sheet.cell(i + 1 , 11).value)
                # print sheet.cell(i + 1 , 1).value
                # print sheet.cell(i + 1 , 2).value
                # print sheet.cell(i + 1 , 3).value
                # print sheet.cell(i + 1 , 4).value
                # print sheet.cell(i + 1 , 5).value
                # print sheet.cell(i + 1 , 6).value
                # print sheet.cell(i + 1 , 7).value
                # print sheet.cell(i + 1 , 8).value
                # print sheet.cell(i + 1 , 9).value
                # print sheet.cell(i + 1 , 10).value
                # print sheet.cell(i + 1 , 11).value
                # continue    
                print(type(sheet.cell(i + 1, 1).value))
                num_cuenta = str(sheet.cell(i + 1 , 0).value)
                debe_detalle = float(sheet.cell(i + 1, 1).value)
                haber_detalle = float(sheet.cell(i + 1, 2).value)
                glosa_detalle = str(sheet.cell(i + 1, 3).value)
                valor_serie=int(sheet.cell(i + 1, 4).value)
                serie=str(valor_serie)
                folio=int(sheet.cell(i + 1, 5).value)
                tipo_documento=int(sheet.cell(i + 1, 7).value)
                valor_rut=str(sheet.cell(i + 1, 8).value)
                #print "valor_rut"+" "+str(valor_rut)
                rut=str(valor_rut)
                codigo_cc=int(sheet.cell(i + 1, 9).value)                
                cod_sucursal=sheet.cell(i + 1, 10).value
                cod_area_funcional=sheet.cell(i + 1, 11).value
                print cod_sucursal
                print cod_area_funcional
                
                if type(cod_sucursal) == float:
                    cod_sucursal = int(cod_sucursal)
                
                if type(cod_area_funcional) == float:
                    cod_area_funcional=int(cod_area_funcional)
                    
                vacio = False
                num_linea = i+1                
                
                if cod_sucursal == None and cod_area_funcional == None:
                    vacio = True
                                                    
                print "linea"
                print vacio
                #print glosa_detalle
                            
                if vacio == True or type(cod_sucursal)<>int or type(cod_area_funcional)<>int:
                    sql=strInsertDetalleComprobanteSSA %(int(cod_comprobante),
                                                                        num_cuenta,
                                                                        cod_empresa,
                                                                        debe_detalle,
                                                                        haber_detalle,
                                                                        glosa_detalle,
                                                                        serie,
                                                                        folio,
                                                                        fecha2,
                                                                        tipo_documento,
                                                                        rut,
                                                                        num_linea,codigo_cc)
                else:
                   sql=strInsertDetalleComprobanteAD %(int(cod_comprobante),
                                                                        num_cuenta,
                                                                        cod_empresa,
                                                                        debe_detalle,
                                                                        haber_detalle,
                                                                        glosa_detalle,
                                                                        serie,
                                                                        folio,
                                                                        fecha2,
                                                                        tipo_documento,
                                                                        rut,
                                                                        num_linea,codigo_cc,cod_sucursal,cod_area_funcional)
                print(sql.upper())
                
                progreso.barra.set_fraction(cont / float(max))
                while gtk.events_pending():
                    gtk.mainiteration(gtk.FALSE)
                cont = cont +1
                
                self.cursor.execute(sql.upper())
                
            self.cursor.execute('commit')
            imprimir_detalle_comprobante(cod_comprobante,self.cursor)
            progreso.destroy()
        except:
                self.cursor.execute('rollback')
                self.error=True
                dialogos.error('Ha ocurrido un error al registrar el comprobante.',str(sys.exc_info()[1]))
                return
    
    def on_btnQuitar_clicked(self,btn=None):      
        model,iter=self.treeComprobante.get_selection().get_selected()
        if model is None or iter is None:
            return
        row=model.get_path(iter)
        sql=strCargaComprobante %(int(model[row][1]))        
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        #try:                
        resp=dialogos.dlgSiNo(None,"Esta seguro que desea Eliminar el comprobante?","Eliminar").response
        if resp==gtk.RESPONSE_NO:            
            return                        
            
        sql1=strDeleteDetalleComprobante %(int(model[row][1]),int(self.cod_empresa))
        self.cursor.execute(sql1)
        sql=strDeleteCabeceraComprobante % int(model[row][1])
        self.cursor.execute('begin')
        self.cursor.execute(sql)      
        self.modelo.remove(iter)
        self.cursor.execute('commit')
        dialogos.dlgAviso(None,"El comprobante ha sido eliminado")                                    
        #except:
        #    error(self.padre,"El comprobante no se puede Eliminar")
        #    self.cursor.execute('rollback')            
        return        
    
    def on_btnBuscar_clicked(self, btn=None):
        self.carga_datos()
    
    def on_btnDesde_clicked(self, widget,btn=None):
        self.entFechaDesde.set_sensitive(True)
        fecha = dialogos.dlgCalendario(None,None,self.entFechaDesde)
        self.entFechaDesde.set_text(fecha.date)
        self.entFechaDesde.set_sensitive(False)
    
    def on_btnHasta_clicked(self, widget,btn=None):
        self.entFechaHasta.set_sensitive(True)            
        fecha = dialogos.dlgCalendario(None,self.entFechaHasta.get_text(),self.entFechaHasta)
        self.entFechaHasta.set_text(fecha.date)
        self.entFechaHasta.set_sensitive(False)
        
    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Comprobante")
      
    def on_optPeriodo_toggled(self,widget,*args):
        if widget.get_active():
            self.hbPeriodo.set_sensitive(True)
        else:
            self.hbPeriodo.set_sensitive(False)
    
    def on_optComprobante_toggled(self,widget,*args):
        if widget.get_active():
            self.hbComprobante.set_sensitive(True)
        else:
            self.hbComprobante.set_sensitive(False)
    
    def on_optTipoComprobante_toggled(self,widget,*args):
        if widget.get_active():
            self.hbTipoComprobante.set_sensitive(True)
        else:
            self.hbTipoComprobante.set_sensitive(False)
    
    def on_treeComprobante_row_activated(self, widget,*args):        
        model,iter = self.treeComprobante.get_selection().get_selected()
        if model is None or iter is None:
            return
        row = model.get_path(iter)
        sql = strCargaComprobante %(int(model[row][1]))
        self.cursor.execute(sql)
        #print sql
        r = self.cursor.fetchall()
        
        if len(r) == 0:
            dialogos.dlgAviso(None,"El comprobante no tiene detalle")
            return
        
        if model[row][7] == 'N':         
            dialogos.dlgAviso(None,"El comprobante esta Nulo")            ##desea editarlo de todas formas        
##            return             
        
        band = False

        sql = strCentralizadoComprobante %(int(model[row][1]))
        self.cursor.execute(sql)
        r1 = self.cursor.fetchall()
        if len(r1)>0 and r1[0][0]=='S':
            dlg = dlgComprobante(self.cnx,self.wins,self.cod_empresa,True,None,None)
            dialogos.dlgAviso(None,"El comprobante esta Centralizado,No se puede Modificar")            
            dlg.mnuGuardar.set_sensitive(False)
            dlg.mnuGuardarComo.set_sensitive(False)
            dlg.btnAnadir.set_sensitive(False)
            dlg.btnQuitar.set_sensitive(False)
            dlg.btnAbrir.set_sensitive(False)
            dlg.btnLimpiarComprobante.set_sensitive(False)
            dlg.centralizado=True
            
        else:
            dlg = dlgComprobante(self.cnx,self.wins,self.cod_empresa,None,self.modelo_cuenta,self.modelo_ficha,self)
        dlg.edit=True
        page=self.ntbComprobante.get_n_pages()
        j=0
        label ="%s N°%s (%s-%s)"%(model[row][4],model[row][2],model[row][13],model[row][14])
        if self.wins.has_key(label):            
            for i in self.wins:
                #label="Comprobante Contable %s"%page
                if self.wins.has_key(label):
                    page+=1
        self.add_tab(dlg.vboxDetalleComprobante, label)
        combo_set_active(dlg.cmbTipoComprobante,model[row][4],0)
        combo_set_active(dlg.cmbOrigen,model[row][6],0)
        combo_set_active(dlg.cmbEmpresa,model[row][12],0)
        if model[row][15]=='C':
            dlg.mnuGuardar.set_sensitive(False)
        if model[row][7] == 'V':
            estado='VIGENTE'
        else:
            estado='PENDIENTE'        
        combo_set_active(dlg.cmbEstado,estado,0)
        dlg.entPeriodo.set_text(model[row][8])
        dlg.periodo = model[row][13]
        dlg.mes = model[row][14]
        dlg.pecPeriodo.set_selected(True)
        dlg.cod_empresa = int(model[row][12])
        dlg.spnFolio.set_value(int(model[row][2]))
        dlg.entGlosa.set_text(model[row][9])
        dlg.pecDistribucionCentroCosto.set_cod(model[row][17])
        #dlg.pecSucursal.set_text(model[row][18])
        #dlg.pecAreaFuncional.set_text(model[row][19])
        
        print "model"+" "+str(model[row][3])
        print "model"+" "+str(model[row][4])
    
        if str(model[row][3])<>'None':
            y,m,d=int(model[row][3][0:4]),int(model[row][3][5:7]),int(model[row][3][8:10])        
            dlg.entFecha.set_date(datetime.date(y,m,d))        
            ##carga el detalle
        debe = 0
        haber = 0
        for i in r:
            dlg.model.append(i)            
            debe+=float(i[MONTO_DEBE])
            haber+=float(i[MONTO_HABER])
            num_linea = i[NUM_LINEA]

        dlg.num_linea = int(num_linea)+1
        dlg.entTotalDebe.set_text(CMon(debe,0))
        dlg.entTotalHaber.set_text(CMon(haber,0))
        dlg.entDiferencia.set_text(CMon((debe-haber),0))
        dlg.cod_comprobante = int(model[row][1]) 
        dlg.cod_empresa_periodo = model[row][12]
        dlg.spnFolio.set_sensitive(False)
        self.ntbComprobante.set_current_page(self.ntbComprobante.get_n_pages()-1)

    def on_btnImprimirCabecera_clicked(self, btn=None):
        
        print "imprimir"
        
        print self.f
        #imprimir_detalle_comprobante(self.f,self.cursor)
        imprimir_detalle_comprobante_masivo(self.f,self.cursor)

class dlgComprobante(GladeConnect, ListStoreProxy):
    def __init__(self, conexion=None, padre=None, empresa=1, editando=None,modelo_cuenta=None,modelo_ficha=None,ventana=None):
        GladeConnect.__init__(self, "glade/wnComprobante.glade", "vboxDetalleComprobante")

        self.cnx = conexion
        self.wins = padre
        self.cursor = self.cnx.cursor()
        self.padre = padre
        self.ventana = None
        self.cod_empresa = empresa        
        self.cmbEstado.set_active(0)
        carga_combo(self.cmbOrigen, self.cnx, strSelectSistema)
        self.setea_contabilidad()

        self.cmbOrigen.set_sensitive(False)
        carga_combo(self.cmbTipoComprobante, self.cnx, strSelectTipoComprobante)
        
        self.cmbTipoComprobante.set_active(2)                        
        self.num_linea = 1
        self.tipo_analisis='N'
        self.num_cuenta = None
        self.cod_sucursal = None
        self.cod_area_funcional = None
        self.crea_columnas()
        self.codigo_comprobante = -1
        self.codigo_oid = -1
        self.codigo_numero = -1
        self.pecPeriodo = completion.CompletionPeriodoContable(self.entPeriodo,
                self.sel_periodo,
                self.cnx, self.cod_empresa)
        self.pecDistribucionCentroCosto = completion.CompletionDistribucionCentroCosto(self.entry1,
                self.sel_distribucioncentrocosto,
                self.cnx, self.cod_empresa)
        if modelo_cuenta is None and editando is not True: 
            self.pecCuentaContable = completion.CompletionCuentaContable(self.entDescripcionCuenta,
                self.sel_cuenta_contable,
                self.cnx,
                self.cod_empresa,'V')
        else:
            if editando is None and editando is not True:
                self.pecCuentaContable=completion.GenericCompletion(self.entDescripcionCuenta, self.sel_cuenta_contable,
                    self.cnx, None, modelo_cuenta)
        if modelo_ficha is None and editando is not True:
            self.pecFicha = completion.CompletionFicha(self.entNombreFicha,
                self.sel_ficha,
                self.cnx,
                self.cod_empresa)
        else:
            if editando is None and editando is not True:
                self.pecFicha=completion.GenericCompletion(self.entNombreFicha, self.sel_ficha,
                    self.cnx, None, modelo_ficha)
        
        sqlWhere = ''
        sqlWhere = 'Where e.cod_empresa=%s'%int(self.cod_empresa)
        carga_combo(self.cmbEmpresa, self.cnx, strSelectEmpresa %sqlWhere)      
        
        self.spnDebe.set_digits(int(sys.__getattribute__('decimales')))
        self.spnHaber.set_digits(int(sys.__getattribute__('decimales')))
        self.cod_tipo_documento = None
        self.cod_tipo_comprobante = None
        
        
        self.iter_editado =None
        self.btnCancelarEdicion.set_sensitive(False)
        self.pecTipoDocumento = completion.CompletionTipoDocumentoContable(self.entTipoDocumento,
                self.sel_tipo_documento,
                self.cnx,
                self.cod_empresa)
        self.pecSucursal = completion.CompletionSucursal(self.txtSucursal,
                self.selecciona_sucursal,
                self.cnx)
        self.pecAreaFuncional = completion.CompletionAreaFuncional(self.txtAreaFuncional,
                self.selecciona_area_funcional,
                self.cnx)       
        self.entPeriodo.grab_focus()                

        self.edit = False
        self.estado = None
        self.cod_comprobante = None        
        self.entRUT.connect('activate',self.on_entRUT_activate)
        self.entRUT.connect('key_press_event',self.on_entRUT_key_press_event)
        self.cmbTipoComprobante.connect('changed',self.on_cmbTipoComprobante_changed)
        self.entPeriodo.connect('changed',self.on_entPeriodo_changed)
        self.entFecha.children()[0].connect('activate',self.on_entFecha_activate)
        self.entFechaDocumento.children()[0].connect('activate',self.entFechaDocumento_activate)
        self.entGlosa.connect('changed',self.on_entGlosa_changed)
        self.entCuenta.connect('activate',self.on_entCuenta_activate)
        self.entFechaDocumento.connect('activate',self.on_entFechaDocumento_activate)

        self.mascara_niveles()
        self.aceptar = 0
        self.mes = None
        self.periodo=None
        self.cod_cc = None
        self.response = None
        self.cod_empresa_periodo = None
        self.num_linea_editada = None
        self.selecciona=False
        self.centralizado=False
        (y,m,d)=localtime()[:3]        
        self.entFecha.set_date(datetime.date(y,m,d))
        sql=strMaxPeriodo%self.cod_empresa
        print (sql)
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            return
        self.entPeriodo.set_text(r[0][0])
        self.mes=r[0][2]
        self.periodo=r[0][1]
        self.cod_empresa_periodo=r[0][3]
        self.pecPeriodo.set_selected(True)
        self.selecciona=True
        self.on_cmbTipoComprobante_changed(self.cmbTipoComprobante)
        self.vboxDetalleComprobante.show_all()
##      self.pecPeriodo.match_all=True                
##      self.pecPeriodo.entry_change(self.pecPeriodo.entry)
##      self.pecPeriodo.completion.complete()      
    #Cambio reciente jqd       
    def selecciona_sucursal(self,completion,model,iter):
        self.txtSucursal.set_text(model.get_value(iter,0))
        self.cod_sucursal = model.get_value(iter,1)
        self.nom_sucursal = model.get_value(iter,0)
        
    def selecciona_area_funcional(self,completion,model,iter):
        self.txtAreaFuncional.set_text(model.get_value(iter,0))
        self.cod_area_funcional = model.get_value(iter,1)
        self.nom_area_funcional = model.get_value(iter,0)

    def setea_contabilidad(self):
        model=self.cmbOrigen.get_model()
        for i in model:
            if i[1]=='CONTABILIDAD GENERAL' or i[0]=='CON':
                self.cmbOrigen.set_active(model.get_path(i.iter)[0])
                
    def mascara_niveles(self):
        if not self.entCuenta.get_text().replace(".","").isdigit():
            self.entCuenta.grab_focus()
            return         
        sql=strSelectEmpresaNivel %(self.cod_empresa,'')
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            return         
        mask=''
        digitos=0
        cuenta_contable=self.entCuenta.get_text().replace(".","")
        inicio=0
        digitos=0
        for i in r:              
            mask+=cuenta_contable[inicio:i[0]+inicio]+"."        
            inicio=inicio+i[0]
        return mask[0:len(mask)-1]
    def on_entFechaDocumento_activate(self,widget,*args):
        self.spnDebe.grab_focus()
    def on_entGlosa_changed(self,widget,*args):
        self.entGlosaDetalle.set_text(self.entGlosa.get_text())
    def on_spnDebe_activate(self,widget,*args):
        self.spnHaber.grab_focus()
    def on_spnHaber_activate(self,widget,*args):
        self.entGlosaDetalle.grab_focus()
    def on_spnFolioDocumento_activate(self,widget,*args):
        if  not self.pecFicha.get_selected():
            self.entFechaDocumento.grab_focus()
            return
        if not self.pecTipoDocumento.get_selected():
            self.entFechaDocumento.grab_focus()
            return
        if  not self.spnFolioDocumento.get_text().isdigit():    
            self.spnFolioDocumento.grab_focus()
            return
        if not self.pecCuentaContable.get_selected():
            return
        sql=strSelectDocumento %(self.entRUT.get_text().upper(),self.cod_tipo_documento,float(self.spnFolioDocumento.get_text()),self.cod_empresa,self.num_cuenta)
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            self.entFechaDocumento.grab_focus()
            return
        if r[0][0] > r[0][1]:
            self.spnHaber.set_value(float(str(r[0][0]-r[0][1])))
            self.spnHaber.grab_focus()
        else:
            self.spnDebe.set_value(float(str(iif(r[0][1]==None,0,r[0][1])-iif(r[0][0]==None,0,r[0][0]))))
            self.spnDebe.grab_focus()
        if r[0][2]<>None:    
            y=int(r[0][2][:4])
            m=int(r[0][2][5:7])
            d=int(r[0][2][8:10])
            dt=datetime.date(y,m,d)
            self.entFechaDocumento.set_date(dt)
            
    def on_entCuenta_activate(self,entry,*args):
        if self.centralizado==True:
            return
        if entry.get_text().replace(".","") not in("",None) and entry.get_text().replace(".","").isdigit():
            entry.set_text(self.mascara_niveles())
            sql=strNivelEmpresaCuentaContable%(self.cod_empresa,entry.get_text())
            self.cursor.execute(sql)
            r=self.cursor.fetchall()            
            if len(r)==0:
                entry.set_text('')
                self.num_cuenta=None
                self.entCuenta.grab_focus()
                self.entDescripcionCuenta.set_text('')                
                return False
            self.entDescripcionCuenta.set_text(r[0][7])           
            self.num_cuenta=entry.get_text()
            self.pecCuentaContable.set_selected(True)
            self.func_tipo_analisis(self.cod_empresa,self.num_cuenta)
            if r[0][5]=='S':
                self.entTipoDocumento.grab_focus()
                if self.entFecha.children()[0].get_text() not in('',None):
                    self.entFechaDocumento.set_date(self.entFecha.get_date())
            else:    
                if r[0][5]=='D':
                    if self.entFecha.children()[0].get_text() not in('',None):
                        self.entFechaDocumento.set_date(self.entFecha.get_date())
                    self.entRUT.grab_focus()
                else:
                    self.spnDebe.grab_focus()
            return False
  
    def on_entPeriodo_changed(self,widget,*args):
        self.selecciona=False
   
    def on_cmbTipoComprobante_changed(self,widget,*args):
        iter=widget.get_active_iter()
        model=widget.get_model()
        cod_tipo_comprobante=model.get_value(iter,0)
        iter1=self.cmbEmpresa.get_active_iter()
        model1=self.cmbEmpresa.get_model()
        cod_tipo_numeracion=model1.get_value(iter1,2)
        if self.selecciona:            
            self.spnFolio.set_value(float(self.determina_folio(cod_tipo_numeracion,self.cod_empresa,self.mes,self.periodo,cod_tipo_comprobante)))
   
    def determina_folio(self,tipo_numeracion,cod_empresa,cod_mes,periodo,tipo_comprobante):
        sqlWhere=''
        if tipo_numeracion=='A':
            sqlWhere="Where c.cod_empresa=%s and c.periodo=%s"%(cod_empresa,periodo)
        elif tipo_numeracion=='M':
            sqlWhere="Where c.cod_empresa=%s and c.periodo=%s and c.mes=%s"%(cod_empresa,periodo,cod_mes)
        elif tipo_numeracion=='T':
            sqlWhere="Where c.cod_empresa=%s and c.periodo=%s and c.cod_tipo_comprobante='%s'"%(cod_empresa,periodo,tipo_comprobante)
        elif tipo_numeracion=='C':
            sqlWhere="Where c.cod_empresa=%s and c.periodo=%s and c.mes=%s and c.cod_tipo_comprobante='%s'"%(cod_empresa,periodo,cod_mes,tipo_comprobante)
        sql=strSelectMaxFolio %sqlWhere
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        return iif(r[0][0] not in('',None),str(r[0][0]),1)
   
    def sel_distribucioncentrocosto(self, completion, model, iter):
        
        self.entry1.set_text(model.get_value(iter,0))
        self.cod_cc = model.get_value(iter, 1)
        #print self.cod_cc
        
    def sel_periodo(self, completion, model, iter):
        self.mes = model.get_value(iter, 1)
        self.periodo = model.get_value(iter, 2)
        self.cod_empresa_periodo=model.get_value(iter,3)
        self.selecciona=True
        self.on_cmbTipoComprobante_changed(self.cmbTipoComprobante)
    def sel_ficha(self, completion, model, iter):
        self.rut_ficha = model.get_value(iter, 1)
        self.entRUT.set_text(self.rut_ficha)
        
    def sel_tipo_documento(self, completion, model, iter):
        self.cod_tipo_documento = model.get_value(iter, 1)
        
    def sel_cuenta_contable(self, completion, model, iter):
        self.num_cuenta = model.get_value(iter, 1)
        self.entCuenta.set_text(self.num_cuenta)
        self.func_tipo_analisis(self.cod_empresa,self.num_cuenta)
        
    ###
        # 
        #cadena=str(self.num_cuenta)
        #cadena2=cadena[0:4]
        #
        #if cadena2=='6.63' or cadena2=='6.64' or cadena2=='6.65' or cadena2=='6.66' or cadena2=='9.94' or cadena2=='9.95':
        #    self.entry1.set_sensitive(True)
        #    self.entry1.set_text("")
        #else:
        #    self.entry1.set_sensitive(False)
        #    self.entry1.set_text("No aplicable")
        #    self.cod_cc=9
        #
        #
    def func_tipo_analisis(self,cod_empresa,num_cuenta):
        sql = strSelectTipoAnalisisCuentaContable % (cod_empresa, num_cuenta)
        self.cursor.execute(sql)
        r = self.cursor.dictfetchone()
        self.tipo_analisis=r['tipo_analisis']
        if r['tipo_analisis'] == 'S':
            self.alnFicha.set_sensitive(False)
            self.alnAnalisis.set_sensitive(True)
            self.tipo_analisis='S'
        elif r['tipo_analisis'] == 'D':
            self.alnFicha.set_sensitive(True)
            self.alnAnalisis.set_sensitive(True)
            self.tipo_analisis='D'
        else:
            self.alnFicha.set_sensitive(False)
            self.alnAnalisis.set_sensitive(False)
        
    def crea_columnas(self):
        columnas = []
        columnas.append([NUM_LINEA,"Linea","int"])
        columnas.append([NUM_CUENTA,"Cuenta","str"])
        columnas.append([DESCRIPCION_CUENTA,"Descripción","str"])
        columnas.append([MONTO_DEBE,"Debe","int"])
        columnas.append([MONTO_HABER,"Haber","int"])
        columnas.append([GLOSA,"Glosa","str"])
        columnas.append([SERIE,"Serie","str"])
        columnas.append([FOLIO_ANALISIS,"Numero / Folio","str"])
        columnas.append([FECHA_ANALISIS,"Fecha Documento","str"])
        columnas.append([DOCUMENTO_ANALISIS,"Documento","str"])
        columnas.append([RUT_ANALISIS,"R.U.C.","str"])
        columnas.append([NOMBRE_ANALISIS,"Ficha","str"])    
        columnas.append([TIPO_ANALISIS,"Tipo Analisis","str"])
        #columnas.append([COD_CC,"Cod. Centro Costo","int"])
        columnas.append([CENTRO_COSTO,"Dist. Centro de Costo","str"])
        columnas.append([SUCURSAL,"Sucursal","str"])
        columnas.append([AREA_FUNCIONAL,"Area Funcional","str"])
        self.model = gtk.ListStore(*(20*[str]))
        SimpleTree.GenColsByModel(self.model, columnas, self.treeDetalle)
        
        self.mapping = {NUM_LINEA: "self.num_linea",
                   NUM_CUENTA: "self.num_cuenta",
                   DESCRIPCION_CUENTA: self.entDescripcionCuenta,
                   MONTO_DEBE: self.spnDebe,
                   MONTO_HABER: self.spnHaber,
                   GLOSA: self.entGlosaDetalle,
                   SERIE: self.entSerie,
                   FOLIO_ANALISIS: self.spnFolioDocumento,
                   FECHA_ANALISIS: self.entFechaDocumento,
                   DOCUMENTO_ANALISIS: self.entTipoDocumento,
                   RUT_ANALISIS: self.entRUT,
                   NOMBRE_ANALISIS: self.entNombreFicha,
                   TIPO_ANALISIS: "self.tipo_analisis",
                   COD_TIPO_DOCUMENTO:"self.cod_tipo_documento",
                   COD_CC:"self.cod_cc",
                   CENTRO_COSTO:self.entry1,
                   SUCURSAL:self.txtSucursal,
                   AREA_FUNCIONAL:self.txtAreaFuncional,
                   COD_SUCURSAL:"self.cod_sucursal",
                   COD_AREA_FUNCIONAL:"self.cod_area_funcional"
                   }              
    
    def on_btnCancelar_clicked(self, btn=None):
        #self.dlgComprobante.destroy()
        del self.wins[self.vboxDetalleComprobante.get_parent().get_tab_label(self.vboxDetalleComprobante).get_label()]
        self.vboxDetalleComprobante.get_parent().remove_page(self.vboxDetalleComprobante.get_parent().get_current_page())
    def on_btnCancelarEdicion_clicked(self,widget,*args):
        self.iter_editado=None
        self.btnCancelarEdicion.set_sensitive(False)
        glosa_detalle=self.entGlosaDetalle.get_text()
        self.clear_widgets()
        self.entCuenta.set_text("")        
        self.entGlosaDetalle.set_text(glosa_detalle)
        self.alnAnalisis.set_sensitive(False)
        self.alnFicha.set_sensitive(False)
        self.entCuenta.grab_focus()
        self.spnFolioDocumento.set_value(1)
        self.entFechaDocumento.children()[0].set_text("")
        
        self.entry1.set_text("")
        #self.entry1.set_sensitive(False)
        self.entry1.set_sensitive(True)


    def on_mnuGuardar_activate(self,widget,*args):
        self.on_btnAceptar_clicked(None)
 
    def on_btnAceptar_clicked(self, btn=None):
        self.codigo_comprobante = -1
        self.codigo_oid = -1
        self.codigo_numero = -1
        ##precondiciones para insertar la cabecera        
        if not self.precondiciones_cabecera_comprobante():            
            return
        ##precondiciones para insertar el detalle
        if not self.precondiciones_detalle_comprobante():
            return
        ## Editado el Comprobante se elimina el detalle y la cabecera 
        if self.edit == True:                                        
            if not VerificaPermiso(self,302):
                return
            if self.cod_comprobante <> None:                    
                try:
                    self.cursor.execute('begin')
 
                    if self.cod_sistema<>'SC':
                        sql1=strDeleteDetalleComprobante %(self.cod_comprobante,int(self.cod_empresa))                   
                        self.cursor.execute(sql1)
                    #sql=""
                    #if(self.cod_cc not in('',None)):
                    #    sql="""update ctb.comprobante set cod_tipo_comprobante='%s' ,                                                                   
                    #                                                periodo=%s,
                    #                                                mes=%s,
                    #                                                estado='%s',
                    #                                                origen='%s',                                                              
                    #                                                fecha='%s',
                    #                                                debe=%s,
                    #                                                haber=%s,
                    #                                                glosa='%s',
                    #                                                cod_empresa=%s,
                    #                                                folio_comprobante=%s,
                    #                                                cod_distribucion_centro_costo = %s
                    #            where
                    #                cod_comprobante=%s
                    #                                                """%(self.cod_tipo_comprobante,
                    #                                                    int(self.periodo),
                    #                                                    int(self.mes),
                    #                                                    self.estado,
                    #                                                    self.cod_sistema,
                    #                                                    self.entFecha.get_date().isoformat(),
                    #                                                    float(self.debe),
                    #                                                    float(self.haber),
                    #                                                    self.entGlosa.get_text(),
                    #                                                    int(self.cod_empresa),
                    #                                                    int(self.spnFolio.get_text()),
                    #                                                    int(self.cod_cc),
                    #                                                    self.cod_comprobante)                                                                 
                    #else:
                    sql="""update ctb.comprobante set cod_tipo_comprobante='%s' ,                                                                   
                                                                    periodo=%s,
                                                                    mes=%s,
                                                                    estado='%s',
                                                                    origen='%s',                                                              
                                                                    fecha='%s',
                                                                    debe=%s,
                                                                    haber=%s,
                                                                    glosa='%s',
                                                                    cod_empresa=%s,
                                                                    folio_comprobante=%s
                                where
                                    cod_comprobante=%s
                                                                    """%(self.cod_tipo_comprobante,
                                                                        int(self.periodo),
                                                                        int(self.mes),
                                                                        self.estado,
                                                                        self.cod_sistema,
                                                                        self.entFecha.get_date().isoformat(),
                                                                        float(self.debe),
                                                                        float(self.haber),
                                                                        self.entGlosa.get_text(),
                                                                        int(self.cod_empresa),
                                                                        int(self.spnFolio.get_text()),
                                                                        self.cod_comprobante)
                    self.cursor.execute(sql)                                              

                except:
                    self.cursor.execute('rollback')
                    self.error=True
                    dialogos.error('Ha ocurrido un error al actualizar el comprobante.',str(sys.exc_info()[1]))
                    return
            else:
                dialogos.dlgAviso(self.dlgComprobante,"El cod_comprobante es Nulo")
                return
        else:              
            if not VerificaPermiso(self,301):
                return
            if not self.pecPeriodo.get_selected():
                dialogos.error("El periodo no ha sido seleccionado")                               
                return
    #try:    
        if self.entTotalDebe.get_text() <> self.entTotalHaber.get_text():
            resp=dialogos.dlgSiNo(None,"El Comprobante Esta descuadrado\n Quiere Guardarlo de todas Maneras como Pendiente?", "Comprobante").response
            self.estado='P'
            if resp==gtk.RESPONSE_NO:
                return                
        if self.edit==False:
            model=self.cmbTipoComprobante.get_model()
            sqlWhere=''
            sqlWhere=' and c.folio_comprobante=%s and c.periodo=%s and c.mes=%s and c.cod_tipo_comprobante=%s'%(str(self.spnFolio.get_text()),self.periodo,self.mes,"'"+str(model.get_value(self.cmbTipoComprobante.get_active_iter(),0))+"'")
            sql=strCargaDatos %(self.cod_empresa,sqlWhere)
            
            self.cursor.execute(sql)
            r=self.cursor.fetchall()
            if len(r)>0:
                model=self.cmbEmpresa.get_model()
                model1=self.cmbTipoComprobante.get_model()
                self.spnFolio.set_value(float(self.determina_folio(model.get_value(self.cmbEmpresa.get_active_iter(),2),self.cod_empresa,self.mes,self.periodo,model1.get_value(self.cmbTipoComprobante.get_active_iter(),0))))
            
            self.cursor.execute('begin')
            self.insertar_cabecera_comprobante()
        if self.cod_sistema<>'SC':
            self.insertar_detalle_comprobante()
            
        self.cursor.execute('commit')

        label = "%s N°%s (%s-%s)"%(self.cod_tipo_comprobante,int(self.spnFolio.get_text()),self.periodo, self.mes)
        if self.wins.has_key(label):
            print "Ya existe"
        else:
            del self.wins[self.vboxDetalleComprobante.get_parent().get_tab_label(self.vboxDetalleComprobante).get_label()]
        
        self.vboxDetalleComprobante.get_parent().get_tab_label(self.vboxDetalleComprobante).set_label(label)
        self.wins[label] =self.vboxDetalleComprobante
        
##            self.dlgComprobante.hide()             
        if self.edit == False:
            if self.codigo_numero > -1:
                dialogos.dlgAviso(None,"El comprobante ha sido guardado de manera Exitosa - N° Correlativo : " + str(self.codigo_numero))
            else:
                dialogos.dlgAviso(None,"El comprobante ha sido guardado de manera Exitosa")
        else:
            dialogos.dlgAviso(None,"El comprobante ha sido actualizado de manera Exitosa")
        
        self.response = True
##            self.on_btnImprimir_clicked(None)
        self.on_btnAbrir_clicked(None)
        #self.on_btnLimpiarComprobante_clicked(None)
    #except:
        #self.cursor.execute('rollback')
        #dialogos.error('Ha ocurrido un error al insertar el comprobante.',str(sys.exc_info()[1]))
          
            #return
                    
    def insertar_cabecera_comprobante(self):
        #if self.entry1.get_text() in ('',None):
        sql=strInsertCabeceraComprobanteNull%(self.cod_tipo_comprobante,
                                                                        int(self.cod_empresa),
                                                                        int(self.periodo),
                                                                        int(self.mes),
                                                                        self.cod_sistema,
                                                                        self.entFecha.get_date().isoformat(),
                                                                        float(self.debe),
                                                                        float(self.haber),
                                                                        self.entGlosa.get_text(),
                                                                        long(self.spnFolio.get_text()),
                                                                        self.estado)
        #else:
        #    sql=strInsertCabeceraComprobanteN%(self.cod_tipo_comprobante,
        #                                                                int(self.cod_empresa),
        #                                                                int(self.periodo),
        #                                                                int(self.mes),
        #                                                                self.cod_sistema,
        #                                                                self.entFecha.get_date().isoformat(),
        #                                                                float(self.debe),
        #                                                                float(self.haber),
        #                                                                self.entGlosa.get_text(),
        #                                                                long(self.spnFolio.get_text()),
        #                                                                self.estado,self.cod_cc)                                                                   
        self.cursor.execute(sql.upper())
    
    def insertar_detalle_comprobante(self):
        sql=strSelectComprobanteM %(self.cod_tipo_comprobante,long(self.spnFolio.get_text()),int(self.periodo),int(self.mes),int(self.cod_empresa))
        self.cursor.execute(sql.upper())
        r=self.cursor.fetchall()
        if len(r)==0:
            dialogos.dlgAviso(None,"No se puede insertar el detalle")
            return
        num_linea=1
        cod_empresa=int(self.cod_empresa)
        
        for i in self.model:
            num_cuenta=i[NUM_CUENTA]              
            debe=float(i[MONTO_DEBE])
            
            haber=float(i[MONTO_HABER])
            glosa=i[GLOSA]
            
            print "cod_cc"
            print i[COD_CC]
            print i[COD_SUCURSAL]
            print i[COD_AREA_FUNCIONAL]
            
            if i[COD_SUCURSAL] == None and  i[COD_AREA_FUNCIONAL] == None:
                i[COD_SUCURSAL] = "NULL"
                i[COD_AREA_FUNCIONAL] = "NULL"    
            
            if i[TIPO_ANALISIS]=='N':
                print "TIPO N"               
                sql=strInsertDetalleComprobanteSA %(int(r[0][0]),
                                                                        num_cuenta,
                                                                        cod_empresa,
                                                                        debe,
                                                                        haber,
                                                                        glosa,
                                                                        num_linea,
                                                                        i[COD_CC],
                                                                        i[COD_SUCURSAL],
                                                                        i[COD_AREA_FUNCIONAL])
                print sql
            else:
                print "TIPO D"
                if i[TIPO_ANALISIS]=='D':
                    sql=strInsertDetalleComprobanteAD %(int(r[0][0]),
                                                                        num_cuenta,
                                                                        int(self.cod_empresa),
                                                                        debe,
                                                                        haber,
                                                                        glosa,
                                                                        i[SERIE],
                                                                        int(i[FOLIO_ANALISIS].replace(".0","")),
                                                                        i[FECHA_ANALISIS],
                                                                        i[COD_TIPO_DOCUMENTO],
                                                                        i[RUT_ANALISIS],
                                                                        num_linea,
                                                                        i[COD_CC],
                                                                        i[COD_SUCURSAL],
                                                                        i[COD_AREA_FUNCIONAL])
                else:
                        print "TIPO U"
                        sql=strInsertDetalleComprobanteAS %(int(r[0][0]),
                                                                        num_cuenta,
                                                                        int(self.cod_empresa),
                                                                        debe,
                                                                        haber,
                                                                        glosa,
                                                                        i[SERIE],
                                                                        int(i[FOLIO_ANALISIS]),
                                                                        i[FECHA_ANALISIS],
                                                                        i[COD_TIPO_DOCUMENTO],
                                                                        num_linea,
                                                                        i[COD_CC],
                                                                        i[COD_SUCURSAL],
                                                                        i[COD_AREA_FUNCIONAL])
 
            print "PUNTO ANTES DE EJECUTAR"    
            self.cursor.execute(sql.upper())
            num_linea+=1                
        
        print "EXIT"
         #===== registro de compras
        sql= "select ctb.f_crea_registro_compras(%s)" % int(r[0][0])
        self.cursor.execute(sql.upper())
        #===== termina registro de compras
        
        self.codigo_comprobante = int(r[0][0])
        
        glo="select oid from ctb.registro_compras where cod_comprobante=%s "%(self.codigo_comprobante)
        self.cursor.execute(glo)
        gl=self.cursor.fetchall()
        
        if len(gl)>0:        
            v_tip_3 = int(str(gl[0][0]))
        
            seni_3 = """select numero
                    from temp_compras
                    where
                                codigo = %s""" % (v_tip_3)
                                
            self.cursor.execute(seni_3)
            rt_3 = self.cursor.fetchall()
            v_tip_3 = int(str(rt_3[0][0]))
        
            self.codigo_numero = v_tip_3
        
    def on_mnuImprimirComprobante_activate(self,widget,*args):
        self.on_btnImprimir_clicked(None)
    def on_mnuImprimirDetalle_activate(self,widget,*args):
        if self.edit==True:
            imprimir_detalle_comprobante(self.cod_comprobante,self.cursor)
            
    def on_btnImprimir_clicked(self, btn=None):
        print ("Imprimir")
        try:
            imprimir_comprobante(self)            
            return 
        except:   
            print "error en impresion de comprobante..............."
            dialogos.dlgAviso(None,"Se obtuvieron errores en la impresion del Comprobante")
    
    def on_btnExportar_clicked(self, btn=None):
        print "Exportar"
        
        if self.cod_comprobante in ('',None):
            dialogos.error("No hay datos para exportar.")
            return
        else:
            
            sql=strCabeceraExportar%(self.cod_comprobante)
            print sql
            self.cursor.execute(sql)
            r=self.cursor.fetchall()
            titulo = "Comprobante Contable "+str(r[0][0])
            empresa = str(r[0][1])
            ruc = str(r[0][2])
            direccion = str(r[0][3])
            folio = str(r[0][4])
            fecha_comp= CDateLocal(r[0][5])
            glosa = str(r[0][6])
    
            
            ###DEFINIENDO ESTILOS PREDETERMINADOS
            style_cabecera = xlwt.easyxf('font: name Times New Roman, colour black, bold on; align: wrap on, vert centre, horiz center;border: left thin, top thin,right thin,bottom thin')
            style_fila_left = xlwt.easyxf('font: name Times New Roman, colour black;border: left thin, top thin,right thin,bottom thin')
            style_fila_center = xlwt.easyxf('font: name Times New Roman, colour black;border: left thin, top thin,right thin,bottom thin;align: horiz center')
            style_fila_right = xlwt.easyxf('font: name Times New Roman, colour black;border: left thin, top thin,right thin,bottom thin;align: horiz right')
            style_titulo = xlwt.easyxf('font: height 300,name Times New Roman, colour black, bold on; align: wrap on, vert centre, horiz center')
            style_fila_sin_borde_left = xlwt.easyxf('font: name Times New Roman, colour black')
            style_fila_sin_borde_right = xlwt.easyxf('font: name Times New Roman, colour black;align: horiz right')
            
            wb = xlwt.Workbook(encoding="utf-8")
            ws = wb.add_sheet('Comprobante_Contable',cell_overwrite_ok=True)        
                
            ws.row(0).height = ws.row(6).height = 500
             
    
            ###ESTILOS DE LA TABLA
            ws.write_merge(0,0,0,5, label=titulo, style=style_titulo)
            ws.write(1,0, empresa, style=style_fila_sin_borde_left)
            ws.write(2,0, ruc, style=style_fila_sin_borde_left)
            ws.write(3,0, direccion, style=style_fila_sin_borde_left)
            ws.write(3,4,"Folio:",style=style_fila_sin_borde_left)
            ws.write(3,5,folio.zfill(9),style=style_fila_sin_borde_right)
            ws.write(4,0, glosa,style=style_fila_sin_borde_left)
            ws.write(4,4,"Fecha:",style=style_fila_sin_borde_left)
            ws.write(4,5,fecha_comp,style=style_fila_sin_borde_right)
    
            ###CABECERAS DE LA TABLA
            ws.write(6, 0, "LINEA".encode('utf-8'),style_cabecera)
            ws.write(6, 1, "CUENTA".encode('utf-8', 'replace'),style_cabecera)
            ws.write(6, 2, "DESCRIPCION DE LA CUENTA".decode('iso8859-7').encode('utf-8'),style_cabecera)
            ws.write(6, 3, "GLOSA DEL DETALLE".decode('iso8859-7').encode('utf-8', 'replace'),style_cabecera)
            ws.write(6, 4, "DEBE".encode('ascii', 'replace'),style_cabecera)
            ws.write(6, 5, "HABER".encode('ascii', 'replace'),style_cabecera)
            
            ####TAMAÑO DE LAS COLUMNAS DE EXCEL
            ws.col(0).width = 2000
            ws.col(1).width = 5000
            ws.col(2).width = 10000
            ws.col(3).width = 21000
            ws.col(4).width = 3500
            ws.col(5).width = 3500
            
            i = 6
            total_debe = 0
            total_haber = 0
            for e in self.model:            
                i = i + 1
                ws.write(i, 0, str(e[NUM_LINEA]),style_fila_center)
                ws.write(i, 1, str(e[NUM_CUENTA]).encode('ascii', 'replace'),style_fila_center)
                ws.write(i, 2, str(e[DESCRIPCION_CUENTA]).encode('ascii', 'replace'),style_fila_left)
                ws.write(i, 3, str(e[GLOSA]).encode('ascii', 'replace'),style_fila_left)
                ws.write(i, 4, float(e[MONTO_DEBE]),style_fila_center)
                ws.write(i, 5, float(e[MONTO_HABER]),style_fila_center)
                total_debe+=float(e[MONTO_DEBE])
                total_haber+=float(e[MONTO_HABER])
            i = i + 1
            ws.write(i, 3, "TOTAL COMPROBANTE" ,style_cabecera)
            ws.write(i, 4, float(total_debe) ,style_fila_center)
            ws.write(i, 5, float(total_haber) ,style_fila_center)
            
            wb.save('Comprobante_Contable.xls')        
            Abre_excel('Comprobante_Contable.xls');
    
    def precondiciones_cabecera_comprobante(self): 
        if len(self.cmbEmpresa.get_model())==0 :
            dialogos.error('No hay Empresa Seleccionada')            
            return 0
        if len(self.cmbTipoComprobante.get_model())==0:
            dialogos.error('No hay Tipo Comprobante seleccionado')            
            return 0
        if len(self.cmbOrigen.get_model())==0:
            dialogos.error('No hay Sistema Seleccionado')            
            return 0
        self.cod_empresa,row,model=combo_get_active(self.cmbEmpresa,True) 
        self.cod_sistema=combo_get_active(self.cmbOrigen,False)
        self.cod_tipo_comprobante=combo_get_active(self.cmbTipoComprobante,False)
        if len(self.model)==0:
            dialogos.error('El comprobante no tiene detalle')            
            return 0
        if self.entGlosa.get_text() in ('',None):
            dialogos.error("Debe completar la Glosa")            
            return 0        
        dia=self.entFecha.children()[0].get_text()[:2]
        ano=self.entFecha.children()[0].get_text()[6:8]
        mes=self.entFecha.children()[0].get_text()[4:5]
        
        if len(dia.replace(" ",""))==0 or len(mes.replace(" ",""))==0 or len(ano.replace(" ",""))==0:
            dialogos.error("El formato de Fecha no es valido")            
            return 0
            
        if self.entFecha.get_date() in('',None):        
            dialogos.error("Debe completar la Fecha")            
            return 0
        date=(self.entFecha.get_date().isoformat())        
        if date in ('',None):            
            dialogos.error("Debe completar la Fecha")
            return 0
        if not self.pecPeriodo.get_selected():
            dialogos.error("Debe seleccionar el Periodo")
            return
        y,m,d=(int(date[0:4]),int(date[5:7]),int(date[8:10]))
        if self.mes in ('',None):            
            dialogos.error("Debe seleccionar el periodo")
            return 0
        if int(self.mes)!=m:            
            dialogos.error("El mes no coincide con la fecha Comprobante")            
            return 0
        if self.periodo in ('',None):            
            dialogos.error("Debe seleccionar el periodo")
            return 0
        if y!=int(self.periodo): 
            dialogos.error(" El periodo contable no coincide con la fecha Comprobante")            
            return 0
        
        ##extrae el tipo de numeracion
        self.cod_tipo_numeracion=model[row][2]
        ##Si se edita el comprobante no se consulta por Folio se supone que este seria el mismo
        if self.edit==False:
            sql=strSelectFolio %(int(self.cod_empresa),int(self.periodo),int(self.mes),long(self.spnFolio.get_text()),self.cod_tipo_numeracion,self.cod_tipo_comprobante)
            self.cursor.execute(sql)
            r=self.cursor.fetchall()
            if len(r)>0:            
                dialogos.error(" El Folio ya se encuentra asignado,debe ingresar un nuevo folio")                
                return 0
       ##si se cambia de eleccion de empresa y no se refresca el periodo contable 
        if int(self.cod_empresa)!=int(self.cod_empresa_periodo):
            dialogos.error(" El periodo contable no corresponde con la empresa")                
            return
        sql=strSelectEmpresaPeriodo %(int(self.cod_empresa),int(self.periodo),int(self.mes))
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            dialogos.error(" Error en Periodo Periodo Contable")                
            return 0
        if r[0][4]=='C':
            dialogos.error("No se puede guardar comprobante en periodo cerrado")
            return 0
        if CNumDb(self.entTotalDebe.get_text())<>CNumDb(self.entTotalHaber.get_text()):
            self.estado='P'
        else:
            self.estado='V'
        self.debe,self.haber=self.totales_comprobante(1)   
        return 1
    
    def precondiciones_detalle_comprobante(self):
        ##Verifica si las cuentas contables en el detalle del comprobante corresponden a la empresa
        strMensaje=''
        for i in self.model:
            sql=strSelectCuentaEmpresa %(i[NUM_CUENTA],int(self.cod_empresa))
            self.cursor.execute(sql)
            r=self.cursor.fetchall()
            if len(r)==0:
                strMensaje="La cuenta Contable no pertenece a esta Empresa en la linea N° " +str(i[NUM_LINEA])
                return 0           
                dialogos.error(strMensaje)             
            if not self.validar_detalle_comprobante(i,i[TIPO_ANALISIS]):                
                strMensaje="La cuenta Contable no tiene completado todos los campos linea N° " +str(i[NUM_LINEA]+"\nN° Cuenta es:"+str(i[NUM_CUENTA])+"Cuenta Contable "+str(i[DESCRIPCION_CUENTA]))
                dialogos.error(strMensaje)
                return 0           
        return 1
    def validar_detalle_comprobante(self,recordset,tipo_analisis):
        if tipo_analisis=='D':
            if recordset[RUT_ANALISIS] in('',None):
                return 0            
        
        if  tipo_analisis=='D' or tipo_analisis=='S':
            if recordset[COD_TIPO_DOCUMENTO] in('',None):
                return 0
            
            if recordset[FECHA_ANALISIS] in('',None):
                return 0
            
            if recordset[FOLIO_ANALISIS] in('',None):
                return 0
        return 1
    def on_btnEditar_clicked(self, btn=None):        
        model,iter=self.treeDetalle.get_selection().get_selected()
        if iter is None or model is None:
            return
        if self.centralizado==True:
            return
        row=model.get_path(iter)
        
        self.cod_cc=int(model[row][COD_CC])
        self.entry1.set_sensitive(True)
        self.entry1.set_text(model[row][CENTRO_COSTO])
        self.pecDistribucionCentroCosto.set_selected(True)
        
        self.num_linea_editada=int(model[row][NUM_LINEA])
        self.entDescripcionCuenta.set_text(model[row][DESCRIPCION_CUENTA])
        self.entCuenta.set_text(model[row][NUM_CUENTA])
        self.pecCuentaContable.set_selected(True)
        self.spnDebe.set_value(float(model[row][MONTO_DEBE]))
        self.spnHaber.set_value(float(model[row][MONTO_HABER]))
        self.entGlosaDetalle.set_text(model[row][GLOSA])
        self.tipo_analisis=model[row][TIPO_ANALISIS]
        self.cod_tipo_documento=model[row][COD_TIPO_DOCUMENTO]
        self.entSerie.set_text("")
        self.spnFolioDocumento.set_value(0)
        self.alnAnalisis.set_sensitive(False)
        self.alnFicha.set_sensitive(False)
        if self.tipo_analisis=='D':
            self.alnAnalisis.set_sensitive(True)
            self.alnFicha.set_sensitive(True)
        if self.tipo_analisis=='S':
            self.alnAnalisis.set_sensitive(True)
        self.num_cuenta=model[row][NUM_CUENTA]
        if  self.tipo_analisis=='D':
            if model[row][NOMBRE_ANALISIS] not in ("",None):
                self.entNombreFicha.set_text(model[row][NOMBRE_ANALISIS])
            if model[row][RUT_ANALISIS] not in ("",None):
                self.entRUT.set_text(model[row][RUT_ANALISIS])
                self.pecFicha.set_selected(True)
        if self.tipo_analisis=='S' or self.tipo_analisis=='D':
            if model[row][FECHA_ANALISIS] not in ("",None):
                y,m,d=(int(model[row][FECHA_ANALISIS][0:4]),int(model[row][FECHA_ANALISIS][5:7]),int(model[row][FECHA_ANALISIS][8:10]))
                self.entFechaDocumento.set_date(datetime.date(y,m,d))
            if model[row][SERIE] not in ("",None):
                self.entSerie.set_text(model[row][SERIE])
            if model[row][FOLIO_ANALISIS] not in ("",None):
                self.spnFolioDocumento.set_value(float(model[row][FOLIO_ANALISIS]))
            if  model[row][DOCUMENTO_ANALISIS] not in ("",None):
                self.entTipoDocumento.set_text(model[row][DOCUMENTO_ANALISIS])
                self.pecTipoDocumento.set_selected(True)
        #print (model[row][SUCURSAL])
        #print COD_AREA_FUNCIONAL   
        #self.pecSucursal.set_cod(model[row][SUCURSAL])
        if model[row][SUCURSAL] not in ("",None):
            self.txtSucursal.set_text(model[row][SUCURSAL])
            self.pecSucursal.set_selected(True)
        if model[row][AREA_FUNCIONAL] not in ("",None):
            self.txtAreaFuncional.set_text(model[row][AREA_FUNCIONAL])
            self.pecAreaFuncional.set_selected(True)
        #model.remove(iter)    
        self.totales_comprobante()        
        self.entCuenta.grab_focus()
        self.iter_editado = iter
        self.btnCancelarEdicion.set_sensitive(True)
        
    def on_btnQuitar_clicked(self, widget, *args):
        model,iter=self.treeDetalle.get_selection().get_selected()
        if iter is None or model is None:
            return
        if self.centralizado==True:
            return
        del self.model[iter]
        self.on_btnCancelarEdicion_clicked(None)
        
    def on_btnAnadir_clicked(self, btn=None):
        
        #se obtiene la descripcion del combo origen
        iter=self.cmbOrigen.get_active_iter()       
        model=self.cmbOrigen.get_model()         
        
        #si el combo es diferente de 'CONTABILIDAD GENERAL' retorna sin añadir
        if model.get_value(iter,1)!='CONTABILIDAD GENERAL' or model.get_value(iter,0)!='CON':
            return
        #se verifica que el monto debe y haber no puedan ser mayor que cero a la vez            
        if self.spnDebe.get_value() > 0  and self.spnHaber.get_value() > 0 :
            dialogos.error("Los monto debe y haber no pueden ser mayor que cero a la vez.")
            return
        #se verifica que se haya seleccionado la descripcion de la cuenta contable
        if not self.pecCuentaContable.get_selected():
            return
           
        #se verifica que se haya seleccionado el centro de costo         
        if self.entry1.get_text()=="":
            dialogos.error("Debe ingresar el Centro de Costo")
            return
        
        #se verifica que este lleno el campo de texto de el numero de cuenta contable
        if  self.entCuenta.get_text()   in ('',None) :
            return
        
        #se verifica que este lleno el campo de texto de la descripcion de cuenta contable
        if self.entDescripcionCuenta.get_text() in ('',None):
            return
        
        #se verifica que este lleno el campo de texto de la glosa            
        if self.entGlosaDetalle.get_text()  in ('',None):
            return
                    
        if self.tipo_analisis=='D' :
            if (self.entNombreFicha.get_text())  in ('',None):
                return
            if self.entRUT.get_text() in ('',None):
                return

        if self.tipo_analisis=='D' or self.tipo_analisis=='S':                        
            dia=self.entFechaDocumento.children()[0].get_text()[:2]
            mes=self.entFechaDocumento.children()[0].get_text()[4:5]
            ano=self.entFechaDocumento.children()[0].get_text()[6:8]
            if len(dia.replace(" ",""))==0 or len(mes.replace(" ",""))==0 or len(ano.replace(" ",""))==0:
                dialogos.error("El formato de Fecha no es valido")            
                return 0
            #if (self.entFechaDocumento.get_date()) in ("",None):                
            #    return        
            if ( self.entTipoDocumento.get_text()) in ("",None):
                return
            if self.spnFolioDocumento.get_value() in ("",None):
                return
            #if self.entry1.get_text() in ("",None):
            #    return
            
        if self.iter_editado != None:
            self.update(self.iter_editado)
            self.iter_editado=None
            #self.btnEditar.set_sensitive(False)
            self.btnCancelarEdicion.set_sensitive(False)
        else:
            self.add()
             
        self.totales_comprobante()
        self.num_cuenta = None
        glosa_detalle=self.entGlosaDetalle.get_text()
        self.entCuenta.set_text("")
        self.on_btnCancelarEdicion_clicked(None)
##      self.entDescripcionCuenta.grab_focus()

    def on_btnBorrar_clicked(self,btn=None):
        self.on_btnAnular_clicked(self.btnBorrar)
        
    def on_btnReversar_clicked(self,btn=None):
        if self.edit==False:
            return
        if self.centralizado=='S':
            return
        if len(self.model)==0:
            return
        for i in self.model:
            if float(i[3])!=0L:                
                self.model.set(i.iter,4,float(i[3]))
                self.model.set(i.iter,3,0L)
            else:                
                self.model.set(i.iter,3,float(i[4]))
                self.model.set(i.iter,4,0L)
        iter=self.cmbTipoComprobante.get_active_iter()
        model=self.cmbTipoComprobante.get_model()
        cod_tipo_comprobante=model.get_value(iter,0)
        iter1=self.cmbEmpresa.get_active_iter()
        model1=self.cmbEmpresa.get_model()
        cod_tipo_numeracion=model1.get_value(iter1,2)
        self.spnFolio.set_value(float(self.determina_folio(cod_tipo_numeracion,self.cod_empresa,self.mes,self.periodo,cod_tipo_comprobante)))
        self.edit=False
        resp=dialogos.dlgSiNo(None,"Se generara un comprobante nuevo reversado a partir del editado\ndesea realizar esta accion de todas maneras").response
        if resp==gtk.RESPONSE_NO:
            return
        self.on_btnAceptar_clicked(None)
        
    def on_mnuGuardarComo_activate(self,btn=None):
        if self.edit==False:
            return
        if self.centralizado=='S':
            return
        resp=dialogos.dlgSiNo(None,"Se generara un comprobante nuevo a partir del editado\ndesea realizar esta accion de todas maneras").response
        if resp==gtk.RESPONSE_NO:
            return
        iter=self.cmbTipoComprobante.get_active_iter()
        model=self.cmbTipoComprobante.get_model()
        cod_tipo_comprobante=model.get_value(iter,0)
        iter1=self.cmbEmpresa.get_active_iter()
        model1=self.cmbEmpresa.get_model()
        cod_tipo_numeracion=model1.get_value(iter1,2)
        self.spnFolio.set_value(float(self.determina_folio(cod_tipo_numeracion,self.cod_empresa,self.mes,self.periodo,cod_tipo_comprobante)))
        self.edit=False
        self.on_mnuGuardar_activate(None)
        if not self.response:
            self.edit=True
            
    def on_btnLimpiarComprobante_clicked(self,btn=None):
##        self.edit=False
##        self.centralizado=False
##        self.clear_widgets()
##        self.rut_ficha=None
##        self.num_cuenta=None
##        self.num_linea_editada=None
##        self.model.clear()
##        self.cod_tipo_documento=None
##        self.spnFolio.set_sensitive(True)
##        self.selecciona=True
##        self.on_cmbTipoComprobante_changed(self.cmbTipoComprobante)
##        self.entGlosa.set_text('')
##        self.entTotalDebe.set_text('')
##        self.entTotalHaber.set_text('')
##        self.entDiferencia.set_text('')
##        self.num_linea=1
##        self.entFechaDocumento.children()[0].set_text('')
##        self.alnFicha.set_sensitive(False)
##        self.alnAnalisis.set_sensitive(False)
##        self.entCuenta.set_text('')
        self.on_btnCancelar_clicked(None)
        self.padre.on_btnAnadir_clicked(None)
      #  self.cod_sucursal = None
      #  self.cod_area_funcional = None
      #  self.nom_sucursal = None
      #  self.nom_area_funcional = None
        
      #  self.txtSucursal.set_text("")
      #  self.txtAreaFuncional.set_text("")
        
    def on_btnAbrir_clicked(self,btn=None):        
        if not self.pecPeriodo.get_selected():
            dialogos.dlgAviso(None,"Debe seleccionar el Periodo")
            return
        sqlWhere=''
        model=self.cmbTipoComprobante.get_model()
        sqlWhere=" and c.periodo=%s and c.mes=%s and c.folio_comprobante=%s and c.cod_tipo_comprobante=%s"%(self.periodo,self.mes,int(self.spnFolio.get_text()),"'"+str(model.get_value(self.cmbTipoComprobante.get_active_iter(),0))+"'")
        sql=strCargaDatos %(self.cod_empresa,sqlWhere)
        #print sql
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            dialogos.dlgAviso(None,"No Existe un Comprobante con esos datos")
            return
        cod_comprobante=r[0][1]
        estado=r[0][7]
        descripcion_periodo=r[0][8]
        folio=r[0][2]
        fecha=r[0][3]
        periodo=r[0][13]
        mes=r[0][14]
        tipo_comprobante=r[0][4]
        origen=r[0][6]
        glosa=r[0][9]
        empresa=self.cod_empresa
        estado_periodo=r[0][15]        
        #self.cod_cc=r[0][16]
        sql = strCargaComprobante %(r[0][1])
        print sql
        self.cursor.execute(sql)
        r = self.cursor.fetchall()
        
        if len(r) == 0:
            dialogos.dlgAviso(None,"El comprobante no tiene detalle")
            return
        
        if estado == 'N':         
            dialogos.dlgAviso(None,"El comprobante esta Nulo")            ##desea editarlo de todas formas        
            return             
        
        band = False
##        dlg = dlgComprobante(self.cnx,self.padre,self.cod_empresa,None,self.modelo_cuenta,self.modelo_ficha)
        sql = strCentralizadoComprobante %(cod_comprobante)
        self.cursor.execute(sql)
        r1 = self.cursor.fetchall()
        if len(r1)>0 and r1[0][0]=='S':
            
            dialogos.dlgAviso(None,"El comprobante esta Centralizado,No se puede Modificar")            
            self.mnuGuardar.set_sensitive(False)
            self.btnAnadir.set_sensitive(False)
            self.btnQuitar.set_sensitive(False)
            self.btnLimpiarComprobante.set_sensitive(False)
            self.centralizado=True           
        
        self.edit=True        
        combo_set_active(self.cmbTipoComprobante,tipo_comprobante,0)
        combo_set_active(self.cmbOrigen,origen,0)
        combo_set_active(self.cmbEmpresa,self.cod_empresa,0)
        if estado_periodo=='C':
            self.mnuGuardar.set_sensitive(False)
        if estado == 'V':
            estado='VIGENTE'
        else:
            estado='PENDIENTE'        
        combo_set_active(self.cmbEstado,estado,0)
        self.entPeriodo.set_text(descripcion_periodo)
        self.pecDistribucionCentroCosto.set_cod(self.cod_cc)
        self.periodo = periodo
        self.mes = mes
        self.pecPeriodo.set_selected(True)
        self.cod_empresa = int(self.cod_empresa)
        self.spnFolio.set_value(int(folio))
        self.entGlosa.set_text(glosa)
        y,m,d=int(str(fecha)[:10][0:4]),int(str(fecha)[:10][5:7]),int(str(fecha)[:10][8:10])        
        self.entFecha.set_date(datetime.date(y,m,d))        
            ##carga el detalle
        self.model.clear()
        debe = 0
        haber = 0
        
        for i in r:
            self.model.append(i)            
            debe+=float(i[MONTO_DEBE])
            haber+=float(i[MONTO_HABER])
            num_linea = i[NUM_LINEA]

        self.num_linea = int(num_linea)+1
        self.entTotalDebe.set_text(CMon(debe,0))
        self.entTotalHaber.set_text(CMon(haber,0))
        self.entDiferencia.set_text(CMon((debe-haber),0))
        self.cod_comprobante = int(cod_comprobante) 
        self.cod_empresa_periodo = self.cod_empresa
        self.spnFolio.set_sensitive(False)
        
    def totales_comprobante(self,col=None):
        debe=0.0
        haber=0.0
        linea=0
        for i in self.model:
            debe+=float(i[MONTO_DEBE])
            haber+=float(i[MONTO_HABER])
            if linea<int(i[NUM_LINEA]):
                linea=int(i[NUM_LINEA])
        if self.num_linea_editada not in (None,''):            
            self.num_linea=self.num_linea_editada
            
        else:
            self.num_linea=linea+1
        self.num_linea_editada=None
        self.entTotalDebe.set_text(CMon(debe,0))
        self.entTotalHaber.set_text(CMon(haber,0))
        self.entDiferencia.set_text(CMon(str(debe-haber),0))
        if col!=None:
            return debe,haber
    
    def on_cmbEmpresa_changed(self,widget,*args):        
        self.pecPeriodo.reload(strSelectPeriodo%(int(combo_get_active(self.cmbEmpresa,False))))
        self.cod_empresa = combo_get_active(self.cmbEmpresa,False)
##        self.pecCuentaContable.reload()
##        self.pecCuentaContable.reload(int(combo_get_active(self.cmbEmpresa,False)))
    
    def limpiaDialogo(self):
        if len(self.model)>0:
            self.model.clear()
        self.entGlosa.set_text('')
        self.entPeriodo.set_text('')
        self.spnFolio.set_value(0)
        self.entTotalDebe.set_text('')
        self.entTotalHaber.set_text('')

    def on_entRUT_activate(self, entry=None, event=None):        
        cod_empresa=combo_get_active(self.cmbEmpresa,False)
        self.entRUT.set_text(CRut(self.entRUT.get_text()))
        sql=strFicha %(self.cod_empresa,self.entRUT.get_text().upper())
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            dialogos.dlgAviso(None,"Debe ingresar la Ficha de la Persona")
            self.entNombreFicha.set_text('')
            return
        self.entNombreFicha.set_text(r[0][1])
        self.pecFicha.set_selected(True)
        self.entTipoDocumento.grab_focus()
        
    def on_entRUT_key_press_event(self,entry,event):
        if (event.keyval==gtk.keysyms.Tab) or (event.keyval==gtk.keysyms.Return):
            self.on_entRUT_activate(None)
            
    def on_btnAnular_clicked(self, btn=None):
        if self.edit==False:
            return
        if not VerificaPermiso(self,303):
            return
        sqlWhere=''                
        model=self.cmbTipoComprobante.get_model()
        tipo_comprobante=model.get_value(self.cmbTipoComprobante.get_active_iter(),0)
        sqlWhere+=" and c.cod_tipo_comprobante='%s'"%(tipo_comprobante)
        periodo=self.periodo
        mes=self.mes
        sqlWhere+=" and c.periodo=%s and c.mes=%s"%(periodo,mes) 
        sqlWhere+=" and c.folio_comprobante=%s"%(int(self.spnFolio.get_text()))
        sql=strCargaDatos %(self.cod_empresa,sqlWhere)
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            dialogos.dlgAviso(None,"El Comprobante no se encuentra guardado")
            return
        if btn.get_name()=='btnAnular':
            if r[0][7]=='N':
                dialogos.dlgAviso(None,"El comprobante ya se encuentra anulado")
                return
            try:                
                resp = dialogos.dlgSiNo(None,"Esta seguro que desea Anular el comprobante?","Anular").response
                if resp == gtk.RESPONSE_NO:
                    return
                self.cursor.execute('begin')
                sql = strAnular %(r[0][1])
                self.cursor.execute(sql)
                
                #se elimina del registro de compras
                sql = "DELETE FROM ctb.registro_compras where cod_comprobante="%(r[0][1])
                self.cursor.execute(sql)
                
                self.cursor.execute('commit')                                
                dialogos.dlgAviso(None,"El comprobante ha sido Anulado")
##                self.on_btnLimpiarComprobante_clicked(None)
                self.on_btnCancelar_clicked(None)
            except:
                self.cursor.execute('rollback')
                dialogos.dlgAviso(None,"El comprobante no se puede Anular")
        else:            
            #try:                
            resp = dialogos.dlgSiNo(None,"Esta seguro que desea Eliminar el comprobante?","Eliminar").response
            if resp == gtk.RESPONSE_NO:
                return
            self.cursor.execute('begin')
            
            iter=self.cmbTipoComprobante.get_active_iter()
            model=self.cmbTipoComprobante.get_model()
            cod_tipo_comprobante=model.get_value(iter,0)
    
            if str(cod_tipo_comprobante) == 'E':

                s_gl="select distinct cod_documento from ctb.detalle_comprobante where cod_comprobante=%s "%(r[0][1])
                print s_gl
                self.cursor.execute(s_gl)
                r_s_gl=self.cursor.fetchall()
                
                print "paso 1"
                if len(r_s_gl)>0:
                    print "paso 2"
                    tiene_nulos = True
                    for i in r_s_gl:
                        if(i[0] in ('',None)):
                            tiene_nulos = False
                    
                    if (tiene_nulos == True):
                        for i in r_s_gl:
                            sql = "select contabilizado from egreso where cod_egreso =%s" % (int(i[0]))
                            print sql
                            self.cursor.execute(sql)
                            r_2 = self.cursor.fetchall()
                            if len(r_2) > 0:
                                sql_2 = "update egreso set contabilizado = false where cod_egreso = %s" % (int(i[0]))
                                print sql_2
                                self.cursor.execute(sql_2)
                    else:
                        sql1=strDeleteDetalleComprobante %(r[0][1],self.cod_empresa)
                        print(sql1)
                        self.cursor.execute(sql1)
                        #dialogos.dlgAviso(None,"No se puede actualizar el esado del egreso a NO CENTRALIZADO ya que no existe la referencia al Egreso")
                        #return
                else:
                    sql1=strDeleteDetalleComprobante %(r[0][1],self.cod_empresa)
                    print(sql1)
                    self.cursor.execute(sql1)
            else:    
                sql1=strDeleteDetalleComprobante %(r[0][1],self.cod_empresa)
                print(sql1)
                self.cursor.execute(sql1)
        
            glo="select oid from ctb.registro_compras where cod_comprobante=%s "%(r[0][1])
            self.cursor.execute(glo)
            gl=self.cursor.fetchall()
    
            if len(gl)>0:
            
                v_tip_3 = int(str(gl[0][0]))
    
                seni_3 = """delete from temp_compras where codigo = %s""" % (v_tip_3)
                self.cursor.execute(seni_3)
            
            #se elimina del registro de compras
            sql2 = "delete from ctb.registro_compras where cod_comprobante=%s"%(r[0][1])
            print(sql2)
            self.cursor.execute(sql2)
            
            sql = strDeleteCabeceraComprobante %(r[0][1])
            print(sql)
            self.cursor.execute(sql)
            
            self.cursor.execute('commit')                                
            dialogos.dlgAviso(None,"El comprobante ha sido Eliminado")
            self.on_btnCancelar_clicked(None)
            #except:
            #    self.cursor.execute('rollback')
            #    dialogos.dlgAviso(None,"El comprobante no se puede Eliminar")
                
    def on_entTipoDocumento_activate(self, widget, *args):
        self.spnFolioDocumento.grab_focus()
    def on_treeDetalle_row_activated(self, widget,*args):        
       self.on_btnEditar_clicked(None)
    
    def on_entGlosaDetalle_activate(self,entry=None,event=None):        
        self.on_btnAnadir_clicked(None)
    def on_entFecha_activate(self,widget,*args):
        self.entGlosa.grab_focus()
    def entFechaDocumento_activate(self,widget,*args):
        self.spnDebe.grab_focus()
                
def carga_combo(combo,cnx, sql):      
    cursor=cnx.cursor()
    cursor.execute(sql)
    r=cursor.fetchall()    
    if len(r)==0:
        return    
    modelo=ifd.ListStoreFromSQL(cnx,sql)
    cell = gtk.CellRendererText()
    combo.pack_start(cell, True)
    combo.add_attribute(cell, 'text', 1)            
    combo.set_model(modelo)
    combo.set_active(0)    
   
def combo_get_active(combo,col=False):
    #0: codigo
    #1: descripcion
    model = combo.get_model()
    iter = combo.get_active_iter()   
    if iter is None:
        return
    row = model.get_path(iter)
    codigo = model[row][0]
    descripcion = model[row][1]
    if col == True:
        return codigo,row,model
    else:
        return codigo

def combo_set_active(combo,value,colmodel=None):
    model = combo.get_model()
    j = 0
    for i in model:
        if i[colmodel] == value:
            combo.set_active(j)
            return
        else:
            j+=1

if __name__ == "__main__":
    cnx = connect("host=localhost dbname=gsac user=erafael password=enra1020")
##    cnx.autocommit()
##    sys.excepthook = debugwindow.show
    d = wnComprobante(cnx)
    
    gtk.main()
