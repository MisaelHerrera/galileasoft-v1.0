#!/usr/bin/env python
# -*- coding: UTF8 -*-

try:
    import xml.dom.ext
except:
    print "No se pudo importar libreria xml"
 
import xml.dom.minidom
import datetime
from comunes import CRut,iif
from SimpleGladeApp import SimpleGladeApp
from dialogos import dlgError,dlgAviso
from strSQL import strMaxLibro
from spg import connect as pg_connect
(INDICE,TAGS)=range(2)
factura_electronica=33
#from XML_intro.Reading import *
class class_documento:
    def __init__(self):
        self.__tipo_dte=None
        self.__folio=None
        self.__fecha_emision=None
        self.__forma_pago=None
        self.__fecha_vencimiento=None
        self.__rut_emisor=None
        self.__razon_social=None
        self.__giro_emisor=None        
        self.__cod_act_economica=None
        self.__direccion_origen=None
        self.__comuna_origen=None
        self.__ciudad_origen=None
        self.__rut_receptor=None
        self.__razon_social_receptor=None
        self.__giro_receptor=None
        self.__contacto=None
        self.__direccion_recepcion=None
        self.__comuna_recepcion=None
        self.__ciudad_recepcion=None
        self.__monto_neto=None
        self.__monto_iva=None
        self.__monto_exento=None
        self.__monto_especifico=None
        self.__monto_total=None        
        
    def get_tipo_dte(self):
        return self.__tipo_dte
    def set_tipo_dte(self,valor):
        self.__tipo_dte=valor
    def get_folio(self):
        return self.__folio
    def set_folio(self,valor):
        self.__folio=valor
    def get_fecha_emision(self):
        return self.__fecha_emision
    def set_fecha_emision(self,valor):
        self.__fecha_emision=valor
    def get_forma_pago(self):
        return self.__forma_pago
    def set_forma_pago(self,valor):
        self.__forma_pago=int(valor)
    def get_fecha_vencimiento(self):
        return self.__fecha_vencimiento
    def set_fecha_vencimiento(self,valor):
        self.__fecha_vencimiento=valor
    def get_rut_emisor(self):
        return self.__rut_emisor
    def set_rut_emisor(self,valor):
        self.__rut_emisor=valor
    def get_razon_social(self):
        return self.__razon_social
    def set_razon_social(self,valor):
        self.__razon_social=valor
    def get_giro_emisor(self):
        return self.__giro_emisor
    def set_giro_emisor(self,valor):
        self.__giro_emisor=valor
    def get_cod_act_economica(self):
        return self.__cod_act_economica
    def set_cod_act_economica(self,valor):
        self.__cod_act_economica=valor
    def get_direccion_origen(self):
        return self.__direccion_origen
    def set_direccion_origen(self,valor):
        self.__direccion_origen=valor
    def get_comuna_origen(self):
        return self.__comuna_origen
    def set_comuna_origen(self,valor):
        self.__comuna_origen=valor
    def get_ciudad_origen(self):    
        return self.__ciudad_origen
    def set_ciudad_origen(self,valor):
        self.__ciudad_origen=valor
    def get_rut_receptor(self):
        return self.__rut_receptor
    def set_rut_receptor(self,valor):
        self.__rut_receptor=valor
    def get_razon_social_receptor(self):
        return self.__razon_social_receptor
    def set_razon_social_receptor(self,valor):
        self.__razon_social_receptor=valor
    def get_giro_receptor(self):
        return self.__giro_receptor
    def set_giro_receptor(self,valor):
        self.__giro_receptor=valor
    def get_contacto(self):
        return self.__contacto
    def set_contacto(self,valor):
        self.__contacto=valor
    def get_direccion_recepcion(self):
        return self.__direccion_recepcion
    def set_direccion_recepcion(self,valor):
        self.__direccion_recepcion=valor
    def get_comuna_recepcion(self):
        return self.__comuna_recepcion
    def set_comuna_recepcion(self,valor):
        self.__comuna_recepcion=valor
    def get_cuidad_recepcion(self):
        return self.__ciudad_recepcion
    def set_ciudad_recepcion(self,valor):
        self.__ciudad_recepcion=valor
    def get_monto_neto(self):
        return self.__monto_neto
    def set_monto_neto(self,valor):
        self.__monto_neto=valor
    def get_monto_iva(self):
        return self.__monto_iva
    def set_monto_iva(self,valor):
        self.__monto_iva=valor
    def get_monto_exento(self):
        return self.__monto_exento
    def set_monto_exento(self,valor):
        self.__monto_exento=valor
    def get_monto_especifico(self):
        return self.__monto_especifico
    def set_monto_especifico(self,valor):
        self.__monto_especifico=valor
    def get_monto_total(self):
        return self.__monto_total
    def set_monto_total(self,valor):
        self.__monto_total=valor    
def get_a_document(name="template_dte.xml"):
    return xml.dom.minidom.parse(name)  

def write_to_file(doc, name="test.xml"):
    file_object = open(name, "w")
    xml.dom.ext.PrettyPrint(doc, file_object)
    file_object.close()

def set_dato(doc, dato, valor):
    if valor is None:
        valor = ""
    doc.childNodes[0].getElementsByTagName(dato)[0].childNodes[0].data = valor
    
def datos_dte(doc):
    set_dato(doc, "Folio", "1877")
    set_dato(doc, "FchEmis", "2006-06-06")
    set_dato(doc, "FchVenc", "2006-06-06")
    set_dato(doc, "RUTRecep", "12163625-5")
    set_dato(doc, u'RznSocRecep', u"Ricardo Mendez L")
    set_dato(doc, u'GiroRecep', u"Particular")
    set_dato(doc, u'DirRecep', u"5 1/2 Norte 3514")
    set_dato(doc, u'CmnaRecep', u"Talca")
    set_dato(doc, u'CiudadRecep', u"Talca")
    set_dato(doc, u'MntExe', u"26000000")
    set_dato(doc, u'MntTotal', u"26000000")
    set_dato(doc, u'NmbItem', u"Vivienda FRANCISCA III")
    set_dato(doc, u'DscItem', u"Etapa 67, Lote 1054 Contrato 12595")
    set_dato(doc, u'PrcItem', u"26000000")
    set_dato(doc, u'MontoItem', u"26000000")
    da = doc.childNodes[0].getElementsByTagName('DatoAdjunto')
    da[3].childNodes[0].data = u"26000000"
def get_tags(doc,tags):
    d=None
    try:
        d= doc.getElementsByTagName(tags)[0].childNodes[0].data
    except:
       d=None
    return d
def get_tipodte(doc,documento):
    return documento.set_tipo_dte(int(get_tags(doc,"TipoDTE")))
def get_datos(doc,documento):
##    documento=class_documento()
    documento.set_folio(int(get_tags(doc,"Folio")))
    documento.set_tipo_dte(int(get_tags(doc,"TipoDTE")))
    documento.set_fecha_emision(get_tags(doc,"FchEmis"))
    documento.set_forma_pago(get_tags(doc,"FmaPago"))
    documento.set_fecha_vencimiento(get_tags(doc,"FchVenc"))
    documento.set_rut_emisor(str(CRut(get_tags(doc,"RUTEmisor"))))
    documento.set_razon_social(str(get_tags(doc,"RznSoc")))
    documento.set_giro_emisor(str(get_tags(doc,"GiroEmis")))
    documento.set_cod_act_economica(get_tags(doc,"Acteco"))
    documento.set_direccion_origen(str(get_tags(doc,"DirOrigen")))
    documento.set_comuna_origen(str(get_tags(doc,"CmnaOrigen")))
    documento.set_ciudad_origen(str(get_tags(doc,"CiudadOrigen")))
    documento.set_rut_receptor(str(CRut(get_tags(doc,"RUTRecep"))))
    documento.set_monto_neto(float(get_tags(doc,"MntNeto")))
    documento.set_monto_iva(float(get_tags(doc,"IVA")))
    documento.set_monto_total(float(get_tags(doc,"MntTotal")))    
class ProcesarXML(SimpleGladeApp):
    def __init__(self, cnx=None,padre=None,empresa=2,root="dlgFichero", path="glade/frm_procesarxml.glade", xmldoc="template.xml",tipo_documento_electronico=33, domain=None, **kwargs):
        if cnx!=None:
            self.cnx=cnx
            self.cursor=self.cnx.cursor()
        self.cod_empresa=empresa
        self.xmldoc=xmldoc
        self.documento=class_documento()
        SimpleGladeApp.__init__(self, path, root, domain)
        self.glosa=None
        self.cod_libro=None
        self.cuenta_proveedor=None
        self.cuenta_especifico=None
        self.cuenta_iva=None
        self.tipo_doc_electronico=tipo_documento_electronico        
        self.selecciona_cuentas()
        
    def on_btnProcesarFichero_clicked(self,widget,*args):
        try:
            print "procesando........."
            filename=self.dlgFichero.get_filename()
            if filename=="":
                dlgAviso(None,"Debe seleccionar un fichero")
                return
            docprocs=get_a_document(filename)
            get_tipodte(docprocs,self.documento)
            self.cod_tipo_libro=self.tipo_libro_contable(self.documento.get_tipo_dte())
            if self.cod_tipo_libro==None:
                dlgAviso(None,"El documento no tiene asociado un libro contable")
                return
            if not self.verificar_dte(self.documento):
                dlgAviso(None,"El documento no corresponde a uno valido para este libro")
                return
            get_datos(docprocs,self.documento)
            if self.verificar_receptor(self.documento):
                dlgAviso(None,"El documento pertenece a otra empresa")
                return
            if not self.verificar_documento(self.documento):
                dlgAviso(None,"El documento se encuentra ingresado al sistema")
                return
            
            self.tipo_doc_contable=self.tipo_documento(self.documento.get_tipo_dte())
            
            self.carga_libro()
            self.procesar_documento(self.documento)
            self.on_btnCancelaFichero_clicked(None)
        except:
            dlgError(None,  "Error al Procesar archivo XML",None, trace = True)
    def on_btnCancelaFichero_clicked(self,widget,*args):
        self.dlgFichero.destroy()
    def verificar_receptor(self,documento):
        sql="select * from ctb.empresa where rut_empresa='%s'"%(documento.get_rut_receptor())
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            return True
        return False
    def verificar_dte(self,documento):
        if self.cod_tipo_libro==1:
            if documento.get_tipo_dte()!=33 and documento.get_tipo_dte()!=61:
                return False
        return True
    def procesar_documento(self,documento):
        if not self.existe_ficha(self.documento.get_rut_emisor()):
            self.crear_ficha(self.documento)
        try:
            sql="""INSERT INTO CTB.DOCUMENTO (
                        cod_empresa,
                        num_documento,
                        rut_documento,
                        cod_tipo_documento,
                        fecha_documento,
                        neto,
                        exento,
                        iva,
                        ie,
                        total,                    
                        cod_libro,
                        glosa,
                        electronica) values(%s,%s,'%s',%s,'%s',%s,%s,%s,%s,%s,%s,'%s',%s)"""%(self.cod_empresa,int(self.documento.get_folio()),
                        str(self.documento.get_rut_emisor().upper()),
                        self.tipo_doc_contable,                        
                        self.documento.get_fecha_emision(),
                        self.documento.get_monto_neto(),
                        iif(self.documento.get_monto_exento()==None,0,self.documento.get_monto_especifico()),
                        self.documento.get_monto_iva(),
                        iif(self.documento.get_monto_especifico()==None,0,self.documento.get_monto_especifico()),
                        float(self.documento.get_monto_total()),
                        self.cod_libro,str(self.glosa),
                        True)
            self.cursor.execute(str(sql))
            sql="select max(cod_documento) from ctb.documento"
            self.cursor.execute(sql)
            r=self.cursor.fetchall()
            self.cod_documento=r[0][0]
            num_linea=1
            monto_debe_proveedor=0
            monto_debe_iva=0
            monto_debe_especifico=0
            monto_haber_proveedor=0
            monto_haber_especifico=0
            monto_haber_iva=0
            
            if self.documento.get_tipo_dte()==33:
                monto_debe_proveedor=0
                monto_haber_proveedor=self.documento.get_monto_total()
                monto_debe_iva=self.documento.get_monto_iva()
                monto_haber_iva=0
                if self.documento.get_monto_especifico()!=None:
                    monto_debe_especifico=self.documento.get_monto_especifico()
                    monto_haber_especifico=0
            else:
                monto_debe_proveedor=self.documento.get_monto_total()
                monto_haber_proveedor=0
                monto_debe_iva=0
                monto_haber_iva=self.documento.get_monto_iva()
                if self.documento.get_monto_especifico()!=None:
                    monto_debe_especifico=0
                    monto_haber_especifico=self.documento.get_monto_especifico()
            sql="""INSERT INTO CTB.DETALLE_DOCUMENTO 
                                                    (cod_documento,
                                                    num_linea,
                                                    num_cuenta,
                                                    monto_debe,
                                                    monto_haber,
                                                    glosa,
                                                    folio)
                                            values(%s,%s,'%s',%s,%s,'%s',%s)"""%(self.cod_documento,num_linea,self.cuenta_proveedor,monto_debe_proveedor,monto_haber_proveedor,self.glosa,self.documento.get_folio())
            self.cursor.execute(sql)
            num_linea+=1
            sql="""INSERT INTO CTB.DETALLE_DOCUMENTO 
                                                    (cod_documento,
                                                    num_linea,
                                                    num_cuenta,
                                                    monto_debe,
                                                    monto_haber,
                                                    glosa,
                                                    folio)
                                            values(%s,%s,'%s',%s,%s,'%s',%s)"""%(self.cod_documento,num_linea,self.cuenta_iva,monto_debe_iva,monto_haber_iva,self.glosa,self.documento.get_folio())
            self.cursor.execute(sql)
            if self.documento.get_monto_especifico()==None:
                num_linea+=1
                sql="""INSERT INTO CTB.DETALLE_DOCUMENTO 
                                                        (cod_documento,
                                                        num_linea,
                                                        num_cuenta,
                                                        monto_debe,
                                                        monto_haber,
                                                        glosa,
                                                        folio)
                                                values(%s,%s,'%s',%s,%s,'%s',%s)"""%(self.cod_documento,num_linea,self.cuenta_especifico,monto_debe_especifico,monto_haber_especifico,self.glosa,self.documento.get_folio())
                self.cursor.execute(sql)
            self.cursor.execute('commit')
            
        except:
            self.cursor.execute('rollback')
            dlgError(None,  "Error al Procesar archivo XML",None, trace = True)
        print "procesando"
    def selecciona_cuentas(self):
        try:           
            sql="select cuenta_proveedor,cuenta_iva,cuenta_especifico from ctb.empresa  where cod_empresa=%s"%self.cod_empresa
            self.cursor.execute(sql)
            r=self.cursor.fetchall()
            if r[0][0]==None and r[0][1]==None:
                dlgAviso(None,"Debe parametrizar las cuentas contables asociadas al libro")                
                return
            self.cuenta_proveedor=r[0][0]
            self.cuenta_iva=r[0][1]
            self.cuenta_especifico=r[0][2]
        except:
            print "Error"
    def carga_libro(self):
        sql=strMaxLibro%(self.cod_empresa,self.cod_tipo_libro)
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            return
        
        self.glosa=r[0][0]
        self.cod_libro=r[0][1]      
        
    def existe_documento(self,documento):
        try:
            sql="select * from ctb.documento where rut_documento='%s' and cod_tipo_documento=%s and num_documento=%s"%(documento.get_rut_emisor(),self.tipo_documento(documento.get_tipo_dte()),documento.get_folio())
            self.cursor.execute(sql)
            r=self.cursor.fetchall()
            if len(r)==0:
                return False
            return True
        except:
            dlgError(None,"Error al consultar el documento")
    def verificar_documento(self,documento):
        if self.existe_documento(documento):            
           return False
        return True
        
    def crear_ficha(self,documento):
        try:
            self.cursor.execute('begin')
            sql="insert into ctb.ficha (rut,nombre,tipo_ficha,direccion,cod_empresa) values('%s','%s','%s','%s')"%(documento.get_rut_emisor(),documento.get_razon_social(),self.tipo_ficha(documento.get_tipo_dte()),documento.get_direccion_origen(),self.cod_empresa)
            self.cursor.execute(sql)
            self.cursor.execute('commit')
##            dlgAviso(None,"")
        except:
            self.cursor.execute('rollback')
            dlgError(None,  "Error al Verficar la Ficha",None, trace = False)
    def tipo_ficha(self,tipo_documento):
        tipo_ficha=None
        if tipo_documento==33:
            tipo_ficha='P'
        return tipo_ficha
    def tipo_documento(self,tipo_documento):
        tipo_documento_contable=None
        if tipo_documento==33:
            tipo_documento_contable=3##Factura de Compra            
        if tipo_documento==61:
            tipo_documento_contable=8
        return tipo_documento_contable
    def tipo_libro_contable(self,tipo_documento):
        tipo_libro_contable=None
        if tipo_documento==33:
            tipo_libro_contable=1##Libro de Compra                     
        if tipo_documento==61:
            tipo_libro_contable=1##Libro de Compra                                     
        return tipo_libro_contable
    def existe_ficha(self,rut):
        try:            
            sql="""select * from ctb.ficha where rut='%s' and cod_empresa=%s and tipo_ficha='%s'"""%(rut,self.cod_empresa,self.tipo_ficha(self.tipo_doc_electronico))
            self.cursor.execute(sql)
            r=self.cursor.fetchall()
            if len(r)==0:
                return False
            return True
        except:
            dlgError(None,  "Error al Verficar la Ficha",None, trace = False)
if __name__ == "__main__":    
    dsn = "host=localhost port=22100 dbname=scp user=rmendez password=1020"
    cn = pg_connect(dsn)        
    p=ProcesarXML(cnx=cn)
    p.dlgFichero.run()
