#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnFicha -- formulario de ingreso, modificación, eliminación de Fichas contables del sistema.

# (C)   Fernando San Martín Woerner     2003, 2004
#       fernando@gnome.cl
# (C)   Claudio Salgado Morales         2003, 2004
#       csalgado@galilea.cl

#This file is part of pyGestor.
#
# pyGestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# (C)   José Luis Álvarez Morales     2006, 2007.
#          josealvarezm@gmail.com


from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
import comunes
from strSQL import strSelectFicha

(RUT,
 NOMBRE,
 TIPO_FICHA,
 DESCRIPCION_TIPO_FICHA,
 DIRECCION,
 TELEFONO,
 CODIGO_EMPRESA,
 DESCRIPCION_EMPRESA,
 CODIGO_BANCO,
 NOMBRE_BANCO,
 NUMERO_CUENTA) = range(11)

class wnFicha(GladeConnect):
    def __init__(self, conexion=None, padre=None, empresa=1, root="wnFicha"):
        GladeConnect.__init__(self, "glade/wnFicha.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.cod_empresa = empresa
        self.padre = padre
        if padre is None:
            self.wnFicha.maximize()
            self.frm_padre = self.wnFicha
            self.padre
        else:
            self.frm_padre = padre.frm_padre        
               
        self.pecFicha = completion.CompletionFicha(self.entNombre,
                self.sel_ficha,
                self.cnx,self.cod_empresa)
        self.crea_columnas()
    def sel_ficha(self, completion, model, it):
        self.rut_ficha = model.get_value(it, 1)
        self.it_rut = it
        self.modelo_pec = model
        self.entRUT.set_text(self.rut_ficha)    
    
    def crea_columnas(self):
        columnas = []
        columnas.append([RUT, "R.U.T.", "str"])
        columnas.append([NOMBRE, "Nombre", "str"])
        columnas.append([DESCRIPCION_TIPO_FICHA, "Tipo Ficha", "str"])
        columnas.append([DIRECCION, "Dirección", "str"])
        columnas.append([TELEFONO, "Teléfono", "str"])
        columnas.append([DESCRIPCION_EMPRESA, "Empresa", "str"])
        columnas.append([NOMBRE_BANCO, "Banco", "str"])
        columnas.append([NUMERO_CUENTA, "Número de Cuenta", "str"])
        
        self.modelo = gtk.ListStore(*(11*[str]))
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeFicha)
        
    def on_btnBuscar_clicked(self, btn=None):
        where=''
        where = "where e.cod_empresa = %s" % self.cod_empresa
        
        if self.optFicha.get_active():
            if self.pecFicha.get_selected():
                where += " and f.rut = '%s'" %self.rut_ficha
            if self.entRUT.get_text() not in('',None):
                if not self.pecFicha.get_selected():
                    where += " and f.rut = '%s'" % self.entRUT.get_text()

        if self.optTipo.get_active():
            active = self.cmbTipoFicha.get_active()
            if active > -1:
                modelo = self.cmbTipoFicha.get_model()
                tipo = modelo[active][0]
                where += " and f.tipo_ficha = '%s'" % tipo[0]
        
        sql = strSelectFicha % where
##        print sql
##        self.modelo = ifd.ListStoreFromSQL(self.cnx, sql) 
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        self.modelo.clear()
        for i in r:
            self.modelo.append(i)
        
    def on_opt_toggled(self, tb=None, w=None):
        opts = {self.optTodos:None,
                    self.optFicha:self.hbFicha,
                    self.optTipo:self.cmbTipoFicha}
        
        for i in opts:
            if not opts[i] is None:
                opts[i].set_sensitive(i.get_name() == tb.get_name())
    
    def carga_datos(self):
        sqlWhere=''
        if self.cod_empresa <> None:
            sqlWhere=' where e.cod_empresa = %s'%self.cod_empresa
        self.modelo = ifd.ListStoreFromSQL(self.cnx, strSelectFicha %sqlWhere)
        self.treeFicha.set_model(self.modelo)
    
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgFicha(self.cnx, self.frm_padre, False, self.cod_empresa)
        dlg.editando = False
        dlg.cod_empresa = self.cod_empresa
        band = False
        while not band:
            response = dlg.dlgFicha.run()
            if response == gtk.RESPONSE_OK:
                if dlg.response != None:
                    self.modelo.append(dlg.response)
                    band = True
            else:
                band = True
        dlg.dlgFicha.destroy()
            
    def on_btnQuitar_clicked(self, btn=None):
        model, it = self.treeFicha.get_selection().get_selected()
        if model is None or it is None:
            return
        rut = model.get_value(it, RUT)
        descripcion = model.get_value(it, NOMBRE)
        codigo = model.get_value(it, CODIGO_EMPRESA)
        
        if dialogos.yesno("¿Desea eliminar Ficha <b>%s</b>?\nEsta acción no se puede deshacer!!!"%descripcion, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'rut':rut, 'cod_empresa' : codigo}
            sql = ifd.deleteFromDict('ctb.ficha', llaves)
            
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()[1]
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>\nLa Ficha <b>%s</b>, se encuentra relacionado."%(model.get_value(it,NOMBRE)))
                
    def on_btnPropiedades_clicked(self, btn=None):
        model, it = self.treeFicha.get_selection().get_selected()
        if model is None or it is None:
            return
        dlg = dlgFicha(self.cnx, self.frm_padre, False, self.cod_empresa)
        dlg.cod_empresa = self.cod_empresa
        
        dlg.entRut.set_text(str(model.get_value(it, RUT)))
        dlg.rut_ficha = str(model.get_value(it, RUT))
        dlg.entRut.set_sensitive(False)
        
        dlg.entNombreF.set_text(model.get_value(it, NOMBRE))
        dlg.tipo_ficha = dlg.pecTipoFicha.select(model.get_value(it, DESCRIPCION_TIPO_FICHA),1)
        
        if model.get_value(it, DIRECCION) not in ('', None):
            dlg.entDireccion.set_text(model.get_value(it, DIRECCION))
        
        if model.get_value(it, TELEFONO) not in ('', None):
            dlg.entTelefono.set_text(model.get_value(it, TELEFONO))
            
        sql = "SELECT descripcion_empresa,cod_empresa FROM ctb.empresa WHERE cod_empresa = %s"%(self.cod_empresa) 
        self.cursor.execute(sql)
        r = self.cursor.fetchall()        
        dlg.entEmpresa.set_text(r[0][0])
        dlg.cod_empresa=r[0][1]        
        if model.get_value(it,8) not in ('',None):
            dlg.codigo_banco = dlg.pecBanco.select(model.get_value(it, NOMBRE_BANCO),1)
        
        if model.get_value(it, NUMERO_CUENTA) not in ('',None):
            dlg.entCuenta.set_text(model.get_value(it, NUMERO_CUENTA))
        
        dlg.editando = True
        band=False
        while band==False:
            response = dlg.dlgFicha.run()
            if response == gtk.RESPONSE_OK:
                if dlg.respuesta==True:
                    del model[it]
                    self.modelo.append(dlg.response)
                    band=True
            else:
                band=True        
        dlg.dlgFicha.destroy()
            
    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Ficha")
            
    def on_treeFicha_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked(None)
        
class dlgFicha(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=None, empresa=1):
        GladeConnect.__init__(self, "glade/wnFicha.glade", "dlgFicha")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.editando = editando
        self.entRut.grab_focus()
        self.dlgFicha.show_all()
        
        self.cod_empresa = empresa
        self.tipo_ficha = None
        self.codigo_banco = None
        self.response = None
        
        sql = "SELECT descripcion_empresa,cod_empresa FROM ctb.empresa WHERE cod_empresa = %s"%(self.cod_empresa) 
        self.cursor.execute(sql)
        r = self.cursor.fetchall()        
        self.entEmpresa.set_text(r[0][0])
        self.cod_empresa=r[0][1]     
        self.entEmpresa.set_sensitive(False)
        self.pecTipoFicha = completion.CompletionTipoFicha(self.entFicha,
                self.sel_tipo_ficha,
                self.cnx)
        
        self.pecBanco = completion.CompletionBanco(self.entBanco,
                self.sel_banco,
                self.cnx)
        self.respuesta=False
        self.cod_banco=None
        self.rut_ficha = None
    def sel_tipo_ficha(self, completion, model, it):
        self.tipo_ficha = model.get_value(it,1)
        
    def sel_banco(self, completion, model, it):
        self.codigo_banco = model.get_value(it,1)
        
    def on_btnAceptar_clicked(self, btn=None):
        if self.entRut.get_text() =="  .   .   - ":
            dialogos.error("El campo <b>R.U.T.</b> no puede estar vacío")
            self.entRut.grab_focus()
            return
        #if not comunes.es_rut(self.entRut.get_text()):
        #    dialogos.error("El rut <b>%s</b> no es valido"%self.entRut.get_text().upper())
        #    self.entRut.grab_focus()
        #    return       
        
        if self.entNombreF.get_text() == "":
            dialogos.error("El campo <b>Nombre</b> no puede estar vacío.")
            self.entNombreF.grab_focus()
            return
        
        if not self.pecTipoFicha.get_selected():
            dialogos.error("El campo <b>Tipo Ficha</b> no puede estar vacío")
            self.entFicha.grab_focus()
            return
        if self.entBanco.get_text()<>'':
            if not self.pecBanco.get_selected():
                dialogos.error("El campo <b>Banco</b> debe ser un valor valido o en Blanco")
                return
            if self.entCuenta.get_text() in ('',None,):
                dialogos.error("El campo <b>Cuenta</b> no puede ser vacio")
                return
        try:
            campos = {}
            llaves = {}
            banco='null'
            cod_banco='null'
            cuenta='null'
            direccion='null'
            telefono='null'
            if self.pecBanco.get_selected():            
                banco=self.entBanco.get_text().upper()
                cod_banco= self.codigo_banco
                cuenta = self.entCuenta.get_text().upper()    
            if self.entDireccion.get_text() not in ('',None):
                direccion=self.entDireccion.get_text().upper()
            if self.entTelefono.get_text() not in('',None):
                telefono=self.entTelefono.get_text().upper()
            
            if not self.editando:
                sql=strSelectFicha %("Where f.rut='%s' and f.cod_empresa=%s"%(self.entRut.get_text(),self.cod_empresa))
                #print sql
                self.cursor.execute(sql)
                r=self.cursor.fetchall()
                if len(r)>0:
                    dialogos.error("La ficha se encuentra ingresada")
                    return
                sql="""insert into ctb.ficha (rut,nombre,direccion,telefono,cod_empresa,cod_banco,numero_cuenta, tipo_ficha)values('%s','%s',%s,%s,%s,%s,%s,'%s') """%(
                self.entRut.get_text(),self.entNombreF.get_text().upper(),"'"+str(direccion)+"'","'"+str(telefono)+"'",self.cod_empresa,cod_banco,cuenta,self.tipo_ficha)
                #print sql
                self.cursor.execute(sql)            
            else:            
                sql="""update ctb.ficha set direccion=%s,telefono=%s,cod_banco=%s,numero_cuenta=%s, nombre='%s', tipo_ficha='%s' where cod_empresa=%s and rut='%s'"""%("'"+str(direccion)+"'","'"+str(telefono)+"'",cod_banco,"'"+str(cuenta)+"'",self.entNombreF.get_text().upper(),self.tipo_ficha,self.cod_empresa,self.rut_ficha)
                self.cursor.execute(sql)           
            self.dlgFicha.hide()
            self.respuesta=True
            self.cursor.execute('commit')
            if banco== 'null':            
                banco=''
                cuenta=''
                cod_banco=''
            if direccion=='null':                
                direccion=''
            if telefono=='null':              
                telefono=''
            self.response= [self.rut_ficha,
                                    self.entNombreF.get_text().upper(),
                                    self.tipo_ficha,                                    
                                    self.entFicha.get_text().upper(),
                                    direccion.upper(),
                                    telefono.upper(),
                                    self.cod_empresa,
                                    self.entEmpresa.get_text().upper(),
                                    cod_banco,
                                    banco,
                                    cuenta.upper()]
        except:
            print sys.exc_info()[1]
            dialogos.error("En el sistema ya existen estos datos")
            self.cursor.execute('rollback')
            self.entRut.grab_focus()
               
    def on_btnCancelar_clicked(self, btn=None):
        self.dlgFicha.hide()
        
if __name__ == '__main__':
    cnx = connect("host=localhost dbname=scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    d = wnFicha(cnx)
    
    gtk.main()
