﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
#      test_db.py -- Pruebas para la base de datos
#       (C) 2003, 2004, 2005 Galilea S.A.
#       Fernando San Martín Woerner    snmartin@galilea.cl

#       Gracias a Leonardo Soto por sus tips para el utf8 coding y una linea huacha que habia en plan de cuentas

# This file is part of pyGestor.
#
# pyGestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
from pyPgSQL.PgSQL import connect
import sys, os
try:
    c = connect(database = "template1")
    c.autocommit = True
    print "La base de datos funciona correctamente"
except:
    print sys.exc_info()[1]
    print "La base de datos no está funcionando"
    print "Intente crear el usuario %s con el siguiente comando:" % os.environ['USER']
    print "#createuser %s" % os.environ['USER']
    print 
    print "Necesita tener acceso de superusuario para hacer esta operación"
    print "Posteriormente puede intentar usar test_db.py para probar la conección"
    sys.exit()

if raw_input("Desea crear una base de datos de demostración (S/N): ").upper() == "S":
    cursor = c.cursor()
    try:
        cursor.execute("create database demo")
    except:
        print "La base de datos ya existe!"
    try:
        cursor.execute("create user %s" % os.environ['USER'])
    except:
        print "El usuario ya existe!"
    print "La base de datos se ha creado!"
    f = open(".pygestor.rc", "w")
    f.writelines('[Demo]\n')
    f.writelines('database=demo\n')
    f.writelines('user=%s\n' % os.environ['USER'])
    f.close()
    print "Archivo de configuración creado"