#!/usr/bin/env python
# -*- coding: utf-8 -*-
# wn_familia -- formulario de ingreso, modificacion, eliminacion de familias
# en el sistema de inventario
# (C)   Fernando San Martín Woerner 2006
#       fernando@gnome.org


from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
import comunes
from strSQL import strSelectFamilia

(CODIGO_FAMILIA,
 DESCRIPCION_FAMILIA,
 NUM_CUENTA_COMPRA,
 DESCRIPCION_CUENTA_COMPRA,
 NUM_CUENTA_VENTA,
 DESCRIPCION_CUENTA_VENTA,
 IMPUESTO,
 PORCENTAJE_IMPUESTO,
 COD_EMPRESA) = range(9)

class Familia(GladeConnect):
    
    def __init__(self, conexion=None, empresa=None, padre=None):
        GladeConnect.__init__(self, "inventario/glade/wnfamilia.glade", "wnFamilia")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.cod_empresa = empresa
        if padre is None:
            self.wnFamilia.maximize()
            self.padre = self.wnFamilia
        else:
            self.padre = padre
            
        self.crea_columnas()
        self.wnFamilia.show_all()
        
    def crea_columnas(self):
        columnas = []
        columnas.append([CODIGO_FAMILIA,"Código","str"])
        columnas.append([DESCRIPCION_FAMILIA,"Descripción","str"])
        columnas.append([DESCRIPCION_CUENTA_COMPRA,"Cuenta para compras","str"])
        columnas.append([DESCRIPCION_CUENTA_VENTA,"Cuenta de Ventas","str"])
        columnas.append([IMPUESTO,"Impuesto","str"])
        columnas.append([PORCENTAJE_IMPUESTO,"Porcentaje de Impuesto","str"])
 
        self.modelo = gtk.ListStore(*(9*[str]))
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeFamilia)
        
    def on_btnBuscar_clicked(self, btn=None):
                
        sql = strSelectFamilia 
        
        self.modelo = ifd.ListStoreFromSQL(self.cnx, sql)
        if not self.modelo is None:
            self.treeFamilia.set_model(self.modelo)

    def on_btnQuitar_clicked(self, btn=None):
        selection = self.treeFamilia.get_selection()
        model, it = selection.get_selected()
        cod = model.get_value(it, CODIGO_FAMILIA)
        if dialogos.yesno("Va eliminar la familia %s, esta acción no se puede deshacer\n¿Está seguro?" % cod, self.padre) == gtk.RESPONSE_YES:
            llaves = {'codigo_familia': cod}
            sql = ifd.deleteFromDict("inventario.familia", llaves)
            self.cursor.execute(sql, llaves)
            model.remove(it)
            
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgFamilia(self.cnx, self.cod_empresa, self.padre, False)
        dlg.editando = False
        response = dlg.dlgFamilia.run()
        if response == gtk.RESPONSE_OK:
            self.on_btnBuscar_clicked()

    def on_treeFicha_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()
            
    def on_btnPropiedades_clicked(self, btn=None):
        model, iter = self.treeFamilia.get_selection().get_selected()
        if model is None:
            return
        
        dlg = dlgFamilia(self.cnx, self.cod_empresa, self.padre, False)
        dlg.entCodigo.set_text(model.get_value(iter, CODIGO_FAMILIA))
        dlg.entDescripcion.set_text(model.get_value(iter, DESCRIPCION_FAMILIA))
        dlg.entDireccion.set_text(model.get_value(iter, DIRECCION))
        dlg.entTelefono.set_text(model.get_value(iter, TELEFONO))
        dlg.entTipoFicha.set_text(model.get_value(iter, DESCRIPCION_TIPO_FICHA))
        dlg.cod_tipo_ficha = model.get_value(iter, TIPO_FICHA)
        dlg.cod_banco = model.get_value(iter, COD_BANCO)
        dlg.entBanco.set_text(model.get_value(iter, NOM_BANCO))
        dlg.entCuenta.set_text(model.get_value(iter, NUMERO_CUENTA))
        dlg.editando = True
        response = dlg.dlgFamilia.run()
        if response == gtk.RESPONSE_OK:
            self.rut_ficha = dlg.entRUT.get_text()
            self.optFicha.set_active(True)
            self.on_btnBuscar_clicked()
            
    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.wnFamilia.hide()

class dlgFamilia(GladeConnect):
    
    def __init__(self, conexion=None, empresa=None, padre=None, editando=False):
        GladeConnect.__init__(self, "inventario/glade/wnfamilia.glade", "dlgFamilia")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.cod_empresa = empresa
        self.entDescripcion.grab_focus()
        self.dlgFamilia.set_transient_for(padre)
        self.dlgFamilia.set_size_request(500,300)
        self.editando = editando
        self.pecCuentaCompra = completion.CompletionCuentaContable(self.entCuentaCompra,
                self.sel_cuenta_compra,
                self.cnx,
                self.cod_empresa)
        
        self.pecCuentaVenta = completion.CompletionCuentaContable(self.entCuentaVenta,
                self.sel_cuenta_venta,
                self.cnx,
                self.cod_empresa)
        self.cod_cuenta_compra = None
        self.cod_cuenta_venta = None
        self.dlgFamilia.show_all()
        
    def sel_cuenta_compra(self, completion, model, iter):
        self.cod_cuenta_compra = model.get_value(iter, 1)
        
    def sel_cuenta_venta(self, completion, model, iter):
        self.cod_cuenta_venta = model.get_value(iter, 1)

    def on_chkImpuesto_toggled(self, btn=None):
        self.entImpuesto.set_sensitive(btn.get_active())
        if not btn.get_active():
            self.entImpuesto.set_text("")
        
    def on_btnCancelar_clicked(self, btn=None):
        self.dlgFamilia.hide()
        
    def on_btnAceptar_clicked(self, btn=None):
        
        if self.entDescripcion.get_text() == "":
            dialogos.error("El campo Descripción no debe estar vacio")
            return
        
        if self.cod_cuenta_compra is None:
            dialogos.error("No ha seleccionado las cuentas para las compras")
            return
        
        if self.cod_cuenta_venta is None:
            dialogos.error("No ha seleccionado la cuenta para las ventas")
            return
            
        campos = {}
        llaves = {}
        
        campos['descripcion_familia']  = self.entDescripcion.get_text().upper()
        campos['num_cuenta_compra'] = self.cod_cuenta_compra
        campos['num_cuenta_venta'] = self.cod_cuenta_venta
        campos['cod_empresa'] = self.cod_empresa
        
        if self.chkImpuesto.get_active():
            campos['impuesto'] = 'S'
            campos['porcentaje_impuesto'] = self.entImpuesto.get_text()
        else:
            campos['impuesto'] = 'N'
        
        if not self.editando:
            sql = ifd.insertFromDict("inventario.familia", campos)
        else:
            llaves['codigo_familia'] = self.entCodigo.get_text()
            sql, campos = ifd.updateFromDict("inventario.familia", campos, llaves)
        
        self.cursor.execute(sql, campos)
            
        self.dlgFamilia.hide()
            
if __name__ == "__main__":
    cnx = connect("dbname=ctb")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    w = Familia(cnx, 1)
    
    gtk.main()
            
