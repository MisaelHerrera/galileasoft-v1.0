﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  Cargos
#  (c) Victor Benitez 2004
#  vbenitez@galilea.cl

#Modulos que se requieren para esta clase

import os
import gtk
from SimpleGladeApp import SimpleGladeApp
import SimpleTree
from comunes import *
from spg import connect

glade_dir = "glade"
class frmCargo(GladeConnect):
    def __init__(self, c=None, p =None, e=1,  root="frmCargo", domain=None, **kwargs):
        self.cnx = c
        self.cursor = self.cnx.cursor()
        path="frm_cargo.glade"
        path = os.path.join(glade_dir, path)
        SimpleGladeApp.__init__(self, path, root, domain, **kwargs)
    #def __init__(self, c = None, p = None, e = None, s = (1, "Talca",4)):
#        GladeConnect.__init__(self, "glade/frm_cargo.glade")

        

        if p:
            self.padre = p
            self.ventana = None
        else:
            self.padre = self
            self.ventana = self.frmCargo
            self.frmCargo.maximize()
            self.frmCargo.show()
#        self.genera_columnas_cargo()
#        self.genera_columnas_funcion()
        
        self.cod_cargo_new = None
    def new(self):
        self.genera_modelo_funcion()
        self.genera_modelo_cargo()
        self.carga_tree_cargo()
        self.carga_tree_funcion()
    def genera_modelo_cargo(self):

        self.modelo_cargo = gtk.ListStore(str,  # 0 descripcion
                                                str)    # 1 codigo

        indice=[]
        indice.append([0,"Descripcion","str"])        
        SimpleTree.GenColsByModel(self.modelo_cargo, indice,self.treeCargo)
        
    def carga_tree_cargo(self):
        sql = """select
                        nom_cargo,
                        cod_cargo::text
                from
                        ctb.cargo
                order by
                        nom_cargo
        """
        self.cursor.execute(sql)
        r = self.cursor.fetchall()
        self.modelo_cargo.clear()
        for i in r :
            self.modelo_cargo.append(i)

    def on_treeCargo_row_activated(self, tree, row, column):
        self.txtDescripcion.set_text(self.modelo_cargo[row][0])
        self.cod_cargo_new=self.modelo_cargo[row][1]
        self.fila_editada = int(row[0])
        self.carga_tree_cargo_funcion()

    def fixed_toggled(self, cell, path, model):
        model = self.modelo_funcion
        iter = model.get_iter((int(path),))
        fixed = model.get_value(iter, 0)

        fixed = not fixed
        model.set(iter, 0, fixed)

    def genera_modelo_funcion(self):

        self.modelo_funcion = gtk.ListStore(bool,               # 0 activa
                                                str,    # 1 descripcion
                                                str)    # 2 codigo
        
        indice=[]
        indice.append([0,"OK","bool"])        
        indice.append([1,"Descripcion","str"])        
        SimpleTree.GenColsByModel(self.modelo_funcion, indice,self.treeFuncion)

    def carga_tree_funcion(self):
        sql = """select
                        'False'::bool,
                        nom_funcion,
                        cod_funcion::text
                from
                        ctb.funcion
                order by
                        nom_funcion
        """
        self.cursor.execute(sql)
        r = self.cursor.fetchall()
        self.modelo_funcion.clear()
        for i in r :
            self.modelo_funcion.append(i)

    def carga_tree_cargo_funcion(self):
        sql = """select
                        case when cf.cod_cargo is null then 'False'::bool else 'True'::bool end as act,
                        f.nom_funcion,
                        f.cod_funcion::text
                from
                        ctb.funcion f left join ctb.cargo_funcion cf on
                                f.cod_funcion = cf.cod_funcion and cf.cod_cargo = %s
                order by
                        nom_funcion
        """ % (self.cod_cargo_new)
        self.cursor.execute(sql)
        r = self.cursor.fetchall()
        self.modelo_funcion.clear()
        for i in r :
            self.modelo_funcion.append(i)
    def on_btnNuevo_clicked(self, btn = None):
        self.cod_cargo_new = None
        self.fila_editada = None
        self.txtDescripcion.set_text("")
        self.carga_tree_cargo()
        self.carga_tree_funcion()

    def on_btnCerrar_clicked(self, btn = None):

        if self.ventana == self.vboxCargo:
            self.on_exit()
        else:
#            del self.padre.wins[self.etiqueta]

            self.padre.remove_tab(self.etiqueta)

            return 1


    def on_btnQuitar_clicked(self, btn = None):
        return 
        if self.cod_cargo_new == None:
            dlgAviso(self.ventana, "Se esperaba un Cargo")
            return

        sql = "delete from cargo where cod_cargo = %s " % (self.cod_cargo_new)
        self.cursor.execute(sql)

        dlgAviso(self.ventana,"El Cargo se ha eliminado correctamente")
        self.on_btnNuevo_clicked()

    def on_btnActualizar_clicked(self, btn = None):
        if not VerificaPermiso(self,402):
            return
        if self.cod_cargo_new == None :
            dlgAviso(self.ventana, "Se esperaba un Cargo")
            return

        sql = """update
                        comun.cargo
                set
                        desc_cargo = '%s'
                where
                        cod_cargo = %s
                """ % (self.txtDescripcion.get_text(),
                        self.cod_cargo_new)
        try:
            self.cursor.execute("begin")
#            self.cursor.execute(sql.upper())

            self.cursor.execute("delete from ctb.cargo_funcion where cod_cargo = %s"%(self.cod_cargo_new))
            for i in self.modelo_funcion:
                if i[0]:
                    sql = "insert into ctb.cargo_funcion (cod_cargo, cod_funcion) values (%s,%s)" % (self.cod_cargo_new,i[2])
                    self.cursor.execute(sql)
        except:
            self.cursor.execute("rollback")
            self.dlgError(self.ventana,"La parametrizacion del cargo no se ha Modificado")
            return
        dlgAviso(self.ventana, "La parametrizacion del Cargo ha sido actualizado correctamente")
        self.cursor.execute("commit")
        self.on_btnNuevo_clicked()

    def on_btnAnadir_clicked(self, btn = None):
        return
        if self.txtDescripcion.get_text() == "" :
            dlgAviso(self.ventana,"Ingrese el Nombre del Cargo")
            return

        if self.cod_cargo_new <> None :
            dlgAviso(self.ventana,"Debe presionar Actualizar")
            return

        sql = """
                insert into comun.cargo
                (desc_cargo
                )
                values
                ('%s')
        """ % (self.txtDescripcion.get_text())
        try:
            self.cursor.execute("begin")
            self.cursor.execute(sql.upper())
            self.cursor.execute("select currval('comun.cargo_cod_cargo_seq'::text)")
            r = self.cursor.fetchall()
            self.cod_cargo_new = r[0][0]
            for i in self.modelo_funcion:
                if i[0]:
                    sql = "insert into cargo_funcion (cod_cargo, cod_funcion) values (%s,%s)" % (self.cod_cargo_new,i[2])
                    self.cursor.execute(sql)
        except:
            self.cursor.execute("roolback")
            dlgError(self.ventana,"Cargo","No se ha Creado el Cargo")
            return

        self.cursor.execute("commit")
        dlgAviso(self.ventana,"El cargo ha sid guardado correctamente")
        self.on_btnNuevo_clicked()


if __name__ == '__main__':

    w = muestra_splash()

    w.set_title("Orden de Compra")

    w.set_decorated(False)
    w.set_position(gtk.WIN_POS_CENTER)

    w.show_all()

    while gtk.events_pending(): gtk.main_iteration()

    cnx = connect("192.168.0.11:22100:scp:fernando:1020")
    cnx.autocommit = 'True'

    while gtk.events_pending(): gtk.main_iteration()

    v = frmCargo(cnx)
    #v.frmOrdenDeCompra.show()

    w.destroy()

    gtk.main()
