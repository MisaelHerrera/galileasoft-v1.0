#!/usr/bin/env python
# -*- coding: utf-8 -*-
# wnConsultaCuenta.py  -- Permite realizar consulta de cuentas contables.
# (C)   Fernando San Martín Woerner 2003, 2004
#       snmartin@galilea.cl

#This file is part of pyGestor.
#
# pyGestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.

# (C)    Ricardo Mendez Lorca        2006
#           rmendez@galilea.cl

# (C)   José Luis Álvarez Morales    2006, 2007.
#           josealvarezm@gmail.com

from GladeConnect import GladeConnect
import completion
import SimpleTree
from spg import connect
import datetime
from time import *
import dialogos
import debugwindow
import sys
import gtk
import os
from string import *
from comunes import *
from strSQL import strSelectTipoDocContable
from strSQL import strSelectEmpresa
from strSQL import strSelectEmpresaNivel
from codificacion import CISO
from reportlab.pdfbase.ttfonts import *
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import A4, landscape

(CUENTA_CONTABLE,
FICHA,
DEBITOS,
CREDITOS,
SALDO) = range(5)

class wnConsultaCuenta(GladeConnect):
    def __init__(self, conexion=None, padre=None, empresa=1, root="wnConsultaCuenta"):
        GladeConnect.__init__(self, "glade/wnConsultaCuenta.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.cod_empresa = empresa
        self.Empresa()
        self.padre = padre
        
        if padre is None:
            self.wnConsultaCuenta.maximize()
            self.frm_padre = self.wnConsultaCuenta
            self.padre
        else:
            self.frm_padre = self.padre.frm_padre
        y=datetime.date.today().year
        (m,d)=(1,1)
        
        self.entFechaDesde.set_text(CDateLocal(datetime.date(y,m,d)))
        self.entFechaHasta.set_text(CDateLocal(datetime.date.today()))        
        self.cod_cuenta_desde = None
        self.cod_cuenta_hasta = None
        self.cod_ficha_desde = None
        self.cod_ficha_hasta = None
        
        self.crea_columnas()
        
        
        
        self.pecCuentaContableD = completion.CompletionCuentaContable(self.entCDesde,
                self.sel_cod_cuenta_desde,
                self.cnx,
                self.cod_empresa)
         
        self.pecCuentaContableH = completion.CompletionCuentaContable(self.entCHasta,
                self.sel_cod_cuenta_hasta,
                self.cnx,
                self.cod_empresa)
        
        self.pecFichaD = completion.CompletionFicha(self.entFDesde,
                self.sel_cod_ficha_desde,
                self.cnx,
                self.cod_empresa)
  
        self.pecFichaH = completion.CompletionFicha(self.entFHasta,
                self.sel_cod_ficha_hasta,
                self.cnx,
                self.cod_empresa)
        self.entRut.connect('activate',self.on_entRut_activate)
        self.entRutHasta.connect('activate',self.on_entRutHasta_activate)
        self.tipos_documentos=[]
        self.cod_tipos_documentos=[]
        self.tipos_documentos.append(0)
        self.modelo_documento.set_value(self.modelo_documento.get_iter_first(),0,True)
        
        if padre is None:
            self.wnConsultaCuenta.show_all()
        else:
            self.vboxCuenta.show_all()
    def Empresa(self):
        Where='Where e.cod_empresa=%s'%(self.cod_empresa)
        sql=strSelectEmpresa %(Where)
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            dialogos.error("No hay Empresa seleccionada!!!")
            return        
        self.descripcion_empresa=r[0][1]
        self.rut_empresa=r[0][9]
        self.direccion_empresa=r[0][10]

    def sel_cod_cuenta_desde(self, completion, model, it):
        self.cod_cuenta_desde = model.get_value(it, 1)
        self.txtNumCuentaDesde.set_text(self.cod_cuenta_desde)    
        
    def sel_cod_cuenta_hasta(self, completion, model, it):
        self.cod_cuenta_hasta = model.get_value(it, 1)
        self.txtNumCuentaHasta.set_text(self.cod_cuenta_hasta)
        
    def sel_cod_ficha_desde(self, completion, model, it):
        self.cod_ficha_desde = model.get_value(it, 1)
        self.entRut.set_text(self.cod_ficha_desde)
##        self.entFDesde.set_text(self.cod_ficha_desde)
    
    def sel_cod_ficha_hasta(self, completion, model, it):
        self.cod_ficha_hasta = model.get_value(it, 1)
        self.entRutHasta.set_text(self.cod_ficha_hasta)
##        self.entFHasta.set_text(self.cod_ficha_hasta)
    
    def crea_columnas(self):
        columnas = []
        columnas.append([CUENTA_CONTABLE, "Cuenta Contable", "str"])
        columnas.append([FICHA, "Ficha", "str"])
        columnas.append([DEBITOS, "Debitos", "str"])
        columnas.append([CREDITOS, "Creditos", "str"])
        columnas.append([SALDO, "Saldo", "str"])
        
        self.modelo = gtk.ListStore(*(5*[str]))
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeConsulta)
        
        indices=[]
        indices.append([0,"OK","bool",self.fixed_toggled])
        indices.append([2,"Tipo Documento","str"])
        self.modelo_documento = gtk.ListStore(bool, str, str)
        SimpleTree.GenColsByModel(self.modelo_documento, indices, self.treeDocumento)
    
        sql = strSelectTipoDocContable
        self.cursor.execute(sql)
        r = self.cursor.fetchall()
        iter = self.modelo_documento.append()
        self.modelo_documento.set(iter,0,False,2,"TODOS")
        for i in r:
            self.modelo_documento.append(i)
    
    def fixed_toggled(self,cell, path, model):
        iter = model.get_iter((int(path),))
        fixed = model.get_value(iter, 0)
        fixed = not fixed
        if model.get_value(iter,2)=='TODOS' and fixed==True:
            self.tipos_documentos=[]
            self.cod_tipos_documentos=[]
            for i in self.modelo_documento:
                it=self.modelo_documento.get_iter((int(i.path[0]),))
                if i[2]!='TODOS' and i[0]==True:
                    self.modelo_documento.set(it,0,False)            
        else:
            it=self.modelo_documento.get_iter_first()
            if self.modelo_documento.get_value(it,0)==True:
                self.tipos_documentos.remove(0)
            self.modelo_documento.set(it,0,False)                        
        self.modelo_documento.set(iter, 0, fixed)
        if fixed==True:            
            self.tipos_documentos.append(int(path[0]))
            if model.get_value(iter,2)!='TODOS':
                self.cod_tipos_documentos.append(int(model.get_value(model.get_iter((int(path),)),1)))
        else:
            if int(path[0]) in [(i) for i in self.tipos_documentos]:
                self.tipos_documentos.remove(int(path[0]))
            if int(path[0]) in [(i) for i in self.cod_tipos_documentos]:
                self.cod_tipos_documentos.remove(int(path[0]))
        print "hola"
    def on_txtNumCuentaDesde_activate(self,widget,*args):
        self.cod_cuenta_desde=None
        if widget.get_text().replace(".","") not in("",None) and widget.get_text().replace(".","").isdigit():
            cuenta=self.mascara_niveles(self.txtNumCuentaDesde)
            self.pecCuentaContableD.set_cod(cuenta)
            if self.cod_cuenta_desde!=None:
                widget.set_text(cuenta)
                self.txtNumCuentaHasta.grab_focus()
            else:
                self.entCDesde.set_text('')
        else:
            self.entCDesde.set_text('')
    def on_txtNumCuentaHasta_activate(self,widget,*args):
        self.cod_cuenta_hasta=None
        if widget.get_text().replace(".","") not in("",None) and widget.get_text().replace(".","").isdigit():
            cuenta=self.mascara_niveles(self.txtNumCuentaHasta)
            self.pecCuentaContableH.set_cod(cuenta)
            if self.cod_cuenta_hasta!=None:
                widget.set_text(cuenta)
            else:
                self.entCHasta.set_text('')
        else:
            self.entCHasta.set_text('')
    def on_entRutHasta_activate(self,widget,*args):
        self.cod_ficha_hasta=None
        if len(widget.get_text().replace(" ",""))>0:
            self.pecFichaH.set_cod(widget.get_text().upper())
            if self.cod_ficha_hasta==None:                
                self.entFHasta.set_text('')
    def on_entRut_activate(self,widget,*args):
        self.cod_ficha_desde=None
        if len(widget.get_text().replace(" ",""))>0:
            self.pecFichaD.set_cod(widget.get_text().upper())
            if self.cod_ficha_desde!=None:
                self.entRutHasta.grab_focus()
            else:
                self.entFDesde.set_text('')
    def on_btnHasta_clicked(self,widget,*args):
        fecha=dlgCalendario(None,self.entFechaHasta.get_text())
        if fecha.date!=None:
            self.entFechaHasta.set_text(fecha.date)
    def on_btnFechaDesde_clicked(self,widget,*args):
        fecha=dlgCalendario(None,self.entFechaDesde.get_text())
        if fecha.date!=None:
            self.entFechaDesde.set_text(fecha.date)
    def mascara_niveles(self,entrycuenta):
        if not entrycuenta.get_text().replace(".","").isdigit():
            entrycuenta.grab_focus()
            return         
        sql=strSelectEmpresaNivel %(self.cod_empresa,'')
        self.cursor.execute(sql)
        r=self.cursor.fetchall()
        if len(r)==0:
            return         
        mask=''
        digitos=0
        cuenta_contable=entrycuenta.get_text().replace(".","")
        inicio=0
        digitos=0
        for i in r:              
            mask+=cuenta_contable[inicio:i[0]+inicio]+"."        
            inicio=inicio+i[0]
        return mask[0:len(mask)-1]
    def on_btnImprimir_clicked(self, btn=None):
        if len(self.cod_tipos_documentos)==0 and self.modelo_documento.get_value(self.modelo_documento.get_iter_first(),0)!=True:
            dialogos.dlgAviso(None,"Debe seleccionar al menos un Tipo de Documento!!!")
            return
        sqlWhere=''
        tipos=''
        if self.modelo_documento.get_value(self.modelo_documento.get_iter_first(),0)!=True:           
            for i in self.cod_tipos_documentos:
                tipos+=str(i)+","
            sqlWhere=" and dc.cod_doc_contable in(%s)"%(tipos)
            sqlWhere=sqlWhere.replace(",)",")")

        fecha_desde = CDateDB(self.entFechaDesde.get_text())
        fecha_hasta = CDateDB(self.entFechaHasta.get_text())
        cuenta_desde = self.txtNumCuentaDesde.get_text()
        cuenta_hasta = self.txtNumCuentaHasta.get_text()
        ficha_desde = self.entRut.get_text().upper()
        ficha_hasta = self.entRutHasta.get_text().upper()

        Rango_Cuenta = cuenta_desde +"-"+ cuenta_hasta
        Rango_Periodo= fecha_desde  +"-"+ fecha_hasta
        Rango_Ficha = ficha_desde  +"-"+ ficha_hasta
        query_ficha =""
        query_cuenta=""
        query = ""

        if len(cuenta_desde) > 0:
            query_cuenta= " and (cc.num_cuenta >= '"+cuenta_desde+"')"

        if len(cuenta_hasta) > 0:
            if cuenta_desde > cuenta_hasta:
                dialogos.dlgAviso(None,"La Cuenta de Inicio debe ser menor que la Cuenta Final. (tambien puede dejar sin tope la Cuenta Final).")
                return 0
            else:
                query_cuenta = query_cuenta+" and (cc.num_cuenta <= '"+cuenta_hasta+"')"

        if len(ficha_desde.replace(" ","").replace(".","").replace("-","")) > 0:
            query_ficha= " and (rut >= '"+ficha_desde+"')"
        if len(ficha_hasta.replace(" ","").replace(".","").replace("-","")) > 0:
            if ficha_desde > ficha_hasta:
                dialogos.dlgAviso(None,"La Ficha de Inicio debe ser menor que la Ficha Final. (tambien puede dejar sin tope Ficha Final).")
                return 0
            else:
                query_ficha = query_ficha+" and (rut <= '"+ficha_hasta+"')"

        query = "select cc.num_cuenta,cc.descripcion_cuenta,cc.tipo_cuenta, dc.rut, f.nombre,td.descripcion_documento, \
                dc.folio_documento,dc.fecha_doc::text,dc.monto_debe::int8,dc.monto_haber::int8,(sd.saldo_debe - sd.saldo_haber)::int8 as saldo ,c.folio_comprobante,c.fecha::text,c.cod_tipo_comprobante \
                from  \
                ctb.detalle_comprobante dc join ctb.comprobante c on c.cod_comprobante = dc.cod_comprobante and c.fecha between '"+ fecha_desde +"' and '"+ fecha_hasta +"' " + query_ficha +" \
                join ctb.cuenta_contable cc on cc.num_cuenta = dc.num_cuenta "+ query_cuenta + "  and cc.cod_empresa=c.cod_empresa and c.cod_empresa="+self.cod_empresa+"               \
                join ctb.tipo_doc_contable td on td.cod_doc_contable = dc.cod_doc_contable  " +sqlWhere+"\
                left join ctb.ficha f on f.rut = dc.rut and f.cod_empresa = dc.cod_empresa\
                left join (select  \
                                rut, cod_comprobante, cod_doc_contable, folio_documento, sum(monto_debe) as saldo_debe, sum(monto_haber) as saldo_haber  \
                                from ctb.detalle_comprobante where true "+ query_ficha +"\
                                group by \
                               rut,cod_comprobante,cod_doc_contable,folio_documento) sd \
                    on sd.rut = dc.rut \
                    and sd.cod_comprobante = dc.cod_comprobante \
                    and sd.cod_doc_contable = dc.cod_doc_contable \
                    and sd.folio_documento = dc.folio_documento where c.estado = 'V'"

        if self.optFichaCuenta.get_active():
            query = query + " order by  f.rut, cc.num_cuenta, dc.folio_documento"
            fichacuenta=True

        else :
            query = query + " order by cc.num_cuenta,f.rut,dc.folio_documento "
            fichacuenta=False

        try :
##            print query
            self.cursor.execute(query)
            r2 = self.cursor.fetchall()
            
        except:
            dialogos.dlgAviso(None,sys.exc_info()[1])
            self.cod_comprobante =0
            return 1

        if len(r2) == 0:
            dialogos.dlgAviso(None,"No se ha encontrado informacion en los rangos seleccionados!!!")
            return 0

        if self.optConSaldo.get_active():
            con_saldo=True
        else:
            con_saldo=False

        def cabecera_consulta_cluenta(self, pc, page_num):

            pc.setFont('Courier', 12)
            w = pc.stringWidth("Cuenta Corriente",'Courier', 12)
            pc.drawString(275 - w/2,800, "Cuenta Corriente")

            pc.drawString(30,782,self.descripcion_empresa)

            pc.drawString(370,782,"Pagina: %s"%(zfill(int(page_num), 10)))

            pc.drawString(30,770,self.rut_empresa)

            pc.drawString(30,758,self.direccion_empresa)

            t = map(str, localtime())
            pc.drawString(370,758,"Fecha : " +  zfill(t[2],2) + "/" + zfill(t[1],2) + "/" + t[0])

            pc.setFont('Courier', 7)
            l = "Documento Referencia".center(40) + "   " + "Movimiento".center(30) + "   " + "Saldo".center(15) + "   " + "Comprobante".ljust(29) + "  "
            pc.drawString(30,748,l)
            pc.line(30, 756, 550, 756)
            strAUX = ""
            strAUX = strAUX + "Documento".ljust(20) + " "
            strAUX = strAUX + "Folio".ljust(8) + " "
            strAUX = strAUX + "Fecha".ljust(10) + " "
            strAUX = strAUX + "Debe".ljust(15) + " "
            strAUX = strAUX + "Haber".ljust(16) + "   "
            strAUX = strAUX + "Referencia".ljust(15) + " "
            strAUX = strAUX + "Tipo".ljust(10) + "  "
            strAUX = strAUX + "Folio".ljust(5) + "  "
            strAUX = strAUX + "Fecha       ".ljust(20)
            pc.drawString(30,739,strAUX)
            pc.line(30, 738, 550, 738)
            pc.setFont('Courier', 7)

        def put_linea(pc,linea,strLinea,pagina):
            if linea > 70:
                c.showPage()
                pagina = pagina + 1
                cabecera_consulta_cluenta(self,pc,pagina)
                linea = 7

            pc.drawString(30,738 -((linea -6)*9) ,strLinea)
            return linea +1,pagina

        def imprime_lineas_pendientes(c,linea,pagina,por_imprimir):
            for i in por_imprimir:
                linea,pagina = put_linea(c,linea,i,pagina)
            por_imprimir = []
            return linea,pagina,por_imprimir

        ficha_activa=str(r2[0][3])
        cuenta_activa=str(r2[0][0])
        nombre_activo=str(r2[0][4])
        no_cta_activa=str(r2[0][1])
        linea=20
        comp = -1       #comprobante inicial de la pagina
        ncomp = 0       # numero de comprobante para la impresion de totales
        ucomp = 0
        total_debe = 0      # total general de debe
        total_haber = 0     # y haber
        total_debe_ficha = 0
        total_haber_ficha = 0
        Tot_cuenta_haber =0
        Tot_cuenta_debe=0
        por_imprimir = []

        c = Canvas('consulta_cuenta.pdf')
        c.setPageCompression(0)
        pagina = 1
        cabecera_consulta_cluenta(self,c,pagina)
        linea = 7       # linea actual en la página

        s = ""
        for j in range(123):
            s = s + "_"

        if fichacuenta:
            l = "Ficha : " + ficha_activa.ljust(70) + nombre_activo.ljust(90)
            por_imprimir.append(l)

            l ="Cuenta : " + cuenta_activa.ljust(70) + str(no_cta_activa)
            por_imprimir.append(l)

        else :
            info = "Cuenta : " + cuenta_activa.ljust(70) + str(no_cta_activa).ljust(90)+"\n"
            por_imprimir.append(info)

            l ="Ficha : " + ficha_activa.ljust(70) + nombre_activo.ljust(90)
            por_imprimir.append(l)

        Tot_cuenta =0

        for i in r2:
            i = map(str, i)     #todo el registro es mapeado como string
            if  (ficha_activa!=i[3]) or (cuenta_activa!=i[0]):
                total_saldo =total_debe-total_haber
                if fichacuenta:
                    total_debe_ficha += total_debe
                    total_haber_ficha += total_haber
                    if (cuenta_activa!=i[0]):
                        total_saldo =total_debe-total_haber
                        l ="TOTAL CUENTA           : ".rjust(42) +str(total_debe).ljust(16)+str(total_haber).ljust(17)+str(total_saldo).ljust(15)
                        por_imprimir.append(l)
                        por_imprimir.append(s)

                        if (con_saldo) and (total_saldo != 0):
                            linea,pagina,por_imprimir = imprime_lineas_pendientes(c,linea,pagina,por_imprimir)
                        elif (con_saldo == False):
                            linea,pagina,por_imprimir = imprime_lineas_pendientes(c,linea,pagina,por_imprimir)
                        else:
                            por_imprimir = []
                            total_debe_ficha -= total_debe
                            total_haber_ficha -= total_haber

                        total_debe=0
                        total_haber=0

                    if (ficha_activa!=i[3]):
                        total_saldo_ficha =total_debe_ficha-total_haber_ficha
                        l ="TOTAL FICHA           : ".rjust(42) +str(total_debe_ficha).ljust(16)+str(total_haber_ficha).ljust(17)+str(total_saldo_ficha).ljust(15)
                        por_imprimir.append(l)
                        total_debe_ficha=0
                        total_haber_ficha=0
                        por_imprimir.append(s)
                        if (con_saldo) and (total_saldo_ficha != 0):
                            linea,pagina,por_imprimir = imprime_lineas_pendientes(c,linea,pagina,por_imprimir)
                        elif (con_saldo == False):
                            linea,pagina,por_imprimir = imprime_lineas_pendientes(c,linea,pagina,por_imprimir)
                        else:
                            por_imprimir = []
                        total_debe=0
                        total_haber=0
                else :
                    if (ficha_activa!=i[3]):
                        l ="TOTAL FICHA            : ".rjust(42) +str(total_debe).ljust(16)+str(total_haber).ljust(17)+str(total_debe-total_haber).ljust(15)
                        por_imprimir.append(l)
                        Tot_cuenta_debe += total_debe
                        Tot_cuenta_haber += total_haber
                        total_saldo_ficha = total_debe - total_haber
                        por_imprimir.append(s)
                        if (con_saldo) and (total_saldo_ficha != 0):
                            linea,pagina,por_imprimir = imprime_lineas_pendientes(c,linea,pagina,por_imprimir)
                        elif (con_saldo==False):
                            linea,pagina,por_imprimir = imprime_lineas_pendientes(c,linea,pagina,por_imprimir)
                        else:
                            por_imprimir =[]
                            Tot_cuenta_debe  -= total_debe
                            Tot_cuenta_haber -= total_haber
                        total_debe=0
                        total_haber=0

                    if (cuenta_activa!=i[0]):
                        Saldo =Tot_cuenta_debe-Tot_cuenta_haber
                        l ="TOTAL CUENTA           : ".rjust(42) +str(Tot_cuenta_debe).ljust(16)+str(Tot_cuenta_haber).ljust(17)+str(Tot_cuenta_debe-Tot_cuenta_haber).ljust(15)
                        por_imprimir.append(l)
                        Tot_cuenta_debe=0
                        Tot_cuenta_haber=0
                        total_debe_ficha=0
                        total_haber_ficha=0
                        por_imprimir.append(s)
                        total_debe=0
                        total_haber=0
                        if (con_saldo) and (Saldo != 0):
                            linea,pagina,por_imprimir = imprime_lineas_pendientes(c,linea,pagina,por_imprimir)

                        elif (con_saldo==False):
                            linea,pagina,por_imprimir = imprime_lineas_pendientes(c,linea,pagina,por_imprimir)

                        else:
                            por_imprimir =[]

                ficha_activa=i[3]
                cuenta_activa=i[0]
                no_cta_activa=i[1]
                nombre_activo=i[4]

                l ="Cuenta : " + cuenta_activa.ljust(70) + str(no_cta_activa)
                por_imprimir.append(l)

                l = "Ficha : " + ficha_activa.ljust(70) + nombre_activo.ljust(90)
                por_imprimir.append(l)

            Dcto=str(i[5])
            Folio=str(i[6])
            Fecha_Doc=str(i[7])
            Debe=int(i[8])
            Haber=int(i[9])
            Saldo=Debe-Haber
            Folio_comp=str(i[11])
            Fecha_Comp=str(i[12])
            Tipo_Comprobante=str(i[13])
            total_debe = total_debe + Debe
            total_haber = total_haber + Haber
            strAUX = Dcto[0:19].ljust(19) + "  "
            strAUX = strAUX + Folio[0:7].ljust(7) + "  "
            strAUX = strAUX + Fecha_Doc[0:10].rjust(10) + "  "
            strAUX = strAUX + str(Debe)[0:14]. ljust(14) + "  "
            strAUX = strAUX + str(Haber)[0:15].ljust(15) + "  "
            strAUX = strAUX + str(Saldo)[0:16].ljust(16) + "  "
            strAUX = strAUX + Tipo_Comprobante.ljust(9) + "  "
            strAUX = strAUX + Folio_comp.ljust(5) + "  "
##            Fecha_Comp = datetime.date.today()
            strAUX = strAUX + Fecha_Comp.ljust(10) + "  "#+ "\n"
##            strAUX = strAUX + ctb_formato_fecha_local(Fecha_Comp).ljust(10) + "  "+ "\n"
            por_imprimir.append(strAUX)

        total_saldo =total_debe-total_haber
        total_debe_ficha += total_debe
        total_haber_ficha += total_haber
        total_saldo_ficha =total_debe_ficha-total_haber_ficha
        Tot_cuenta_debe += total_debe
        Tot_cuenta_haber += total_haber
        if (not(con_saldo)) :
            if fichacuenta:

                l = "TOTAL CUENTA          : ".rjust(42) +str(total_debe).ljust(16)+str(total_haber) .ljust(17)+str(total_saldo).ljust(15)
                por_imprimir.append(l)

                l ="TOTAL FICHA           : ".rjust(42) +str(total_debe_ficha).ljust(16)+str(total_haber_ficha).ljust(17)+str(total_saldo_ficha).ljust(15)
                por_imprimir.append(l)

            else:
                Tot_cuenta_haber =Tot_cuenta_haber +total_haber_ficha
                Tot_cuenta_debe=Tot_cuenta_debe +total_debe_ficha

                l ="TOTAL FICHA           : ".rjust(42) +str(total_debe).ljust(16)+str(total_haber).ljust(17)+str(total_debe-total_haber).ljust(15)#+"\n"
                por_imprimir.append(l)

                l ="TOTAL CUENTA           : ".rjust(42) +str(total_debe_ficha).ljust(16)+str(total_haber_ficha).ljust(17)+str(total_saldo_ficha).ljust(15)#+"\n"
                por_imprimir.append(l)

        elif    ((total_saldo_ficha != 0) and fichacuenta) or  (not(fichacuenta) and (Tot_cuenta_debe-Tot_cuenta_haber!=0)):
            if fichacuenta:
                l = "TOTAL CUENTA          : ".rjust(42) +str(total_debe).ljust(16)+str(total_haber) .ljust(17)+str(total_saldo).ljust(15)#+"\n"
                por_imprimir.append(l)

                l = "TOTAL FICHA           : ".rjust(42) +str(total_debe_ficha).ljust(16)+str(total_haber_ficha).ljust(17)+str(total_saldo_ficha).ljust(15)#+"\n"
                por_imprimir.append(l)

            else:
                l = "TOTAL FICHA           : ".rjust(42) +str(total_debe).ljust(16)+str(total_haber).ljust(17)+str(total_debe-total_haber).ljust(15)+"\n"
                por_imprimir.append(l)
                l ="TOTAL CUENTA          : ".rjust(42) +str(Tot_cuenta_debe).ljust(16)+str(Tot_cuenta_haber).ljust(17)+str(Tot_cuenta_debe - Tot_cuenta_haber).ljust(15)+"\n"
                por_imprimir.append(l)

        if ((not fichacuenta  and (con_saldo) and (Tot_cuenta_debe - Tot_cuenta_haber != 0)) or (fichacuenta  and (con_saldo) and (total_saldo_ficha != 0)) ):
            linea,pagina,por_imprimir = imprime_lineas_pendientes(c,linea,pagina,por_imprimir)

        elif (con_saldo==False):
            linea,pagina,por_imprimir = imprime_lineas_pendientes(c,linea,pagina,por_imprimir)

        else:
            por_imprimir =[]

        c.showPage()
        c.save()
        Abre_pdf("consulta_cuenta.pdf")

    def on_btnBuscar_clicked(self, btn=None):
        if len(self.cod_tipos_documentos)==0 and self.modelo_documento.get_value(self.modelo_documento.get_iter_first(),0)!=True:
            dialogos.dlgAviso(None,"Debe seleccionar al menos un Tipo de Documento!!!")
            return
        
        sqlWhere=''
        tipos=''
        if self.modelo_documento.get_value(self.modelo_documento.get_iter_first(),0)!=True:           
            for i in self.cod_tipos_documentos:
                tipos+=str(i)+","
            sqlWhere=" and dc.cod_doc_contable in(%s)"%(tipos)
            sqlWhere=sqlWhere.replace(",)",")")
                
        fecha_desde = CDateDB(self.entFechaDesde.get_text())
        fecha_hasta = CDateDB(self.entFechaHasta.get_text())
        
        cuenta_desde = self.txtNumCuentaDesde.get_text()
        cuenta_hasta = self.txtNumCuentaHasta.get_text()
        ficha_desde = self.entRut.get_text().upper()
        ficha_hasta = self.entRutHasta.get_text().upper()
        
        def cabecera_consulta_cluenta(self, pc, page_num):
            pc.setFont('Courier', 12)
            w = pc.stringWidth("Resumen Cuenta Corriente",'Courier', 12)
            pc.drawString(275 - w/2,800, "Resumen Cuenta Corriente")

            pc.drawString(30,782,self.descripcion_empresa)

            pc.drawString(370,782,"Pagina: %s"%(zfill(int(page_num), 10)))

            pc.drawString(30,770,self.rut_empresa)

            pc.drawString(30,758,self.direccion_empresa)

            t = map(str, localtime())
            pc.drawString(370,758,"Fecha : " +  zfill(t[2],2) + "/" + zfill(t[1],2) + "/" + t[0])

            pc.setFont('Courier', 7)
            l = "  "
            pc.drawString(30,748,l)
            pc.line(30, 756, 550, 756)
            strAUX = ""
            strAUX = strAUX + "Num. Cuenta".ljust(20) + " "
            strAUX = strAUX + "Cuenta Contable".ljust(25) + " "
            strAUX = strAUX + "Ficha".ljust(13) + " "
            strAUX = strAUX + " ".ljust(25) + " "
            strAUX = strAUX + "Debe".rjust(10) + "   "
            strAUX = strAUX + "Haber".rjust(10) + " "
            strAUX = strAUX + "Saldo".rjust(10) + "  "

            pc.drawString(30,739,strAUX)
            pc.line(30, 738, 550, 738)
            pc.setFont('Courier', 7)

        def put_linea(pc,linea,strLinea,pagina):
            if linea > 70:
                c.showPage()
                pagina = pagina + 1
                cabecera_consulta_cluenta(self,pc,pagina)
                linea = 7

            pc.drawString(30,738 -((linea -6)*9) ,strLinea)
            return linea +1,pagina

        query_cuenta=""
        if len(cuenta_desde) > 0:
            query_cuenta= " and (dc.num_cuenta >= '"+cuenta_desde+"')"

        if len(cuenta_hasta) > 0:
            if cuenta_desde > cuenta_hasta:
                dialogos.dlgAviso(None,"La Cuenta de Inicio debe ser menor que la Cuenta Final. (tambien puede dejar sin tope la Cuenta Final).")
                return 0

            else:
                query_cuenta= query_cuenta +" and (dc.num_cuenta <= '"+cuenta_hasta+"')"
        query_ficha =""
        if len(ficha_desde.replace(".","").replace(" ","").replace("-","")) > 0:
            query_ficha= " and (dc.rut >= '"+ficha_desde+"')"

        if len(ficha_hasta.replace(".","").replace(" ","").replace("-","")) > 0:
            if ficha_desde > ficha_hasta:
                dialogos.dlgAviso(None,"La Ficha de Inicio debe ser menor que la Ficha Final. (tambien puede dejar sin tope Ficha Final).")
                return 0
            else:
                query_ficha= query_ficha +" and (dc.rut <= '"+ficha_hasta+"')"
        Empresa=" and e.cod_empresa=%s"%self.cod_empresa
        query =  "select \
                         cc.descripcion_cuenta\
                        , case when f.nombre is Null then '' else f.nombre end as nombre \
                        ,sum(dc.monto_debe::int8)::int8 as debe,\
                        sum(dc.monto_haber::int8)::int8 as haber, \
                        case when f.rut is Null then '' else f.rut end as rut ,cc.num_cuenta\
                from \
                        ctb.detalle_comprobante dc join ctb.comprobante c on c.cod_comprobante = dc.cod_comprobante and c.fecha between '"+ fecha_desde +"' and '"+ fecha_hasta +"' "+ query_ficha + "  join ctb.empresa e on e.cod_empresa=dc.cod_empresa "+Empresa+"\
                        join ctb.cuenta_contable cc on cc.num_cuenta = dc.num_cuenta "+ query_cuenta +" and cc.cod_empresa=e.cod_empresa\
                        join ctb.tipo_doc_contable td on td.cod_doc_contable = dc.cod_doc_contable " +sqlWhere+" \
                        left join ctb.ficha f on f.rut = dc.rut and f.cod_empresa = dc.cod_empresa and e.cod_empresa=f.cod_empresa\
                group by  \
                        cc.descripcion_cuenta\
                        ,f.nombre,f.rut,cc.num_cuenta"

        
        if self.optFichaCuenta.get_active():
            query = query + " order by  f.rut, cc.num_cuenta"
        else:
            query = query + " order by cc.num_cuenta, f.rut"

        if self.optConSaldo.get_active():
            con_saldo=1
        else:
            con_saldo=0

        try:
            self.cursor.execute(query)
            r = self.cursor.fetchall()

        except:
            dialogos.dlgAviso(None, "Ha ocurrido un error al consultar la base de datos.X")
        modelo = self.treeConsulta.get_model()
        iter = modelo.get_iter_first()
        if not modelo:
            strcod = unicode('Cuenta Contable','latin-1')
            column = gtk.TreeViewColumn(strcod.encode('utf-8'), gtk.CellRendererText(), text=0)
            self.treeConsulta.append_column(column)

            strcod = unicode('Ficha','latin-1')
            column = gtk.TreeViewColumn(strcod.encode('utf-8'), gtk.CellRendererText(), text=0)
            self.treeConsulta.append_column(column)

            lbl = unicode('Debitos','latin-1')
            column = gtk.TreeViewColumn(lbl.encode('utf-8'), gtk.CellRendererText(), text=2)
            self.treeConsulta.append_column(column)

            lbl = unicode('Creditos','latin-1')
            column = gtk.TreeViewColumn(lbl.encode('utf-8'), gtk.CellRendererText(), text=3)
            self.treeConsulta.append_column(column)

            column = gtk.TreeViewColumn('Saldo', gtk.CellRendererText(), text=4)
            self.treeConsulta.append_column(column)

        store = gtk.ListStore(gobject.TYPE_STRING,
                                  gobject.TYPE_STRING,
                                  gobject.TYPE_STRING,
                                  gobject.TYPE_STRING,
                                  gobject.TYPE_STRING)

        iter = modelo.get_iter_first()
        strLinea = ""
        if len(r) == 0:
            dialogos.dlgAviso(None,"No se ha encontrado informacion en los rangos seleccionados")
            return 0

        c = Canvas('resumen_cuenta_corriente.pdf')
        c.setPageCompression(0)
        pagina = 1
        cabecera_consulta_cluenta(self,c,pagina)
        linea = 7       # linea actual en la página

        strLinea = r[0][5].ljust(20) + " " + r[0][0][:25].ljust(25) + " " + r[0][4].ljust(13) + " " + r[0][1][:25].ljust(25) + " "
        rut = r[0][4]
        num_cuenta = r[0][5]
        cta=   r[0][0]
        ficha= r[0][1]
        debe=  0
        haber= 0
        tdebe =0
        thaber =0
        cta = unicode(cta, 'latin-1')
        cta.encode('utf-8')
        ficha= unicode(ficha, 'latin-1')
        ficha.encode('utf-8')
        for i in r:
            cta_i= str(i[0])
            cta_i = unicode(cta_i, 'latin-1')
            cta_i.encode('utf-8')
            ficha_i = str(i[1])
            ficha_i= unicode(ficha_i, 'latin-1')
            ficha_i.encode('utf-8')
            if cta == cta_i and ficha == ficha_i:
                debe = debe + i[2]
                haber= haber + i[3]
                saldo= debe-haber
            else :
                if con_saldo==1:
                    if saldo!=0:
                        iter = store.append()
                        store.set(iter,0,cta,1,ficha,2,debe,3,haber,4,debe-haber)
                        strLinea = strLinea + str(debe).rjust(10) + "   "
                        strLinea = strLinea + str(haber).rjust(10) + " "
                        strLinea = strLinea + str(debe - haber).rjust(10) + "  "
                        linea,pagina = put_linea(c,linea,strLinea,pagina)
                        tdebe +=debe
                        thaber += haber
                else:
                    iter = store.append()
                    store.set(iter,0,cta,1,ficha,2,debe,3,haber,4,debe-haber)
                    strLinea = strLinea + str(debe).rjust(10) + "   "
                    strLinea = strLinea + str(haber).rjust(10) + " "
                    strLinea = strLinea + str(debe - haber).rjust(10) + "  "
                    linea,pagina = put_linea(c,linea,strLinea,pagina)
                    tdebe +=debe
                    thaber += haber

                strLinea = i[5].ljust(20) + " " + i[0][:25].ljust(25) + " " + i[4].ljust(13) + " " + iif(i[1] in('',None),' ',i[1][:25].ljust(25)) + " "
                cta =i[0]
                ficha=i[1]
                debe = i[2]
                haber = i[3]
                saldo = debe - haber
                cta = unicode(cta, 'latin-1')
                cta.encode('utf-8')
                ficha= unicode(ficha, 'latin-1')
                ficha.encode('utf-8')
        if con_saldo==1:
            if saldo!=0:
                iter = store.append()
                store.set(iter, 0, cta, 1,ficha, 2,debe,3,haber,4,debe-haber)
                strLinea = strLinea + str(debe).rjust(10) + "   "
                strLinea = strLinea + str(haber).rjust(10) + " "
                strLinea = strLinea + str(debe - haber).rjust(10) + "  "
                linea,pagina = put_linea(c,linea,strLinea,pagina)
                tdebe +=debe
                thaber += haber

        else:
            iter = store.append()
            store.set(iter, 0, cta, 1,ficha, 2,debe,3,haber,4,debe-haber)
            strLinea = strLinea + str(debe).rjust(10) + "   "
            strLinea = strLinea + str(haber).rjust(10) + " "
            strLinea = strLinea + str(debe - haber).rjust(10) + "  "
            linea,pagina = put_linea(c,linea,strLinea,pagina)
            tdebe +=debe
            thaber += haber

        strLinea = "".ljust(20) + " " + "TOTALES".ljust(25) + " " + "".ljust(13) + " " + "".ljust(25) + " "
        strLinea = strLinea + str(tdebe).rjust(10) + "   "
        strLinea = strLinea + str(thaber).rjust(10) + " "
        strLinea = strLinea + str(tdebe - thaber).rjust(10) + "  "
        linea,pagina = put_linea(c,linea,strLinea,pagina)
        self.treeConsulta.set_model(store)
        c.showPage()
        c.save()
        Abre_pdf("resumen_cuenta_corriente.pdf")
        return 0

    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Consulta_de_Cuentas")
   
if __name__ == "__main__":
    cnx = connect("dbname=scc")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    d = wnConsultaCuenta(cnx)
    
    gtk.main()
