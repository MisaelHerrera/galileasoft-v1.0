#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnTipoFicha -- formulario de ingreso, modificación, eliminación de Tipo de Fichas contables.

# (C)   José Luis Álvarez Morales 2006, 2007.
#           josealvarezm@gmail.com

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
import comunes
from strSQL import strSelectTipoFicha

(CODIGO,
 DESCRIPCION) = range(2)

class wnTipoFicha(GladeConnect):
    def __init__(self, conexion=None, padre=None, root="wnTipoFicha"):
        GladeConnect.__init__(self, "glade/wnTipoFicha.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnTipoFicha.maximize()
            self.frm_padre = self.wnTipoFicha
        else:
            self.frm_padre = self.padre.frm_padre
        
        self.crea_columnas()
        self.carga_datos()
            
    def crea_columnas(self):
        columnas = []
        columnas.append([CODIGO, "Código", "str"])
        columnas.append([DESCRIPCION, "Tipo de Ficha", "str"])
        
        self.modelo = gtk.ListStore(str, str)
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeTipoFicha)
        
    def carga_datos(self):
        self.cursor.execute(strSelectTipoFicha)
        r=self.cursor.fetchall()
        self.modelo.clear()
        for i in r:
            self.modelo.append(i)
    
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgTipoFicha(self.cnx, self.frm_padre, False)
        dlg.editando = False
        band = False
        while not band:
            response = dlg.dlgTipoFicha.run()
            if response == gtk.RESPONSE_OK:
                if dlg.response != None:
                    self.modelo.append(dlg.response)
                    band = True
            else:
                band = True
        dlg.dlgTipoFicha.destroy()
            
    def on_btnQuitar_clicked(self, btn=None):
        model, it = self.treeTipoFicha.get_selection().get_selected()
        if model is None or it is None:
            return
        descripcion = model.get_value(it, DESCRIPCION)
        codigo = model.get_value(it, CODIGO)
        
        if dialogos.yesno("¿Desea eliminar el Tipo de Ficha contable <b>%s</b>?\nEsta acción no se puede deshacer!!!"%descripcion, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'cod_tipo_ficha':codigo}
            sql = ifd.deleteFromDict("ctb.tipo_ficha",llaves)
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>\nEl tipo de Ficha <b>%s</b> se encuentra relacionado."%model.get_value(it,DESCRIPCION))
                
    def on_btnPropiedades_clicked(self, btn=None):
        model, it = self.treeTipoFicha.get_selection().get_selected()
        if model is None or it is None:
            return
        dlg = dlgTipoFicha(self.cnx, self.frm_padre, False)
        dlg.entCodigo.set_text(model.get_value(it, CODIGO))
        dlg.entDescripcion.set_text(model.get_value(it, DESCRIPCION))
        
        dlg.entCodigo.set_sensitive(False)
        
        dlg.editando = True
        band = False
        while not band:
            response = dlg.dlgTipoFicha.run()
            if response == gtk.RESPONSE_OK:
                if dlg.response != None:
                    del model[it]
                    self.modelo.append(dlg.response)
                    band=True
            else:
                band=True
        dlg.dlgTipoFicha.destroy()
            
    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Tipo_Ficha")
            
    def on_treeTipoFicha_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()
        
class dlgTipoFicha(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=None):
        GladeConnect.__init__(self, "glade/wnTipoFicha.glade", "dlgTipoFicha")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.entCodigo.grab_focus()
        self.response = None
        self.dlgTipoFicha.show()
        self.editando = editando
        
    def on_btnAceptar_clicked(self, btn=None):

        if self.entCodigo.get_text() == "" or len(self.entCodigo.get_text()) > 1:
            dialogos.error("El campo <b>Código</b> no puede estar vacío o exceder de un caracter.")
            return
        
        if self.entDescripcion.get_text() == "" or len(self.entDescripcion.get_text().replace(" ",""))==0:
            dialogos.error("El campo <b>Descripción Tipo Ficha</b> no puede estar vacío.")
            return
        
        campos = {}
        llaves = {}
        
        campos['descripcion_tipo_ficha'] = self.entDescripcion.get_text().upper()
                
        if not self.editando:
            sql ="""select cod_tipo_ficha,descripcion_tipo_ficha from ctb.tipo_ficha where cod_tipo_ficha='%s' or descripcion_tipo_ficha='%s'"""%(self.entDescripcion.get_text().upper(),self.entCodigo.get_text().upper())
            self.cursor.execute(sql)
            r=self.cursor.fetchall()
            if len(r)>0:
                dialogos.error("tipo de Ficha <b>%s</b> se encuentra registrada"%self.entDescripcion.get_text().upper())
                return
            campos ['cod_tipo_ficha'] = self.entCodigo.get_text().upper()
            sql = ifd.insertFromDict("ctb.tipo_ficha", campos)
        else:
            llaves['cod_tipo_ficha'] = self.entCodigo.get_text()
            sql, campos = ifd.updateFromDict("ctb.tipo_ficha", campos, llaves)
        
        try:
            self.cursor.execute(sql,campos)
            self.response=True
            self.dlgTipoFicha.hide()
            
            self.response = [self.entCodigo.get_text().upper(),
                                    self.entDescripcion.get_text().upper()]
                                    
        except:
            print sys.exc_info()[1]
            cod_tipo_ficha = self.entCodigo
            dialogos.error("En la Base de Datos ya existe el TIpo Ficha <b>%s</b>."%campos['cod_tipo_ficha'])
            self.entCodigo.grab_focus()
            
    def on_btnCancelar_clicked(self, btn=None):
        self.dlgTipoFicha.hide()
        
if __name__ == "__main__":
    cnx = connect("host=localhost dbname=scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    d = wnTipoFicha(cnx)
    
    gtk.main()
        
    
