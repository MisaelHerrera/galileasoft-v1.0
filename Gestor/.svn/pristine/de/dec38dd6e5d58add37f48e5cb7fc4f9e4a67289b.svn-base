#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnMoneda -- formulario de ingreso, modificación, eliminación de Monedas contables del sistema.

# (C)   Fernando San Martín Woerner 2004
#       snmartin@galilea.cl

#This file is part of pyGestor.
#
# pyGestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# (C)   José Luis Álvarez Morales   2006, 2007.
#           josealvarezm@gmail.com

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
from strSQL import strSelectMoneda

(CODIGO,
 DESCRIPCION_MONEDA,
 SIMBOLO, 
 DECIMALES, 
 ESPACIOS) = range(5)

class wnMoneda(GladeConnect):
    def __init__(self, conexion=None, padre=None, root="wnMoneda"):
        GladeConnect.__init__(self, "glade/wnMoneda.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnMoneda.maximize()
            self.frm_padre = self.wnMoneda
        else:
            self.frm_padre = self.padre.frm_padre 
            
        self.crea_columnas()
        self.carga_datos()

    def crea_columnas(self):
        self.modelo=gtk.ListStore(str,str,str, str, str)
        columnas = []
        columnas.append([CODIGO,"Codigo","str"])
        columnas.append([DESCRIPCION_MONEDA,"Descripción Moneda", "str"])
        columnas.append([SIMBOLO,"Símbolo Moneda","str"])              
        columnas.append([DECIMALES,"Decimales","int"])              
        columnas.append([ESPACIOS,"","str"])
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeMoneda)
        self.carga_datos()
    def carga_datos(self):
        self.cursor.execute(strSelectMoneda)
        r=self.cursor.fetchall()
        self.modelo.clear()
        for i in r:
            self.modelo.append(i)
        
        
    def on_btnAnadir_clicked(self, btn=None, data=None):
        dlg = dlgMoneda(self.cnx, self.frm_padre, False)
        dlg.editando= False
        band = False
        while not band:
            response = dlg.dlgMoneda.run()
            if response == gtk.RESPONSE_OK:
                if dlg.resp ==True:
                    self.modelo.append(dlg.response)
                    band = True
            else:
                band = True
        dlg.dlgMoneda.destroy()
                
    def on_btnQuitar_clicked(self, btn=None):
        model, it = self.treeMoneda.get_selection().get_selected()
        if model is None or it is None:
            return
        descripcion = model.get_value(it, DESCRIPCION_MONEDA)
        codigo = model.get_value(it, CODIGO)
        
        if dialogos.yesno("¿Desea eliminar el Tipo de Moneda Contable <b>%s</b>?\nEsta acción no se puede deshacer!!!"%descripcion, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'cod_moneda': codigo}
            sql = ifd.deleteFromDict("ctb.moneda",llaves)
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>\nEl tipo de Moneda <b>%s</b> se encuentra relacionado.")
                
    def on_btnPropiedades_clicked(self, btn=None):
        model, it = self.treeMoneda.get_selection().get_selected()
        if model is None or it is None:
            return
        
        dlg = dlgMoneda(self.cnx, self.frm_padre, False)
        dlg.entCodigo.set_text(model.get_value(it,CODIGO))
        dlg.entDescripcion.set_text(model.get_value(it, DESCRIPCION_MONEDA))
        dlg.entSimbolo.set_text(model.get_value(it, SIMBOLO))
        dlg.entSimbolo.set_text(model.get_value(it, SIMBOLO))
        dlg.entDecimales.set_text(model.get_value(it, DECIMALES))
        
        dlg.editando= True 
        band=False
        while band==False:        
            response = dlg.dlgMoneda.run()
            if response == gtk.RESPONSE_OK:
                if dlg.resp==True:
                    del model[it]
                    self.modelo.append(dlg.response)
                    band=True
            else:
                band=True
        dlg.dlgMoneda.destroy()
        
    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Moneda")
        
    def on_treeMoneda_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()
        
class dlgMoneda(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=False,codigo=1):
        GladeConnect.__init__(self, "glade/wnMoneda.glade", "dlgMoneda")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.entDescripcion.grab_focus()
        self.dlgMoneda.show()
        self.response = None
        self.editando=editando
        self.resp=False
    def on_btnAceptar_clicked(self,btn=None):
        if self.entDescripcion.get_text() == "" or len(self.entDescripcion.get_text().replace(" ",""))==0:
            dialogos.error("El campo <b>Descripción</b> no debe estar vacío!!!")
            self.entDescripcion.grab_focus()
            return
        
        if self.entSimbolo.get_text() == "" or len(self.entSimbolo.get_text()) > 10 or len(self.entSimbolo.get_text().replace(" ",""))==0:
            dialogos.error("El campo <b>Símbolo</b> no debe estar vacío o exceder de los 10 caracteres.")
            self.entSimbolo.grab_focus()
            return
    
        campos ={}
        llaves = {}
            
        campos['descripcion_moneda'] = self.entDescripcion.get_text().upper()
        campos['simbolo_moneda'] = self.entSimbolo.get_text().upper()
        campos['decimales'] = self.entDecimales.get_text().upper()
            
        if not self.editando:
            sql="""select descripcion_moneda,  simbolo_moneda from ctb.moneda where descripcion_moneda='%s' or simbolo_moneda='%s'"""%(self.entDescripcion.get_text().upper(),self.entSimbolo.get_text().upper())
            
            self.cursor.execute(sql)
            r=self.cursor.fetchall()
            if len(r)>0:
                dialogos.error("La moneda o el simbolo se encuentran ingresadas.")
                return
            self.cursor.execute(" select case when max(cod_moneda) is Null then 1 else max(cod_moneda)+1  end from ctb.moneda")
            r = self.cursor.fetchall()
            self.entCodigo.set_text(str(r[0][0]))
            campos['cod_moneda'] = self.entCodigo.get_text()
            sql = ifd.insertFromDict("ctb.moneda",campos)
        else:
            llaves['cod_moneda'] = self.entCodigo.get_text()
            sql, campos = ifd.updateFromDict("ctb.moneda", campos, llaves)
        self.resp=True
        
        try:
            self.cursor.execute(sql,campos)
            self.dlgMoneda.hide()
        
            self.response = [self.entCodigo.get_text(),
                                    self.entDescripcion.get_text().upper(),
                                    self.entSimbolo.get_text().upper(), 
                                    self.entDecimales.get_text(), 
                                    '']
                
        except:
            print sys.exc_info()[1]
            descripcion_moneda = self.entDescripcion
            dialogos.error("En el sistema ya existe la Moneda Contable <b>%s</b>."%campos['descripcion_moneda'])
            self.entDescripcion.grab_focus()

    def on_btnCancelar_clicked(self, btn=None):
        self.dlgMoneda.hide()

if __name__ == "__main__":
    cnx = connect("host=localhost dbname = scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    w = wnMoneda(cnx)
    
    gtk.main()
