#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnNivelCuenta -- formulario de ingreso, eliminación y edición, de los distintos tipos de Niveles de Cuentas contables.
# (C)    José Luis Álvarez Morales 2006, 2007.
#           jalvarez@galilea.cl

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
from strSQL import strSelectNivelCuenta

(CODIGO,
  CODIGO_EMPRESA,
  DESCRIPCION_EMPRESA,
  DIGITOS) = range(4) 

class wnNivelCuenta (GladeConnect):
    def __init__ (self, conexion = None, padre= None, root="wnNivelCuenta"):
        GladeConnect.__init__ (self, "glade/wnNivelCuenta.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnNivelCuenta.maximize()
            self.frm_padre = self.wnNivelCuenta
            self.padre
        else:
            self.frm_padre = self.padre.frm_padre

        self.crea_columnas()
        self.carga_datos()

    def crea_columnas (self):
        columnas = []
        columnas.append([CODIGO, "Código", "str"])
        columnas.append([DESCRIPCION_EMPRESA,"Nombre Empresa", "str"])
        columnas.append([DIGITOS, "Dígitos", "str"])
    
        self.carga_datos()
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeNivelCuenta)
        
    def carga_datos(self):
        self.modelo = ifd.ListStoreFromSQL(self.cnx, strSelectNivelCuenta)
        self.treeNivelCuenta.set_model(self.modelo)
    
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgNivelCuenta(self.cnx, self.frm_padre, False)
        dlg.editando =False
        response = dlg.dlgNivelCuenta.run()
        if response == gtk.RESPONSE_OK:
            self.carga_datos()
            
    def on_btnQuitar_clicked (self, btn=None):
        model, it = self.treeNivelCuenta.get_selection().get_selected()
        if model is None or it is None:
            return
        descripcion = model.get_value(it, DESCRIPCION_EMPRESA)
        codigo = model.get_value(it, CODIGO)
        
        if dialogos.yesno("Desea eliminar los dígitos, <b>%s</b>\nEsta acción no se puede deshacer!!!"% descripcion, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'cod_nivel':codigo}
            sql = ifd.deleteFromDict("ctb.nivel_cuenta", llaves)
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()[1]
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>\nLos Dígitos de Nivel de Cuenta<b>%s</b>, se encuentra relacionado.")
    
    def on_btnPropiedades_clicked (self, btn=None):
        model, it = self.treeNivelCuenta.get_selection().get_selected()
        if model is None or it is None:
            return
        dlg = dlgNivelCuenta(self.cnx, self.frm_padre, False)
        dlg.entCodigo.set_text(model.get_value(it, CODIGO))
        dlg.codigo_empresa = dlg.pecEmpresa.select(model.get_value(it, DESCRIPCION_EMPRESA),1)
        dlg.spnDigitos.set_text(model.get_value(it, DIGITOS))
        
        dlg.editando= True
        response = dlg.dlgNivelCuenta.run()
        if response == gtk.RESPONSE_OK:
            self.modelo.clear()
            self.carga_datos()
        
    def on_btnCerrar_clicked (self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Nivel_Cuenta")
    
    def on_treeNivelCuenta_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()


class dlgNivelCuenta(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=None):
        GladeConnect.__init__(self, "glade/wnNivelCuenta.glade", "dlgNivelCuenta")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.editando = editando
        self.codigo_empresa = None
        self.entCodigo.grab_focus()
        self.dlgNivelCuenta.show_all()
        
        self.pecEmpresa = completion.CompletionEmpresa(self.entEmpresa,
                self.sel_empresa,
                self.cnx)

    def sel_empresa(self, completion, model, it):
        self.codigo_empresa = model.get_value(it,1)
        
    def on_btnAceptar_clicked(self, btn=None):
        if self.entCodigo.get_text() == "":
            dialogos.error("El Campo <b>Código</b> no puede estar vacío.")
        
        if self.entEmpresa.get_text() == "":
            dialogos.error ("El Campo <b>Nombre Empresa</b> no puede estar vacío.")
            return
        
        if self.spnDigitos.get_text() <= '0':
            dialogos.error ("El Campo <b>Dígitos</b> no puede estar vacío.")
            return
        
        campos = {}
        llaves = {}
        campos ['cod_nivel']=self.entCodigo.get_text().upper()
        campos ['cod_empresa']=self.codigo_empresa
        campos['digitos'] = self.spnDigitos.get_text()   
        
        if not self.editando:
            sql = ifd.insertFromDict("ctb.nivel_cuenta", campos)
            
        else:
            llaves['cod_nivel'] = self.entCodigo.get_text()
            sql, campos = ifd.updateFromDict("ctb.nivel_cuenta", campos, llaves)
            
        self.cursor.execute(sql, campos)
        self.dlgNivelCuenta.hide()
        
    def on_btnCancelar_clicked (self, btn=None):
        self.dlgNivelCuenta.hide()
        

if __name__ == "__main__":
    cnx = connect("host=localhost dbname=scc user=rmendez password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    w = wnNivelCuenta(cnx)
    
    gtk.main()
