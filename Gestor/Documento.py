#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2005 by Async Open Source and Sicem S.L.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


import sys, os
from reportlab.platypus import *
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.rl_config import defaultPageSize
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4, landscape,legal

##from code128 import Code128
from comunes import *
from codificacion import CISO

from reportlab.lib.units import inch
from reportlab.pdfgen import canvas
class PdfEgreso:

    def __init__(self, title = "Documento Generico", filename = "output.pdf", barcode="",companyInfo=[]):
        self.Elements = []
        self.Data = []
        self.Headers = []

        self.filename = filename

        self.Title = title
        self.Author = "Constructora Pehueche Ltda."

        self.URL = "http://www.galilea.cl"
        if companyInfo ==[]:
            self.CompanyInfo = [["CONSTRUCTORA PEHUENCHE LTDA.", 0],
                ["RUT: 78.246.760-3", 1],
                ["CASA MATRIZ 3 ORIENTE 1424 TALCA", 2],
                ["FONO 56-71-514400, FAX: 56-71-514450", 3]]
        else:
            self.CompanyInfo = companyInfo
        self.email = "compras@constructorapehuenche.cl"
        self.pageinfo = "%s / %s / %s" % (self.Author, self.email, self.Title)
        self.barcode = barcode


    def drawCompanyInfo(self, canvas, data):

        canvas.setFont('Times-Bold',10)
        for i, p in data:
            canvas.drawString(50, self.p_size[1]- (50 + (p*10)), i)

    def myFirstPage(self, canvas, doc):
        canvas.saveState()

        self.drawCompanyInfo(canvas, self.CompanyInfo
                )

        canvas.setFont('Times-Bold',16)
        espacio =0
        for i in self.Title:
            if espacio != 0:
                canvas.setFont('Times-Bold',12)
            canvas.drawCentredString(self.PAGE_WIDTH / 2.0, self.PAGE_HEIGHT-108 + espacio, i)
            espacio = espacio - 12
        #canvas.drawCentredString(self.p_size[0] / 2.0, self.p_size[1]-108, self.Title)
        if self.barcode != "":
            bc = Code128(self.barcode)
            bc.xo = 0
            bc.drawOn(canvas, self.p_size[0] / 2.0 - 75, self.p_size[1]-140)
            canvas.setFont('Times-Bold',7)
            canvas.drawCentredString(self.p_size[0] / 2.0, self.p_size[1]-147 , self.barcode)
        canvas.setFont('Times-Bold',9)
        factor = 1.7

#        canvas.rect(50, factor*inch, 500, factor*inch -160)
#        canvas.rect(50, factor*inch, 100, factor*inch -160)
#        canvas.rect(150, factor*inch, 250, factor*inch -160)
#        canvas.drawString(52, factor * inch - 10,"CONDICIONES DE")
#        canvas.drawString(52, factor * inch - 20,"PAGO 30 DIAS")
#        canvas.drawString(52, factor * inch - 30,"FECHA DE ENTREGA")
#        canvas.drawString(245, factor * inch - 10,"ADQUISICIONES")
#        canvas.drawString(435, factor * inch - 10,CISO("V°B° GERENCIA"))

        #canvas.drawString(inch, 0.75 * inch, "%s" % self.pageinfo)
        canvas.restoreState()

    def drawHeaders(self, data, colswidht = [400, 200]):

        t = Table(data, colswidht)
        t.setStyle([('BOX', (0,0), (-1, -1), 0.25, colors.black)
                        ])
        self.Elements.append(t)

    def go(self,alineacion = 1):
        self.Elements.insert(0,Spacer(0,inch))
        self.alineacion = alineacion
        if alineacion == 1 :
            self.PAGE_WIDTH = landscape(A4)[1]
            self.PAGE_HEIGHT = landscape(A4)[0]
            self.p_size = A4
        else:
            self.PAGE_WIDTH = landscape(A4)[0]
            self.PAGE_HEIGHT = landscape(A4)[1]
            self.p_size = landscape(A4)
        try:
            self.doc = SimpleDocTemplate(self.filename,pagesize=self.p_size)
        
            self.doc.build(self.Elements,onFirstPage=self.myFirstPage)
            Abre_pdf(self.filename)
        except:
            error('', str(sys.exc_info()[1]))



    def format(self,data, Type):
        if Type == 'str':
            return (CISO(data))
        elif Type == 'dte':
            return CDateLocal(data)
        elif Type == 'int':
            return CMon(data,0)
        elif Type == 'dbl':
            return CMon(data,2)
        else:
            return data
            
    def drawComent(self, data, cols, colswidht = None):

        data.insert(0, cols)

        t = Table(data, colswidht,len(data) * [10])
        #(col, fila)
        t.setStyle([('BOX', (0,0), (-1, -1), 0.25, colors.black),
                        ('BOX', (0,0), (-1, 0), 0.25, colors.black),
                        ('ALIGN', (0,0), (-1,0), 'CENTER'),
                        ('ALIGN', (0,1), (0,-1), 'LEFT'),
                        ('SIZE',(0,1), (-1,-1),8),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONT', (0,0), (-1,0), 'Times-Bold'),
                        ('SIZE', (0,0), (-1,0), 9)])

        self.Elements.append(Spacer(1, 0.05*inch))


        self.Elements.append(t)
        self.Data = data
    def drawData(self, data, cols, colswidht = None,ColsAlign=None,ColsType=None,AltoFila=None):
        if ColsType != None:
            for i in data:
                for j in range(len(ColsType)):
                    i[j] = self.format(i[j],ColsType[j]) 
        data.insert(0, cols)
        if AltoFila ==None:
            t = Table(data, colswidht)
        else:
            t = Table(data, colswidht,[AltoFila] * len(data))
        #(col, fila)
        style = [('BOX', (0,0), (-1, -1), 0.25, colors.black),
                        ('INNERGRID', (0,0), (-1, -1), 0.25, colors.black),
                        ('ALIGN', (0,0), (-1,0), 'CENTER'),
                        ('ALIGN', (0,1), (0,-1), 'LEFT'),
                        ('SIZE',(0,1), (-1,-1),8),
                        ('FONT', (0,0), (-1,0), 'Times-Bold'),
                        ('SIZE', (0,0), (-1,0), 9)]
        col =0
        if ColsAlign !=None:
            for i in ColsAlign:
                style.append(('ALIGN',(col,1),(col,-1),i))
                #print ('ALIGN',(col,1),(col,-1),i)
                col = col + 1
        t.setStyle(style)

        self.Elements.append(Spacer(0.5, 0.05*inch))


        self.Elements.append(t)
        self.Data = data

    def drawTotal(self, data, colswidht):

        t = Table(data, colswidht)

        t.setStyle([('ALIGN', (0,0), (-1,-1), 'RIGHT'),
                        ('FONT', (0,0),(-1,-1), 'Times-Bold')])

        self.Elements.append(t)


class PdfComprobanteContable:

    def __init__(self, title = " ", filename = "output.pdf",
                 barcode="",companyInfo=None,
                 folio = None,
                 row_heights=None,
                 logo=False,
                 total=None,
                 sucursal=None,
                 fecha=None):
        self.Elements = []
        self.Data = []
        self.Headers = []
        self.row_heights = row_heights
        self.filename = filename
        self.folio = folio
        self.Title = title
        self.total = total
        self.logo = logo
        self.sucursal = sucursal
        self.fecha = fecha
        self.Author = "Constructora Galilea S.A."
        print companyInfo
        self.URL = "http://www.galilea.cl"
        if companyInfo ==[]:
            self.CompanyInfo = [["GALILEA S.A.", 0],
                ["RUT: 78.246.760-3", 1],
                ["MELIPILLA: FONO 8319531 - Merced 669", 2],
                ["RANCAGUA, MACHALI:  FONO 230755 - Campos 221", 3],
                ["CURICO:  FONO 328333 - Merced 198", 4],
                ["TALCA:  FONO 514450 - 2 Norte 965", 5],
                ["CHILLAN, SAN CARLOS, BULNES:  FONO 234466 - Constitucion 408", 6],
                ["CONCEPCION:  FONO 216080 - Castellon 396", 7],
                ["LOS ANGELES:  FONO 8319531 - Av. Ricardo Vicuña 555", 8],
                ["PUERTO MONTT - A contar del 2006", 9]]
        else:
            self.CompanyInfo = companyInfo
        self.email = "ventas@galilea.cl"
        self.pageinfo = "%s / %s / %s" % (self.Author, self.email, self.Title)
        self.barcode = barcode

    def drawSucursal(self, canvas, data):

        canvas.setFont('Times-Roman',8)
        for i, p in data:
            canvas.drawString(350, self.p_size[1]- (50 + (p*10)), i)

    def drawFolio(self, canvas, data):

        canvas.setFont('Times-Bold',18)
        for i, p in data:
            canvas.drawString(350, self.p_size[1]- (50 + (p*10)), i)

    def drawCompanyInfo(self, canvas, data):
        canvas.setFont('Times-Roman',18)
        
        canvas.setFont('Times-Roman',8)
        for i, p in data:
            canvas.drawString(40, self.p_size[1]- (50 + (p*12)), i[:30])

    def myFirstPage(self, canvas, doc):
        canvas.saveState()

        if not self.CompanyInfo is None:
            self.drawCompanyInfo(canvas, self.CompanyInfo)
        top = (11-1.4) * inch
        left = 5.1*inch
        if self.logo:
            canvas.drawInlineImage("logo.jpg", 0.8*inch, top+0.7*inch, 1.1*inch, 1*inch)

        canvas.setFont('Times-Bold',16)
        if not self.folio is None:
            canvas.setFont('Times-Bold',16)
            le = (len(self.folio) * 16)/2
            if sys.platform == 'win32':
                canvas.drawString(570 - le , self.p_size[1]- (80 + (1*10)),  self.folio)# win32
            else:
                canvas.drawString(590 - le , self.p_size[1]- (65 + (1*10)),  self.folio) #linux
        
        canvas.drawCentredString(self.p_size[0] / 2.0, self.p_size[1]-108, self.Title)
        if self.barcode != "":
            bc = Code128(self.barcode)
            bc.xo = 0
            bc.drawOn(canvas, self.p_size[0] / 2.0 - 75, self.p_size[1]-140)
        
        if not self.total is None:
            canvas.setFont('Times-Bold',11)
            if sys.platform == 'win32':
                canvas.drawString(520, 195, CMon(self.total,0))#win32
            else:
                canvas.drawString(530, 165, CMon(self.total,0))#linix
        if not self.sucursal is None:
            if sys.platform == 'win32':
                canvas.drawString(100, self.p_size[1]- (80 + (100)), self.sucursal)#win32
            else:
                canvas.drawString(90, self.p_size[1]- (65 + (100)), self.sucursal)#linux
        if not self.fecha is None:
            if sys.platform == 'win32':
                canvas.drawString(450, self.p_size[1]- (80 + (100)), self.fecha)#win32
            else:
                canvas.drawString(450, self.p_size[1]- (65 + (100)), self.fecha)#linix
                
        canvas.setFont('Times-Bold',9)
        factor = 1.7
        canvas.restoreState()

    def drawHeaders(self, data, colswidht = [400, 200]):

        t = Table(data, colswidht)
        t.setStyle([('BOX', (0,0), (-1, -1), 0.25, colors.black)
                        ])
        self.Elements.append(t)
    def drawHeaders(self, data,cols,coldwith):
        data.insert(0,cols)
        t = Table(data,coldwith)
##        t.setStyle(TableStyle([('BACKGROUND', (0,0), (-1, -1),  colors.white)]))
        self.Elements.append(t)
    def go(self,alineacion = 1, margen_top = None):
        self.Elements.insert(0,Spacer(0,inch))
        self.alineacion = alineacion
        if alineacion == 1 :
            self.p_size =A4
        else:
            self.p_size = landscape(A4)
        self.doc = SimpleDocTemplate(self.filename,pagesize=self.p_size)
        if margen_top is not None:
            self.doc.topMargin = margen_top
        #self.doc.build(self.Elements,onFirstPage=self.myFirstPage, onLaterPages=self.myFirstPage)
        self.doc.build(self.Elements,onFirstPage=self.myFirstPage)
        Abre_pdf(self.filename)

    def drawDataBlanco(self, data, cols, colswidht = None,ColsAlign=None, row_height=None):
        
        data.insert(0, cols)

        t = Table(data, colswidht)
        #(col, fila)
        style = [('BOX', (0,0), (-1, -1), 0.25, colors.white),
                        ('ALIGN', (0,1), (0,-1), 'LEFT'),
                        ('SIZE',(0,1), (-1,-1),8),
                        ('FONT', (0,0), (-1,0), 'Times-Roman'),
                        ('SIZE', (0,0), (-1,0), 9)]
        
        if not row_height is None:
            rh = len(data)*[row_height]
        else:
            rh = len(data)*[10]
        print rh
        t = Table(data, colswidht, rh)
        style = [('BOX', (0,0), (-1, -1), 0.25, colors.white),
                        ('ALIGN', (0,1), (0,-1), 'LEFT'),
                        ('SIZE',(0,1), (-1,-1),6),
                        ('FONT', (0,0), (-1,0), 'Times-Roman'),
                        ('SIZE', (0,0), (-1,0), 6)]
        
        col =0
        for i in ColsAlign:
            style.append(('ALIGN',(col,1),(col,-1),i))
#            print ('ALIGN',(col,1),(col,-1),i)
            col = col + 1
        t.setStyle(style)

        self.Elements.append(Spacer(0.5, 0.05*inch))

        self.Elements.append(t)
        self.Data = data

    def drawData(self, data, cols, colswidht = None,ColsAlign=None, with_title=True, row_height=None):
        #(col, fila)
        
        if with_title:
            data.insert(0, cols)
            if not row_height is None:
                rh = len(data)*[row_height]
            else:
                rh = len(data)*[10]
            print rh
            t = Table(data, colswidht, rh)
            style = [('BOX', (0,1), (-1, -1), 0.25, colors.black),
##                        ('INNERGRID', (0,0), (-1, -1), 0.25, colors.black),
##                        ('ALIGN', (0,0), (-1,0), 'CENTER'),
##                        ('ALIGN', (0,4), (-1,0), 'RIGHT'),
                        ('VALIGN', (0,0), (-1,-1), 'MIDDLE'),
                        ('SIZE',(0,1), (-1,-1),6),
                        ('FONT', (0,0), (-1,0), 'Times-Bold'),
                        ('SIZE', (0,0), (-1,0), 6)]
            s=1
        else:
            if not row_height is None:
                rh = len(data)*[row_height]
            else:
                rh = len(data)*[10]
            print rh
            t = Table(data, colswidht, rh)
            style = [('BOX', (0,0), (-1, -1), 0.25, colors.black),
##                        ('ALIGN', (0,0), (0,-1), 'LEFT'),
                        ('SIZE',(0,0), (-1,-1),6)]
            s=0
        col =0
        for i in ColsAlign:
            style.append(('ALIGN',(col,s),(col,-1),i))
            col = col + 1
        t.setStyle(style)
       

        self.Elements.append(Spacer(0.5, 0.05*inch))

        self.Elements.append(t)
        self.Data = data

    def drawTotal(self, data, colswidht):

        t = Table(data, colswidht)

        t.setStyle([('ALIGN', (0,0), (-1,-1), 'RIGHT'),
                        ('FONT', (0,0),(-1,-1), 'Times-Bold'),
                        ('SIZE',(0,0), (-1,-1),6)])

        self.Elements.append(t)
        
        
    def drawObs(self, data, colswidht):

        t = Table(data, colswidht)

        t.setStyle([('ALIGN', (0,0), (-1,-1), 'LEFT'),
                        ('FONT', (0,0),(-1,-1), 'Times-Bold'),
                        ('SIZE',(0,0), (-1,-1),10)])

        self.Elements.append(t)
        
        
    def drawObsData(self, data, colswidht):

        t = Table(data, colswidht)

        t.setStyle([('ALIGN', (0,0), (-1,-1), 'LEFT'),
                        ('FONT', (0,0),(-1,-1), 'Times-Bold'),
                        ('SIZE',(0,0), (-1,-1),8)])

        self.Elements.append(t)
class PdfCustomDetail:

    def __init__(self, title = " ", filename = "output.pdf",
                 barcode="",companyInfo=None,
                 folio = None,
                 row_heights=None,
                 logo=True,
                 total=None,
                 sucursal=None,
                 fecha=None):
        self.Elements = []
        self.Data = []
        self.Headers = []
        self.row_heights = row_heights
        self.filename = filename
        self.folio = folio
        self.Title = title
        self.total = total
        self.logo = logo
        self.sucursal = sucursal
        self.fecha = fecha
        self.Author = "Constructora Galilea S.A."
        print companyInfo
        self.URL = "http://www.galilea.cl"
        if companyInfo ==[]:
            self.CompanyInfo = [["GALILEA S.A.", 0],
                ["RUT: 78.246.760-3", 1],
                ["MELIPILLA: FONO 8319531 - Merced 669", 2],
                ["RANCAGUA, MACHALI:  FONO 230755 - Campos 221", 3],
                ["CURICO:  FONO 328333 - Merced 198", 4],
                ["TALCA:  FONO 514450 - 2 Norte 965", 5],
                ["CHILLAN, SAN CARLOS, BULNES:  FONO 234466 - Constitucion 408", 6],
                ["CONCEPCION:  FONO 216080 - Castellon 396", 7],
                ["LOS ANGELES:  FONO 8319531 - Av. Ricardo Vicuña 555", 8],
                ["PUERTO MONTT - A contar del 2006", 9]]
        else:
            self.CompanyInfo = companyInfo
        self.email = "ventas@galilea.cl"
        self.pageinfo = "%s / %s / %s" % (self.Author, self.email, self.Title)
        self.barcode = barcode

    def drawSucursal(self, canvas, data):

        canvas.setFont('Times-Roman',8)
        for i, p in data:
            canvas.drawString(350, self.p_size[1]- (50 + (p*10)), i)

    def drawFolio(self, canvas, data):

        canvas.setFont('Times-Bold',18)
        for i, p in data:
            canvas.drawString(350, self.p_size[1]- (50 + (p*10)), i)

    def drawCompanyInfo(self, canvas, data):
        canvas.setFont('Times-Roman',18)
        
        canvas.setFont('Times-Roman',8)
        for i, p in data:
            canvas.drawString(400, self.p_size[1]- (50 + (p*12)), i[:30])

    def myFirstPage(self, canvas, doc):
        canvas.saveState()

        if not self.CompanyInfo is None:
            self.drawCompanyInfo(canvas, self.CompanyInfo)
        top = (11-1.4) * inch
        left = 5.1*inch
        if self.logo:
            canvas.drawInlineImage("logo.jpg", 0.8*inch, top+0.7*inch, 1.1*inch, 1*inch)

        canvas.setFont('Times-Bold',16)
        if not self.folio is None:
            canvas.setFont('Times-Bold',16)
            le = (len(self.folio) * 16)/2
            if sys.platform == 'win32':
                canvas.drawString(570 - le , self.p_size[1]- (80 + (1*10)),  self.folio)# win32
            else:
                canvas.drawString(590 - le , self.p_size[1]- (65 + (1*10)),  self.folio) #linux
        
        canvas.drawCentredString(self.p_size[0] / 2.0, self.p_size[1]-108, self.Title)
        if self.barcode != "":
            bc = Code128(self.barcode)
            bc.xo = 0
            bc.drawOn(canvas, self.p_size[0] / 2.0 - 75, self.p_size[1]-140)
        
        if not self.total is None:
            canvas.setFont('Times-Bold',11)
            if sys.platform == 'win32':
                canvas.drawString(520, 195, CMon(self.total,0))#win32
            else:
                canvas.drawString(530, 165, CMon(self.total,0))#linix
        if not self.sucursal is None:
            if sys.platform == 'win32':
                canvas.drawString(100, self.p_size[1]- (80 + (100)), self.sucursal)#win32
            else:
                canvas.drawString(90, self.p_size[1]- (65 + (100)), self.sucursal)#linux
        if not self.fecha is None:
            if sys.platform == 'win32':
                canvas.drawString(450, self.p_size[1]- (80 + (100)), self.fecha)#win32
            else:
                canvas.drawString(450, self.p_size[1]- (65 + (100)), self.fecha)#linix
                
        canvas.setFont('Times-Bold',9)
        factor = 1.7
        canvas.restoreState()

    def drawHeaders(self, data, colswidht = [400, 200]):

        t = Table(data, colswidht)
        t.setStyle([('BOX', (0,0), (-1, -1), 0.25, colors.black)
                        ])
        self.Elements.append(t)

    def go(self,alineacion = 1, margen_top = None):
        self.Elements.insert(0,Spacer(0,inch))
        self.alineacion = alineacion
        if alineacion == 1 :
            self.p_size = A4
        else:
            self.p_size = landscape(A4)
        self.doc = SimpleDocTemplate(self.filename,pagesize=self.p_size)
        if margen_top is not None:
            self.doc.topMargin = margen_top
        #self.doc.build(self.Elements,onFirstPage=self.myFirstPage, onLaterPages=self.myFirstPage)
        self.doc.build(self.Elements,onFirstPage=self.myFirstPage)
        Abre_pdf(self.filename)

    def drawDataBlanco(self, data, cols, colswidht = None,ColsAlign=None, row_height=None):
        
        data.insert(0, cols)

        t = Table(data, colswidht)
        #(col, fila)
        style = [('BOX', (0,0), (-1, -1), 0.25, colors.white),
                        ('ALIGN', (0,1), (0,-1), 'LEFT'),
                        ('SIZE',(0,1), (-1,-1),8),
                        ('FONT', (0,0), (-1,0), 'Times-Roman'),
                        ('SIZE', (0,0), (-1,0), 9)]
        
        if not row_height is None:
            rh = len(data)*[row_height]
        else:
            rh = len(data)*[10]
        print rh
        t = Table(data, colswidht, rh)
        style = [('BOX', (0,0), (-1, -1), 0.25, colors.white),
                        ('ALIGN', (0,1), (0,-1), 'LEFT'),
                        ('SIZE',(0,1), (-1,-1),8),
                        ('FONT', (0,0), (-1,0), 'Times-Roman'),
                        ('SIZE', (0,0), (-1,0), 9)]
        
        col =0
        for i in ColsAlign:
            style.append(('ALIGN',(col,1),(col,-1),i))
#            print ('ALIGN',(col,1),(col,-1),i)
            col = col + 1
        t.setStyle(style)

        self.Elements.append(Spacer(0.5, 0.05*inch))

        self.Elements.append(t)
        self.Data = data

    def drawData(self, data, cols, colswidht = None,ColsAlign=None, with_title=True, row_height=None):
        #(col, fila)
        
        if with_title:
            data.insert(0, cols)
            if not row_height is None:
                rh = len(data)*[row_height]
            else:
                rh = len(data)*[10]
            print rh
            t = Table(data, colswidht, rh)
            style = [('BOX', (0,1), (-1, -1), 0.25, colors.black),
#                        ('INNERGRID', (0,0), (-1, -1), 0.25, colors.black),
                        ('ALIGN', (0,0), (-1,0), 'CENTER'),
                        ('ALIGN', (0,1), (0,-1), 'LEFT'),
                        ('VALIGN', (0,0), (-1,-1), 'MIDDLE'),
                        ('SIZE',(0,1), (-1,-1),8),
                        ('FONT', (0,0), (-1,0), 'Times-Bold'),
                        ('SIZE', (0,0), (-1,0), 8)]
            s=1
        else:
            if not row_height is None:
                rh = len(data)*[row_height]
            else:
                rh = len(data)*[10]
            print rh
            t = Table(data, colswidht, rh)
            style = [('BOX', (0,0), (-1, -1), 0.25, colors.black),
                        ('ALIGN', (0,0), (0,-1), 'LEFT'),
                        ('SIZE',(0,0), (-1,-1),8)]
            s=0
        col =0
        for i in ColsAlign:
            style.append(('ALIGN',(col,s),(col,-1),i))
            col = col + 1
        t.setStyle(style)
       

        self.Elements.append(Spacer(0.5, 0.05*inch))

        self.Elements.append(t)
        self.Data = data

    def drawTotal(self, data, colswidht):

        t = Table(data, colswidht)

        t.setStyle([('ALIGN', (0,0), (-1,-1), 'RIGHT'),
                        ('FONT', (0,0),(-1,-1), 'Times-Bold'),
                        ('SIZE',(0,0), (-1,-1),8)])

        self.Elements.append(t)
