#!/usr/bin/env python
# -*- coding: UTF8 	-*-
# wnBanco -- formulario de ingreso, eliminación y edición, de los distintos Bancos.

# (C)    José Luis Álvarez Morales   2006
#           josealvarezm@gmail.com

from GladeConnect import GladeConnect
import completion
import SimpleTree
import ifd
from spg import connect
import gtk
import dialogos
import debugwindow
import sys
from strSQL import strSelectBanco

(CODIGO,
 NOMBRE_BANCO,
 SUCURSAL) = range(3)

class wnBanco(GladeConnect):
    def __init__(self, conexion=None, padre=None, root="wnBanco"):
        GladeConnect.__init__(self, "glade/wnBanco.glade", root)
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.padre = padre
        if padre is None:
            self.wnBanco.maximize()
            self.frm_padre = self.wnBanco
            self.padre
        else:
            self.frm_padre = padre.frm_padre
            
        self.crea_columnas()
        self.carga_datos()
    
    def crea_columnas(self):
        columnas = []
        columnas.append([CODIGO, "Código", "str"])
        columnas.append([NOMBRE_BANCO, "Nombre Banco", "str"])
        columnas.append([SUCURSAL, "Código Sucursal", "str"])
        
        self.modelo = gtk.ListStore(*(3*[str]))
        SimpleTree.GenColsByModel(self.modelo, columnas, self.treeBanco)
        
    def carga_datos(self):
        self.cursor.execute(strSelectBanco)
        r=self.cursor.fetchall()
        self.modelo.clear()
        for i in r:
            self.modelo.append(i)
    
    def on_btnAnadir_clicked(self, btn=None):
        dlg = dlgBanco(self.cnx, self.frm_padre, False)
        dlg.editando = False
        band = False
        while not band:
            response = dlg.dlgBanco.run()
            if response == gtk.RESPONSE_OK:
                if dlg.commit== True:
                    self.modelo.append(dlg.response)
                    band = True
            else:
                band = True
        dlg.dlgBanco.destroy()
            
    def on_btnQuitar_clicked(self, btn=None):
        model, it = self.treeBanco.get_selection().get_selected()
        if model is None or it is None:
            return
        codigo = model.get_value(it, CODIGO)
        descripcion = model.get_value(it, NOMBRE_BANCO)
        
        if dialogos.yesno("¿Desea eliminar el Banco <b>%s</b>?\nEsta acción no se puede deshacer!!!"%descripcion, self.frm_padre) == gtk.RESPONSE_YES:
            llaves = {'cod_banco':codigo}
            sql = ifd.deleteFromDict('ctb.banco', llaves)
            
            try:
                self.cursor.execute(sql, llaves)
                model.remove(it)
            except:
                print sys.exc_info()[1]
                dialogos.error("<b>NO SE PUEDE ELIMINAR</b>\nEl Banco <b>%s</b>, se encuentra relacionado.")
                
    def on_btnPropiedades_clicked(self, btn=None):
        model, it = self.treeBanco.get_selection().get_selected()
        if model is None or it is None:
            return
        dlg = dlgBanco(self.cnx, self.frm_padre, False)
        dlg.entCodigo.set_text(model.get_value(it, CODIGO))
        dlg.entDescripcion.set_text(model.get_value(it, NOMBRE_BANCO))
        dlg.spnSucursal.set_text(model.get_value(it, SUCURSAL))
               
        dlg.editando = True
        band=False
        while not band:        
            response = dlg.dlgBanco.run()
            if response == gtk.RESPONSE_OK:
                if dlg.commit==True:
                    del model[it]
                    self.modelo.append(dlg.response)
                    band=True
            else:
                band=True
        dlg.dlgBanco.destroy()
            
    def on_btnCerrar_clicked(self, btn=None):
        if self.padre is None:
            self.on_exit()
        else:
            self.padre.remove_tab("Banco")
            
    def on_treeBanco_row_activated(self, tree=None, path=None, col=None):
        self.on_btnPropiedades_clicked()
        
class dlgBanco(GladeConnect):
    def __init__(self, conexion=None, padre=None, editando=None):
        GladeConnect.__init__(self, "glade/wnBanco.glade", "dlgBanco")
        self.cnx = conexion
        self.cursor = self.cnx.cursor()
        self.editando = editando
        self.response = None
        self.entDescripcion.grab_focus()
        self.dlgBanco.show_all()
        self.commit=False
    def on_btnAceptar_clicked(self, btn=None):
        if self.entDescripcion.get_text() == "" or len(self.entDescripcion.get_text().replace(" ",""))==0:
            dialogos.error("El campo <b>Nombre Banco</b> no puede estar vacío.")
            return
        
        if self.spnSucursal.get_text() < '0':
            dialogos.error("El campo <b>Código Sucursal</b> debe especificar un código numérico.")
            return
      
        campos = {}
        llaves = {}
        campos ['nombre_banco'] = self.entDescripcion.get_text().upper()
        campos['cod_sbif'] = self.spnSucursal.get_text()   
        
        if not self.editando:
            self.cursor.execute("select nextval('ctb.banco_cod_banco_seq')")
            r = self.cursor.fetchall()
            self.entCodigo.set_text(str(r[0][0]))
            campos['cod_banco'] = self.entCodigo.get_text()
            sql = ifd.insertFromDict("ctb.banco", campos)
        else:
            llaves['cod_banco'] = self.entCodigo.get_text()
            sql, campos = ifd.updateFromDict("ctb.banco", campos, llaves)
        try:
            self.cursor.execute(sql, campos)
            self.dlgBanco.hide()
            
            self.response = [self.entCodigo.get_text(),
                                    self.entDescripcion.get_text().upper(),
                                    self.spnSucursal.get_text()]
            self.commit=True
        except:
            print sys.exc_info()[1]
            nombre_banco = self.entDescripcion
            dialogos.error("En la Base de Datos ya existe el Banco <b>%s</b>."%campos['nombre_banco'])
            self.entDescripcion.grab_focus()    
            
               
    def on_btnCancelar_clicked(self, btn=None):
        self.dlgBanco.hide()
        
if __name__ == '__main__':
    cnx = connect("host=localhost dbname=scc user=jalvarezm password=1020")
    cnx.autocommit()
    sys.excepthook = debugwindow.show
    d = wnBanco(cnx)
    
    gtk.main()
