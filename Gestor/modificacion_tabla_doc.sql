ALTER TABLE ctb.documento ADD COLUMN cod_empresa_imputada integer;
ALTER TABLE ctb.documento ADD CONSTRAINT fk_empresa_e FOREIGN KEY (cod_empresa_imputada) REFERENCES ctb.empresa (cod_empresa) ON UPDATE NO ACTION ON DELETE NO ACTION;
GRANT ALL on ctb.documento to public;
