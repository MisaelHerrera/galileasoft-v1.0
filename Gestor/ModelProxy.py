#!/usr/bin/env python

import gtk
from kiwi.ui.dateentry import DateEntry
from kiwi.ui.entry import KiwiEntry
from ctb_rutinas import CMon, CNumDb
class ListStoreProxy:
    
    def __init__(self, model=None, mapping=None):
        self.model = model
        self.mapping = mapping
        
    def add(self, iter=None):
        
        
        if iter is None:
            # add new iter in the model
            iter = self.model.append()
        
        for column in self.mapping:
            
            var = self.mapping[column]
            
            #Find the way to retrive data from var
            
            #if type(var) in (int, float, str, bool):
            if type(var) is str:      
                if var not in('',None):
                    value = eval(var)                
            elif type(var) is gtk.Entry:
                value = var.get_text()
            elif type(var) is KiwiEntry:
                value=var.get_text()
            elif type(var) in (gtk.ToggleButton, gtk.CheckButton, gtk.RadioButton): 
                value = var.get_value()
            elif type(var) is gtk.SpinButton:        
                if var.get_digits() == 0:
                    v=int(float(var.get_value()))
                else:
                    v=round(float(var.get_value()), var.get_digits())
                value = v
            elif type(var) is DateEntry:
                value = var.get_date()
                
            
            self.model.set_value(iter, column, value)
            
    def update(self, iter):
        self.add(iter)
        
    def clear_widgets(self):
        for column in self.mapping:
            
            var = self.mapping[column]
            
            if type(var) is gtk.Entry:
                var.set_text("")
            elif type(var) in (gtk.ToggleButton, gtk.CheckButton, gtk.RadioButton, gtk.SpinButton):
                var.set_value(0)
            elif type(var) is KiwiEntry:
                var.set_text('')
##            elif type(var) is DateEntry:
##                var.set_date('')
