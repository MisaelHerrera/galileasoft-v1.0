#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ctb_honorarios - Centralizado e Ingreso  de Boletas de Honorarios

# (C)   Fernando San Martín Woerner 2004
#       snmartin@galilea.cl

#This file is part of pyGestor.
#
# pyGestor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyGestor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


from GladeConnect import GladeConnect
from ctb_rutinas import *
from string import *
from types import StringType
import sys
from time import *
from calendar import *
import gobject
import gtk

import os

#from reportlab.test import unittest
#from reportlab.test.utils import makeSuiteForClasses
from reportlab.pdfbase.ttfonts import *
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import A4, landscape


if os.name == "nt":
    sys.path.append("C:\\Python23\\Lib\\site-packages\\pyPgSQL")

if os.name == "posix":
    sys.path.append("/usr/lib/python2.3/site-packages/pyPgSQL")


from ctb_busqueda import ctb_busqueda


class ctb_honorario(GladeConnect):



    aviso = ctb_aviso
    pregunta = ctb_pregunta_si_no
    carga_empresas = cmb_carga_empresas
    desc_empresa = ""
    cod_empresa = -1

    def __init__(self, c=None,e=None):



        GladeConnect.__init__(self, "glade/ctb_honorario.glade")

        self.cnx = c
        self.cursor = self.cnx.cursor()
        self.padre = None

        self.fila_editada = -1

        self.cod_empresa = e

        self.mes = 0

        self.periodo = 0

        self.buscando = 0



        self.cmb_carga_periodo()

        self.select = 0

        self.crea_modelo()
        self.txtFecha.set_text(str(ceros(localtime()[2],2)) + "/"+ str(ceros(localtime()[1],2)) +"/" + str(ceros(localtime()[0],4)) )
        self.cursor.execute("select e.cuenta_retencion, c.descripcion_cuenta, e.tipo_numeracion \
                    from ctb.empresa e inner join ctb.cuenta_contable c \
                    on e.cuenta_retencion = c.num_cuenta and e.cod_empresa = c.cod_empresa")

        r = self.cursor.fetchall()
        self.txtDescripcionRetencion.set_text (r[0][1])
        self.txtCuentaRetencion.set_text(r[0][0])
        self.tipo_numeracion = r[0][2]

        self.cursor.execute("select e.cuenta_liquido, c.descripcion_cuenta \
                    from ctb.empresa e inner join ctb.cuenta_contable c \
                    on e.cuenta_liquido = c.num_cuenta and e.cod_empresa = c.cod_empresa")
        r = self.cursor.fetchall()

        self.txtDescripcionLiquido.set_text (r[0][1])
        self.txtCuentaLiquido.set_text(r[0][0])

        if not self.cod_empresa:
            ctb_error(self.padre.ctb_frm_main, "No se ha seleccionado una empresa")
            return 0

        try:
            self.cursor.execute("select descripcion_empresa, rut_empresa, direccion_empresa from ctb.empresa \
                        where cod_empresa = " + str(self.cod_empresa))

            r = self.cursor.fetchall()
        except:
            ctb_error(self.padre.ctb_frm_main, sys.exc_info()[1])
            return 0

        desc_empresa = r[0][0]
        rut_empresa = r[0][1]
        direccion_empresa = r[0][2]

        self.desc_empresa = desc_empresa
        self.rut_empresa = rut_empresa
        self.direccion_empresa = direccion_empresa

        self.limpia_campos()


    def crea_modelo(self):


        store = gtk.ListStore(gobject.TYPE_BOOLEAN, gobject.TYPE_STRING,
                  gobject.TYPE_STRING,
                  gobject.TYPE_STRING,
                  gobject.TYPE_STRING,
                  gobject.TYPE_STRING,
                  gobject.TYPE_STRING,
                  gobject.TYPE_STRING,
                  gobject.TYPE_STRING,
                  gobject.TYPE_STRING,
                  gobject.TYPE_STRING,
                  gobject.TYPE_STRING,
                  gobject.TYPE_STRING)


        renderer = gtk.CellRendererToggle()
        renderer.connect('toggled', self.fixed_toggled, store)

        column = gtk.TreeViewColumn('Centr.', renderer, active=0)
        column.set_clickable(True)
        column.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
        column.set_fixed_width(50)

        self.treeHonorario.append_column(column)


        strcod = unicode('Tipo','latin-1')
        column = gtk.TreeViewColumn(strcod.encode('utf-8'), gtk.CellRendererText(), text=1)
        column.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
        column.set_fixed_width(100)

        self.treeHonorario.append_column(column)


        strcod = unicode('Nş Comprobante','latin-1')
        column = gtk.TreeViewColumn(strcod.encode('utf-8'), gtk.CellRendererText(), text=2)


        self.treeHonorario.append_column(column)


        strcod = unicode('Fecha Boleta','latin-1')
        column = gtk.TreeViewColumn(strcod.encode('utf-8'), gtk.CellRendererText(), text=3)

        self.treeHonorario.append_column(column)

        strcod = unicode('Nş Boleta','latin-1')
        column = gtk.TreeViewColumn(strcod.encode('utf-8'), gtk.CellRendererText(), text=4)

        self.treeHonorario.append_column(column)

        strcod = unicode('Ficha','latin-1')
        column = gtk.TreeViewColumn(strcod.encode('utf-8'), gtk.CellRendererText(), text=5)

        self.treeHonorario.append_column(column)

        strcod = unicode('Prestador','latin-1')
        column = gtk.TreeViewColumn(strcod.encode('utf-8'), gtk.CellRendererText(), text=6)

        self.treeHonorario.append_column(column)

        strcod = unicode('Nş Cuenta','latin-1')
        column = gtk.TreeViewColumn(strcod.encode('utf-8'), gtk.CellRendererText(), text=7)

        self.treeHonorario.append_column(column)

        strcod = unicode('Descripción','latin-1')
        column = gtk.TreeViewColumn(strcod.encode('utf-8'), gtk.CellRendererText(), text=8)

        self.treeHonorario.append_column(column)

        strcod = unicode('Debe','latin-1')
        column = gtk.TreeViewColumn(strcod.encode('utf-8'), gtk.CellRendererText(), text=9)

        self.treeHonorario.append_column(column)

        strcod = unicode('Haber','latin-1')
        column = gtk.TreeViewColumn(strcod.encode('utf-8'), gtk.CellRendererText(), text=10)

        self.treeHonorario.append_column(column)

        strcod = unicode('Concepto','latin-1')
        column = gtk.TreeViewColumn(strcod.encode('utf-8'), gtk.CellRendererText(), text=11)

        self.treeHonorario.append_column(column)

        strcod = unicode('Glosa','latin-1')
        column = gtk.TreeViewColumn(strcod.encode('utf-8'), gtk.CellRendererText(), text=12)

        self.treeHonorario.append_column(column)

        self.treeHonorario.set_model(store)


    def limpia_datos(self):
        self.treeHonorario.get_model().clear()
        self.select = 0

    def cmb_carga_periodo(self):

        if self.cod_empresa:
            try:

                self.cursor.execute("select p.descripcion_periodo from ctb.periodo_contable p where p.cod_empresa ='"+ str(self.cod_empresa) + "' and p.estado = 'A' order by periodo desc, mes desc")
                r = self.cursor.fetchall()

            except:

                print sys.exc_info()[1]
                self.aviso(self.padre.ctb_frm_main,sys.exc_info()[1])
                return 1

            if len(r) == 0: return   0

            st = []

            for i in r:

                st.append(i[0])

            self.cmbPeriodo.set_popdown_strings(st)
            return 0

        else:
            return 1



    def fixed_toggled(self, cell, ruta, model):


        model = self.treeHonorario.get_model()
        iter = model.get_iter((int(ruta),))
        fixed = model.get_value(iter, 0)
        tipo = model.get_value(iter, 1)
        comp = model.get_value(iter, 2)
        concepto = model.get_value(iter, 11)
        path = model.get_path(iter)[0]

        if comp is None:

            fixed = not fixed

        if concepto == "HONORARIOS":

            ih = iter

            ir = model.get_iter((path + 1,))

            il = model.get_iter((path + 2,))



        if concepto == "RETENCION":

            ir = iter

            ih = model.get_iter((path - 1,))

            il = model.get_iter((path + 1,))



        if concepto == "LIQUIDO":

            il = iter

            ih = model.get_iter((path - 2,))

            ir = model.get_iter((path - 1,))


        model.set(ih, 0, fixed)
        model.set(ir, 0, fixed)
        model.set(il, 0, fixed)

    def calcula_totales(self):



        model = self.treeHonorario.get_model()

        iter = model.get_iter_first()

        doc = 0

        total = 0

        while iter:

            total += long(ctb_formato_numero_db(model.get_value(iter, 9)))

            doc += 1

            iter = model.iter_next(iter)



        self.lblMonto.set_text(ctb_formato_moneda(str(total)))

        self.lblDocumentos.set_text(ctb_formato_moneda(str(doc/3)))

        return 0



    def on_btnTodos_clicked(self, btn=None):



        model = self.treeHonorario.get_model()

        iter = model.get_iter_first()
        self.select = not self.select

        while iter:

            model.set(iter, 0, self.select)
            iter = model.iter_next(iter)

        return 0



    def on_btnQuitar_clicked(self, btn=None):



        model, iter = self.treeHonorario.get_selection().get_selected()



        concepto = model.get_value(iter, 11)
        boleta = model.get_value(iter, 4)
        ficha = model.get_value(iter, 5)

        path = model.get_path(iter)[0]

        if concepto == "HONORARIOS":

            ih = iter

            ir = model.get_iter((path + 1,))

            il = model.get_iter((path + 2,))



        if concepto == "RETENCION":

            ir = iter

            ih = model.get_iter((path - 1,))

            il = model.get_iter((path + 1,))



        if concepto == "LIQUIDO":

            il = iter

            ih = model.get_iter((path - 2,))

            ir = model.get_iter((path - 1,))


        resp = ctb_pregunta_si_no(self, self.padre.ctb_frm_main, "Quiere eliminar el asiento seleccionado ?", "Quitar asiento")

        if resp == gtk.RESPONSE_NO:

            return 0

        try:

            self.cursor.execute("delete from ctb.honorario where numero_boleta = " + boleta + " and rut_ficha = '" + ficha + "'")

        except:

            self.aviso(self.padre.ctb_frm_main, sys.exc_info()[1])

            return 0



        model.remove(ih)
        model.remove(ir)
        model.remove(il)
        self.calcula_totales()



    def on_btnBuscar_clicked(self, btn=None):

        self.limpia_datos()
        self.select = 0

        self.cursor.execute("select mes, periodo from ctb.periodo_contable where descripcion_periodo = '" + self.cmbPeriodo.entry.get_text() + "'")
        r = self.cursor.fetchall()

        self.mes = r[0][0]
        self.periodo = r[0][1]

        sql ="select * from ctb.vw_honorario where date_part('year',fecha_boleta) = " + str(self.periodo) + " and date_part('month',fecha_boleta) = " + str(self.mes)

        self.cursor.execute(sql)
        r = self.cursor.fetchall()

        store = self.treeHonorario.get_model()

        n = 0

        t = 0

        for i in r:

            n = n + 1

            iter = store.append()

            if i[2] is None:
                comp = 0
            else:
                comp = 1

            #HONORARIOS
            print i[3]
            store.set(iter, 0, comp,
            1, i[13],
            2, i[2],
            3, ctb_formato_fecha_local(i[3]),
            4, i[4],
            5, i[5],
            6, i[16],
            7, i[7],
            8, i[10],
            9, ctb_alinea_derecha(long(i[6]), 25),
            10, ctb_alinea_derecha(0, 25),
            11, "HONORARIOS",
            12, "HONORARIOS " + i[16] + " BOLETA # " + str(str(i[4])))

            iter = store.append()

            #RETENCION
            store.set(iter, 0, comp,
            1, i[13],
            2, i[2],
            3, ctb_formato_fecha_local(i[3]),
            4, i[4],
            5, i[5],
            6, i[16],
            7, i[14],
            8, i[12],
            9, ctb_alinea_derecha(0, 25),
            10, ctb_alinea_derecha(long(i[8]), 25),
            11, "RETENCION",
            12, "RETENCION " + i[16] + " BOLETA # " + str(str(i[4])))

            iter = store.append()

            #LIQUIDO
            store.set(iter, 0, comp,
            1, i[13],
            2, i[2],
            3, ctb_formato_fecha_local(i[3]),
            4, i[4],
            5, i[5],
            6, i[16],
            7, i[15],
            8, i[11],
            9, ctb_alinea_derecha(0, 25),
            10, ctb_alinea_derecha(long(i[9]), 25),
            11, "LIQUIDO",
            12, "LIQUIDO " + i[16] + " BOLETA # " + str(str(i[4])))


        self.treeHonorario.set_model(store)

        self.calcula_totales()

        return 0


    def on_treeHonorario_row_activated(self, tree, row, column):

        model, iter = tree.get_selection().get_selected()

        concepto = model.get_value(iter, 11)

        tipo = model.get_value(iter, 1)
        comp = model.get_value(iter, 2)

        if tipo and comp:

            return 0


        path = model.get_path(iter)[0]


        if concepto == "HONORARIOS":

            ih = iter

            ir = model.get_iter((path + 1,))

            il = model.get_iter((path + 2,))



        if concepto == "RETENCION":

            ir = iter

            ih = model.get_iter((path - 1,))

            il = model.get_iter((path + 1,))

        if concepto == "LIQUIDO":

            il = iter

            ih = model.get_iter((path - 2,))

            ir = model.get_iter((path - 1,))


        self.txtFecha.set_text(ctb_formato_fecha_local(model.get_value(iter, 3)))

        self.txtNumero.set_text(model.get_value(iter, 4))

        self.txtFicha.set_text(model.get_value(iter, 5))

        self.txtPrestador.set_text(model.get_value(iter, 6))



        self.txtCuentaHonorario.set_text(model.get_value(ih, 7))

        self.txtDescripcionHonorario.set_text(model.get_value(ih, 8))

        self.txtDebeHonorario.set_text(model.get_value(ih, 9))

        self.txtHaberHonorario.set_text(model.get_value(ih, 10))

        self.txtDebeRetencion.set_text(model.get_value(ir, 9))

        self.txtHaberRetencion.set_text(model.get_value(ir, 10))

        self.txtDebeLiquido.set_text(model.get_value(il, 9))

        self.txtHaberLiquido.set_text(model.get_value(il, 10))

        return 0


    def limpia_campos(self):

        self.txtNumero.set_text("")

        self.txtFicha.set_text("")

        self.txtPrestador.set_text("")

        self.txtCuentaHonorario.set_text("")

        self.txtDescripcionHonorario.set_text("")

        self.txtDebeHonorario.set_text("0")

        self.txtHaberHonorario.set_text("0")

        self.txtDebeRetencion.set_text("0")

        self.txtHaberRetencion.set_text("0")

        self.txtDebeLiquido.set_text("0")

        self.txtHaberLiquido.set_text("0")


    def on_btnAgregar_clicked(self, btn=None):


        store = self.treeHonorario.get_model()
        iter = store.get_iter_first()

        edita_retencion = 0

        edita_liquido = 0

        edita_honorario = 0


        self.cursor.execute("begin")

        rut = self.txtFicha.get_text()

        boleta = self.txtNumero.get_text()

        existe = 0

        try:


            self.cursor.execute("select cod_honorario, cod_comprobante from ctb.honorario \
                where numero_boleta = " + boleta + " and \
                rut_ficha = '" + rut + "'")
            r = self.cursor.fetchall()

            if len(r):

                existe = 1

                if r[0][1]:
                    self.cursor.execute("rollback")
                    self.aviso(self.padre.ctb_frm_main,"La boleta ya se ha centralizado.")
                    return
                else:
                    cod_honorario = r[0][0]

        except:
            self.cursor.execute("rollback")
            self.aviso(self.padre.ctb_frm_main, sys.exc_info()[1])
            return

        while iter:

            rut = store.get_value(iter, 5)
            boleta = store.get_value(iter, 4)
            concepto = store.get_value(iter, 11)

            if rut == self.txtFicha.get_text() and boleta == self.txtNumero.get_text():


                if concepto == "RETENCION":

                    edita_retencion = 1
                    store.set(iter, 0, 0,
                    1, None,
                    2, None,
                    3, ctb_formato_fecha_db(self.txtFecha.get_text()),#ctb_get_fecha(self.dtFecha),
                    4, self.txtNumero.get_text(),
                    5, self.txtFicha.get_text(),
                    6, self.txtPrestador.get_text(),
                    7, self.txtCuentaRetencion.get_text(),
                    8, self.txtDescripcionRetencion.get_text(),
                    9, ctb_alinea_derecha(self.txtDebeRetencion.get_text(), 25),
                    10, ctb_alinea_derecha(self.txtHaberRetencion.get_text(), 25),
                    11, "RETENCION",
                    12, "RETENCION " + i[6] + " BOLETA # " + str(i[4]))

                if concepto == "LIQUIDO":

                    edita_liquido = 1

                    store.set(iter, 0, 0,
                    1, None,
                    2, None,
                    3, ctb_formato_fecha_db(self.txtFecha.get_text()),# ctb_get_fecha(self.dtFecha),
                    4, self.txtNumero.get_text(),
                    5, self.txtFicha.get_text(),
                    6, self.txtPrestador.get_text(),
                    7, self.txtCuentaLiquido.get_text(),
                    8, self.txtDescripcionLiquido.get_text(),
                    9, ctb_alinea_derecha(self.txtDebeLiquido.get_text(), 25),
                    10, ctb_alinea_derecha(self.txtHaberLiquido.get_text(), 25),
                    11, "LIQUIDO",
                    12,  "LIQUIDO " + i[6] + " BOLETA # " + str(i[4]))

                if concepto == "HONORARIOS":

                    edita_honorario = 1

                    store.set(iter, 0, 0,
                    1, None,
                    2, None,
                    3, ctb_formato_fecha_db(self.txtFecha.get_text()),
                    4, self.txtNumero.get_text(),
                    5, self.txtFicha.get_text(),
                    6, self.txtPrestador.get_text(),
                    7, self.txtCuentaHonorario.get_text(),
                    8, self.txtDescripcionHonorario.get_text(),
                    9, ctb_alinea_derecha(self.txtDebeHonorario.get_text(), 25),
                    10, ctb_alinea_derecha(self.txtHaberHonorario.get_text(), 25),
                    11, "HONORARIOS",
                    12, "HONORARIOS " + i[6] + " BOLETA # " + str(i[4]))

            iter = store.iter_next(iter)

        if (edita_honorario and edita_liquido and edita_retencion) or existe:

            if existe:

                ficha = self.txtFicha.get_text()
                boleta = self.txtNumero.get_text()

                resp = ctb_pregunta_si_no(self, self.padre.ctb_frm_main, "Ya existe otra boleta, żDesar actualizarla?", "Quitar asiento")

                if resp == gtk.RESPONSE_NO:
                    self.cursor.execute("rollback")
                    return 0

                try:
                    self.cursor.execute("delete from ctb.honorario where numero_boleta = " + boleta + " and rut_ficha = '" + ficha + "'")
                except:
                    self.cursor.execute("rollback")
                    self.aviso(self.padre.ctb_frm_main, sys.exc_info()[1])
                    return 0

            try:


                self.cursor.execute("insert into \
                        ctb.honorario (cod_empresa, \
                        fecha_boleta, \
                        numero_boleta, \
                        rut_ficha, \
                        cuenta_honorario, \
                        monto_honorario, \
                        monto_retencion, \
                        monto_liquido) \
                        values (" + str(self.cod_empresa) + ",\
                        '" + ctb_formato_fecha_db(self.txtFecha.get_text()) + "', \
                        " + self.txtNumero.get_text() + ", \
                        '" + self.txtFicha.get_text() + "', \
                        '" + self.txtCuentaHonorario.get_text() + "', \
                        " + ctb_formato_numero_db(self.txtDebeHonorario.get_text()) + ", \
                        " + ctb_formato_numero_db(self.txtHaberRetencion.get_text()) + ", \
                        " + ctb_formato_numero_db(self.txtHaberLiquido.get_text()) + ")" )

                self.cursor.execute("commit")
                self.calcula_totales()
                self.on_btnBuscar_clicked()
                self.limpia_campos()
                self.txtNumero.grab_focus()
                return 0

            except:
                self.cursor.execute("rollback")
                self.aviso(self.padre.ctb_frm_main, sys.exc_info()[1])
                return 0

        else:
            try:

                self.cursor.execute("insert into \
                            ctb.honorario (cod_empresa, \
                                     fecha_boleta, \
                                     numero_boleta, \
                                     rut_ficha, \
                                     cuenta_honorario, \
                                     monto_honorario, \
                                     monto_retencion, \
                                     monto_liquido) \
                            values (" + str(self.cod_empresa) + ",\
                                 '" + ctb_formato_fecha_db(self.txtFecha.get_text()) + "', \
                                 " + self.txtNumero.get_text() + ", \
                                 '" + self.txtFicha.get_text() + "', \
                                 '" + self.txtCuentaHonorario.get_text() + "', \
                                 " + ctb_formato_numero_db(self.txtDebeHonorario.get_text()) + ", \
                                 " + ctb_formato_numero_db(self.txtHaberRetencion.get_text()) + ", \
                                 " + ctb_formato_numero_db(self.txtHaberLiquido.get_text()) + ")" )
            except:

                self.cursor.execute("rollback")

                self.aviso(self.padre.ctb_frm_main, sys.exc_info()[1])
                return 0

            iter = store.append()

            store.set(iter, 0, 0,
                        1, None,
                        2, None,
                        3, ctb_formato_fecha_db(self.txtFecha.get_text()),
                        4, self.txtNumero.get_text(),
                        5, self.txtFicha.get_text(),
                        6, self.txtPrestador.get_text(),
                        7, self.txtCuentaHonorario.get_text(),
                        8, self.txtDescripcionHonorario.get_text(),
                        9, ctb_alinea_derecha(self.txtDebeHonorario.get_text(), 25),
                        10, ctb_alinea_derecha(self.txtHaberHonorario.get_text(),25),
                        11, "HONORARIOS",
                        12, "HONORARIOS " + self.txtPrestador.get_text() + " BOLETA # " + str(self.txtNumero.get_text()))

            iter = store.append()

            store.set(iter, 0, 0,
                        1, None,
                        2, None,
                        3, ctb_formato_fecha_db(self.txtFecha.get_text()),
                        4, self.txtNumero.get_text(),
                        5, self.txtFicha.get_text(),
                        6, self.txtPrestador.get_text(),
                        7, self.txtCuentaRetencion.get_text(),
                        8, self.txtDescripcionRetencion.get_text(),
                        9, ctb_alinea_derecha(self.txtDebeRetencion.get_text(),25),
                        10, ctb_alinea_derecha(self.txtHaberRetencion.get_text(),25),
                        11, "RETENCION",
                        12, "RETENCION " + self.txtPrestador.get_text() + " BOLETA # " + str(self.txtNumero.get_text()))

            iter = store.append()


            store.set(iter, 0, 0,
                        1, None,
                        2, None,
                        3, ctb_formato_fecha_db(self.txtFecha.get_text()),
                        4, self.txtNumero.get_text(),
                        5, self.txtFicha.get_text(),
                        6, self.txtPrestador.get_text(),
                        7, self.txtCuentaLiquido.get_text(),
                        8, self.txtDescripcionLiquido.get_text(),
                        9, ctb_alinea_derecha(self.txtDebeLiquido.get_text(),25),
                        10, ctb_alinea_derecha(self.txtHaberLiquido.get_text(),25),
                        11, "LIQUIDO",
                        12, "LIQUIDO " + self.txtPrestador.get_text() + " BOLETA # " + str(self.txtNumero.get_text()))


        self.cursor.execute("commit")

        self.treeHonorario.set_model(store)
        self.limpia_campos()
        self.txtNumero.grab_focus()
        self.calcula_totales()
        return 0



    def on_btnCentralizar_clicked(self, btn=None):

        self.genera_comprobante(self.cnx)


    def on_btnBuscaHonorario_clicked(self,btn=None):

        self.num_cuenta = 0

        sql = "SELECT cc.num_cuenta, cc.descripcion_cuenta from ctb.empresa e join ctb.cuenta_contable cc on cc.cod_empresa = e.cod_empresa and e.cod_empresa = "+ str(self.cod_empresa) +" and cc.nivel_cuenta = e.nivel_cuenta order by cc.descripcion_cuenta;"

        try:

            self.buscando = True

            self.num_cuenta, self.desc_cuenta = ctb_busqueda(self.padre.ctb_frm_main, self.cnx,

            sql,1,(0,1),

            ["Número Cuenta", "Descripción Cuenta"])

            self.buscando = False

            self.txtCuentaHonorario.set_text(self.num_cuenta)

            self.txtDescripcionHonorario.set_text(self.desc_cuenta)

        except:

            self.aviso(self.padre.ctb_frm_main,sys.exc_info()[1])

            return

        self.calcula_totales()

    def on_txtCuentaHonorario_key_press_event(self, entry, event):

        if event.keyval == gtk.keysyms.Down:

            return True

        elif event.keyval == gtk.keysyms.Up:

            return True

        elif event.keyval == gtk.keysyms.F1:

            self.on_btnBuscaHonorario_clicked()

        return



    def on_txtCuentaHonorario_focus_out_event(self, entry=None, event=None):
        self.txtCuentaHonorario.set_text(ctb_formato_cuenta(self.cnx,self.cod_empresa,self.txtCuentaHonorario.get_text()))
        sql = "SELECT descripcion_cuenta from ctb.cuenta_contable where cod_empresa = "+ str(self.cod_empresa) +" and num_cuenta = '" + self.txtCuentaHonorario.get_text() +"'"
        try:
            self.cursor.execute(sql)
            r = self.cursor.fetchall()

            self.txtDescripcionHonorario.set_text(r[0][0])
        except:
            if not self.buscando :
                self.aviso(self.padre.ctb_frm_main,sys.exc_info()[1])


    def on_txtFicha_key_press_event(self, entry, event):

        if event.keyval == gtk.keysyms.Down:

            return True

        elif event.keyval == gtk.keysyms.Up:

            return True

        elif event.keyval == gtk.keysyms.F1:

            sql = "SELECT f.rut,f.nombre from ctb.ficha f where f.cod_empresa = "+ str(self.cod_empresa) +" order by f.nombre;"

            try:

                self.buscando = True

                rut, nombre = ctb_busqueda(self.padre.ctb_frm_main, self.cnx,

                                sql,1,(0,1),

                                ["R.U.T.","NOMBRE"])

                self.buscando = False

                self.txtFicha.set_text(rut)

                self.txtPrestador.set_text(nombre)

            except:

                self.aviso(self.padre.ctb_frm_main,sys.exc_info()[1])

        return



    def on_txtPeriodo_focus_out_event(self, entry=None, event=None):



        self.cursor.execute("select periodo, mes from ctb.periodo_contable where cod_empresa ='"+ str(self.cod_empresa) + "' and descripcion_periodo = '" + entry.get_text() + "'")
        r = self.cursor.fetchall()



        self.periodo = r[0][0]
        self.mes = r[0][1]


        lt = (int(self.periodo), int(self.mes), monthrange(int(self.periodo), int(self.mes))[1],0,0,0,0,0,0)





        self.txtFecha.set_text(str(ceros(monthrange(int(self.periodo), int(self.mes))[1],2)+"/"+ ceros(self.mes,2) +"/"+ ceros(self.periodo,4)))



        self.on_btnBuscar_clicked()



    def on_txtFicha_focus_out_event(self, entry=None, event=None):

        self.txtFicha.set_text(formato_rut(self.txtFicha.get_text()))
        if self.txtFicha.get_text() == "00.000.000-0" or ctb_es_rut(self.txtFicha.get_text()) == False:

            if not self.buscando :

                self.aviso(self.padre.ctb_frm_main,"El RUT no es vĂĄlido")

            self.txtPrestador.set_text("")

            return False

        try:

            self.cursor.execute("select nombre from ctb.ficha where rut ='"+ self.txtFicha.get_text() +"'")
            r = self.cursor.fetchall()

        except:

            if not self.buscando :

                self.aviso(self.padre.ctb_frm_main,sys.exc_info()[1])

            self.txtPrestador.set_text("")

            return False

        if len(r) == 0 :

            if not self.buscando :

                self.aviso(self.padre.ctb_frm_main,"El RUT no se encuentra en la Base de Datos")

            self.txtPrestador.set_text("")

            return False

        else:

            self.txtPrestador.set_text(r[0][0])

            return False


    def on_txtDebeHonorario_focus_out_event(self, entry=None, event=None):

        monto = int(entry.get_text())

        if monto:

            self.txtHaberRetencion.set_text(ctb_formato_moneda(str(int(round(monto * 0.1)))))

            self.txtHaberLiquido.set_text(ctb_formato_moneda(str(int(round(monto * 0.9)))))

        self.txtDebeHonorario.set_text(ctb_formato_moneda(monto))


    def on_btnFicha_clicked(self,btn=None):

        sql = "SELECT f.rut,f.nombre from ctb.ficha f where f.cod_empresa = "+ str(self.cod_empresa) +" order by f.nombre;"

        try:

            self.buscando =True

            rut, nombre = ctb_busqueda(self.padre.ctb_frm_main, self.cnx,

                    sql,1,(0,1),

                    ["R.U.T.","NOMBRE"])

            self.buscando=False

            self.txtFicha.set_text(rut)

            self.txtDebeHonorario.get_text()

            self.txtPrestador.set_text(nombre)

            self.txtFicha.grab_focus()

        except:

            self.aviso(self.padre.ctb_frm_main,sys.exc_info()[1])

            self.txtFicha.grab_focus()

            return


    def on_btnFecha_clicked(self,btn=None):

        fecha =self.txtFecha.get_text()

        self.txtFecha.set_text(ctb_calendar(self,None,fecha))

    def on_btnImprimir_clicked(self,btn=None):


        self.cursor.execute("select mes, periodo from ctb.periodo_contable where descripcion_periodo = '" + self.cmbPeriodo.entry.get_text() + "'")
        r = self.cursor.fetchall()

        self.mes = r[0][0]
        self.periodo = r[0][1]

        sql = "select  \
                h.fecha_boleta, \
                'BOLETA HONORARIO', \
                h.numero_boleta, \
                h.rut_ficha, \
                f.nombre, \
                h.monto_honorario, \
                h.monto_retencion, \
                h.monto_liquido\
                from ctb.vw_honorario h inner join ctb.ficha f on h.rut_ficha = f.rut \
                where not (cod_comprobante is null) \
                and date_part('YEAR', fecha_boleta) = " + str(self.periodo) + " \
                and date_part('month', fecha_boleta) = " + str(self.mes)



        try:
            self.cursor.execute(sql)

            r = self.cursor.fetchall()

            if len(r) ==0:

                self.aviso(self.padre.ctb_frm_main, "No hay datos para generar el Libro de Honorarios con estos Parametros")
                return 0

        except:
            ctb_error(self.padre.ctb_frm_main, sys.exc_info()[1])
            return 0

        c = Canvas('LibroHonorarios.pdf',pagesize=landscape(A4))
        c.setPageCompression(0)

        def cabecera_libro(self, pc, page_num):

            pc.setFont('Courier', 12)
            w = pc.stringWidth("LIBRO DE HONORARIOS",'Courier', 12)
            pc.drawString((726 /2) - w/2,582, "LIBRO DE HONORARIOS")
            pc.drawString(30,570,self.desc_empresa)
            pc.drawString(30,558,self.rut_empresa)
            pc.drawString(30,549,self.direccion_empresa)
            l ="Pagina: %s"%(zfill(int(page_num), 10))
            w = pc.stringWidth(l,'Courier', 12)
            pc.drawString(726 - w,570,l)
            t = map(str, localtime())
            pc.drawString(726 -w ,558,"Fecha : " +  zfill(t[2],2) + "/" + zfill(t[1],2) + "/" + t[0])
            pc.setFont('Courier', 7)
            pc.line(30, 548, 756, 548)


            l = "Fecha".center(12) + "Documento".center(20) + "N Boleta".center(15) + "R.U.T.".center(15) + "Prestador".center(40) + "Honorarios".center(20) + "Retencion".center(20) + "Liquido".center(20)

            pc.drawString(30,532,l)
            pc.line(30, 528, 756, 528)
            pc.setFont('Courier', 7)


        def pone_linea(pc,linea,strLinea,pagina):

            if linea > 60:
                c.showPage()
                pagina = pagina + 1
                cabecera_libro(self,pc,pagina)
                linea = 7
            pc.drawString(30,528 -((linea -6)*9) ,strLinea)
            return linea +1,pagina


        t_honorario = t_retencion = t_liquido = docs = 0

        pagina = 1

        cabecera_libro(self,c,pagina)

        linea = 7

        for i in r:

            docs += 1
            t_honorario += i[5]
            t_retencion += i[6]
            t_liquido += i[7]

            i = map(str, i)

            l = ctb_formato_fecha_local(i[0][:10]).rjust(12) + " " + i[1].rjust(15)+ " " +  ctb_formato_moneda(i[2]).rjust(20) + " " + i[3].rjust(15) + " " + i[4][:30].ljust(30) + " " + ctb_formato_moneda(int(float(i[5]))).rjust(20)  + " "+ ctb_formato_moneda(int(float(i[6]))).rjust(20) + " " + ctb_formato_moneda(int(float(i[7]))).rjust(20)

            linea, pagina = pone_linea(c, linea, l, pagina)

        l = ""

        linea, pagina = pone_linea(c, linea, l, pagina)

        ltotal = "[ " + str(docs) + " ] Documentos  Total General : "

        l = ltotal.rjust(98) + ctb_formato_moneda(int(t_honorario)).rjust(20) + " " + ctb_formato_moneda(int(t_retencion)).rjust(20) + " " + ctb_formato_moneda(int(t_liquido)).rjust(20)
        linea, pagina = pone_linea(c, linea, l, pagina)

        c.showPage()
        c.save()

        Abre_pdf("LibroHonorarios.pdf")



    def on_btnCerrar_clicked(self, btn=None):

        """Destruye el formulario de la clase, cuando pinchamos en el botón cerrar

        """

        self.padre.ntbPrincipal.remove_page(self.padre.ntbPrincipal.get_current_page())

        del self.padre.wins["Libro de Honorarios"]

    def genera_comprobante(self, cnx=None, window=None):


        def on_btnFecha_click(self,btn=None):

            fecha =txtFecha.get_text()

            txtFecha.set_text(ctb_calendar(self,None,fecha))

        def on_txtComprobante_focus_out(t=None, e=None):

            folio = txtComprobante.get_text()
            tipo = cmbTipo.entry.get_text()[0]

            if folio == "":
                return


            if ctb_existe_comprobante(self,folio,tipo) :
                self.aviso(dialog, "El comprobante ya existe")
                return


        lbl = unicode("Comprobante Contable", "latin-1")

        dialog = gtk.Dialog(lbl.encode("utf-8"), self.padre.ctb_frm_main, 0,(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_OK, gtk.RESPONSE_OK))


        hbox = gtk.HBox(False, 8)
        hbox.set_border_width(8)
        dialog.vbox.pack_start(hbox, 1, False, 0)


        label = gtk.Label("Folio Comprobante")
        label.set_use_underline(True)

        hbox.pack_start(label, 0, 0, 0)

        txtComprobante = gtk.Entry()
        txtComprobante.connect("focus-out-event", on_txtComprobante_focus_out)
        hbox.pack_start(txtComprobante, True, True, 0)

        label = gtk.Label("Tipo de Comprobante")
        label.set_use_underline(True)

        hbox.pack_start(label, 0, 0, 0)

        cmbTipo = gtk.Combo()
        cmbTipo.set_value_in_list(1,0)
        cmbTipo.set_popdown_strings(['TRASPASO', 'INGRESO','EGRESO'])

        hbox.pack_start(cmbTipo, True, True, 0)

        hbox = gtk.HBox(False, 8)
        hbox.set_border_width(8)
        dialog.vbox.pack_start(hbox, 1, False, 0)

        label = gtk.Label("Fecha de Comprobante")
        label.set_use_underline(True)

        hbox.pack_start(label, 0, 0, 0)
        #lt = (int(self.periodo), int(self.mes), monthrange(int(self.periodo), int(self.mes))[1],0,0,0,0,0,0)
        #dtFecha = gnome.ui.DateEdit(int(mktime(lt)), False, True)

        hboxFecha = gtk.HBox(False, 8)

        txtFecha = gtk.Entry()

        txtFecha.set_text(str(ceros(monthrange(int(self.periodo), int(self.mes))[1],2)+"/"+ ceros(self.mes,2) +"/"+ ceros(self.periodo,4)))

        hboxFecha.pack_start(txtFecha, False, False, 0)

        btnFecha = gtk.Button("")

        btnFecha.connect("clicked", on_btnFecha_click)

        hboxFecha.pack_start(btnFecha, False, False, 0)



        hbox.pack_start(hboxFecha, False, False, 0)



        dialog.show_all()

        hbox = gtk.HBox(False, 8)

        hbox.set_border_width(8)

        dialog.vbox.pack_start(hbox, 1, False, 0)

        hbox.show()



        label = gtk.Label("Glosa Comprobante")

        label.set_use_underline(True)



        hbox.pack_start(label, 0, 0, 0)

        label.show()



        txtGlosa = gtk.Entry()

        hbox.pack_start(txtGlosa, True, True, 0)

        txtGlosa.show()



        l = "LIBRO DE HONORARIOS DE "



        txtGlosa.set_text(l + " " + self.cmbPeriodo.entry.get_text())



        frame = gtk.Frame("Registro")

        dialog.vbox.pack_start(frame, 0, True, 0)

        frame.show()



        txtLog = gtk.TextView()

        frame.add(txtLog)

        txtLog.show()

        textbuffer = gtk.TextBuffer(None)
        txtLog.set_buffer(textbuffer)
        txtLog.set_editable(False)
        txtLog.set_cursor_visible(False)
        txtLog.set_wrap_mode(True)
        txtLog.set_right_margin(1000)
        txtLog.set_size_request(200,200)
        dialog.set_default_response(gtk.RESPONSE_OK)

        txtComprobante.grab_focus()

        response = dialog.run()


        if response == gtk.RESPONSE_OK:


            tipo = cmbTipo.entry.get_text()[0]
            folio = txtComprobante.get_text()

            if ctb_existe_comprobante(self,folio,tipo) :
                self.aviso(dialog, "No se ha centrlizado debido a que el comprobante ya existe!")
                dialog.destroy()
                return


            fecha = ctb_formato_fecha_db(txtFecha.get_text())
            glosa = txtGlosa.get_text()
            self.cursor.execute("begin work")

            v = ctb_formato_numero_db(self.lblMonto.get_text())

            try:

                sql = "insert into ctb.comprobante (\
                    cod_tipo_comprobante,\
                    centralizado, \
                    periodo, \
                    mes,\
                    fecha, \
                    debe, \
                    haber, \
                    glosa, \
                    cod_empresa, \
                    folio_comprobante,\
                    origen) values (\
                    '" + tipo + "', \
                    'S', \
                    " + str(self.periodo) + ", \
                    " + str(self.mes) + ", \
                    '" + fecha + "', \
                    " + v + ", \
                    " + v + ", \
                    '" + glosa + "', \
                    " + str(self.cod_empresa) + ", \
                    " + folio + ", \
                    'CC')"

                self.cursor.execute(sql)
            except:

                self.cursor.execute("rollback work")
                self.aviso(dialog, sys.exc_info()[1])
                dialog.destroy()
                return

            self.cursor.execute("select cod_comprobante from ctb.comprobante \
                    where folio_comprobante = " + folio + " \
                    and cod_empresa = 1 and periodo = " + str(self.periodo) + " \
                    and mes = " + str(self.mes) + " and cod_tipo_comprobante = '" + tipo + "' \
                    and glosa = '" + glosa + "'")
            r = self.cursor.fetchall()

            codigo = r[0][0]

            self.cursor.execute("select cod_doc_contable from ctb.tipo_doc_contable where descripcion_documento = 'BOLETA DE HONORARIO' ")
            r = self.cursor.fetchall()

            tipo_doc = r[0][0]

            model = self.treeHonorario.get_model()
            iter = model.get_iter_first()
            t = 0
            old = - 1

            n = 1

            end_iter = textbuffer.get_end_iter()

            while iter:

                sel = model.get_value(iter, 0)
                if sel:

                    comp = model.get_value(iter, 2)

                    if not comp:

                        boleta = model.get_value(iter, 4)
                        ficha = model.get_value(iter, 5)
                        debe = model.get_value(iter, 9)
                        haber = model.get_value(iter, 10)
                        cuenta = model.get_value(iter, 7)
                        fecha_boleta = model.get_value(iter, 3)
                        glosa = model.get_value(iter, 12)

                        if not debe:
                            debe = 0
                        if not haber:
                            haber= 0

                        try:
                            textbuffer.insert(end_iter, str(n) + ": Centralizando " + str(boleta) + " " + ficha + "\n")
                            end_iter = textbuffer.get_start_iter()
                            txtLog.set_buffer(textbuffer)

                            #~ print ctb_formato_numero_db(float(round(debe))), float(round(debe))
                            #~ print "insert"
                            sql = "insert into ctb.detalle_comprobante ( cod_comprobante, \
                                num_cuenta, \
                                cod_empresa, \
                                monto_debe, \
                                monto_haber, \
                                folio_documento, \
                                fecha_doc, \
                                cod_doc_contable, \
                                rut, \
                                num_linea, glosa) values (" + str(codigo) + ", \
                                '" + cuenta + "', \
                                " + str(self.cod_empresa) + ", \
                                " + ctb_formato_numero_db(debe) + ", \
                                " + ctb_formato_numero_db(haber) + ", \
                                " + str(boleta) + ", \
                                '" + ctb_formato_fecha_db(fecha_boleta) + "', \
                                " + str(tipo_doc) + ", \
                                '" + ficha + "', \
                                " + str(n) + ", '" + glosa + "')"


                            self.cursor.execute(sql)

                            #~ print "update", self.cod_empresa, boleta, ficha
                            self.cursor.execute("update ctb.honorario set cod_comprobante = " + str(codigo) + " \
                                where cod_empresa = " + str(self.cod_empresa) + " \
                                and numero_boleta = " + str(boleta) + "\
                                and rut_ficha = '" + ficha + "'")
                            #~ print "updated", n

                            old = boleta

                            if (n / 5) <> ((n+1) / 5):
                                while gtk.events_pending():
                                    gtk.mainiteration(False)


                        except:
                            self.cursor.execute("rollback work")
                            #~ print StringType(sys.exc_info()[1])
                            self.aviso(self.padre.ctb_frm_main, "Documento : " + str(boleta) + " - " + StringType(sys.exc_info()[1]))
                            #~ print debe, haber, fecha_boleta, boleta
                            #~ print ficha
                            dialog.destroy()
                            return

                        t = t + int(ctb_formato_numero_db(model.get_value(iter, 9)))
                        n += 1

                iter = model.iter_next(iter)

            try:
                if old == -1:
                    self.cursor.execute("rollback work")
                else:
                    self.cursor.execute("update ctb.comprobante set debe = " + str(t) + ", haber = " + str(t) + " where \
                    cod_comprobante = " + str(codigo))
                    self.cursor.execute("commit work")
                    self.on_btnBuscar_clicked()
            except:

                self.cursor.execute("rollback work")
                self.aviso(dialog, sys.exc_info()[1])


            self.aviso(self.padre.ctb_frm_main, 'Se han centralizado ' + str(n-1) + ' transacciones con un total de ' + ctb_formato_moneda(t))

        dialog.destroy()
